## 一、环境准备

### 1、部署nacos注册中心和配置中心

下载nacos-server 1.2.1

https://github.com/alibaba/nacos/releases

1、解压 **nacos-server-1.2.1.tar.gz**

2、修改配置文件，用数据库方式存储
`nacos-server-1.2.1\conf\application.properties`

3、尾行添加内容 对应自己上面创建的数据库地址

```properties
spring.datasource.platform=mysql

db.num=1
db.url.0=jdbc:mysql://ip地址+端口/nacos_devtest?characterEncoding=utf8&connectTimeout=1000&socketTimeout=3000&autoReconnect=true
db.user=数据库用户名
db.password=数据库密码
```

4、导入nacos里面的sql到自己的数据库，文件在 `conf/nacos-mysql.sql`

5、然后启动nacos

双击 `bin/startup.cmd`

停止命令 `bin/shutdown.cmd`

访问：[http://127.0.0.1:8848/nacos](http://127.0.0.1:8848/nacos "http://127.0.0.1:8848/nacos")
用户名：nacos
密码：nacos

5、点击配置 `配置列表` 菜单

**导入日期最新的配置**

`nacos_config_export_xxx.zip`




### 3、统一maven版本（可选，建议3.6版本以上即可）

解压  **apache-maven-3.6.1-bin.tar.gz**

修改文件`config/settings.xml`

配置阿里仓库

```xml
<mirrors>
     <mirror>
        <id>nexus-aliyun</id>
        <mirrorOf>central</mirrorOf>
        <name>Nexus aliyun</name>
        <url>http://maven.aliyun.com/nexus/content/groups/public</url>
     </mirror>
</mirrors>
```

配置自己仓库地址 **建议不是C盘就可以**

```xml
<localRepository>E:/Installation/maven/repo</localRepository>
```



## 二、运行项目

### 1、下载项目

```shell
git clone http://192.168.0.2:3000/tsq/tsq-microservice.git
```

### 2、修改每个服务的配置中心地址

```yaml
spring:
  cloud:
    nacos:
      server-addr: ${NACOS_SERVER_ADDR:192.168.136.170:8848}
      xxxxxx.....

```

**修改自己的nacos注册中心地址**

**注意 此文件不用提交到git仓库**

### 4、启动顺序

1. 运行`AdminCenterProviderApplication`
2. 运行`AuthCenterProviderApplication`
3. 运行`FileCenterApplication`（不上传文件可不启动）
5. 运行`MonitorApplication`（开发环境可不启动）
5. 运行`GatewayApplication`

### 5、业务模块介绍

1. auth-center模块，用于验证权限代码

   使用技术 spring cloud oauth2+jwt，实现验证用户。

   `com.tsq.security.auth.config`包下是配置信息

   `com.tsq.security.auth.controller`是暴露的接口，在这里重新了auth2的默认实现，用户包装统一返回数据格式

   `com.tsq.security.auth.service` 用户auth2的 `UserDetailsService`的实现类，在这里使用`Feign`调用`admin-center`模块 ，所以运行`auth-center`的前提必须运行`admin-center`模块

2. admin-center模块，用于管理后台数据模块

   admin-center分为3个模块，分别是 `admin-center-api`、`admin-center-client` 、`admin-center-provider`

   `admin-center-api` 模块主要用于提供POJO和远程调用接口

   `admin-center-client` 模块主要用于远程调用的暴露和`sentinel`的熔断

   `admin-center-provider` 模块是服务提供者

   maven关系

   `admin-center-provider`里面包含`admin-center-api` 

   `admin-center-client`里面也包含`admin-center-api` 

3. file-center模块，用于文件上传功能

4. monitor-center模块，用于spring boot admin监控使用

5. gateway-center模块，用于网关层

   `com.tsq.gateway.config` 用于一些网关配置

   `com.tsq.gateway.filter`用于一些拦截器

   `com.tsq.gateway.filter.AccessGatewayFilter`这个类是用于权限判断的，没有权限不可以访问内部的接口

   `com.tsq.gateway.handler`用于一些处理

   `com.tsq.gateway.route`动态路由使用

6. 



## 三、代码讲解

### 1，如何写出增删改查

1. 首先创建一个controller

2. 继承 `com.tsq.web.base.controller.BaseController<Service,Entity>`

3. 添加此contoller的请求前缀

4. 添加Swagger文档

   例子：

   ```java
   package com.tsq.admin.provider.controller;
   
   import com.tsq.admin.api.model.SysOperation;
   import com.tsq.admin.provider.service.ISysOperationService;
   import com.tsq.web.base.controller.BaseController;
   import io.swagger.annotations.Api;
   import org.springframework.web.bind.annotation.RequestMapping;
   import org.springframework.web.bind.annotation.RestController;
   
   /**
    * <p>
    * 功能操作表 前端控制器
    * </p>
    *
    * @author dcy
    * @since 2019-09-06
    */
   @RestController
   @RequestMapping("/oper")
   @Api(value = "SysOperationController", tags = {"功能操作接口"})
   public class SysOperationController extends BaseController<ISysOperationService, SysOperation> {
   
   }
   ```

   此时：就会有`BaseController`里面的增删改查方法

   访问路径/open/page，/open/saveOrUpdate 等等

   > **参数问题：不可以有多个参数，只可以有一个。**

### 2、如何构造条件

有两种方式：

> 注：1、当需要多个条件，并且条件复杂推荐使用dto；2、当条件少，并且实体类能满足条件，使用实体类即可（model）

1. 使用`com.baomidou.mybatisplus.annotation.TableField` 注解（不建议使用）

   ```java
   @TableField(condition = SqlCondition.LIKE)
   private String username;
   ```

   使用Wrappers构造条件 `Wrappers.query(entity)`

   

2. 使用自定义注解 `com.tsq.db.base.binding.BindQuery`注解（建议使用）

   创建DTO

   ```java
   @Data
   public class UserSearchDTO extends PageModel {
       private String username;
   }
   ```

   

   例子：

   1、相等

   ```java
   @BindQuery(comparison = Comparison.EQ)
   private String username;
   ```

   2、别名模糊查询

   ```java
   @BindQuery(comparison = Comparison.LIKE,alias = "user")
   private String username;
   ```

   拼接条件：user.username like '%xxxx%'

   

   调用 `QueryWrapper<T> wrapper = QueryBuilder.toWrapper(dto);`

   > 注意：
   >
   > 1、PageModel 类是用于分页使用 包含当前页面、每页多少条（默认30）、排序字段和排序类型
   >
   > 2、如有条件不需要dto添加，可以使用以下代码
   >
   > 比如：通过上述代码返回`wrapper`，执行`wrapper.eq("user." + BaseModel.DEL_FLAG, 0);` 进行继续拼接条件
   >
   > 3、entity和dto名称不同，可以使用 `@BindQuery`的 `field`，绑定数据字段。数据库字段，默认为空，自动根据驼峰转下划线

返回的 `QueryWrapper` 是mybatis-plus的类，地址： [https://mp.baomidou.com/guide/wrapper.html#abstractwrapper](https://mp.baomidou.com/guide/wrapper.html#abstractwrapper)

可以自行添加条件构造

### 3、分页查询如果编写

1. 单表查询并且构造自定义条件

   ```java
   @RestController
   @RequestMapping("/user")
   @Api(value = "SysUserInfoController", tags = {"用户操作接口"})
   public class SysUserInfoController extends BaseController<ISysUserInfoService, SysUserInfo>{
       @GetMapping(value = "/pageListByDto")
   	public ResponseData<IPage<SysUserInfo>> pageListByDto(UserSearchDTO userSearchDTO) {
   		return ResponseData.success(baseService.pageListByDto(userSearchDTO));
   	}
   }
   
   ```

   > baseService 就是 ISysUserInfoService

2. 多表查询并且构造自定义条件

   cotroller代码

   ```java
   @RestController
   @RequestMapping("/user")
   @Api(value = "SysUserInfoController", tags = {"用户操作接口"})
   public class SysUserInfoController extends BaseController<ISysUserInfoService, SysUserInfo>{
       @GetMapping(value = "/pageList")
   	public ResponseData<IPage<Map<String,Object>>> pageList(UserSearchDTO userSearchDTO) {
   		return ResponseData.success(baseService.getUserPageList(userSearchDTO));
   	}
   }
   
   ```

   service代码

   ```java
   IPage<Map<String, Object>> getUserPageList(UserSearchDTO userSearchDTO);
   
   ```

   serviceImpl代码

   ```java
   @Override
   public IPage<Map<String, Object>> getUserPageList(UserSearchDTO userSearchDTO) {
       QueryWrapper<SysUserInfo> wrapper = QueryBuilder.toWrapper(userSearchDTO);
       return sysUserInfoMapper.getUserPageList(this.getPagePlus(userSearchDTO), wrapper);
   }
   ```

   mapper代码

   ```java
       /**
        * 获取 用户列表
        *
        * @param page
        * @param wrapper
        * @return
        */
       IPage<Map<String, Object>> getUserPageList(Page<SysUserInfo> page, @Param(Constants.WRAPPER) Wrapper<SysUserInfo> wrapper);
   
   ```

   xml代码

   ```xml
   <!-- 通用查询结果列 别名 -->
   <sql id="Base_Column_List_Alias">
           user.user_id AS userId,user.hos_id AS hosId, user.sub_id AS subId,user.username AS username,user.password,user.nick_name AS nickName,user.user_type AS
           userType,user.email,user.phone_number AS phoneNumber,user.id_card AS idCard, user.birthday,user.sex,user.duties,user.avatar_path AS avatarPath,user.user_status AS
           userStatus,user.exa_status AS exaStatus,user.create_by AS createBy,user.create_date AS createDate,user.update_by AS updateBy,user.update_date
           AS updateDate,user.del_flag AS delFlag,user.remark
   </sql>
   
   <select id="getUserPageList" resultType="java.util.Map">
           select <include refid="Base_Column_List_Alias"/>,depa.depa_name AS depaName
           FROM sys_user_info user LEFT JOIN sys_department depa on user.depa_id = depa.depa_id
           ${ew.customSqlSegment}
   </select>
   ```

   此时就已完成了

   > 注意：
   >
   > 1、controller的参数DTO 必须继承 `com.tsq.db.base.model.PageModel` ，否则会报错，找不到对应的分页参数
   >
   > 2、serviceImpl 代码 `QueryWrapper<SysUserInfo> wrapper = QueryBuilder.toWrapper(userSearchDTO);`此行代码是固定写法，返回的泛型对应自己的Entity
   >
   > 3、`this.getPagePlus(userSearchDTO)` 是固定写法，返回的是分页信息；
   >
   > 4、mapper参数 `Page<SysUserInfo> page, @Param(Constants.WRAPPER) Wrapper<SysUserInfo> wrapper`； 两个参数是固定写法，Page的泛型是对应自己Entity, Wrapper的泛型也是一样；`@Param(Constants.WRAPPER)` 此代码是把对象赋值到常量 `Constants.WRAPPER` 上，保持统一，为了后续的xml使用；
   >
   > 5、xml代码部分 `${ew.customSqlSegment}` ，此代码是取出条件构造的sql，进行拼接。
   >
   > 6、`<include refid="Base_Column_List_Alias"/>` 这事mybatis的语法，不要使用粘贴sql，建议使用引入`Include`标签
>
   > 7、所有的参数命名必须返回驼峰式命名；例如：depa.depa_name AS depaName

   mybatis-plus分页：[https://mp.baomidou.com/guide/page.html](https://mp.baomidou.com/guide/page.html)

   返回的 ``QueryWrapper` 是mybatis-plus的类，地址： [https://mp.baomidou.com/guide/wrapper.html#abstractwrapper](https://mp.baomidou.com/guide/wrapper.html#abstractwrapper)

### 4、数据返回格式

数据统一返回 `com.tsq.common.model.ResponseData<T>`

正常的：`ResponseData.success();`

失败的：`ResponseData.error();`

更多方法请看类

### 5、后台如何验证参数

1、在方法上添加 `com.dcy.common.annotation.ValidResult` 注解

2、Entity实现 `com.dcy.common.model.ValidBaseInterface` 接口

代码：

controller代码

```java
@PostMapping("/saveUser")
@ApiOperation(value = "保存user，用于演示")
@ValidResult
public ResponseData saveUser(@RequestBody SysUserInfo sysUserInfo) throws Exception {
    return ResponseData.success();
}
```



entity代码

```java
public class SysUserInfo extends BaseModel implements ValidBaseInterface {

}
```



## 四、代码规范问题

### 1、添加idea的类注释模板

修改注释模块：添加自己的名称 `Class`和 `Interface` 都需要

```
/**
 * @Author：自己的名称（中英文随意）
 * @Description: 
 * @Date: ${DATE} ${TIME}
 */
```

### 2、注释

创建的所有的POJO每个属性必须有注释，如果有swagger文档可以不写注释

例如：

没有swagger文档必须写注释

```java
/**
 * @Author：dcy
 * @Description: 授权权限使用
 * @Date: 2019/9/16 11:08
 */
@Data
public class SysRolePowerDTO {
    /**
     * 角色Id
     */
    private String roleId;
    /**
     * 授权权限Ids
     */
    private List<String> powerIds;
}
```

有 `ApiModelProperty` 注解 可以不写注释

```java
/**
 * <p>
 * 参数配置表
 * </p>
 *
 * @author dcy
 * @since 2019-09-06
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value = "SysConfig对象", description = "参数配置表")
public class SysConfig extends BaseModel {

    @ApiModelProperty(value = "参数名称")
    private String configName;

}
```

**方法上面必须写注释，关键代码必须写注释**

例如

```java
/**
* 保存授权用户组
*
* @param sysUserInfoGroupDTO
* @return
*/
boolean saveAuthGroup(SysUserInfoGroupDTO sysUserInfoGroupDTO);
```



### 3、命名格式

1. Service/DAO 层方法命名规约
   1） 获取单个对象的方法用 get 做前缀。

   2） 获取多个对象的方法用 list 做前缀，复数形式结尾如：listObjects。
   3） 获取统计值的方法用 count 做前缀。
   4） 插入的方法用 save/insert 做前缀。
   5） 删除的方法用 remove/delete 做前缀。
   6） 修改的方法用 update 做前缀。

   个人建议：获取数据使用get前缀

   例如：

   获取一条；getUserByUserId

   获取多个对象：getUserList

   分页获取对象：getUserPageList

   获取统计值：getUserCount

   插入、删除、修改按照以上方法即可

2. 领域模型命名规约
   1） 数据对象：xxxDO，xxx 即为数据表名。对应 `com.tsq.admin.api.model`路径
   2） 数据传输对象：xxxDTO，xxx 为业务领域相关的名称。对应 `com.tsq.admin.api.dto`路径
   3） 展示对象：xxxVO，xxx 一般为网页名称。对应 `com.tsq.admin.api.vo`
   4） POJO 是 DO/DTO/BO/VO 的统称，禁止命名成 xxxPOJO。

##### 
