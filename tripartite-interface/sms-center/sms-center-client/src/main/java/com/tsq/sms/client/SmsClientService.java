package com.tsq.sms.client;

import com.tsq.common.model.ResponseData;
import com.tsq.sms.api.InterfaceService;
import com.tsq.sms.api.dto.SmsDTO;
import com.tsq.sms.api.service.SmsRemoteService;
import feign.hystrix.FallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;

@FeignClient(name = InterfaceService.SERVICE_NAME, fallbackFactory = SmsClientService.SmsServiceFallbackFactory.class)
public interface SmsClientService extends SmsRemoteService {
    @Component
    class SmsServiceFallbackFactory implements FallbackFactory<SmsClientService> {
        @Override
        public SmsClientService create(Throwable throwable) {
            return new SmsClientService() {
                @Override
                public ResponseData<SmsDTO> smsTo(SmsDTO smsDTO) {
                    return ResponseData.error();
                }

                @Override
                public ResponseData<SmsDTO> login(SmsDTO smsDTO) {
                    return ResponseData.error();
                }

                @Override
                public ResponseData<SmsDTO> smsMess(SmsDTO smsDTO) {
                    return ResponseData.error();
                }
            };
        }
    }
}
