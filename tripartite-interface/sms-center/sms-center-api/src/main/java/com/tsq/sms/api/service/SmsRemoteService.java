package com.tsq.sms.api.service;

import com.tsq.common.model.ResponseData;
import com.tsq.sms.api.dto.SmsDTO;
import org.springframework.web.bind.annotation.PostMapping;

public interface SmsRemoteService {

    @PostMapping("/sms/to")
    ResponseData<SmsDTO> smsTo(SmsDTO smsDTO);

    @PostMapping("/sms/login")
    ResponseData<SmsDTO> login(SmsDTO smsDTO);

    @PostMapping("/sms/mess")
    ResponseData<SmsDTO> smsMess(SmsDTO smsDTO);
}

