package com.tsq.sms.api.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Tango
 * @since 2020/8/24
 */

@Getter
@Setter
public class SmsDTO {

    public SmsDTO(){
    }
    private String signName;   //短信签名名称  君办
    private String templateName;  // 具体值  1111
    private String templateParams; //字段 code   验证码 ：{"code":"1111"}
    private String phoneNumbers; //  手机号码
}
