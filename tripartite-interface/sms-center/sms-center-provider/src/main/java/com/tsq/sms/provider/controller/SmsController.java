package com.tsq.sms.provider.controller;

import com.aliyuncs.CommonResponse;
import com.tsq.common.model.ResponseData;
import com.tsq.sms.api.dto.SmsDTO;
import com.tsq.sms.provider.service.SmsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Tango
 * @since 2020/8/24
 */

@RestController
@RequestMapping("/sms")
@Api(value = "SmsController", tags = {"短信接口"})
public class SmsController {

    @Autowired
    private SmsService smsService;

/*    @ApiOperation(value = "单人短信", notes = "单人短信")
    @PostMapping("/to")
    public ResponseData<SmsDTO> smsTo(@RequestBody SmsDTO smsDTO) {
        return smsService.smsTo(smsDTO);
    }

    @ApiOperation(value = "批量短信", notes = "批量短信")
    @PostMapping("/mass")
    public ResponseData<SmsDTO>  pushMass(@RequestBody SmsDTO smsDTO) {
        return smsService.smsMass(smsDTO);

    }*/

}
