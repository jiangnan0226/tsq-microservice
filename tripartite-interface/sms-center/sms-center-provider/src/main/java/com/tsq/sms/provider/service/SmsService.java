package com.tsq.sms.provider.service;

import com.tsq.common.model.ResponseData;
import com.tsq.sms.api.dto.SmsDTO;

public interface SmsService {

    ResponseData<SmsDTO> login(SmsDTO smsDTO);

    ResponseData<SmsDTO> smsTo(SmsDTO smsDTO);

    ResponseData<SmsDTO> smsMass(SmsDTO smsDTO);
}
