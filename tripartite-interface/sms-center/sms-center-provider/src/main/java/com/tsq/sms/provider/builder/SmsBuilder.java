package com.tsq.sms.provider.builder;

import com.aliyuncs.CommonRequest;
import com.aliyuncs.http.MethodType;


/**
 * @author Tango
 * @since 2020/8/24
 */
public class SmsBuilder {

    private CommonRequest request;

    private SmsBuilder() {
    }

    public static SmsBuilder init(String version, String domain, String region) {
        SmsBuilder smsBuilder = new SmsBuilder();
        CommonRequest commonRequest = new CommonRequest();
        commonRequest.setSysMethod(MethodType.POST);
        commonRequest.setSysVersion(version);
        commonRequest.setSysDomain(domain);
        commonRequest.setSysAction("SendSms");
        commonRequest.putQueryParameter("RegionId", region);
        smsBuilder.request = commonRequest;
        return smsBuilder;
    }

    public SmsBuilder sign(String signName) {
        request.putQueryParameter("SignName", signName);
        return this;
    }

    public SmsBuilder to(String phoneNumbers) {
        request.putQueryParameter("PhoneNumbers", phoneNumbers);
        return this;
    }

    public SmsBuilder template(String templateCode) {
        request.putQueryParameter("TemplateCode", templateCode);
        return this;
    }


    public SmsBuilder param(String param) {
        request.putQueryParameter("TemplateParam", param);
        return this;
    }

    public CommonRequest build() {
        return request;
    }


}
