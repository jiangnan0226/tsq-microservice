package com.tsq.file.provider.utils;

/**
 * @Author：ly
 * @Description:
 * @Date: 2020/7/2 0002 上午 9:35
 */
public class Constant {

    //文件类型
    public static final String FILE_TYPE = "image";

    //文件后缀
    public static final String FILE_SPLIT = ".";

    //成功提示语
    public static final String SUCCESS_MSG = "操作成功";

    //失败提示语
    public static final String ERROR_MSG = "操作失败";

    //图片类型
    public static final  String POINT =".";

    public static final  String JPG ="jpg";

    public static final  String JPEG ="jpeg";

    public static final  String BMP ="bmp";

    public static final  String PNG ="png";

    public static final  String GIF ="gif";

    public static final  String WEBP ="webp";

    //缩略图边界值
    public static final Integer EDGE_SIZE = 3000;

    //缩略比例  大于1表示放大，小于1表示缩小
    public static final Double SCALE = 0.8;
}
