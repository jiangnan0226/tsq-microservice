package com.tsq.file.provider.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.tsq.common.model.ResponseData;
import com.tsq.file.api.FileInfo;
import com.tsq.file.provider.service.IFileInfoService;
import com.tsq.file.provider.utils.Constant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2019/9/18 14:02
 */
@RestController
@RequestMapping("/file")
@Api(value = "FileController", tags = {"文件操作接口"})
public class FileController {

    @Autowired
    private IFileInfoService iFileInfoService;

    @ApiOperation(value = "分页查询", notes = "分页查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "fileInfo", value = "查询对象", dataType = "FileInfo", paramType = "query")
    })
    @GetMapping(value = "/page")
    public ResponseData<IPage<FileInfo>> page(FileInfo fileInfo) {
        return ResponseData.success(iFileInfoService.pageList(fileInfo));
    }

    @ApiOperation(value = "文件上传", notes = "文件上传")
    @PostMapping("/upload")
    public ResponseData<FileInfo> upload(@RequestParam("file") MultipartFile file) throws Exception {
        return ResponseData.success(iFileInfoService.upload(file));
    }

    @ApiOperation(value = "删除文件", notes = "根据文件id删除文件")
    @PostMapping("/delete")
    public ResponseData<Boolean> delete(@RequestParam("fileId") String fileId) throws Exception {
        Boolean isDelete = iFileInfoService.deleteFile(fileId);
        if(!isDelete){
            ResponseData.error(Constant.ERROR_MSG);
        }
        return ResponseData.success(isDelete);
    }

    @ApiOperation(value = "文件上传(base64图片上传)", notes = "文件上传(base64图片上传)")
    @PostMapping("/uploadBase64Image")
    public ResponseData<FileInfo> uploadBase64Image(@RequestParam("base64Image") String base64Image) throws Exception {
        return ResponseData.success(iFileInfoService.uploadBase64Image(base64Image));
    }

    @ApiOperation(value = "批量上传图片（文件）", notes = "批量上传图片（文件）")
    @PostMapping(value = "/uploadFileArr")
    public ResponseData<String> uploadFileArr(@RequestParam(value = "files") MultipartFile[] files) throws Exception {
        return ResponseData.success(iFileInfoService.uploadFileArr(files));
    }

    @ApiOperation(value = "批量上传图片（base64图片）", notes = "批量上传图片（base64图片）")
    @PostMapping("/uploadBase64ImageArr")
    public ResponseData<String> uploadBase64ImageArr(@RequestParam("base64ImageArr") String[] base64ImageArr) throws Exception {
        return ResponseData.success(iFileInfoService.uploadBase64ImageArr(base64ImageArr));
    }

    @ApiOperation(value = "获取图片信息", notes = "获取图片信息")
    @PostMapping("/getFileInfo")
    public ResponseData<FileInfo> getFileInfo(@RequestParam("fileId") String fileId) throws Exception {
        return ResponseData.success(iFileInfoService.getFileInfo(fileId));
    }

    @ApiOperation(value = "下载图片", notes = "下载图片")
    @PostMapping("/downLoad")
    public ResponseData<Boolean> downLoad(@RequestParam("fileName") String fileName,HttpServletResponse response) throws Exception {
        return ResponseData.success(iFileInfoService.downLoad(fileName,response));
    }
}
