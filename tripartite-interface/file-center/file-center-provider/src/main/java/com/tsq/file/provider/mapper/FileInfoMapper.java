package com.tsq.file.provider.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tsq.file.api.FileInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author dcy
 * @since 2019-09-18
 */
@Mapper
public interface FileInfoMapper extends BaseMapper<FileInfo> {

}
