package com.tsq.file.provider.utils;

import lombok.extern.slf4j.Slf4j;
import net.coobird.thumbnailator.Thumbnails;
import org.springframework.web.multipart.MultipartFile;

import java.awt.image.BufferedImage;
import java.io.*;

/**
 * @Author：ly
 * @Description:
 * @Date: 2020/7/1 0001 上午 11:17
 */
@Slf4j
public class ImageUtil {

    /**
     * file 文件
     * 压缩指定文件
     * @throws IOException
     */
    public static byte[] compressFile(File file) {
        byte[] retByte = null;
        try {
            BufferedImage bufferedImage =  Thumbnails.of(new FileInputStream(file)).scale(Constant.SCALE).asBufferedImage();
            //获取图片类型
            String imageType = getImageType(file.getName());
            //BufferedImage 转  byte[]
            retByte = FileUtil.imageToBytes(bufferedImage,imageType);
        } catch (IOException ex) {
            ex.printStackTrace();
            log.error(ex.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());
        }
        return retByte;
    }

    /**
     * 生成缩略图
     * @param file  文件
     * @throws IOException
     */
    public static byte[] createThumbnail(MultipartFile file) throws IOException {
        byte[] retByte = null;
        String fileName = file.getOriginalFilename();
        System.out.println(fileName+"   "+file.getSize());
        if(/*file.getSize() > Constant.EDGE_SIZE &&*/isImage(fileName)) {
            File f = FileUtil.inputStreamToFile(file);
            retByte = ImageUtil.compressFile(f);
        }
        return retByte;
    }

    /**
     * 判断文件是否是图片
     * @param fileName  文件名
     * @throws IOException
     */
    public static Boolean isImage(String fileName){
        Boolean isImage = false;
        if(fileName.contains(Constant.POINT + Constant.BMP)
            || fileName.contains(Constant.POINT + Constant.GIF)
            || fileName.contains(Constant.POINT + Constant.PNG)
            || fileName.contains(Constant.POINT + Constant.JPEG)
            || fileName.contains(Constant.POINT + Constant.JPG)
            || fileName.contains(Constant.POINT + Constant.WEBP)
        ){
            isImage = true;
        }
        return isImage;
    }

    /**
     * 判断图片类型
     * @param fileName  文件名
     * @throws IOException
     */
    public static String getImageType(String fileName){
        String imageType = "";
        if(fileName.contains(Constant.POINT + Constant.BMP)){
            imageType =  Constant.BMP;
        }else if(fileName.contains(Constant.POINT + Constant.GIF)){
            imageType = Constant.GIF;
        }else if(fileName.contains(Constant.POINT + Constant.PNG)){
            imageType = Constant.PNG;
        }else if(fileName.contains(Constant.POINT + Constant.JPEG)){
            imageType = Constant.JPEG;
        }else if(fileName.contains(Constant.POINT + Constant.JPG)){
            imageType = Constant.JPG;
        }else if(fileName.contains(Constant.POINT + Constant.WEBP)){
            imageType = Constant.WEBP;
        }else{
            imageType = Constant.PNG;
        }
        return imageType;
    }

}
