package com.tsq.file.provider.service;

import com.qiniu.common.QiniuException;
import com.tsq.file.api.FileInfo;
import com.tsq.web.base.service.BaseService;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author dcy
 * @since 2019-09-18
 */
public interface IFileInfoService extends BaseService<FileInfo> {

    /**
     * 上传文件
     *
     * @param file
     * @return
     * @throws Exception
     */
    FileInfo upload(MultipartFile file) throws Exception;


    /**
     * 删除文件 （暂时不用）
     *
     * @param id
     */
    Boolean deleteFile(String id);

    /**
     * 上传文件(base64图片上传)
     *
     * @param base64Image
     * @return
     * @throws Exception
     */
    FileInfo uploadBase64Image(String base64Image) throws Exception;

    /**
     * 获取文件
     *
     * @param
     * @return
     * @throws Exception
     */
    FileInfo getFileInfo(String fileId) throws QiniuException;

    /**
     * 批量上传文件
     *
     * @param files
     * @return
     * @throws Exception
     */
    String uploadFileArr(MultipartFile[] files) throws Exception;

    /**
     * 批量上传文件(base64图片)
     *
     * @param base64ImageArr
     * @return
     * @throws Exception
     */
    String uploadBase64ImageArr(String[] base64ImageArr)throws Exception;

    /**
     * 下载文件
     */
    Boolean downLoad(String fileName, HttpServletResponse response) throws IOException;
}
