package com.tsq.weixin.provider.controller;

import com.tsq.common.model.ResponseData;
import com.tsq.weixin.api.enums.ArticleTypeEnum;
import com.tsq.weixin.api.model.Article;
import com.tsq.weixin.provider.service.ArticleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/article")
@Api(value = "ArticleController", tags = {"文章查询接口"})
public class ArticleController {

    @Autowired
    private ArticleService articleService;

    @GetMapping("/detail")
    @ApiOperation(value = "查看文章详情", notes = "查看文章详情")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "articleId", value = "文章id", dataType = "int", paramType = "param")
    })
    public ResponseData<Article> getArticle(@RequestParam int articleId) {

        return ResponseData.success(articleService.findById(articleId));
    }

    @GetMapping("/category")
    @ApiOperation(value = "查看目录", notes = "查看目录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "type", value = "文章类型,COMMON(1), QUESTION(5)", dataType = "String", paramType = "param"),
            @ApiImplicitParam(name = "category", value = "分类", dataType = "String", paramType = "param")
    })
    public ResponseData<List<Article>> getCategory(@RequestParam String type,@RequestParam String category) {

        ArticleTypeEnum articleType = ArticleTypeEnum.of(type);
        if (articleType == null) return ResponseData.error("文章类型有误");
        List<Article> articles = articleService.selectCategory(articleType, category);
        return ResponseData.success(articles);
    }

    @GetMapping("/search")
    @ApiOperation(value = "文章搜索,不返回文章详情字段", notes = "文章搜索,不返回文章详情字段")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "type", value = "文章类型,COMMON(1), QUESTION(5)", dataType = "String", paramType = "param"),
            @ApiImplicitParam(name = "category", value = "分类", dataType = "String", paramType = "param"),
            @ApiImplicitParam(name = "keywords", value = "关键字", dataType = "String", paramType = "param")
    })
    public ResponseData<List<Article>> getQuestions(@RequestParam String type,
                                                    @RequestParam(required = false) String category,
                                                    @RequestParam(required = false) String keywords) {

        ArticleTypeEnum articleType = ArticleTypeEnum.of(type);
        if (articleType == null) return ResponseData.error("文章类型有误");
        if (StringUtils.isEmpty(keywords)) return ResponseData.error("关键词不得为空");
        List<Article> articles = articleService.search(articleType, category, keywords);
        return ResponseData.success(articles);
    }
}
