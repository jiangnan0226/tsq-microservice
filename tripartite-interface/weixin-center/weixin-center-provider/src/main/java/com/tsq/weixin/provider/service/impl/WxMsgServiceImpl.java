package com.tsq.weixin.provider.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tsq.web.base.service.impl.BaseServiceImpl;
import com.tsq.weixin.api.model.WxMsg;
import com.tsq.weixin.provider.mapper.WxMsgMapper;
import com.tsq.weixin.provider.service.WxMsgService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
@Transactional(rollbackFor = Exception.class)
public class WxMsgServiceImpl extends BaseServiceImpl<WxMsgMapper, WxMsg> implements WxMsgService {


    @Override
    public IPage<WxMsg> queryPage(WxMsg wxMsg) {

        String msgType = wxMsg.getMsgType();
        Date createTime = wxMsg.getCreateTime();
        String openid = wxMsg.getOpenid();
        String appid = wxMsg.getAppid();
        long currentPage = wxMsg.getCurrent();
        long pageSize = wxMsg.getSize();

        return this.baseMapper.selectPage(new Page<>(currentPage,pageSize),
                new QueryWrapper<WxMsg>()
                        .eq(StringUtils.isNotEmpty(appid), "appid", appid)
                        .in(StringUtils.isNotEmpty(msgType),"msg_type", Arrays.asList(msgType.split(",")))
                        .eq(StringUtils.isNotEmpty(openid),"openid",openid)
                        .ge(StringUtils.isNotEmpty((String.valueOf(createTime))),"create_time",createTime));
    }

    @Override
    public WxMsg getById(Long id) {
        return super.getOne(new LambdaQueryWrapper<WxMsg>().eq(WxMsg::getId,id));
    }

    @Override
    public boolean removeByIds(List<Long> asList) {
        return super.removeByIds(asList);
    }

    @Override
    public void addWxMsg(WxMsg log) {
        this.baseMapper.insert(log);
    }


}
