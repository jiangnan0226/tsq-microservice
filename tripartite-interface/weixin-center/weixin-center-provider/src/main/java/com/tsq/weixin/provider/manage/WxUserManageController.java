package com.tsq.weixin.provider.manage;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.tsq.common.model.ResponseData;
import com.tsq.weixin.api.model.WxUser;
import com.tsq.weixin.provider.service.WxUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import me.chanjar.weixin.mp.api.WxMpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/manage/wxUser")
@Api(value = "WxUserManageController", tags = {"用户表管理接口"})
public class WxUserManageController {
    @Autowired
    private WxUserService userService;
    @Autowired
    private WxMpService wxMpService;

    @GetMapping("/list")
    @ApiOperation(value = "查看列表", notes = "查看列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "appid", value = "appid", dataType = "String", paramType = "head"),
            @ApiImplicitParam(name = "wxUser", value = "字段为openid,nickname,province,city,remark,current,size的wxUser", dataType = "WxUser", paramType = "param")
    })
    public ResponseData<IPage<WxUser>> list(@RequestHeader String appid, WxUser wxUser) {

        wxUser.setAppid(appid);
        IPage<WxUser> page = userService.queryPage(wxUser);
        return ResponseData.success(page);
    }

    @PostMapping("/listByIds")
    @ApiOperation(value = "通过openid数组查看列表信息", notes = "通过openid数组查看列表信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "appid", value = "appid", dataType = "String", paramType = "head"),
            @ApiImplicitParam(name = "openids", value = "openid的数组", dataType = "String[]", paramType = "body")
    })
    public ResponseData<List<WxUser>> listByIds(@RequestHeader String appid,@RequestBody String[] openids){

        List<WxUser> list = userService.listByIds(Arrays.asList(openids));
        return ResponseData.success(list);
    }

    @GetMapping("/info")
    @ApiOperation(value = "通过openid查看列表信息", notes = "通过openid查看列表信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "appid", value = "appid", dataType = "String", paramType = "head"),
            @ApiImplicitParam(name = "openid", value = "openid", dataType = "String", paramType = "param")
    })
    public ResponseData<WxUser> info(@RequestHeader String appid, @RequestParam String openid) {

        WxUser wxUser = userService.getById(openid);
        return ResponseData.success(wxUser);
    }

    @PostMapping("/delete")
    @ApiOperation(value = "通过openid删除列表信息", notes = "通过openid删除列表信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "appid", value = "appid", dataType = "String", paramType = "head"),
            @ApiImplicitParam(name = "ids", value = "openid的数组", dataType = "String[]", paramType = "body")
    })
    public ResponseData<Boolean> delete(@RequestHeader String appid, @RequestBody String[] ids) {

        return ResponseData.success(userService.removeByIds(Arrays.asList(ids)));
    }

    @PostMapping("/syncWxUsers")
    @ApiOperation(value = "同步用户列表", notes = "同步用户列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "appid", value = "appid", dataType = "String", paramType = "head")
    })
    public ResponseData<String> syncWxUsers(@RequestHeader String appid) {

        wxMpService.switchoverTo(appid);
        userService.syncWxUsers(appid);
        return ResponseData.success("任务已建立");
    }

}
