package com.tsq.weixin.provider.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tsq.weixin.api.model.WxMsg;
import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Mapper;

@Mapper
@CacheNamespace(flushInterval = 10*1000L)//缓存过期时间（毫秒）
public interface WxMsgMapper extends BaseMapper<WxMsg> {
}
