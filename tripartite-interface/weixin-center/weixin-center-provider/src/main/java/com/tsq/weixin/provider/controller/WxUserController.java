package com.tsq.weixin.provider.controller;

import com.tsq.common.model.ResponseData;
import com.tsq.weixin.api.model.WxUser;
import com.tsq.weixin.provider.service.WxUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import me.chanjar.weixin.mp.api.WxMpService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/wxUser")
@RequiredArgsConstructor
@Api(value = "WxUserController", tags = {"微信用户（粉丝）接口"})
public class WxUserController {
    Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    WxUserService wxUserService;
    private final WxMpService wxMpService;

    @GetMapping("/getUserInfo")
    @ApiOperation(value = "查看用户详情", notes = "查看用户详情")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "appid", value = "appid", dataType = "String", paramType = "head"),
            @ApiImplicitParam(name = "openid", value = "openid", dataType = "String", paramType = "head")
    })
        public ResponseData<WxUser> getUserInfo(@RequestHeader String appid, @RequestHeader String openid){

        this.wxMpService.switchoverTo(appid);
        WxUser wxUser = wxUserService.getById(openid);
        return ResponseData.success(wxUser);
    }

}
