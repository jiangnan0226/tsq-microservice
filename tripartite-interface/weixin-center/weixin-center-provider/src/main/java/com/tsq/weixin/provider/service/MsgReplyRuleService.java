package com.tsq.weixin.provider.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.tsq.web.base.service.BaseService;
import com.tsq.weixin.api.model.MsgReplyRule;

import java.util.List;

public interface MsgReplyRuleService extends BaseService<MsgReplyRule> {

    /**
     * 查看回复规则列表
     *
     * @param msgReplyRule matchValue,分页信息(size,current)msgReplyRule对象
     * @return PageUtils
     */
    IPage<MsgReplyRule> queryPage(MsgReplyRule msgReplyRule);

    /**
     * 通过ruleId查询回复规则
     *
     * @param ruleId 主键id
     * @return MsgReplyRule
     */
    MsgReplyRule getById(Integer ruleId);

    /**
     * 添加回复规则
     *
     * @param msgReplyRule MsgReplyRule实体类
     * @return boolean
     */
    boolean save(MsgReplyRule msgReplyRule);

    /**
     * 修改回复规则
     *
     * @param msgReplyRule MsgReplyRule实体类
     * @return boolean
     */
    boolean updateById(MsgReplyRule msgReplyRule);

    /**
     * 删除回复规则
     *
     * @param asList ruleId的数组
     * @return boolean
     */
    boolean removeByIds(List<Integer> asList);

    /**
     * 查找匹配的回复规则
     *
     * @param appId appId
     * @param exactMatch 是否精确匹配
     * @param keywords 关键字
     * @return List<MsgReplyRule>
     */
    List<MsgReplyRule> getMatchedRules(String appId, boolean exactMatch, String keywords);

    /**
     * 获取当前时段内所有有效的回复规则
     *
     * @return List<MsgReplyRule> 有效的规则列表
     */
    List<MsgReplyRule> getValidRules();

    /**
     * 获取所有的回复规则
     *
     * @return List<MsgReplyRule>
     */
    List<MsgReplyRule> getRules();
}
