package com.tsq.weixin.provider.manage;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.tsq.common.model.ResponseData;
import com.tsq.weixin.api.model.TemplateMsgLog;
import com.tsq.weixin.provider.service.TemplateMsgLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import me.chanjar.weixin.mp.api.WxMpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

@RestController
@RequestMapping("/manage/templateMsgLog")
@Api(value = "TemplateMsgLogManageController", tags = {"模版消息发送记录接口"})
public class TemplateMsgLogManageController {

    @Autowired
    private TemplateMsgLogService templateMsgLogService;
    @Autowired
    private WxMpService wxMpService;

    @GetMapping("/list")
    @ApiOperation(value = "查看列表", notes = "查看列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "appid", value = "appid", dataType = "String", paramType = "header"),
            @ApiImplicitParam(name = "templateMsgLog", value = "openid,size,current的templateMsgLog", dataType = "TemplateMsgLog", paramType = "param")
    })
    public ResponseData<IPage<TemplateMsgLog>> list(@RequestHeader String appid, TemplateMsgLog templateMsgLog) {
        templateMsgLog.setAppid(appid);
        IPage<TemplateMsgLog> page = templateMsgLogService.queryPage(templateMsgLog);

        return ResponseData.success(page);
    }

    @GetMapping("/info")
    @ApiOperation(value = "查看log信息", notes = "查看log信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "appid", value = "appid", dataType = "String", paramType = "head"),
            @ApiImplicitParam(name = "logId", value = "id主键", dataType = "Integer", paramType = "param")
    })
    public ResponseData<TemplateMsgLog> info(@RequestHeader String appid, @RequestParam Integer logId) {
        return ResponseData.success(templateMsgLogService.getById(logId));
    }

    @PostMapping("/save")
    @ApiOperation(value = "保存log信息", notes = "保存log信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "appid", value = "appid", dataType = "String", paramType = "head"),
            @ApiImplicitParam(name = "templateMsgLog", value = "TemplateMsgLog", dataType = "TemplateMsgLog", paramType = "body")
    })
    public ResponseData<Boolean> save(@RequestHeader String appid,@RequestBody TemplateMsgLog templateMsgLog) {

        templateMsgLog.setAppid(appid);
        return ResponseData.success(templateMsgLogService.save(templateMsgLog));
    }

    @PostMapping("/update")
    @ApiOperation(value = "修改log信息", notes = "修改log信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "appid", value = "appid", dataType = "String", paramType = "head"),
            @ApiImplicitParam(name = "templateMsgLog", value = "TemplateMsgLog", dataType = "TemplateMsgLog", paramType = "body")
    })
    public ResponseData<Boolean> update(@RequestHeader String appid,@RequestBody TemplateMsgLog templateMsgLog) {
        return ResponseData.success(templateMsgLogService.updateById(templateMsgLog));
    }

    @PostMapping("/delete")
    @ApiOperation(value = "删除log信息", notes = "删除log信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "appid", value = "appid", dataType = "String", paramType = "head"),
            @ApiImplicitParam(name = "logIds", value = "id的数组", dataType = "Integer[] ", paramType = "body")
    })
    public ResponseData<Boolean> delete(@RequestHeader String appid, @RequestBody Integer[] logIds) {
        return ResponseData.success(templateMsgLogService.removeByIds(Arrays.asList(logIds)));
    }
}
