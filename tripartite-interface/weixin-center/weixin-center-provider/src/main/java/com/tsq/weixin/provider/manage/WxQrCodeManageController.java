package com.tsq.weixin.provider.manage;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.tsq.common.model.ResponseData;
import com.tsq.weixin.api.form.WxQrCodeForm;
import com.tsq.weixin.api.model.WxQrCode;
import com.tsq.weixin.provider.service.WxQrCodeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.result.WxMpQrCodeTicket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

/**
 * 公众号带参二维码管理
 * https://github.com/Wechat-Group/WxJava/wiki/MP_二维码管理
 */
@RestController
@RequestMapping("/manage/wxQrCode")
@Api(value = "WxQrCodeManageController", tags = {"公众号带参二维码管理接口"})
public class WxQrCodeManageController {
    @Autowired
    private WxQrCodeService wxQrCodeService;
    @Autowired
    private WxMpService wxMpService;

    @PostMapping("/createTicket")
    @ApiOperation(value = "创建带参二维码ticket", notes = "创建带参二维码ticket")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "appid", value = "appid", dataType = "String", paramType = "head"),
            @ApiImplicitParam(name = "form", value = "WxQrCodeForm对象", dataType = "WxQrCodeForm", paramType = "body")
    })
    public ResponseData<WxMpQrCodeTicket> createTicket(@RequestHeader String appid, @RequestBody WxQrCodeForm form) throws WxErrorException {
        wxMpService.switchoverTo(appid);
        WxMpQrCodeTicket ticket = wxQrCodeService.createQrCode(appid,form);
        return ResponseData.success(ticket);
    }

    @GetMapping("/list")
    @ApiOperation(value = "根据场景查看列表", notes = "查看列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "appid", value = "appid", dataType = "String", paramType = "head"),
            @ApiImplicitParam(name = "wxQrCode", value = "sceneStr,current,size的wxQrCode", dataType = "WxQrCode", paramType = "param")
    })
    public ResponseData<IPage<WxQrCode>> list(@RequestHeader String appid, WxQrCode wxQrCode) {

        wxQrCode.setAppid(appid);
        IPage<WxQrCode> page = wxQrCodeService.queryPage(wxQrCode);
        return ResponseData.success(page);
    }


    @GetMapping("/info")
    @ApiOperation(value = "查看信息", notes = "查看信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "appid", value = "appid", dataType = "String", paramType = "head"),
            @ApiImplicitParam(name = "id", value = "主键id", dataType = "Long", paramType = "param")
    })
    public ResponseData<WxQrCode> info(@RequestHeader String appid, @RequestParam Long id) {

        WxQrCode wxQrCode = wxQrCodeService.getById(id);
        return ResponseData.success(wxQrCode);
    }

    @PostMapping("/delete")
    @ApiOperation(value = "删除信息", notes = "查看信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "appid", value = "appid", dataType = "String", paramType = "head"),
            @ApiImplicitParam(name = "ids", value = "主键id数组", dataType = "Long[]", paramType = "body")
    })
    public ResponseData<Boolean> delete(@RequestHeader String appid, @RequestBody Long[] ids) {

        return ResponseData.success(wxQrCodeService.removeByIds(Arrays.asList(ids)));
    }

}