package com.tsq.weixin.provider.service;

import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.bean.material.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;

public interface WxAssetsService {

    /**
     *  获取素材总数
     * @return WxMpMaterialCountResult
     * @throws WxErrorException WxErrorException
     */
    WxMpMaterialCountResult materialCount() throws WxErrorException;

    /**
     * 根据类别分页获取非图文素材列表
     * @param type 类型
     * @param page 页数
     * @return WxMpMaterialFileBatchGetResult
     * @throws WxErrorException WxErrorException
     */
    WxMpMaterialFileBatchGetResult materialFileBatchGet(String type, int page) throws WxErrorException;

    /**
     * 分页获取图文素材列表
     * @param page 页数
     * @return WxMpMaterialNewsBatchGetResult
     * @throws WxErrorException WxErrorException
     */
    WxMpMaterialNewsBatchGetResult materialNewsBatchGet(int page) throws WxErrorException;

    /**
     * 添加图文永久素材
     * @param articles List<WxMpNewsArticle>
     * @return WxMpMaterialUploadResult
     * @throws WxErrorException WxErrorException
     */
    WxMpMaterialUploadResult materialNewsUpload(List<WxMpNewsArticle> articles)throws WxErrorException;

    /**
     * 更新图文素材中的某篇文章
     * @param form WxMpMaterialArticleUpdate
     * @return WxMpMaterialUploadResult
     * @throws WxErrorException WxErrorException
     */
    boolean materialArticleUpdate(WxMpMaterialArticleUpdate form) throws WxErrorException;

    /**
     * 添加多媒体永久素材
     * @param mediaType 文件类型
     * @param fileName 文件名
     * @param file     MultipartFile文件
     * @return WxMpMaterialUploadResult
     * @throws WxErrorException WxErrorException
     */
    WxMpMaterialUploadResult materialFileUpload(String mediaType, String fileName, MultipartFile file) throws WxErrorException, IOException;

    /**
     * 删除素材
     * @param mediaId mediaId
     * @return boolean
     * @throws WxErrorException WxErrorException
     */
    boolean materialDelete(String mediaId)throws WxErrorException;

}
