package com.tsq.weixin.provider.manage;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.tsq.common.model.ResponseData;
import com.tsq.weixin.api.form.TemplateMsgBatchForm;
import com.tsq.weixin.api.model.MsgTemplate;
import com.tsq.weixin.provider.service.MsgTemplateService;
import com.tsq.weixin.provider.service.TemplateMsgService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Arrays;

@RestController
@RequestMapping("/manage/msgTemplate")
@Api(value = "MsgTemplateManageController", tags = {"消息模板操作接口"})
public class MsgTemplateManageController {
    @Autowired
    private MsgTemplateService msgTemplateService;
    @Autowired
    private TemplateMsgService templateMsgService;
    @Autowired
    private WxMpService wxMpService;

    @GetMapping("/list")
    @ApiOperation(value = "查看消息模板列表", notes = "查看消息模板列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "appid", value = "appid", dataType = "String", paramType = "header"),
            @ApiImplicitParam(name = "msgTemplate", value = "title,name,current,size的MsgTemplate对象", dataType = "MsgTemplate", paramType = "param")
    })
    public ResponseData<IPage<MsgTemplate>> list(@RequestHeader String appid, MsgTemplate msgTemplate) {

        msgTemplate.setAppid(appid);
        IPage<MsgTemplate> page = msgTemplateService.queryPage(msgTemplate);
        return ResponseData.success(page);
    }

    @GetMapping("/info")
    @ApiOperation(value = "通过id查看消息模板", notes = "通过id查看消息模板")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "id主键", dataType = "Long", paramType = "param")
    })
    public ResponseData<MsgTemplate> info(@RequestParam Long id) {

        MsgTemplate msgTemplate = msgTemplateService.getById(id);
        return ResponseData.success(msgTemplate);
    }

    @GetMapping("/getByName")
    @ApiOperation(value = "通过name查看消息模板", notes = "通过name查看消息模板")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "模版名称", dataType = "String", paramType = "param")
    })
    public ResponseData<MsgTemplate> getByName( @RequestParam String name){

        MsgTemplate msgTemplate = msgTemplateService.selectByName(name);
        return ResponseData.success(msgTemplate);
    }

    @PostMapping("/save")
    @ApiOperation(value = "保存消息模板", notes = "保存消息模板")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "msgTemplate", value = "MsgTemplate对象", dataType = "MsgTemplate", paramType = "body")
    })
    public ResponseData<Boolean> save(@RequestBody MsgTemplate msgTemplate) {

        return ResponseData.success(msgTemplateService.save(msgTemplate));
    }

    @PostMapping("/update")
    @ApiOperation(value = "修改消息模板", notes = "修改消息模板")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "msgTemplate", value = "MsgTemplate对象", dataType = "MsgTemplate", paramType = "body")
    })
    public ResponseData<Boolean> update(@RequestBody MsgTemplate msgTemplate) {

        return ResponseData.success(msgTemplateService.updateById(msgTemplate));
    }

    @PostMapping("/delete")
    @ApiOperation(value = "删除消息模板", notes = "删除消息模板")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "主键id的数组", dataType = "String[]", paramType = "body")
    })
    public  ResponseData<Boolean> delete(@RequestBody String[] ids) {

        return ResponseData.success(msgTemplateService.removeByIds(Arrays.asList(ids)));
    }

    @PostMapping("/syncWxTemplate")
    @ApiOperation(value = "同步公众号模板", notes = "同步公众号模板")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "appid", value = "appid", dataType = "appId", paramType = "head")
    })
    public ResponseData<Boolean> syncWxTemplate(@RequestHeader String appid) throws WxErrorException {

        this.wxMpService.switchoverTo(appid);
        return ResponseData.success(msgTemplateService.syncWxTemplate(appid));
    }

    @PostMapping("/sendMsgBatch")
    @ApiOperation(value = "批量向用户发送模板消息", notes = "批量向用户发送模板消息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "appId", value = "appId", dataType = "appId", paramType = "header"),
            @ApiImplicitParam(name = "form", value = "TemplateMsgBatchForm对象", dataType = "TemplateMsgBatchForm", paramType = "body")
    })
    public ResponseData<Boolean> sendMsgBatch(@RequestHeader String appid,@RequestBody TemplateMsgBatchForm form) {
        System.out.println(form);

        this.wxMpService.switchoverTo(appid);
        templateMsgService.sendMsgBatch(form, appid);
        return ResponseData.success();
    }

}
