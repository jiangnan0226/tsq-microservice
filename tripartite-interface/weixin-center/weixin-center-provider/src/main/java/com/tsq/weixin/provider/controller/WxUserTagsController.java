package com.tsq.weixin.provider.controller;

import com.alibaba.fastjson.JSONArray;
import com.tsq.common.model.ResponseData;
import com.tsq.weixin.api.form.WxUserTaggingForm;
import com.tsq.weixin.api.model.WxUser;
import com.tsq.weixin.provider.service.WxUserService;
import com.tsq.weixin.provider.service.WxUserTagsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import me.chanjar.weixin.common.error.WxError;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/wxUserTags")
@Api(value = "WxUserTagsController", tags = {"微信用户标签操作接口"})
public class WxUserTagsController {
    @Autowired
    WxUserTagsService wxUserTagsService;
    @Autowired
    WxUserService wxUserService;
    private final WxMpService wxMpService;

    @GetMapping("/userTags")
    @ApiOperation(value = "查看用户标签", notes = "查看用户标签")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "appid", value = "appid", dataType = "String", paramType = "head"),
            @ApiImplicitParam(name = "openid", value = "openid", dataType = "String", paramType = "head")
    })
    public ResponseData<JSONArray> userTags(@RequestHeader String appid, @RequestHeader String openid){
        if(openid==null){
            return ResponseData.error("none_openid");
        }
        this.wxMpService.switchoverTo(appid);
        WxUser wxUser = wxUserService.getById(openid);
        if(wxUser==null){
            wxUser=wxUserService.refreshUserInfo(openid,appid);
            if(wxUser==null) {
                return ResponseData.error("not_subscribed");
            }
        }
        return ResponseData.success(wxUser.getTagidList());
    }

    @PostMapping("/tagging")
    @ApiOperation(value = "为用户绑定标签", notes = "为用户绑定标签")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "appid", value = "appid", dataType = "String", paramType = "head"),
            @ApiImplicitParam(name = "openid", value = "openid", dataType = "String", paramType = "head"),
            @ApiImplicitParam(name = "form", value = "WxUserTaggingForm对象", dataType = "WxUserTaggingForm", paramType = "body"),
    })
    public ResponseData<Boolean> tagging(@RequestHeader String appid,@RequestHeader String openid , @RequestBody WxUserTaggingForm form) {
        this.wxMpService.switchoverTo(appid);
        boolean flag = false;
        try {
           flag = wxUserTagsService.tagging(form.getTagid(),openid);
        }catch (WxErrorException e){
            WxError error = e.getError();
            if(50005==error.getErrorCode()){//未关注公众号
                return ResponseData.error("not_subscribed");
            }else {
                return ResponseData.error(error.getErrorMsg());
            }
        }
        return ResponseData.success(flag);
    }

    @PostMapping("/untagging")
    @ApiOperation(value = "取消用户所绑定的某一个标签", notes = "取消用户所绑定的某一个标签")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "appid", value = "appid", dataType = "String", paramType = "head"),
            @ApiImplicitParam(name = "openid", value = "openid", dataType = "String", paramType = "head"),
            @ApiImplicitParam(name = "form", value = "WxUserTaggingForm对象", dataType = "WxUserTaggingForm", paramType = "body"),
    })
    public ResponseData<Boolean> untagging(@RequestHeader String appid,@RequestHeader String openid,
                                           @RequestBody WxUserTaggingForm form) throws WxErrorException {

        this.wxMpService.switchoverTo(appid);
        return ResponseData.success(wxUserTagsService.untagging(form.getTagid(),openid));
    }

}