package com.tsq.weixin.provider.manage;


import com.tsq.common.model.ResponseData;
import com.tsq.weixin.api.form.WxUserBatchTaggingForm;
import com.tsq.weixin.api.form.WxUserTagForm;
import com.tsq.weixin.provider.service.WxUserService;
import com.tsq.weixin.provider.service.WxUserTagsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.tag.WxUserTag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/manage/wxUserTags")
@Api(value = "WxUserTagsManageController", tags = {"用户标签操作接口"})
public class WxUserTagsManageController {
    @Autowired
    private WxUserService wxUserService;
    @Autowired
    private WxUserTagsService wxUserTagsService;
    @Autowired
    private WxMpService wxMpService;

    @GetMapping("/list")
    @ApiOperation(value = "查询用户标签", notes = "查询用户标签")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "appid", value = "appid", dataType = "String", paramType = "head")
    })
    public ResponseData<List<WxUserTag>> list(@RequestHeader String appid) throws WxErrorException {

        wxMpService.switchoverTo(appid);
        List<WxUserTag> wxUserTags =  wxUserTagsService.getWxTags();
        return ResponseData.success(wxUserTags);
    }

    @PostMapping("/save")
    @ApiOperation(value = "修改或新增标签", notes = "修改或新增标签")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "appid", value = "appid", dataType = "String", paramType = "head"),
            @ApiImplicitParam(name = "form", value = "用户标签对象", dataType = "WxUserTagForm", paramType = "body")
    })
    public ResponseData<Boolean> save(@RequestHeader String appid, @RequestBody WxUserTagForm form) throws WxErrorException{

        wxMpService.switchoverTo(appid);
        Long tagId = form.getId();
        if(tagId==null || tagId<=0){
            return ResponseData.success(wxUserTagsService.creatTag(form.getName()));
        }else {
            return ResponseData.success(wxUserTagsService.updateTag(tagId,form.getName()));
        }
    }

    @PostMapping("/delete")
    @ApiOperation(value = "删除标签", notes = "删除标签")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "appid", value = "appid", dataType = "String", paramType = "head"),
            @ApiImplicitParam(name = "tagId", value = "标签id", dataType = "Long", paramType = "body")
    })
    public ResponseData<Boolean> delete(@RequestHeader String appid, @RequestBody Long tagId) throws WxErrorException{

        if(tagId==null || tagId<=0){
            return ResponseData.error("标签ID不得为空");
        }
        wxMpService.switchoverTo(appid);
        return ResponseData.success(wxUserTagsService.deleteTag(tagId));
    }

    @PostMapping("/batchTagging")
    @ApiOperation(value = "批量给用户打标签", notes = "批量给用户打标签")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "appid", value = "appid", dataType = "String", paramType = "head"),
            @ApiImplicitParam(name = "form", value = "用户标签对象", dataType = "WxUserBatchTaggingForm", paramType = "body")
    })
    public ResponseData<Boolean> batchTagging(@RequestHeader String appid, @RequestBody WxUserBatchTaggingForm form) throws WxErrorException{

        wxMpService.switchoverTo(appid);
        return ResponseData.success(wxUserTagsService.batchTagging(form.getTagid(),form.getOpenidList()));
    }

    @PostMapping("/batchUnTagging")
    @ApiOperation(value = "批量移除用户标签", notes = "批量移除用户标签")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "appid", value = "appid", dataType = "String", paramType = "head"),
            @ApiImplicitParam(name = "form", value = "用户标签对象", dataType = "WxUserBatchTaggingForm", paramType = "body")
    })
    public ResponseData<Boolean> batchUnTagging(@RequestHeader String appid, @RequestBody WxUserBatchTaggingForm form) throws WxErrorException{

        wxMpService.switchoverTo(appid);
        return ResponseData.success(wxUserTagsService.batchUnTagging(form.getTagid(),form.getOpenidList()));
    }
}