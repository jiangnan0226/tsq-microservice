package com.tsq.weixin.provider.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tsq.weixin.api.model.WxAccount;
import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Mapper;

@Mapper
@CacheNamespace(flushInterval = 300000L)//缓存五分钟过期
public interface WxAccountMapper extends BaseMapper<WxAccount> {
}
