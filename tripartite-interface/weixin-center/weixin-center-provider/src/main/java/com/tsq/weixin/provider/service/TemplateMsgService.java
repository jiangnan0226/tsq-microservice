package com.tsq.weixin.provider.service;

import com.tsq.weixin.api.form.TemplateMsgBatchForm;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateMessage;

public interface TemplateMsgService{

    /**
     * 发送微信模版消息
     * @param appid String
     * @param msg WxMpTemplateMessage
     */
    void sendTemplateMsg(WxMpTemplateMessage msg,String appid);

    /**
     * 批量向用户发送模板消息
     * 通过用户筛选条件（一般使用标签筛选），将消息发送给数据库中所有符合筛选条件的用户
     * @param form TemplateMsgBatchForm
     * @param appid String
     */
    void sendMsgBatch(TemplateMsgBatchForm form, String appid);

}
