package com.tsq.weixin.provider.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.tsq.web.base.service.BaseService;
import com.tsq.weixin.api.model.TemplateMsgLog;

public interface TemplateMsgLogService extends BaseService<TemplateMsgLog> {
    /**
     * 记添加访问log到队列中，队列数据会定时批量插入到数据库
     *
     * @param log TemplateMsgLog
     */
    void addLog(TemplateMsgLog log);

    /**
     * 查看列表
     *
     * @param templateMsgLog openid,size,current的templateMsgLog
     * @return  IPage<TemplateMsgLog>
     */
    IPage<TemplateMsgLog> queryPage(TemplateMsgLog templateMsgLog);
}
