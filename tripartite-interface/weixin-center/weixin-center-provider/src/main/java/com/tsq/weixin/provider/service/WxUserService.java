package com.tsq.weixin.provider.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.tsq.web.base.service.BaseService;
import com.tsq.weixin.api.model.WxUser;

import java.util.List;

public interface WxUserService extends BaseService<WxUser> {

    /**
     * 分页查询用户数据
     *
     * @param wxUser 字段为openid,nickname,province,city,remark,current,size的wxUser
     * @return IPage<WxUser>
     */
    IPage<WxUser> queryPage(WxUser wxUser);

    /**
     * 根据openid更新用户信息
     *
     * @param appid appid
     * @param openid openid
     * @return WxUser
     */
    WxUser refreshUserInfo(String openid,String appid);

    /**
     * 异步批量更新用户信息
     *
     * @param appid appid
     * @param openidList openid的集合
     */
    void refreshUserInfoAsync(String appid, String[] openidList);

    /**
     * 数据存在时更新，否则新增
     *
     * @param user WxUser对象
     */
    void updateOrInsert(WxUser user);

    /**
     * 取消关注，更新关注状态
     *
     * @param openid openid
     */
    void unsubscribe(String openid);

    /**
     * 同步用户列表
     *
     * @param appid appid
     */
    void syncWxUsers(String appid);

    /**
     * 通过传入的openid列表，同步用户列表
     *
     * @param appId //多app
     * @param openids openid的集合
     */
    void syncWxUsers(String appId, List<String> openids);

    /**
     * 查看用户标签
     *
     * @param openid openid
     * @return WxUser
     */
    WxUser getById(String openid);

}
