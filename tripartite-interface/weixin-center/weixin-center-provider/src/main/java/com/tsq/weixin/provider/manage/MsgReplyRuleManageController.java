package com.tsq.weixin.provider.manage;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.tsq.common.model.ResponseData;
import com.tsq.weixin.api.model.MsgReplyRule;
import com.tsq.weixin.provider.service.MsgReplyRuleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

@RestController
@RequestMapping("/manage/msgReplyRule")
@Api(value = "MsgReplyRuleManageController", tags = {"自动回复规则操作接口"})
public class MsgReplyRuleManageController {
    @Autowired
    private MsgReplyRuleService msgReplyRuleService;

    @GetMapping("/list")
    @ApiOperation(value = "查看回复规则列表", notes = "查看回复规则列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "appid", value = "appid", dataType = "String", paramType = "header"),
            @ApiImplicitParam(name = "msgReplyRule", value = "matchValue,分页信息(size,current)msgReplyRule对象)", dataType = "MsgReplyRule", paramType = "param")

    })
    public ResponseData<IPage<MsgReplyRule>> list(@RequestHeader String appid,MsgReplyRule msgReplyRule) {

        msgReplyRule.setAppid(appid);
        IPage<MsgReplyRule> page = msgReplyRuleService.queryPage(msgReplyRule);
        return ResponseData.success(page);
    }

    @GetMapping("/info")
    @ApiOperation(value = "通过ruleId查询回复规则", notes = "通过ruleId查询回复规则")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ruleId", value = "主键id", dataType = "Integer", paramType = "param"),

    })
    public ResponseData<MsgReplyRule> info(@RequestParam("ruleId") Integer ruleId) {

        MsgReplyRule msgReplyRule = msgReplyRuleService.getById(ruleId);
        return ResponseData.success(msgReplyRule);
    }

    @PostMapping("/save")
    @ApiOperation(value = "添加回复规则", notes = "添加回复规则")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "msgReplyRule", value = "MsgReplyRule实体类", dataType = "MsgReplyRule", paramType = "body"),

    })
    public ResponseData<Boolean> save(@RequestBody MsgReplyRule msgReplyRule) {
        return ResponseData.success(msgReplyRuleService.save(msgReplyRule));
    }

    @PostMapping("/update")
    @ApiOperation(value = "修改回复规则", notes = "修改回复规则")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "msgReplyRule", value = "MsgReplyRule实体类", dataType = "MsgReplyRule", paramType = "body"),

    })
    public ResponseData<Boolean> update(@RequestBody MsgReplyRule msgReplyRule) {
        return ResponseData.success(msgReplyRuleService.updateById(msgReplyRule));
    }

    @PostMapping("/delete")
    @ApiOperation(value = "删除回复规则", notes = "删除回复规则")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ruleIds", value = "ruleId的数组", dataType = "Integer[]", paramType = "body"),

    })
    public ResponseData<Boolean> delete(@RequestBody Integer[] ruleIds) {
        return ResponseData.success(msgReplyRuleService.removeByIds(Arrays.asList(ruleIds)));
    }

}
