package com.tsq.weixin.provider.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tsq.web.base.service.impl.BaseServiceImpl;
import com.tsq.weixin.api.model.MsgTemplate;
import com.tsq.weixin.api.validator.Assert;
import com.tsq.weixin.provider.mapper.MsgTemplateMapper;
import com.tsq.weixin.provider.service.MsgTemplateService;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.template.WxMpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional(rollbackFor = Exception.class)
public class MsgTemplateImpl extends BaseServiceImpl<MsgTemplateMapper,MsgTemplate> implements MsgTemplateService {

    @Autowired
    private WxMpService wxService;

    @Override
    public IPage<MsgTemplate> queryPage(MsgTemplate msgTemplate) {

        String title = msgTemplate.getTitle();
        String name = msgTemplate.getName();
        String appid = msgTemplate.getAppid();
        long currentPage = msgTemplate.getCurrent();
        long pageSize = msgTemplate.getSize();

        return this.baseMapper.selectPage(new Page<>(currentPage,pageSize), new QueryWrapper<MsgTemplate>()
                .eq(!StringUtils.isEmpty(appid), "appid", appid)
                .like(!StringUtils.isEmpty(title), "title", title)
                .like(!StringUtils.isEmpty(name), "name", name));

    }

    @Override
    public MsgTemplate getById(Long id) {
        return super.getOne(new QueryWrapper<MsgTemplate>().eq("id",id));
    }

    @Override
    public MsgTemplate selectByName(String name) {

        Assert.isBlank(name, "模板名称不得为空");
        return this.getOne(new QueryWrapper<MsgTemplate>()
                .eq("name", name)
                .eq("status", 1)
                .last("LIMIT 1"));
    }

    @Override
    public boolean save(MsgTemplate msgTemplate) {
      return super.save(msgTemplate);
    }

    @Override
    public boolean updateById(MsgTemplate msgTemplate) {
      return super.updateById(msgTemplate);
    }

    @Override
    public boolean removeByIds(List<String> asList) {
      return super.removeByIds(asList);
    }

    @Override
    public boolean syncWxTemplate(String appid) throws WxErrorException {

        List<WxMpTemplate> wxMpTemplateList= wxService.getTemplateMsgService().getAllPrivateTemplate();
        List<MsgTemplate> templates = wxMpTemplateList.stream()
                                        .map(item->{
                                            MsgTemplate msgTemplate = new MsgTemplate(item,appid);
                                            if (this.selectByName(msgTemplate.getName())==null){
                                                this.save(msgTemplate);
                                            };
                                            return msgTemplate;
                                        }).collect(Collectors.toList());
        return templates.isEmpty();
    }
}
