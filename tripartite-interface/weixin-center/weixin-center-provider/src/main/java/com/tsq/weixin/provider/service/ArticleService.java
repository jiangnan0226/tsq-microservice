package com.tsq.weixin.provider.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.tsq.web.base.service.BaseService;
import com.tsq.weixin.api.enums.ArticleTypeEnum;
import com.tsq.weixin.api.model.Article;

import java.util.List;

public interface ArticleService extends BaseService<Article> {

    /**
     * 查询文章详情，每次查询后增加点击次数
     *
     * @param articleId  文章id
     * @return Article
     */
    Article findById(int articleId);

    /**
     * 查看目录，不返回文章详情字段
     *
     * @param articleType 文章类型
     * @param category 分类
     * @return List<Article>
     */
    List<Article> selectCategory(ArticleTypeEnum articleType, String category);

    /**
     * 文章查找，不返回文章详情字段
     *
     * @param articleType 文章类型
     * @param category 分类
     * @param keywords 关键字
     * @return List<Article>
     */
    List<Article> search(ArticleTypeEnum articleType, String category, String keywords);

    /**
     * 查找文章列表
     *
     * @param article 字段为title,type,category,subCategory,current,size的article
     * @return Page<Article>
     */
    IPage<Article> queryPage(Article article);

    /**
     * 添加或编辑文章,同名文章不可重复添加
     *
     * @param article 文章
     */
    boolean save(Article article);

}
