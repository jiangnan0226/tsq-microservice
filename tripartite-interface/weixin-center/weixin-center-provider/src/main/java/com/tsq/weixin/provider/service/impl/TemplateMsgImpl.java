package com.tsq.weixin.provider.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.tsq.weixin.api.config.TaskExcutor;
import com.tsq.weixin.api.form.TemplateMsgBatchForm;
import com.tsq.weixin.api.model.TemplateMsgLog;
import com.tsq.weixin.api.model.WxAccount;
import com.tsq.weixin.api.model.WxUser;
import com.tsq.weixin.provider.service.MsgTemplateService;
import com.tsq.weixin.provider.service.TemplateMsgLogService;
import com.tsq.weixin.provider.service.TemplateMsgService;
import com.tsq.weixin.provider.service.WxUserService;
import lombok.RequiredArgsConstructor;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateMessage;
import me.chanjar.weixin.mp.util.WxMpConfigStorageHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashMap;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Transactional(rollbackFor = Exception.class)
public class TemplateMsgImpl implements TemplateMsgService {
    Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private TemplateMsgLogService templateMsgLogService;
    private final WxMpService wxService;
    @Autowired
    MsgTemplateService msgTemplateService;
    @Autowired
    WxUserService wxUserService;


    /**
     * 发送微信模版消息,使用固定线程的线程池
     */
    @Override
    @Async
    public void sendTemplateMsg(WxMpTemplateMessage msg,String appid) {
        TaskExcutor.submit(() -> {
            String result;
            try {
                wxService.switchover(appid);
                result = wxService.getTemplateMsgService().sendTemplateMsg(msg);
            } catch (WxErrorException e) {
                result = e.getMessage();
            }

            //保存发送日志
            TemplateMsgLog log = new TemplateMsgLog(msg,appid, result);
            templateMsgLogService.addLog(log);
        });
    }

    @Override
    @Async
    public void sendMsgBatch(TemplateMsgBatchForm form, String appid) {

        logger.info("批量发送模板消息任务开始,参数：{}",form.toString());
        wxService.switchover(appid);
        WxMpTemplateMessage.WxMpTemplateMessageBuilder builder = WxMpTemplateMessage.builder()
                .templateId(form.getTemplateId())
                .url(form.getUrl())
                .miniProgram(form.getMiniprogram())
                .data(form.getData());
        Map<String, Object> filterParams = form.getWxUserFilterParams();
        if(filterParams==null) {
            filterParams=new HashMap<>(8);
        }
        long currentPage=1L,totalPages=Long.MAX_VALUE;
        filterParams.put("limit","500");
        while (currentPage<=totalPages){
            filterParams.put("page",String.valueOf(currentPage));
            //按条件查询用户
            WxUser wxUser = new WxUser();
            wxUser.setCurrent(Integer.parseInt(filterParams.get("page").toString()));
            wxUser.setSize(Integer.parseInt(filterParams.get("limit").toString()));
            IPage<WxUser> wxUsers = wxUserService.queryPage(wxUser);
            logger.info("批量发送模板消息任务,使用查询条件，处理第{}页，总用户数：{}",currentPage,wxUsers.getTotal());
            wxUsers.getRecords().forEach(user->{
                WxMpTemplateMessage msg = builder.toUser(user.getOpenid()).build();
                this.sendTemplateMsg(msg,appid);
            });
            currentPage=wxUsers.getCurrent()+1L;
            totalPages=wxUsers.getPages();
        }
        logger.info("批量发送模板消息任务结束");
    }

}
