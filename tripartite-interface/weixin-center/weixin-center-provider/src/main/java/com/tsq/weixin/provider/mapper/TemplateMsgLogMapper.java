package com.tsq.weixin.provider.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tsq.weixin.api.model.TemplateMsgLog;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TemplateMsgLogMapper extends BaseMapper<TemplateMsgLog> {
}
