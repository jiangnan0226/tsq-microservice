package com.tsq.weixin.provider.service.impl;

import com.tsq.weixin.provider.service.WxUserService;
import com.tsq.weixin.provider.service.WxUserTagsService;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.tag.WxUserTag;
import me.chanjar.weixin.mp.util.WxMpConfigStorageHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@CacheConfig(cacheNames = {"wxUserTagsServiceCache"})
@Slf4j
public class WxUserTagsServiceImpl implements WxUserTagsService {

    @Autowired
    private WxMpService wxService;
    @Autowired
    private WxUserService wxUserService;
    public static final String CACHE_KEY="'WX_USER_TAGS'";

    @Override
    public List<WxUserTag> getWxTags() throws WxErrorException {
        log.info("拉取公众号用户标签");
        return wxService.getUserTagService().tagGet();
    }

    @Override
    public boolean creatTag(String name) throws WxErrorException {
        return wxService.getUserTagService().tagCreate(name) != null;
    }

    @Override
    public boolean updateTag(Long tagid, String name) throws WxErrorException {
        return wxService.getUserTagService().tagUpdate(tagid,name);
    }

    @Override
    public boolean deleteTag(Long tagid) throws WxErrorException {
       return wxService.getUserTagService().tagDelete(tagid);
    }

    @Override
    public boolean batchTagging(Long tagid, String[] openidList) throws WxErrorException {
        boolean flag = wxService.getUserTagService().batchTagging(tagid,openidList);
        wxUserService.refreshUserInfoAsync(WxMpConfigStorageHolder.get(), openidList);//标签更新后更新对应用户信息
        return flag;
    }

    @Override
    public boolean batchUnTagging(Long tagid, String[] openidList) throws WxErrorException {
        boolean flag = wxService.getUserTagService().batchUntagging(tagid,openidList);
        wxUserService.refreshUserInfoAsync(WxMpConfigStorageHolder.get(), openidList);//标签更新后更新对应用户信息
        return flag;
    }

    @Override
    public boolean tagging(Long tagid, String openid) throws WxErrorException {
        boolean flag = wxService.getUserTagService().batchTagging(tagid,new String[]{openid});
        String appid = WxMpConfigStorageHolder.get();
        wxUserService.refreshUserInfo(openid,appid);
        return flag;
    }

    @Override
    public boolean untagging(Long tagid, String openid) throws WxErrorException {
        boolean flag = wxService.getUserTagService().batchUntagging(tagid,new String[]{openid});
        String appid = WxMpConfigStorageHolder.get();
        wxUserService.refreshUserInfo(openid,appid);
        return flag;
    }

}
