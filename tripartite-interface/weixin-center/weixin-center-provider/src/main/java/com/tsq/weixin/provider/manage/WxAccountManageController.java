package com.tsq.weixin.provider.manage;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.tsq.common.model.ResponseData;
import com.tsq.weixin.api.model.WxAccount;
import com.tsq.weixin.provider.service.WxAccountService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/manage/wxAccount")
@Api(value = "WxAccountManageController", tags = {"公众号账号操作接口"})
public class WxAccountManageController {
    @Autowired
    private WxAccountService wxAccountService;

    @GetMapping("/list")
    @ApiOperation(value = "查看列表", notes = "查看列表")
    public ResponseData<List<WxAccount>> list(){

        List<WxAccount> list = wxAccountService.list();
        return ResponseData.success(list);
    }

    @GetMapping("/info")
    @ApiOperation(value = "通过appid查询信息", notes = "通过appid查询信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "appid", value = "appid", dataType = "String", paramType = "head")
    })
    public ResponseData<WxAccount> info(@RequestHeader String appid){

		WxAccount wxAccount = wxAccountService.getById(appid);
        return ResponseData.success(wxAccount);
    }

    @PostMapping("/save")
    @ApiOperation(value = "保存信息", notes = "保存信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "wxAccount", value = "WxAccount", dataType = "WxAccount", paramType = "body")
    })
    public ResponseData<Boolean> save(@RequestBody WxAccount wxAccount){

        return ResponseData.success(wxAccountService.save(wxAccount));
    }


    @PostMapping("/delete")
    @ApiOperation(value = "删除信息", notes = "删除信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "appids", value = "appids的数组", dataType = "String[]", paramType = "body")
    })
    public ResponseData<Boolean> delete(@RequestBody String[] appids){

        return ResponseData.success(wxAccountService.removeByIds(Arrays.asList(appids)));
    }

}
