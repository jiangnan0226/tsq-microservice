package com.tsq.weixin.provider.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.tsq.web.base.service.BaseService;
import com.tsq.weixin.api.form.WxQrCodeForm;
import com.tsq.weixin.api.model.WxQrCode;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.bean.result.WxMpQrCodeTicket;

import java.util.List;
import java.util.Map;

public interface WxQrCodeService extends BaseService<WxQrCode> {

    /**
     * 创建公众号带参二维码
     *
     * @param appId appId
     * @param form WxQrCodeForm对象
     * @return WxMpQrCodeTicket
     */
    WxMpQrCodeTicket createQrCode(String appId, WxQrCodeForm form)  throws WxErrorException;

    /**
     * 查看列表
     *
     * @param  wxQrCode sceneStr,current,size的wxQrCode
     * @return IPage<WxQrCode>
     */
    IPage<WxQrCode> queryPage( WxQrCode wxQrCode);

    /**
     * 查看信息
     *
     * @param id 主键id
     * @return WxQrCode
     */
    WxQrCode getById(Long id);

    /**
     * 删除信息
     *
     * @param asList 主键id数组
     * @return boolean
     */
    boolean removeByIds(List<Long> asList);
}
