package com.tsq.weixin.provider.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.tsq.weixin.api.model.MsgTemplate;
import me.chanjar.weixin.common.error.WxErrorException;

import java.util.List;

public interface MsgTemplateService {

    /**
    * 查看消息模板列表
    *
    * @param msgTemplate title,name,currentPage,pageSize的MsgTemplate对象
    * @return  IPage<MsgTemplate>
    */
    IPage<MsgTemplate> queryPage(MsgTemplate msgTemplate);

    /**
     * 通过id查看消息模板
     *
     * @param id id主键
     * @return  MsgTemplate
     */
    MsgTemplate getById(Long id);

    /**
     * 通过name查看消息模板
     *
     * @param name 模版名称
     * @return  MsgTemplate
     */
    MsgTemplate selectByName(String name);

    /**
     * 保存消息模板
     *
     * @param msgTemplate MsgTemplate对象
     * @return  boolean
     */
    boolean save(MsgTemplate msgTemplate);

    /**
     * 修改消息模板
     *
     * @param msgTemplate MsgTemplate对象
     * @return  boolean
     */
    boolean updateById(MsgTemplate msgTemplate);

    /**
     * 删除消息模板
     *
     * @param asList id的数组
     * @return  boolean
     */
    boolean removeByIds(List<String> asList);

    /**
     * 同步公众号模板
     *
     * @param appid appid
     * @return  boolean
     */
    boolean syncWxTemplate(String appid) throws WxErrorException;

}
