package com.tsq.weixin.provider.manage;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.tsq.common.model.ResponseData;
import com.tsq.weixin.api.form.WxMsgReplyForm;
import com.tsq.weixin.api.model.WxMsg;
import com.tsq.weixin.provider.service.MsgReplyService;
import com.tsq.weixin.provider.service.WxMsgService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import me.chanjar.weixin.mp.api.WxMpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

@RestController
@RequestMapping("/manage/wxMsg")
@Api(value = "WxMsgManageController", tags = {"微信消息接口"})
public class WxMsgManageController {
    @Autowired
    private WxMsgService wxMsgService;
    @Autowired
    private MsgReplyService msgReplyService;
    @Autowired
    private WxMpService wxMpService;

    @GetMapping("/list")
    @ApiOperation(value = "微信消息列表", notes = "微信消息列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "appid", value = "appid", dataType = "String", paramType = "head"),
            @ApiImplicitParam(name = "wxMsg", value = "msgType,createTime,openid,current,size的wxMsg", dataType = "WxMsg", paramType = "param")
    })
    public ResponseData<IPage<WxMsg>> list(@RequestHeader String appid, WxMsg wxMsg){

        wxMsg.setAppid(appid);
        return ResponseData.success(wxMsgService.queryPage(wxMsg));
    }

    @GetMapping("/info")
    @ApiOperation(value = "通过id查询微信消息", notes = "通过id查询微信消息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "appid", value = "appid", dataType = "String", paramType = "head"),
            @ApiImplicitParam(name = "id", value = "主键id", dataType = "Long", paramType = "param")
    })
    public ResponseData<WxMsg> info(@RequestHeader String appid, @RequestParam Long id){

        WxMsg wxMsg = wxMsgService.getById(id);
        return ResponseData.success(wxMsg);
    }

    @PostMapping("/reply")
    @ApiOperation(value = "回复微信消息", notes = "回复微信消息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "appid", value = "appid", dataType = "String", paramType = "head"),
            @ApiImplicitParam(name = "form", value = "WxMsgReplyForm实体类", dataType = "WxMsgReplyForm", paramType = "body")
    })
    public ResponseData<Void> reply(@RequestHeader String appid, @RequestBody WxMsgReplyForm form){

        msgReplyService.reply(form.getOpenid(),form.getReplyType(),form.getReplyContent());
        return ResponseData.success();
    }

    @PostMapping("/delete")
    @ApiOperation(value = "删除微信消息", notes = "删除微信消息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "appid", value = "appid", dataType = "String", paramType = "head"),
            @ApiImplicitParam(name = "ids", value = "主键id数组", dataType = "Long[]", paramType = "body")
    })
    public ResponseData<Boolean> delete(@RequestHeader String appid, @RequestBody Long[] ids){

        return ResponseData.success(wxMsgService.removeByIds(Arrays.asList(ids)));
    }
}
