package com.tsq.weixin.provider.manage;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.tsq.common.model.ResponseData;
import com.tsq.weixin.api.model.Article;
import com.tsq.weixin.provider.service.ArticleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

@RestController
@RequestMapping("/manage/article")
@Api(value = "ArticleManageController", tags = {"cms文章操作接口"})
public class ArticleManageController {
    @Autowired
    private ArticleService articleService;

    @GetMapping("/list")
    @ApiOperation(value = "查看列表", notes = "查看列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "article", value = "字段为title,type,category,subCategory,current,size的article", dataType = "Article", paramType = "query")
    })
    public ResponseData<IPage<Article>> list( Article article) {

        IPage<Article> articleList = articleService.queryPage(article);
        return ResponseData.success(articleList);
    }

    @GetMapping("/info")
    @ApiOperation(value = "通过id查询信息", notes = "通过id查询信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "id主键", dataType = "Integer", paramType = "param")
    })
    public ResponseData<Article> info(@RequestParam("id") Integer id) {

        Article article = articleService.findById(id);
        return ResponseData.success(article);
    }

    @PostMapping("/save")
    @ApiOperation(value = "保存信息", notes = "保存信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "article", value = "Article", dataType = "Article", paramType = "body")
    })
    public ResponseData<Boolean> save(@RequestBody Article article) {
        return ResponseData.success(articleService.save(article));
    }

    @PostMapping("/delete")
    @ApiOperation(value = "删除信息", notes = "删除信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "id的数组", dataType = "Integer[]", paramType = "body")
    })
    public ResponseData<Boolean> delete(@RequestBody Integer[] ids) {
        return ResponseData.success(articleService.removeByIds(Arrays.asList(ids)));
    }
}
