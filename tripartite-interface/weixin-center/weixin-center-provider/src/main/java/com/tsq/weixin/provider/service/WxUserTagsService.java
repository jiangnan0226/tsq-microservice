package com.tsq.weixin.provider.service;

import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.bean.tag.WxUserTag;

import java.util.List;

public interface WxUserTagsService{
    /**
     * 获取公众号用户标签
     * @return List<WxUserTag>
     * @throws WxErrorException WxErrorException
     */
    List<WxUserTag> getWxTags() throws WxErrorException;

    /**
     * 创建标签
     * @param name 标签名称
     * @return boolean
     * @throws WxErrorException WxErrorException
     */
    boolean creatTag(String name) throws WxErrorException;

    /**
     * 修改标签
     * @param tagId 标签ID
     * @param name 标签名称
     * @return boolean
     * @throws WxErrorException WxErrorException
     */
    boolean updateTag(Long tagId, String name) throws WxErrorException;

    /**
     * 删除标签
     * @param tagid 标签ID
     * @return boolean
     * @throws WxErrorException WxErrorException
     */
    boolean deleteTag(Long tagid) throws WxErrorException;

    /**
     * 批量给用户打标签
     * @param tagId 标签ID
     * @param openidList 用户openid列表
     * @return boolean
     * @throws WxErrorException WxErrorException
     */
    boolean batchTagging(Long tagId, String[] openidList) throws WxErrorException;

    /**
     * 批量取消用户标签
     * @param tagId 标签ID
     * @param openidList 用户openid列表
     * @return boolean
     * @throws WxErrorException WxErrorException
     */
    boolean batchUnTagging(Long tagId, String[] openidList) throws WxErrorException;

    /**
     * 为用户绑定标签
     * @param tagId 标签ID
     * @param openid 用户openid
     * @return boolean
     * @throws WxErrorException WxErrorException
     */
    boolean tagging(Long tagId, String openid) throws WxErrorException;

    /**
     * 取消用户所绑定的某一个标签
     * @param tagId 标签ID
     * @param openid 用户openid
     * @return boolean
     * @throws WxErrorException WxErrorException
     */
    boolean untagging(Long tagId, String openid) throws WxErrorException;
}
