package com.tsq.weixin.provider.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tsq.web.base.service.impl.BaseServiceImpl;
import com.tsq.weixin.api.model.TemplateMsgLog;
import com.tsq.weixin.provider.mapper.TemplateMsgLogMapper;
import com.tsq.weixin.provider.service.TemplateMsgLogService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

@Service
public class  TemplateMsgLogImpl extends BaseServiceImpl<TemplateMsgLogMapper,TemplateMsgLog> implements TemplateMsgLogService{

    @Override
    public IPage<TemplateMsgLog> queryPage(TemplateMsgLog templateMsgLog) {
        String appid = templateMsgLog.getAppid();
        long currentPage = templateMsgLog.getCurrent();
        long pageSize = templateMsgLog.getSize();

        return this.baseMapper.selectPage(new Page<>(currentPage, pageSize),
                new QueryWrapper<TemplateMsgLog>()
                        .eq(!StringUtils.isEmpty(appid),"appid", appid));
    }

    @Override
    public void addLog(TemplateMsgLog log) {
        this.baseMapper.insert(log);
    }

}
