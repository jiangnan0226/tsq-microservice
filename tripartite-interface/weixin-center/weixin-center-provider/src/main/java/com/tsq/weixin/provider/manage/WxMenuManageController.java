package com.tsq.weixin.provider.manage;

import com.tsq.common.model.ResponseData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import me.chanjar.weixin.common.bean.menu.WxMenu;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.menu.WxMpMenu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 微信公众号菜单管理
 * 官方文档：https://developers.weixin.qq.com/doc/offiaccount/Custom_Menus/Creating_Custom-Defined_Menu.html
 * WxJava开发文档：https://github.com/Wechat-Group/WxJava/wiki/MP_自定义菜单管理
 */
@RestController
@RequestMapping("/manage/wxMenu")
@RequiredArgsConstructor
@Api(value = "WxMenuManageController", tags = {"微信公众号菜单管理接口"})
public class WxMenuManageController {
    private final WxMpService wxService;
    @Autowired
    private WxMpService wxMpService;

    @GetMapping("/getMenu")
    @ApiOperation(value = "获取公众号菜单", notes = "获取公众号菜单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "appid", value = "appid", dataType = "String", paramType = "head")
    })
    public ResponseData<WxMpMenu> getMenu(@RequestHeader String appid) throws WxErrorException {

        wxMpService.switchoverTo(appid);
        WxMpMenu wxMpMenu = wxService.getMenuService().menuGet();
        return ResponseData.success(wxMpMenu);
    }

    @PostMapping("/updateMenu")
    @ApiOperation(value = "创建、更新菜单", notes = "创建、更新菜单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "appid", value = "appid", dataType = "String", paramType = "head"),
            @ApiImplicitParam(name = "wxMenu", value = "WxMenu对象", dataType = "WxMenu", paramType = "body")
    })
    public ResponseData<Boolean> updateMenu(@RequestHeader String appid,@RequestBody WxMenu wxMenu) throws WxErrorException {

        wxMpService.switchoverTo(appid);
        wxService.getMenuService().menuCreate(wxMenu);
        return ResponseData.success();
    }
}
