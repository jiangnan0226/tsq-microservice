package com.tsq.weixin.provider.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.tsq.web.base.service.BaseService;
import com.tsq.weixin.api.model.WxMsg;

import java.util.List;

public interface WxMsgService extends BaseService<WxMsg> {

    /**
     * 查看微信消息
     *
     * @param wxMsg msgType,createTime,openid,appId的wxMsg
     * @return  IPage<WxMsg>
     */
    IPage<WxMsg> queryPage(WxMsg wxMsg);

    /**
     * 通过id查询微信消息
     *
     * @param id 主键id
     * @return  WxMsg
     */
    WxMsg getById(Long id);

    /**
     * 通过id删除微信消息
     *
     * @param asList 主键id集合
     * @return  boolean
     */
    boolean removeByIds(List<Long> asList);

    /**
     * 保存消息到数据库（会先添加到队列，再使用定时任务写入）
     *
     * @param buildOutMsg WxMsg
     */
    void addWxMsg(WxMsg buildOutMsg);
}
