package com.tsq.weixin.provider.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.tsq.web.base.service.BaseService;
import com.tsq.weixin.api.model.WxAccount;

import java.io.Serializable;
import java.util.Collection;

public interface WxAccountService extends BaseService<WxAccount> {
    /**
     * 分页查询用户数据
     * @param wxAccount 查询参数
     * @return IPage<WxAccount> 分页结果
     */
    IPage<WxAccount> queryPage(WxAccount wxAccount);

    /**
     * 保存公众号账号
     *
     * @param entity WxAccount对象
     * @return  boolean
     */
    boolean save(WxAccount entity);

    /**
     * 删除公众号账号
     *
     * @param idList id的数组
     * @return  boolean
     */
    boolean removeByIds(Collection<? extends Serializable> idList);
}
