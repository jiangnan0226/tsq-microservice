package com.tsq.weixin.provider;

import org.minbox.framework.api.boot.autoconfigure.swagger.annotation.EnableApiBootSwagger;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication(scanBasePackages = "com.tsq")
@EnableDiscoveryClient
@EnableApiBootSwagger
@EnableCaching
@MapperScan(basePackages = {"com.tsq.weixin.provider.mapper"})
public class WeixinCenterProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(WeixinCenterProviderApplication.class, args);
    }
}
