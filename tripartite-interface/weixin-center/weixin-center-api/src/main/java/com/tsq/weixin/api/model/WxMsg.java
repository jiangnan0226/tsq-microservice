package com.tsq.weixin.api.model;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.FastjsonTypeHandler;
import com.tsq.db.base.model.PageModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.util.WxMpConfigStorageHolder;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName(value = "wx_msg",autoResultMap = true)
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value = "Article对象", description = "Article对象")
public class WxMsg extends PageModel implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    @TableId(type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "appid")
    private String appid;

    @ApiModelProperty(value = "微信用户ID")
    private String openid;

    @ApiModelProperty(value = "微信号")
    @TableField(exist = false)
    private String appUsername;

    @ApiModelProperty(value = "消息方向")
    private byte inOut;

    @ApiModelProperty(value = "消息类型")
    private String msgType;

    @ApiModelProperty(value = "消息详情")
    @TableField(value = "detail",typeHandler = FastjsonTypeHandler.class)
    private JSONObject detail;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    public static class WxMsgInOut{
        static final byte IN=0;
        static final byte OUT=1;
    }

    public WxMsg() {
    }
    public WxMsg(WxMpXmlMessage wxMessage) {
        this.openid=wxMessage.getFromUser();
        this.appid= WxMpConfigStorageHolder.get();
        this.inOut = WxMsgInOut.IN;
        this.msgType = wxMessage.getMsgType();
        this.detail = new JSONObject();
        Long createTime = wxMessage.getCreateTime();
        this.createTime = createTime==null?new Date():new Date(createTime*1000);
        if(WxConsts.XmlMsgType.TEXT.equals(this.msgType)){
            this.detail.put("content",wxMessage.getContent());
        }else if(WxConsts.XmlMsgType.IMAGE.equals(this.msgType)){
            this.detail.put("picUrl",wxMessage.getPicUrl());
            this.detail.put("mediaId",wxMessage.getMediaId());
        }else if(WxConsts.XmlMsgType.VOICE.equals(this.msgType)){
            this.detail.put("format",wxMessage.getFormat());
            this.detail.put("mediaId",wxMessage.getMediaId());
        }else if(WxConsts.XmlMsgType.VIDEO.equals(this.msgType) ||
                WxConsts.XmlMsgType.SHORTVIDEO.equals(this.msgType)){
            this.detail.put("thumbMediaId",wxMessage.getThumbMediaId());
            this.detail.put("mediaId",wxMessage.getMediaId());
        }else if(WxConsts.XmlMsgType.LOCATION.equals(this.msgType)){
            this.detail.put("locationX",wxMessage.getLocationX());
            this.detail.put("locationY",wxMessage.getLocationY());
            this.detail.put("scale",wxMessage.getScale());
            this.detail.put("label",wxMessage.getLabel());
        }else if(WxConsts.XmlMsgType.LINK.equals(this.msgType)){
            this.detail.put("title",wxMessage.getTitle());
            this.detail.put("description",wxMessage.getDescription());
            this.detail.put("url",wxMessage.getUrl());
        }else if(WxConsts.XmlMsgType.EVENT.equals(this.msgType)){
            this.detail.put("event",wxMessage.getEvent());
            this.detail.put("eventKey",wxMessage.getEventKey());
        }
    }
    public static WxMsg buildOutMsg(String msgType,String openid,JSONObject detail){
        WxMsg wxMsg = new WxMsg();
        wxMsg.appid= WxMpConfigStorageHolder.get();
        wxMsg.msgType = msgType;
        wxMsg.openid = openid;
        wxMsg.detail = detail;
        wxMsg.createTime=new Date();
        wxMsg.inOut = WxMsgInOut.OUT;
        return wxMsg;
    }
}
