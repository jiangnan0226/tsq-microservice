package com.tsq.weixin.api.model;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.tsq.db.base.model.PageModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import me.chanjar.weixin.mp.bean.template.WxMpTemplate;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName(value = "wx_msg_template")
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value = "MsgTemplate对象", description = "MsgTemplate对象")
public class MsgTemplate extends PageModel implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    @TableId(type = IdType.AUTO)
    private long id;

    @ApiModelProperty(value = "appid")
    private String appid;

    @ApiModelProperty(value = "公众号模板ID")
    private String templateId;

    @ApiModelProperty(value = "模版名称")
    @TableField(value = "`name`")
    private String name;

    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "模板内容")
    private String content;

    @ApiModelProperty(value = "消息内容")
    private JSONArray data;

    @ApiModelProperty(value = "链接")
    private String url;

    @ApiModelProperty(value = "小程序信息")
    @TableField(value = "`miniprogram`")
    private JSONObject miniProgram;

    @ApiModelProperty(value = "是否有效")
    @TableField(value = "`status`")
    private boolean status;

    @ApiModelProperty(value = "修改时间")
    private Date updateTime;

    public MsgTemplate(){

    }

    public MsgTemplate(WxMpTemplate mpTemplate, String appid) {
        this.appid = appid;
        this.templateId=mpTemplate.getTemplateId();
        this.title=mpTemplate.getTitle();
        this.name=mpTemplate.getTemplateId();
        this.content = mpTemplate.getContent();
        this.status=true;
    }

}

