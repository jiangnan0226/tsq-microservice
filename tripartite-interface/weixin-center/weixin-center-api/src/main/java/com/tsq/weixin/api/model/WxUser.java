package com.tsq.weixin.api.model;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.FastjsonTypeHandler;
import com.tsq.db.base.model.PageModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName(value = "wx_user",autoResultMap = true)
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value = "WxUser对象", description = "WxUser对象")
public class WxUser extends PageModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "微信openid")
    @TableId(type = IdType.INPUT)
    private String openid;

    @ApiModelProperty(value = "appid")
    private String appid;

    @ApiModelProperty(value = "手机号")
    private String phone;

    @ApiModelProperty(value = "昵称")
    private String nickname;

    @ApiModelProperty(value = "性别(0-未知、1-男、2-女)")
    private int sex;

    @ApiModelProperty(value = "城市")
    private String city;

    @ApiModelProperty(value = "省份")
    private String province;

    @ApiModelProperty(value = "头像")
    @TableField(value = "headimgurl")
    private String headImgUrl;

    @ApiModelProperty(value = "订阅时间")
    @JSONField(name = "subscribe_time")
    private Date subscribeTime;

    @ApiModelProperty(value = "是否关注")
    private boolean subscribe;

    @ApiModelProperty(value = "unionId")
    @TableField(value = "unionid")
    private String unionId;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "标签ID列表")
    @TableField(value = "tagid_list",typeHandler = FastjsonTypeHandler.class)
    private JSONArray tagidList;

    @ApiModelProperty(value = "关注场景")
    private String subscribeScene;

    @ApiModelProperty(value = "扫码场景值")
    private String qrSceneStr;

    public WxUser() {
    }

    public WxUser(String openid) {
        this.openid = openid;
    }

    public WxUser(WxMpUser wxMpUser,String appid) {
        this.openid = wxMpUser.getOpenId();
        this.appid = appid;
        this.subscribe=wxMpUser.getSubscribe();
        if(wxMpUser.getSubscribe()){
            this.nickname = wxMpUser.getNickname();
            this.sex = wxMpUser.getSex();
            this.city = wxMpUser.getCity();
            this.province = wxMpUser.getProvince();
            this.headImgUrl = wxMpUser.getHeadImgUrl();
            this.subscribeTime = new Date(wxMpUser.getSubscribeTime()*1000);
            this.unionId=wxMpUser.getUnionId();
            this.remark=wxMpUser.getRemark();
            this.tagidList= JSONArray.parseArray(JSONObject.toJSONString(wxMpUser.getTagIds()));
            this.subscribeScene=wxMpUser.getSubscribeScene();
            String qrScene =  wxMpUser.getQrScene();
            this.qrSceneStr= StringUtils.isEmpty(qrScene) ? wxMpUser.getQrSceneStr() : qrScene;
        }
    }
}
