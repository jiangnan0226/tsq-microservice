package com.tsq.weixin.api.dto;

import lombok.Data;

import java.util.Date;

@Data
public class WxUserDTO {

    /**
     * 微信openid
     */
    private String openid;
    /**
     * appid
     */
    private String appid;
    /**
     * 手机号
     */
    private String phone;
    /**
     * 昵称
     */
    private String nickname;
    /**
     * 性别(0-未知、1-男、2-女)
     */
    private int sex;
    /**
     * 城市
     */
    private String city;
    /**
     * 省份
     */
    private String province;
    /**
     * 头像
     */
    private String headImgUrl;
    /**
     * 订阅时间
     */
    private Date subscribeTime;
    /**
     * 是否关注
     */
    private boolean subscribe;
    /**
     * unionId
     */
    private String unionId;
    /**
     * 备注
     */
    private String remark;
    /**
     * 标签ID列表
     */
    private String tagIdList;
    /**
     * 关注场景
     */
    private String subscribeScene;
    /**
     * 扫码场景值
     */
    private String qrSceneStr;

}

