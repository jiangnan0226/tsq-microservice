package com.tsq.weixin.api.dto;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.Data;

import java.util.Date;

@Data
public class TemplateMsgLogDTO {

    /**
    * 主键id
    */
    private int logId;
    /**
     * appId
     */
    private String appId;
    /**
     * 用户openid
     */
    private String touser;
    /**
     * templateid
     */
    private String templateId;
    /**
     * 消息数据
     */
    private JSONArray data;
    /**
     * 消息链接
     */
    private String url;
    /**
     * 小程序信息
     */
    private JSONObject miniprogram;
    /**
     * 发送时间
     */
    private Date sendTime;
    /**
     * 发送结果
     */
    private String sendResult;
}
