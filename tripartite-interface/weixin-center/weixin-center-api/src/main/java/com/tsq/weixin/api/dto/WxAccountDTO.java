package com.tsq.weixin.api.dto;

import lombok.Data;

@Data
public class WxAccountDTO{

	/**
	 * id
	 */
	private String appid;
	/**
	 * 公众号名称
	 */
	private String name;
	/**
	 * 账号类型
	 */
	private int type;
	/**
	 * 认证状态
	 */
	private boolean verified;
	/**
	 * appsecret
	 */
	private String secret;
	/**
	 * token
	 */
	private String token;
	/**
	 * aesKey
	 */
	private String aesKey;
}
