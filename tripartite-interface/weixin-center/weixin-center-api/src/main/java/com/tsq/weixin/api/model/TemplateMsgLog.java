package com.tsq.weixin.api.model;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.tsq.db.base.model.PageModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateMessage;

import java.io.Serializable;
import java.util.Date;

/**
 * 模板消息日志
 * @author Nifury
 */
@Data
@TableName("wx_template_msg_log")
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value = "Article对象", description = "Article对象")
public class TemplateMsgLog extends PageModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "主键id")
    private int logId;

    @ApiModelProperty(value = "appid")
    private String appid;

    @ApiModelProperty(value = "用户openid")
    private String touser;

    @ApiModelProperty(value = "templateId")
    private String templateId;

    @ApiModelProperty(value = "消息数据")
    private JSONArray data;

    @ApiModelProperty(value = "消息链接")
    private String url;

    @ApiModelProperty(value = "小程序信息")
    private JSONObject miniprogram;

    @ApiModelProperty(value = "发送时间")
    private Date sendTime;

    @ApiModelProperty(value = "发送结果")
    private String sendResult;

    public TemplateMsgLog() {
    }

    public TemplateMsgLog(WxMpTemplateMessage msg, String appid, String sendResult) {
        this.appid = appid;
        this.touser = msg.getToUser();
        this.templateId = msg.getTemplateId();
        this.url = msg.getUrl();
        this.miniprogram = JSONObject.parseObject(JSON.toJSONString(msg.getMiniProgram()));
        this.data = JSONArray.parseArray(JSON.toJSONString(msg.getData()));
        this.sendTime = new Date();
        this.sendResult = sendResult;
    }

}

