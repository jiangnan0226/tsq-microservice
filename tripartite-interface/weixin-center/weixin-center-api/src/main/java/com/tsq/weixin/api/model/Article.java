package com.tsq.weixin.api.model;

import com.baomidou.mybatisplus.annotation.*;
import com.tsq.db.base.model.PageModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = true)
@TableName("wx_article")
@Accessors(chain = true)
@ApiModel(value = "Article对象", description = "Article对象")
public class Article extends PageModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    @TableId(type = IdType.AUTO)
    private int id;

    @ApiModelProperty(value = "文章类型")
    private int type;

    @TableField(insertStrategy = FieldStrategy.IGNORED)//title重复则不插入
    @NotEmpty(message = "标题不得为空")
    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "文章摘要")
    private String tags;

    @ApiModelProperty(value = "文章标签")
    private String summary;

    @ApiModelProperty(value = "内容")
    private String content;

    @ApiModelProperty(value = "分类")
    private String category;

    @ApiModelProperty(value = "二级目录")
    private String subCategory;

    @ApiModelProperty(value = "点击次数")
    private int openCount;

    @ApiModelProperty(value = "指向外链")
    private String targetLink;

    @ApiModelProperty(value = "文章首图")
    private String image;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "修改时间")
    private Date updateTime;
}
