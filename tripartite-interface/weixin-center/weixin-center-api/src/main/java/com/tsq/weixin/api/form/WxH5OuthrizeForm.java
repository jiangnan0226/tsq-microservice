package com.tsq.weixin.api.form;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class WxH5OuthrizeForm {
    @NotEmpty(message = "code不得为空")
    private String code;
}
