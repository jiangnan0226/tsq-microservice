package com.tsq.weixin.api.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.tsq.db.base.model.PageModel;
import com.tsq.weixin.api.form.WxQrCodeForm;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("wx_qr_code")
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value = "WxQrCode对象", description = "WxQrCode对象")
public class WxQrCode extends PageModel implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键id")
    @TableId(type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "appid")
    private String appid;

    @ApiModelProperty(value = "二维码类型")
    private Boolean isTemp;

    @ApiModelProperty(value = "场景值ID")
    private String sceneStr;

    @ApiModelProperty(value = "二维码ticket")
    private String ticket;

    @ApiModelProperty(value = "二维码图片解析后的地址")
    private String url;

    @ApiModelProperty(value = "该二维码失效时间")
    private Date expireTime;

    @ApiModelProperty(value = "该二维码创建时间")
    private Date createTime;

    public WxQrCode() {

    }

    public WxQrCode(WxQrCodeForm form) {
        this.isTemp = form.getIsTemp();
        this.sceneStr = form.getSceneStr();
        this.createTime = new Date();
    }

}
