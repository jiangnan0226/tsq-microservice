package com.tsq.weixin.api.dto;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

import java.util.Date;
@Data
public class WxMsgDTO {

    /**
     * 主键
     */
    private Long id;
    /**
     * appid
     */
    private String appid;
    /**
     * 微信用户ID
     */
    private String openid;
    /**
     * 消息方向
     */
    private byte inOut;
    /**
     * 消息类型
     */
    private String msgType;
    /**
     * 消息详情
     */
    private JSONObject detail;
    /**
     * 创建时间
     */
    private Date createTime;
}
