package com.tsq.weixin.api.dto;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.Data;

import java.util.Date;

@Data
public class MsgTemplateDTO {

    /**
    * 主键id
    */
    private long id;
    /**
     * appid
     */
    private long appid;
    /**
     * 公众号模板ID
     */
    private String templateId;
    /**
     * 模版名称
     */
    private String name;
    /**
     * 标题
     */
    private String title;
    /**
     * 模板内容
     */
    private String content;
    /**
     * 消息内容
     */
    private JSONArray data;
    /**
     * 链接
     */
    private String url;
    /**
     * 小程序信息
     */
    private JSONObject miniProgram;
    /**
     * 是否有效
     */
    private boolean status;
    /**
     * 修改时间
     */
    private Date updateTime;
}
