package com.tsq.weixin.api.dto;


import lombok.Data;

import java.util.Date;

@Data
public class ArticleDTO  {

    /**
     * id
     */
    private int id;
    /**
     * 文章类型
     */
    private int type;
    /**
     * 标题
     */
    private String title;
    /**
     * 文章摘要
     */
    private String tags;
    /**
     * 文章标签
     */
    private String summary;
    /**
     * 内容
     */
    private String content;
    /**
     * 分类
     */
    private String category;
    /**
     * 二级目录
     */
    private String subCategory;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    private Date updateTime;
    /**
     * 点击次数
     */
    private int openCount;
    /**
     * 指向外链
     */
    private String targetLink;
    /**
     * 文章首图
     */
    private String image;
}
