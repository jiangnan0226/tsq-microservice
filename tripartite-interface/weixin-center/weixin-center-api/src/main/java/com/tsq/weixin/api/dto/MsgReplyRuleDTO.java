package com.tsq.weixin.api.dto;

import lombok.Data;

import java.sql.Time;
import java.util.Date;

@Data
public class MsgReplyRuleDTO {

    /**
     * rule_id
     */
    private int ruleId;
    /**
     * appid
     */
    private String appid;
    /**
     * 规则名称
     */
    private String ruleName;
    /**
     * 匹配的关键词、事件等
     */
    private String matchValue;
    /**
     * 是否精确匹配
     */
    private boolean exactMatch;
    /**
     * 回复消息类型
     */
    private String replyType;
    /**
     * 回复消息内容
     */
    private String replyContent;
    /**
     * 规则是否有效
     */
    private boolean status;
    /**
     * 备注说明
     */
    private String desc;
    /**
     * 生效起始时间
     */
    private Time effectTimeStart;
    /**
     * 生效结束时间
     */
    private Time effectTimeEnd;
    /**
     * 规则优先级
     */
    private int priority;
    /**
     * 修改时间
     */
    private Date updateTime;

}
