package com.tsq.weixin.api.form;

import com.alibaba.fastjson.JSON;
import lombok.Data;

@Data
public class AccountBindForm {
    String phoneNum;
    String idCodeSuffix;

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
