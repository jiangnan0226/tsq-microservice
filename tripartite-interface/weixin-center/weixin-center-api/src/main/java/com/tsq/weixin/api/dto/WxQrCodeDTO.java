package com.tsq.weixin.api.dto;

import lombok.Data;

import java.util.Date;

@Data
public class WxQrCodeDTO {

    /**
     * ID
     */
    private Long id;
    /**
     * appid
     */
    private String appid;
    /**
     * 二维码类型,是否为临时二维码
     */
    private Boolean isTemp;
    /**
     * 场景值ID
     */
    private String sceneStr;
    /**
     * 二维码ticket
     */
    private String ticket;
    /**
     * 二维码图片解析后的地址
     */
    private String url;
    /**
     * 该二维码失效时间
     */
    private Date expireTime;
    /**
     * 该二维码创建时间
     */
    private Date createTime;

}
