package com.tsq.weixin.api.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.tsq.db.base.model.PageModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

@Data
@TableName("wx_msg_reply_rule")
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value = "MsgReplyRule对象", description = "MsgReplyRule对象")
public class MsgReplyRule extends PageModel implements Serializable{
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "appid")
    private String appid;

    @ApiModelProperty(value = "主键id")
    @TableId(type = IdType.AUTO)
    private int ruleId;

    @ApiModelProperty(value = "规则名称")
    @NotEmpty(message = "规则名称不得为空")
    private String ruleName;

    @ApiModelProperty(value = "匹配的关键词、事件等")
    @NotEmpty(message = "匹配关键词不得为空")
    private String matchValue;

    @ApiModelProperty(value = "是否精确匹配")
    private boolean exactMatch;

    @ApiModelProperty(value = "回复消息类型")
    private String replyType;

    @NotEmpty(message = "回复内容不得为空")
    @ApiModelProperty(value = "回复消息内容")
    private String replyContent;

    @ApiModelProperty(value = "规则是否有效")
    @TableField(value = "`status`")
    private boolean status;

    @ApiModelProperty(value = "备注说明")
    @TableField(value = "`desc`")
    private String desc;

    @ApiModelProperty(value = "生效起始时间")
    private Time effectTimeStart;

    @ApiModelProperty(value = "生效结束时间")
    private Time effectTimeEnd;

    @ApiModelProperty(value = "规则优先级")
    private int priority;

    @ApiModelProperty(value = "修改时间")
    private Date updateTime;

}
