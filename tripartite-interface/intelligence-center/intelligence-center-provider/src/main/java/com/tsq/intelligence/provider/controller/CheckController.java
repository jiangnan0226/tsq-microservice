package com.tsq.intelligence.provider.controller;

import com.tsq.common.model.ResponseData;
import com.tsq.intelligence.api.vo.*;
import com.tsq.intelligence.provider.config.Config;
import com.tsq.intelligence.provider.service.ICheckService;
import com.tsq.intelligence.provider.utils.Constant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.ParseException;

@RequestMapping(value = "check")
@RestController("check")
@Api(value = "CheckController", tags = {"手写文字识别"})
public class CheckController {

    @Autowired
    private ICheckService checkService;

    @ApiOperation(value = "图像识别", notes = "图像识别")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "图片文件", value = "图片文件", dataType = "file", paramType = "query", required = true)
    })
    @PostMapping(value = "/currencyCheck")
    public ResponseData<CurrencyCheckVO> currencyCheck(@RequestParam("file") MultipartFile file) throws IOException, ParseException {
        CurrencyCheckVO currencyCheck= checkService.currencyCheck(file);
        if(!Constant.SUCCESS.equals(currencyCheck.getCode())){
            return ResponseData.error(currencyCheck.getDesc());
        }
        return ResponseData.success(currencyCheck);
    }

    @ApiOperation(value = "场景识别", notes = "场景识别")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "图片文件", value = "图片文件", dataType = "file", paramType = "query", required = true)
    })
    @PostMapping(value = "/imageCheck")
    public ResponseData<SceneCheckVO> imageCheck(@RequestParam("file") MultipartFile file) throws IOException, ParseException {
        SceneCheckVO sceneCheckVO = checkService.imageCheck(file);
        if(!Constant.SUCCESS.equals(sceneCheckVO.getCode())){
            return ResponseData.error(sceneCheckVO.getDesc());
        }
        return ResponseData.success(sceneCheckVO);
    }

    @ApiOperation(value = "色情内容过滤", notes = "色情内容过滤")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "图片文件", value = "图片文件", dataType = "file", paramType = "query", required = true)
    })
    @PostMapping(value = "/pornCheck")
    public ResponseData<PornCheckVO> pornCheck(@RequestParam("file") MultipartFile file) throws IOException, ParseException {
        PornCheckVO pornCheckVO = checkService.pornCheck(file);
        if(!Constant.SUCCESS.equals(pornCheckVO.getCode())){
            return ResponseData.error(pornCheckVO.getDesc());
        }
        return ResponseData.success(pornCheckVO);
    }

    @ApiOperation(value = "政治人物检查", notes = "政治人物检查")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "图片文件", value = "图片文件", dataType = "file", paramType = "query", required = true)
    })
    @PostMapping(value = "/politicianCheck")
    public ResponseData<PoliticianCheckVO> politicianCheck(@RequestParam("file") MultipartFile file) throws IOException, ParseException {
        PoliticianCheckVO politicianCheckVO = checkService.politicianCheck(file);
        if(!Constant.SUCCESS.equals(politicianCheckVO.getCode())){
            return ResponseData.error(politicianCheckVO.getDesc());
        }
        return ResponseData.success(politicianCheckVO);
    }

    @ApiOperation(value = "暴恐敏感信息过滤", notes = "暴恐敏感信息过滤")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "图片文件", value = "图片文件", dataType = "file", paramType = "query", required = true)
    })
    @PostMapping(value = "/sensitiveCheck")
    public ResponseData<SensitiveCheckVO> sensitiveCheck(@RequestParam("file") MultipartFile file) throws IOException, ParseException {
        SensitiveCheckVO sensitiveCheckVO = checkService.sensitiveCheck(file);
        if(!Constant.SUCCESS.equals(sensitiveCheckVO.getCode())){
            return ResponseData.error(sensitiveCheckVO.getDesc());
        }
        return ResponseData.success(sensitiveCheckVO);
    }

    @ApiOperation(value = "广告内容过滤", notes = "广告内容过滤")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "图片文件", value = "图片文件", dataType = "file", paramType = "query", required = true)
    })
    @PostMapping(value = "/advertisingCheck")
    public ResponseData<AdvertisingCheckVO> advertisingCheck(@RequestParam("file") MultipartFile file) throws IOException, ParseException {
        AdvertisingCheckVO advertisingCheckVO = checkService.advertisingCheck(file);
        if(!Constant.SUCCESS.equals(advertisingCheckVO.getCode())){
            return ResponseData.error(advertisingCheckVO.getDesc());
        }
        return ResponseData.success(advertisingCheckVO);
    }

}
