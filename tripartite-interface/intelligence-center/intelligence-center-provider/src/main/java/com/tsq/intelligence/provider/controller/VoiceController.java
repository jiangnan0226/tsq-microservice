package com.tsq.intelligence.provider.controller;

import cn.hutool.core.convert.Convert;
import com.tsq.common.model.ResponseData;
import com.tsq.intelligence.api.dto.HandWritingDTO;
import com.tsq.intelligence.api.dto.VoiceDTO;
import com.tsq.intelligence.api.vo.HandWritingVO;
import com.tsq.intelligence.api.vo.VoiceVO;
import com.tsq.intelligence.provider.config.Config;
import com.tsq.intelligence.provider.service.IVoiceService;
import com.tsq.intelligence.provider.service.impl.VoiceServiceImpl;
import com.tsq.intelligence.provider.utils.Constant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.ParseException;

@RequestMapping(value = "voice")
@RestController("voice")
@Api(value = "VoiceController", tags = {"语音识别"})
public class VoiceController {

    @Autowired
    private IVoiceService voiceService;

    @ApiOperation(value = "语音识别", notes = "语音识别")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "语音识别", value = "语音识别", dataType = "map", paramType = "query", required = true)
    })
    @PostMapping(value = "/transferVoiceToTextDelay")
    @ResponseBody
    public ResponseData<String> transferVoiceToTextDelay(VoiceDTO voiceDTO,@RequestParam("file") MultipartFile file) throws IOException, ParseException {
        VoiceVO voiceVO= voiceService.transferVoiceToTextDelay(voiceDTO,file);
        if(voiceVO.getOk() != Convert.toInt(Constant.SUCCESS)){
            return ResponseData.error(voiceVO.getFailed());
        }
        return ResponseData.success(voiceVO.getContent());
    }

}
