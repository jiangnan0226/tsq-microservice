package com.tsq.intelligence.provider.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.tsq.common.model.ResponseData;
import com.tsq.intelligence.api.vo.ai.*;
import com.tsq.intelligence.provider.utils.AiUtil;
import com.tsq.intelligence.provider.utils.MusicUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.encoder.org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RequestMapping(value = "search")
@RestController("search")
@Api(value = "SearchController", tags = {"搜索场景"})
public class SearchController {

    @ApiOperation(value = "AI技能", notes = "AI技能")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "AI技能", value = "AI技能", dataType = "search", paramType = "query", required = true)
    })
    @PostMapping(value = "/typeSearch")
    public ResponseData<Object> typeSearch(@RequestParam("search") String search){
        JSONObject iSearch= AiUtil.aiFunction(search);
        JSONObject intentJson = JSONObject.parseObject(iSearch.getJSONArray("data").getJSONObject(0).get("intent").toString());
        String service = intentJson.getString("service");
        ResponseData<Object> req=null;
        switch (service){
            //分类1
            case "mapU"://地图导航
                req = mapU(iSearch);
                break;
            //分类2
            case "iFlytekQA":
                req = iFlytekKnowledgeMap(iSearch);
                break;
            case "EGO.healthKnowledge"://健康知识
                req = healthKnowledge(iSearch);
                break;
            case "RUYIAI.RuyiIdiomGame"://如意成语接龙
                req = ruyiIdiomGame(iSearch);
                break;
            case "FITME.idiom"://方得查成语
                req = idiom(iSearch);
                break;
            case "AIUI.idiomSolitaire"://成语接龙
                req = idiomSolitaire(iSearch);
                break;
            case "RUYIAI.RuyiDatetime"://如意时间询问
                req = ruyiDatetime(iSearch);
                break;
            case "datetimeX"://时间日期查询
                req = datetimeX(iSearch);
                break;
            case "RUYIAI.RuyiPoetry"://如意古诗词
                req = ruyiPoetry(iSearch);
                break;
            case "RUYIAI.RuyiPoem"://如意诗词大会
                req = ruyiPoem(iSearch);
                break;
            case "RUYIAI.poetryChallenge"://诗词挑战
                req = poetryChallenge(iSearch);
                break;
            case "RUYIAI.poetry"://诗词对答
                req = poetry(iSearch);
                break;

            case "AIUI.guessNumber"://猜数字游戏
                req = guessNumber(iSearch);
                break;
            case "KLLI3.skipNumber"://跳数字游戏
                req = skipNumber(iSearch);
                break;
            case "LEIQIAO.moraGame"://剪刀石头布游戏
                req = moraGame(iSearch);
                break;
            case "RUYIAI.RuyiEdibleWood"://如意食材相克
                req = ruyiEdibleWood(iSearch);
                break;
            case "RUYIAI.RuyiFood"://如意食材百科
                req = RuyiFood(iSearch);
                break;
            case "RUYIAI.RuyiWords"://如意挑战单词
                req = ruyiWords(iSearch);
                break;
            case "RUYIAI.RuyiEnglish"://如意每日英语
                req = ruyiEnglish(iSearch);
                break;
            case "IFLYTEK.englishEveryday"://英语每日一句
                req = englishEveryday(iSearch);
                break;
            case "AIUI.Riddle"://谜语
                req = riddle(iSearch);
                break;
            case "RUYIAI.RuyiLantern"://如意灯谜猜猜
                req = ruyiLantern(iSearch);
                break;
            case "RUYIAI.RuyiBrainGuess"://如意动脑猜猜
                req = ruyiBrainGuess(iSearch);
                break;
            case "RUYIAI.RuyiGuessWho"://如意猜猜我是谁
                req = ruyiGuessWho(iSearch);
                break;
            case "RUYIAI.RuyiFingerGuess"://如意猜拳
                req = ruyiFingerGuess(iSearch);
                break;
            case "RUYIAI.RuyiCalendar"://如意黄历
                req = ruyiCalendar(iSearch);
                break;
            case "RUYIAI.RuyiMasterBrain"://如意最强大脑
                req = ruyiMasterBrain(iSearch);
                break;
            case "LEIQIAO.voiceCopy"://会说话的小鹦鹉
                req = voiceCopy(iSearch);
                break;
            case "RUYIAI.RuyiRepeat"://如意鹦鹉学舌
                req = ruyiRepeat(iSearch);
                break;
            case "LEIQIAO.length"://单位换算-长度换算
                req = length(iSearch);
                break;
            case "LEIQIAO.temperature"://单位换算-温度换算
                req = temperature(iSearch);
                break;
            case "KLLI3.areaScaler"://面积换算
                req = areaScaler(iSearch);
                break;
            case "KLLI3.powerScaler"://功率换算
                req = powerScaler(iSearch);
                break;
            case "KLLI3.weightScaler"://重量换算
                req = weightScaler(iSearch);
                break;
            case "KLLI3.volumeScaler"://体积换算
                req = volumeScaler(iSearch);
                break;
            case "ZUOMX.queryCapital"://省会查询
                req = queryCapital(iSearch);
                break;
            case "LEIQIAO.cityOfPro"://国内城市查询
                req = cityOfPro(iSearch);
                break;
            case "KLLI3.captialInfo"://首都查询
                req = captialInfo(iSearch);
                break;
            case "LEIQIAO.BMI"://体重指数查询
                req = bMI(iSearch);
                break;
            case "AIUI.forex"://汇率查询与货币兑换
                req = forex(iSearch);
                break;
            case "AIUI.virusSearch"://新冠疫情查询
                req = virusSearch(iSearch);
                break;
            case "KLLI3.FamilyNames"://百家姓
                req = familyNames(iSearch);
                break;
            case "KLLI3.poetryChallenge"://诗词挑战
                req = poetryChallenge(iSearch);
                break;
            case "AIUI.garbageClassify"://垃圾分类
                req = garbageClassify(iSearch);
                break;
            case "AIUI.brainTeaser"://脑筋急转弯
                req = brainTeaser(iSearch);
                break;
            case "joke"://笑话
                req = joke(iSearch);
                break;
            case "PHOTO.challenge"://口算挑战
                req = challenge(iSearch);
                break;
            case "AIUI.calc"://计算器
                req = calc(iSearch);
                break;
            case "translation"://翻译
                req = translation(iSearch);
                break;
            case "AIUI.collegeScore"://高校分数线
                req = collegeScore(iSearch);
                break;

            //分类3
            case "animalCries"://动物叫声
                req = animalCries(iSearch);
                break;
            case "RUYIAI.RuyiMusicMaster"://如意音乐大师
                req = ruyiMusicMaster(iSearch);
                break;
            case "RUYIAI.RuyiMuseMusic"://如意冥想音乐
                req = ruyiMuseMusic(iSearch);
                break;
            case "musicX"://音乐
                req = musicX(iSearch);
                break;
            case "RUYIAI.RuyiGuessMusic"://如意我爱猜歌名
                req = ruyiGuessMusic(iSearch);
                break;

            //分类4
            case "RUYIAI.RuyiNews"://如意新闻广播
                req = ruyiNews(iSearch);
                break;
            case "news"://新闻
                req = news(iSearch);
                break;
            case "IFLYTEK.radio"://广播电台
                req = radio(iSearch);
                break;
            case "LEIQIAO.openClass"://公开课
                req = openClass(iSearch);
                break;

            //分类5
            case "weather"://天气
                req = weather(iSearch);
                break;

            //分类6
            case "baike"://百科
                req = baike(iSearch,search);
                break;

            //分类7
//            case "RUYIAI.RuyiConstellate"://如意星座运势
//                req = ruyiConstellate(iSearch);
//                break;
            case "chineseZodiac"://生肖运势
                req = chineseZodiac(iSearch);
                break;
            case "constellation"://星座
                req = constellation(iSearch);
                break;

            //分类8
            case "websearch"://网络搜索
                req = websearch(iSearch,search);
                break;
            case "lottery"://彩票
                req = lottery(iSearch,search);
                break;
            case "petrolPrice"://今日油价
                req = petrolPrice(iSearch,search);
                break;
            case "college"://高校查询
                req = college(iSearch,search);
                break;
            case "RUYIAI.RuyiChinese"://如意汉语词典
                req = ruyiChinese(iSearch);
                break;
            case "holiday"://假期安排
                req = holiday(iSearch,search);
                break;
            case "train"://火车
                req = train(iSearch,search);
                break;
            case "IFLYTEK.wordsMeaning"://词语解释
                req = wordsMeaning(iSearch,search);
                break;
            case "stock"://股票
                req = stock(iSearch,search);
                break;
            case "carNumber"://汽车尾号限行
                req = carNumber(iSearch,search);
                break;
            case "sentenceMaking"://造句
                req = sentenceMaking(iSearch,search);
                break;
            case "calendar"://万年历
                req = calendar(iSearch,search);
                break;
            case "LEIQIAO.historyToday"://历史上的今天
                req = historyToday(iSearch,search);
                break;
            case "AIUI.idiomsDict"://成语词典
                req = idiomsDict(iSearch,search);
                break;

            //分类9
            case "LEIQIAO.speech"://名人演讲
                req = speech(iSearch);
                break;

            //其他
            case "RUYIAI.RuyiQuotes"://如意名人名言
                req = ruyiQuotes(iSearch);
                break;
            case "AIUI.ktvSong"://KTV点歌
                req = ktvSong(iSearch);
                break;
            case "app"://应用
                req = app(iSearch);
                break;
        }
        if(null == req){//无固定方法的技能默认走分类2
            JSONObject getData = getData(iSearch,1,2);
            getData.put("type",2);
            ASingleTextVO brainTeaserVO = ASingleTextVO.retData(getData.toJSONString());
            return ResponseData.success(brainTeaserVO);
        }
        return req;
    }

    /**
     * 地图导航
     * @param iSearch
     * @return
     */
    public ResponseData<Object> mapU(JSONObject iSearch){
        JSONObject getData = getData(iSearch,2,1);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",1);
        if(1 == Integer.parseInt(getData.getString("reType"))){
            List<JSONObject> itemData = new ArrayList<>();
            for(Object data : getData.getJSONArray("item")){
                //截取地点类型最后一个";"后的值
                JSONObject jsonData = JSONObject.parseObject(data.toString());
                String category = StringUtils.substringAfterLast(jsonData.getString("category"), ";");
                jsonData.put("category",category);
                itemData.add(jsonData);
            }
            getData.put("item",itemData);
        }
        getData.put("text","已为您找到相应结果");
        MapUVO mapUVO = MapUVO.retData(getData.toJSONString());
        return ResponseData.success(mapUVO);
    }

    /**
     * 如意成语接龙
     * @param iSearch
     * @return
     */
    public ResponseData<Object> ruyiIdiomGame(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO ruyiIdiomGameVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(ruyiIdiomGameVO);
    }

    /**
     * 方得查成语
     * @param iSearch
     * @return
     */
    public ResponseData<Object> idiom(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO idiomVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(idiomVO);
    }

    /**
     * 成语接龙
     * @param iSearch
     * @return
     */
    public ResponseData<Object> idiomSolitaire(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO idiomSolitaireVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(idiomSolitaireVO);
    }

    /**
     * 知识图谱
     * @param iSearch
     * @return
     */
    public ResponseData<Object> iFlytekKnowledgeMap(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO iFlytekKnowledgeMapVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(iFlytekKnowledgeMapVO);
    }

    /**
     * 如意时间询问
     * @param iSearch
     * @return
     */
    public ResponseData<Object> ruyiDatetime(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO ruyiDatetimeVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(ruyiDatetimeVO);
    }

    /**
     * 如意古诗词
     * @param iSearch
     * @return
     */
    public ResponseData<Object> ruyiPoetry(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO ruyiPoetryVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(ruyiPoetryVO);
    }

    /**
     * 如意诗词大会
     * @param iSearch
     * @return
     */
    public ResponseData<Object> ruyiPoem(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO ruyiPoemVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(ruyiPoemVO);
    }

    /**
     * 诗词挑战
     * @param iSearch
     * @return
     */
    public ResponseData<Object> poetryChallenge(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO poetryChallengeVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(poetryChallengeVO);
    }

    /**
     * 诗词对答
     * @param iSearch
     * @return
     */
    public ResponseData<Object> poetry(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO poetryVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(poetryVO);
    }

    /**
     * 猜数字游戏
     * @param iSearch
     * @return
     */
    public ResponseData<Object> guessNumber(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO guessNumberVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(guessNumberVO);
    }

    /**
     * 跳数字游戏
     * @param iSearch
     * @return
     */
    public ResponseData<Object> skipNumber(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO skipNumberVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(skipNumberVO);
    }

    /**
     * 剪刀石头布游戏
     * @param iSearch
     * @return
     */
    public ResponseData<Object> moraGame(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO moraGameVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(moraGameVO);
    }

    /**
     * 如意食材相克
     * @param iSearch
     * @return
     */
    public ResponseData<Object> ruyiEdibleWood(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO ruyiEdibleWoodVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(ruyiEdibleWoodVO);
    }

    /**
     * 如意食材百科
     * @param iSearch
     * @return
     */
    public ResponseData<Object> RuyiFood(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO RuyiFoodVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(RuyiFoodVO);
    }

    /**
     * 如意挑战单词
     * @param iSearch
     * @return
     */
    public ResponseData<Object> ruyiWords(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO ruyiWordsVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(ruyiWordsVO);
    }

    /**
     * 如意每日英语
     * @param iSearch
     * @return
     */
    public ResponseData<Object> ruyiEnglish(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO ruyiEnglishVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(ruyiEnglishVO);
    }

    /**
     * 英语每日一句
     * @param iSearch
     * @return
     */
    public ResponseData<Object> englishEveryday(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO englishEverydayVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(englishEverydayVO);
    }

    /**
     * 谜语
     * @param iSearch
     * @return
     */
    public ResponseData<Object> riddle(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO riddleVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(riddleVO);
    }

    /**
     * 如意灯谜猜猜
     * @param iSearch
     * @return
     */
    public ResponseData<Object> ruyiLantern(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO ruyiLanternVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(ruyiLanternVO);
    }

    /**
     * 如意黄历
     * @param iSearch
     * @return
     */
    public ResponseData<Object> ruyiCalendar(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO ruyiCalendarVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(ruyiCalendarVO);
    }


    /**
     * 如意最强大脑
     * @param iSearch
     * @return
     */
    public ResponseData<Object> ruyiMasterBrain(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO ruyiMasterBrainVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(ruyiMasterBrainVO);
    }

    /**
     * 如意动脑猜猜
     * @param iSearch
     * @return
     */
    public ResponseData<Object> ruyiBrainGuess(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO ruyiBrainGuessVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(ruyiBrainGuessVO);
    }

    /**
     * 会说话的小鹦鹉
     * @param iSearch
     * @return
     */
    public ResponseData<Object> voiceCopy(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO voiceCopyVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(voiceCopyVO);
    }

    /**
     * 如意鹦鹉学舌
     * @param iSearch
     * @return
     */
    public ResponseData<Object> ruyiRepeat(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO ruyiRepeatVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(ruyiRepeatVO);
    }

    /**
     * 单位换算-长度换算
     * @param iSearch
     * @return
     */
    public ResponseData<Object> length(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO lengthVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(lengthVO);
    }

    /**
     * 单位换算-温度换算
     * @param iSearch
     * @return
     */
    public ResponseData<Object> temperature(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO temperatureVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(temperatureVO);
    }

    /**
     * 面积换算
     * @param iSearch
     * @return
     */
    public ResponseData<Object> areaScaler(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO areaScalerVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(areaScalerVO);
    }

    /**
     * 功率换算
     * @param iSearch
     * @return
     */
    public ResponseData<Object> powerScaler(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO powerScalerVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(powerScalerVO);
    }

    /**
     * 重量换算
     * @param iSearch
     * @return
     */
    public ResponseData<Object> weightScaler(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO weightScalerVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(weightScalerVO);
    }

    /**
     * 体积换算
     * @param iSearch
     * @return
     */
    public ResponseData<Object> volumeScaler(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO volumeScalerVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(volumeScalerVO);
    }

    /**
     * 省会查询
     * @param iSearch
     * @return
     */
    public ResponseData<Object> queryCapital(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO queryCapitalVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(queryCapitalVO);
    }

    /**
     * 百家姓
     * @param iSearch
     * @return
     */
    public ResponseData<Object> familyNames(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO familyNamesVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(familyNamesVO);
    }

    /**
     * 国内城市查询
     * @param iSearch
     * @return
     */
    public ResponseData<Object> cityOfPro(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO cityOfProVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(cityOfProVO);
    }

    /**
     * 首都查询
     * @param iSearch
     * @return
     */
    public ResponseData<Object> captialInfo(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO captialInfoVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(captialInfoVO);
    }

    /**
     * 体重指数查询
     * @param iSearch
     * @return
     */
    public ResponseData<Object> bMI(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO bMIVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(bMIVO);
    }

    /**
     * 汇率查询与货币兑换
     * @param iSearch
     * @return
     */
    public ResponseData<Object> forex(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO forexVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(forexVO);
    }

    /**
     * 新冠疫情查询
     * @param iSearch
     * @return
     */
    public ResponseData<Object> virusSearch(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO virusSearchVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(virusSearchVO);
    }

    /**
     * 垃圾分类
     * @param iSearch
     * @return
     */
    public ResponseData<Object> garbageClassify(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO garbageClassifyVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(garbageClassifyVO);
    }

    /**
     * 脑筋急转弯
     * @param iSearch
     * @return
     */
    public ResponseData<Object> brainTeaser(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO brainTeaserVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(brainTeaserVO);
    }

    /**
     * 笑话
     * @param iSearch
     * @return
     */
    public ResponseData<Object> joke(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO jokeVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(jokeVO);
    }

    /**
     * 健康知识
     * @param iSearch
     * @return
     */
    public ResponseData<Object> healthKnowledge(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO healthKnowledgeVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(healthKnowledgeVO);
    }

    /**
     * 口算挑战
     * @param iSearch
     * @return
     */
    public ResponseData<Object> challenge(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO challengeVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(challengeVO);
    }

    /**
     * 计算器
     * @param iSearch
     * @return
     */
    public ResponseData<Object> calc(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO calcVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(calcVO);
    }

    /**
     * 如意猜猜我是谁
     * @param iSearch
     * @return
     */
    public ResponseData<Object> ruyiGuessWho(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO ruyiGuessWhoVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(ruyiGuessWhoVO);
    }

    /**
     * 如意猜拳
     * @param iSearch
     * @return
     */
    public ResponseData<Object> ruyiFingerGuess(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO ruyiFingerGuessVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(ruyiFingerGuessVO);
    }

    /**
     * 翻译
     * @param iSearch
     * @return
     */
    public ResponseData<Object> translation(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO translationVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(translationVO);
    }

    /**
     * 高校分数线
     * @param iSearch
     * @return
     */
    public ResponseData<Object> collegeScore(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO collegeScoreVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(collegeScoreVO);
    }

    /**
     * 时间日期查询
     * @param iSearch
     * @return
     */
    public ResponseData<Object> datetimeX(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO datetimeXVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(datetimeXVO);
    }

    /**
     * 动物叫声
     * @param iSearch
     * @return
     */
    public ResponseData<Object> animalCries(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,3);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",3);
        getData.put("text",getData.getString("text"));
        getData.put("sid",getData.getString("sid"));
        MusicPlayVO animalCriesVO = MusicPlayVO.retData(getData.toJSONString());
        return ResponseData.success(animalCriesVO);
    }

    /**
     * 如意音乐大师
     * @param iSearch
     * @return
     */
    public ResponseData<Object> ruyiMusicMaster(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,3);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",3);
        getData.put("text",getData.getString("text"));
        getData.put("sid",getData.getString("sid"));
        MusicPlayVO ruyiMusicMasterVO = MusicPlayVO.retData(getData.toJSONString());
        return ResponseData.success(ruyiMusicMasterVO);
    }

    /**
     * 如意冥想音乐
     * @param iSearch
     * @return
     */
    public ResponseData<Object> ruyiMuseMusic(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,3);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",3);
        getData.put("text",getData.getString("text"));
        MusicPlayVO musicPlayVO = MusicPlayVO.retData(getData.toJSONString());
        return ResponseData.success(musicPlayVO);
    }

    /**
     * 如意我爱猜歌名
     * @param iSearch
     * @return
     */
    public ResponseData<Object> ruyiGuessMusic(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,3);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",3);
        getData.put("text",getData.getString("text"));
        getData.put("sid",getData.getString("sid"));
        MusicPlayVO ruyiGuessMusicVO = MusicPlayVO.retData(getData.toJSONString());
        return ResponseData.success(ruyiGuessMusicVO);
    }

    /**
     * 如意新闻广播
     * @param iSearch
     * @return
     */
    public ResponseData<Object> ruyiNews(JSONObject iSearch){
        JSONObject getData = getData(iSearch,2,4);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",4);
        NewsVO radioVO = NewsVO.retData(getData.toJSONString());
        return ResponseData.success(radioVO);
    }

    /**
     * 新闻
     * @param iSearch
     * @return
     */
    public ResponseData<Object> news(JSONObject iSearch){
        JSONObject getData = getData(iSearch,2,4);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        JSONArray item = getData.getJSONArray("item");
        List<JSONObject> newsItem = new ArrayList<>();
        for(Object data : item){
            JSONObject json = JSONObject.parseObject(data.toString());
            json.put("text",json.getString("title"));
            json.put("img",json.getString("imgUrl"));
            newsItem.add(json);
        }
        getData.put("type",4);
        getData.put("text","");
        getData.put("item",newsItem);
        NewsVO newsVO = NewsVO.retData(getData.toJSONString());
        return ResponseData.success(newsVO);
    }

    /**
     * 广播电台
     * @param iSearch
     * @return
     */
    public ResponseData<Object> radio(JSONObject iSearch){
        JSONObject getData = getData(iSearch,2,4);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",4);
        NewsVO radioVO = NewsVO.retData(getData.toJSONString());
        return ResponseData.success(radioVO);
    }

    /**
     * 公开课
     * @param iSearch
     * @return
     */
    public ResponseData<Object> openClass(JSONObject iSearch){
        JSONObject getData = getData(iSearch,2,4);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",4);
        NewsVO openClassVO = NewsVO.retData(getData.toJSONString());
        return ResponseData.success(openClassVO);
    }

    /**
     * 天气
     * @param iSearch
     * @return
     */
    public ResponseData<Object> weather(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,5);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        JSONObject exp = getData.getJSONObject("exp");

        getData.put("type",5);
        getData.put("uv",exp.getJSONObject("uv").getString("level"));
        getData.put("ct",exp.getJSONObject("ct").getString("level"));
        getData.put("gm",exp.getJSONObject("gm").getString("level"));
        getData.put("xc",exp.getJSONObject("xc").getString("level"));
        getData.put("yd",exp.getJSONObject("yd").getString("level"));
        WeatherVO weatherVO = WeatherVO.retData(getData.toJSONString());
        return ResponseData.success(weatherVO);
    }

    /**
     * 百科
     * @param iSearch
     * @return
     */
    public ResponseData<Object> baike(JSONObject iSearch,String search){
        JSONObject getData = getData(iSearch,1,6);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",6);
        getData.put("urlGoto","https://www.baidu.com/s?ie=UTF-8&wd="+search);
        BaikeAndConstellationVO baikeVO = BaikeAndConstellationVO.retData(getData.toJSONString());
        return ResponseData.success(baikeVO);
    }

//    /**
//     * 如意星座运势
//     * @param iSearch
//     * @return
//     */
//    public ResponseData<Object> ruyiConstellate(JSONObject iSearch){
//        JSONObject getData = getData(iSearch,1,6);
//        if(null != getData.getString("code")){
//            return ResponseData.success(getData);
//        }
//        getData.put("type",7);
//        RuyiConstellateVO ruyiConstellateVO = RuyiConstellateVO.retData(getData.toJSONString());
//        return ResponseData.success(ruyiConstellateVO);
//    }

    /**
     * 星座查询
     * @param iSearch
     * @return
     */
    public ResponseData<Object> constellation(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,7);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",7);
        getData.put("title",getData.getString("name"));
        getData.put("summary",getData.getString("text"));
        JSONObject fortune = JSONObject.parseObject(getData.getJSONArray("fortune").get(0).toString());
        getData.put("fortune",fortune.getString("description"));
        BaikeAndConstellationVO constellationVO = BaikeAndConstellationVO.retData(getData.toJSONString());
        return ResponseData.success(constellationVO);
    }

    /**
     * 生肖运势
     * @param iSearch
     * @return
     */
    public ResponseData<Object> chineseZodiac(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,7);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",7);
        getData.put("title",getData.getString("name"));
        BaikeAndConstellationVO chineseZodiacVO = BaikeAndConstellationVO.retData(getData.toJSONString());
        return ResponseData.success(chineseZodiacVO);
    }

    /**
     * 音乐
     * @param iSearch
     * @return
     */
    public ResponseData<Object> musicX(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,3);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",3);
        MusicPlayVO musicXVO = MusicPlayVO.retData(getData.toJSONString());
        return ResponseData.success(musicXVO);
    }

    /**
     * 名人演讲
     * @param iSearch
     * @return
     */
    public ResponseData<Object> speech(JSONObject iSearch){
        JSONObject getData = getData(iSearch,2,9);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",9);
        getData.put("text","已为您查找到下列演讲");
        SpeechVO speechVO = SpeechVO.retData(getData.toJSONString());
        return ResponseData.success(speechVO);
    }

    /**
     * 如意名人名言
     * @param iSearch
     * @return
     */
    public ResponseData<Object> ruyiQuotes(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        ASingleTextVO ruyiQuotesVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(ruyiQuotesVO);
    }

    /**
     * KTV点歌
     * @param iSearch
     * @return
     */
    public ResponseData<Object> ktvSong(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",2);
        String value= getData.getString("value");
        JSONObject songId = MusicUtil.searchMusic(value);
        if(0 == Integer.parseInt(songId.getString("code"))){
            getData.put("text",songId.getString("msg"));
        }else {
            getData.put("url",MusicUtil.searchMusicId(songId.getString("msg")));
        }
        MusicVO ktvSongVO = MusicVO.retData(getData.toJSONString());
        return ResponseData.success(ktvSongVO);

    }

    /**
     * 网络搜索
     * @param iSearch
     * @return
     */
    public ResponseData<Object> websearch(JSONObject iSearch,String search){
        JSONObject getData = getData(iSearch,1,8);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",8);
        getData.put("url","https://www.baidu.com/s?ie=UTF-8&wd="+search);
        MultitermVO websearchVO = MultitermVO.retData(getData.toJSONString());
        return ResponseData.success(websearchVO);
    }

    /**
     * 彩票
     * @param iSearch
     * @return
     */
    public ResponseData<Object> lottery(JSONObject iSearch,String search){
        JSONObject getData = getData(iSearch,1,8);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",8);
        getData.put("url","https://www.baidu.com/s?ie=UTF-8&wd="+search);
        MultitermVO lotteryVO = MultitermVO.retData(getData.toJSONString());
        return ResponseData.success(lotteryVO);
    }

    /**
     * 今日油价
     * @param iSearch
     * @return
     */
    public ResponseData<Object> petrolPrice(JSONObject iSearch,String search){
        JSONObject getData = getData(iSearch,1,8);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",8);
        getData.put("url","https://www.baidu.com/s?ie=UTF-8&wd="+search);
        MultitermVO petrolPriceVO = MultitermVO.retData(getData.toJSONString());
        return ResponseData.success(petrolPriceVO);
    }

    /**
     * 假期安排
     * @param iSearch
     * @return
     */
    public ResponseData<Object> holiday(JSONObject iSearch,String search){
        JSONObject getData = getData(iSearch,1,8);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",8);
        getData.put("url","https://www.baidu.com/s?ie=UTF-8&wd="+search);
        MultitermVO holidayVO = MultitermVO.retData(getData.toJSONString());
        return ResponseData.success(holidayVO);
    }

    /**
     * 火车
     * @param iSearch
     * @return
     */
    public ResponseData<Object> train(JSONObject iSearch,String search){
        JSONObject getData = getData(iSearch,1,8);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",8);
        if(1 == getData.getInteger("reType")){
            getData.put("url","https://www.baidu.com/s?ie=UTF-8&wd="+getData.getString("keyword")+
                    getData.getString("sCityAddr")+"到"+getData.getString("cityAddr")+"的火车");
        }
        MultitermVO trainVO = MultitermVO.retData(getData.toJSONString());
        return ResponseData.success(trainVO);
    }

    /**
     * 词语解释
     * @param iSearch
     * @return
     */
    public ResponseData<Object> wordsMeaning(JSONObject iSearch,String search){
        JSONObject getData = getData(iSearch,1,8);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",8);
        getData.put("url","https://www.baidu.com/s?ie=UTF-8&wd="+search);
        MultitermVO wordsMeaningVO = MultitermVO.retData(getData.toJSONString());
        return ResponseData.success(wordsMeaningVO);
    }

    /**
     * 股票
     * @param iSearch
     * @return
     */
    public ResponseData<Object> stock(JSONObject iSearch,String search){
        JSONObject getData = getData(iSearch,1,8);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",8);
        getData.put("url","https://www.baidu.com/s?ie=UTF-8&wd="+search);
        MultitermVO stockVO = MultitermVO.retData(getData.toJSONString());
        return ResponseData.success(stockVO);
    }

    /**
     * 汽车尾号限行
     * @param iSearch
     * @return
     */
    public ResponseData<Object> carNumber(JSONObject iSearch,String search){
        JSONObject getData = getData(iSearch,1,8);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",8);
        getData.put("url","https://www.baidu.com/s?ie=UTF-8&wd="+search);
        MultitermVO carNumberVO = MultitermVO.retData(getData.toJSONString());
        return ResponseData.success(carNumberVO);
    }

    /**
     * 造句
     * @param iSearch
     * @return
     */
    public ResponseData<Object> sentenceMaking(JSONObject iSearch,String search){
        JSONObject getData = getData(iSearch,1,8);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",8);
        getData.put("url","https://www.baidu.com/s?ie=UTF-8&wd="+search);
        MultitermVO sentenceMakingVO = MultitermVO.retData(getData.toJSONString());
        return ResponseData.success(sentenceMakingVO);
    }

    /**
     * 高校查询
     * @param iSearch
     * @return
     */
    public ResponseData<Object> college(JSONObject iSearch,String search){
        JSONObject getData = getData(iSearch,1,8);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",8);
        getData.put("url","https://www.baidu.com/s?ie=UTF-8&wd="+search);
        MultitermVO collegeVO = MultitermVO.retData(getData.toJSONString());
        return ResponseData.success(collegeVO);
    }

    /**
     * 如意汉语词典
     * @param iSearch
     * @return
     */
    public ResponseData<Object> ruyiChinese(JSONObject iSearch){//,String search
        JSONObject getData = getData(iSearch,1,8);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",8);
//        getData.put("url","https://www.baidu.com/s?ie=UTF-8&wd="+search);
        MultitermVO ruyiChineseVO = MultitermVO.retData(getData.toJSONString());
        return ResponseData.success(ruyiChineseVO);
    }

    /**
     * 万年历
     * @param iSearch
     * @return
     */
    public ResponseData<Object> calendar(JSONObject iSearch,String search){
        JSONObject getData = getData(iSearch,1,8);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",8);
        getData.put("url","https://www.baidu.com/s?ie=UTF-8&wd="+search);
        MultitermVO calendarVO = MultitermVO.retData(getData.toJSONString());
        return ResponseData.success(calendarVO);
    }

    /**
     * 历史上的今天
     * @param iSearch
     * @return
     */
    public ResponseData<Object> historyToday(JSONObject iSearch,String search){
        JSONObject getData = getData(iSearch,1,8);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",8);
        getData.put("url","https://www.baidu.com/s?ie=UTF-8&wd="+search);
        MultitermVO historyTodayVO = MultitermVO.retData(getData.toJSONString());
        return ResponseData.success(historyTodayVO);
    }

    /**
     * 成语词典
     * @param iSearch
     * @return
     */
    public ResponseData<Object> idiomsDict(JSONObject iSearch,String search){
        JSONObject getData = getData(iSearch,1,8);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",8);
        getData.put("url","https://www.baidu.com/s?ie=UTF-8&wd="+search);
        MultitermVO idiomsDictVO = MultitermVO.retData(getData.toJSONString());
        return ResponseData.success(idiomsDictVO);
    }

    /**
     * 应用
     * @param iSearch
     * @return
     */
    public ResponseData<Object> app(JSONObject iSearch){
        JSONObject getData = getData(iSearch,1,2);
        if(null != getData.getString("code")){
            return ResponseData.success(getData);
        }
        getData.put("type",10);
        getData.put("value",getData.getString("text"));
        getData.put("text","");
        ASingleTextVO appVO = ASingleTextVO.retData(getData.toJSONString());
        return ResponseData.success(appVO);
    }

    /**
     * 返回单条/多条
     * @param iSearch
     * @param count 判断多条或单条
     * @return
     */
    public JSONObject getData(JSONObject iSearch, int count,int type){
//        JSONObject iSearch = AiUtil.aiFunction(search);
        JSONObject data = new JSONObject();
        data.put("sid", iSearch.get("sid"));
        JSONObject intent = JSONObject.parseObject(iSearch.getJSONArray("data").getJSONObject(0).get("intent").toString());
        if("IFLYTEK.app".equals(intent.getString("category"))){
            JSONObject slotsValue = intent.getJSONArray("semantic").getJSONObject(0);
            data.put("text",slotsValue.getJSONArray("slots").getJSONObject(0).getString("value"));
            data.put("intent",slotsValue.getString("intent"));
            return data;
        }
        String text = intent.getJSONObject("answer").getString("text");
        data.put("text",text);
        if(2 == type){
            //ktvSong
            if("AIUI.ktvSong".equals(intent.getString("category"))){
                JSONArray slots= intent.getJSONArray("semantic").getJSONObject(0).getJSONArray("slots");
                for(Object value:slots){
                    JSONObject slotsJsonObject =JSONObject.parseObject(value.toString());
                    if("song".equals(slotsJsonObject.getString("name"))){
                        data.put("value",slotsJsonObject.getString("value"));
                    }
                }
            }
            return data;
        }
        //1类
        if(1 == type){
            //默认给0
            data.put("reType",0);
            //如果返回结果中包含"moreResults",说明没有返回item
            if(null != intent.getJSONArray("moreResults")){
                JSONArray slots= intent.getJSONArray("semantic").getJSONObject(0).getJSONArray("slots");
                for(Object value:slots){
                    JSONObject slotsJsonObject =JSONObject.parseObject(value.toString());
                    //获取返回结果中的error
                    JSONObject error = intent.getJSONArray("moreResults").getJSONObject(0).getJSONObject("data").getJSONObject("error");
                    //如果error存在代表返回"未查询到结果"
                    if(null != error){
                        if("endLoc.ori_loc".equals(slotsJsonObject.getString("name"))){
                            data.put("value",slotsJsonObject.getString("value"));
                            data.put("text",error.getString("message"));
                            return data;
                        }
                    }
                }
            }
            else {
                //代表有值（item），给1
                data.put("reType",1);
            }
        }
        //分类8
        if(8 == type){
            //"火车"特殊，因为包含对话，所以需要给前端判断值：reType
            if("IFLYTEK.train".equals(intent.getString("category"))){
                if("dataInvalid".equals(intent.getString("dialog_stat"))){
                    data.put("reType",0);
                }else {
                    data.put("reType",1);
                    JSONArray slots= intent.getJSONArray("semantic").getJSONObject(0).getJSONArray("slots");
                    for(Object value:slots){
                        JSONObject slotsJsonObject =JSONObject.parseObject(value.toString());
                        //组装返回百度链接时的搜索值
                        if("endLoc.city".equals(slotsJsonObject.getString("name"))){
                            data.put("cityAddr",slotsJsonObject.getString("value"));
                        }
                        if("startLoc.cityAddr".equals(slotsJsonObject.getString("name"))){
                            data.put("sCityAddr",slotsJsonObject.getString("value"));
                        }
                    }
                    data.put("keyword",intent.getString("text"));
                }
                return data;
            }
        }
        //code：用于判断是否出现error，即请求成功但无结果或其他情况，0为一切正常，1为异常处理
        int code = Integer.parseInt(intent.get("rc").toString());
        if(0 == code){
            //条数：多条。直接取返回结果数组放入item返回
            if (1 < count) {
                JSONArray result = intent.getJSONObject("data").getJSONArray("result");
                data.put("item", result);
                return data;
            } else {
                //条数：单条，"新闻"类与"如意冥想音乐"特殊，需要进行二次选择，比如："我想看新闻"后需要选择哪种类型的新闻"早间，娱乐……"
                if("RUYIAI.RuyiNews" .equals(intent.getString("category")) || "RUYIAI.RuyiMuseMusic" .equals(intent.getString("category"))){
                    JSONObject semantic = JSONObject.parseObject(intent.getJSONArray("semantic").getString(0));
                    if("open".equals(semantic.get("intent")) || "exit".equals(semantic.get("intent"))){
                        return data;
                    }
                }
                if("IFLYTEK.train" .equals(intent.getString("category"))){
                        return data;
                }
                JSONObject result = intent.getJSONObject("data").getJSONArray("result").getJSONObject(0);
                result.put("text", text);
                result.put("sid", iSearch.get("sid"));
                return result;
            }
        }
        if(1 == code){
            data.put("type",type);
            data.put("code",1);
            data.put("msg",text);
            data.put("text","输入异常");
        }
        if(2 == code){
            data.put("type",type);
            data.put("code",2);
            data.put("msg",text);
            data.put("text","系统内部异常");
        }
        if(3 == code){
            data.put("type",type);
            data.put("code",3);
            data.put("msg",text);
            data.put("text","业务操作失败，没搜索到结果或信源异常");
        }
        if(4 == code){
            data.put("type",type);
            data.put("code",4);
            data.put("msg",text);
            data.put("text","文本没有匹配的技能场景，技能不理解或不能处理该文本");
        }
        data.put("desc",intent.getJSONObject("data").getJSONObject("error").get("message"));
        return data;
    }

    private JSONObject getKtvSong(JSONObject iSearch){
        JSONObject data = new JSONObject();
        JSONObject intent = JSONObject.parseObject(iSearch.getJSONArray("data").getJSONObject(0).get("intent").toString());
        JSONArray slots= intent.getJSONArray("semantic").getJSONObject(0).getJSONArray("slots");
        for(Object value:slots){
            JSONObject slotsJsonObject =JSONObject.parseObject(value.toString());
            if("song".equals(slotsJsonObject.getString("name"))){
                data.put("value",slotsJsonObject.getString("value"));
            }
        }
        return data;
    }
}
