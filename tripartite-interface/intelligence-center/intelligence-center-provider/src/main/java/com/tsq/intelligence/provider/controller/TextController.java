package com.tsq.intelligence.provider.controller;

import cn.hutool.core.convert.Convert;
import com.tsq.common.model.ResponseData;
import com.tsq.intelligence.api.dto.*;
import com.tsq.intelligence.api.vo.*;
import com.tsq.intelligence.provider.service.ITextService;
import com.tsq.intelligence.provider.utils.Constant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 文字识别
 * </p>
 *
 * @author lvyang
 * @since 2020-06-02
 */
@RequestMapping(value = "text")
@RestController("text")
@Api(value = "TextController", tags = {"文字识别接口"})
public class TextController {

    @Autowired
    private ITextService textService;

    @ApiOperation(value = "手写文字识别", notes = "手写文字识别")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "手写文字", value = "手写文字", dataType = "map", paramType = "query", required = true)
    })
    @PostMapping(value = "/transferImageToText")
    @ResponseBody
    public ResponseData<List<String>> transferImageToText(HandWritingDTO handWritingDTO , @RequestParam("file") MultipartFile file) throws IOException, ParseException {
        HandWritingVO handWriting= textService.transferImageToText(handWritingDTO,file);
        if(!Constant.SUCCESS.equals(handWriting.getCode())){
            return ResponseData.error(handWriting.getDesc());
        }
        return ResponseData.success(handWriting.getWordList());
    }

    @ApiOperation(value = "印刷文字识别", notes = "印刷文字识别")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "印刷文字", value = "印刷文字", dataType = "map", paramType = "query", required = true)
    })
    @PostMapping(value = "/transferPrintWordToText")
    public ResponseData<List<String>> transferPrintWordToText(HandWritingDTO handWritingDTO ,@RequestParam("file") MultipartFile file) throws IOException, ParseException {
        PrintWordVO printWordVO = textService.transferPrintWordToText(handWritingDTO,file);
        if(!Constant.SUCCESS.equals(printWordVO.getCode())){
            return ResponseData.error(printWordVO.getDesc());
        }
        return ResponseData.success(printWordVO.getWordList());
    }

    @ApiOperation(value = "多语种印刷文字识别", notes = "多语种印刷文字识别")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "多语种印刷文字", value = "多语种印刷文字", dataType = "map", paramType = "query", required = true)
    })
    @PostMapping(value = "/transferMorePrintWordToText")
    public ResponseData<List<String>> transferMorePrintWordToText(HandWritingDTO handWritingDTO ,@RequestParam("file") MultipartFile file) throws IOException, ParseException {
        MorePrintWordVO morePrintWordVO = textService.transferMorePrintWordToText(handWritingDTO,file);
        if(!Constant.SUCCESS.equals(morePrintWordVO.getCode())){
            return ResponseData.error(morePrintWordVO.getDesc());
        }
        return ResponseData.success(morePrintWordVO.getWordsList());
    }

    @ApiOperation(value = "名片识别", notes = "名片识别")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "图片", value = "图片", dataType = "map", paramType = "query", required = true)
    })
    @PostMapping(value = "/transferCardToText")
    public ResponseData<BusinessCardVO> transferCardToText(HandWritingDTO handWritingDTO ,@RequestParam("file") MultipartFile file) throws IOException, ParseException {
        BusinessCardVO businessCardVO = textService.transferCardToText(handWritingDTO,file);
        if(!Constant.SUCCESS.equals(businessCardVO.getCode())){
            return ResponseData.error(businessCardVO.getDesc());
        }
        return ResponseData.success(businessCardVO);
    }

    @ApiOperation(value = "身份证识别", notes = "身份证识别")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "图片", value = "图片", dataType = "map", paramType = "query", required = true)
    })
    @PostMapping(value = "/transferIdCardToText")
    public ResponseData<IdCardVO> transferIdCardToText(IdCardDTO idCardDTO , @RequestParam("file") MultipartFile file) throws IOException, ParseException {
        IdCardVO idCard =textService.transferIdCardToText(idCardDTO,file);
        if(!Constant.SUCCESS.equals(idCard.getCode())){
            return ResponseData.error(idCard.getDesc());
        }
        return ResponseData.success(idCard);
    }

    @ApiOperation(value = "银行卡识别", notes = "银行卡识别")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "图片", value = "图片", dataType = "map", paramType = "query", required = true)
    })
    @PostMapping(value = "/transferBankCardToText")
    public ResponseData<BankCardVO> transferBankCardToText(BankCardDTO bankCardDTO , @RequestParam("file") MultipartFile file) throws IOException, ParseException {
        BankCardVO bankCardVO = textService.transferBankCardToText(bankCardDTO,file);
        if(!Constant.SUCCESS.equals(bankCardVO.getCode())){
            return ResponseData.error(bankCardVO.getDesc());
        }
        return ResponseData.success(bankCardVO);
    }

    @ApiOperation(value = "营业执照识别", notes = "营业执照识别")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "图片", value = "图片", dataType = "map", paramType = "query", required = true)
    })
    @PostMapping(value = "/transferLicenseToText")
    public ResponseData<BusinessLicenseVO> transferLicenseToText(CommonDTO commonDTO , @RequestParam("file") MultipartFile file) throws IOException, ParseException {
        BusinessLicenseVO businessLicenseVO =  textService.transferLicenseToText(commonDTO,file);
        if(!Constant.SUCCESS.equals(businessLicenseVO.getCode())){
            return ResponseData.error(businessLicenseVO.getDesc());
        }
        return ResponseData.success(businessLicenseVO);
    }

    @ApiOperation(value = "增值税发票识别", notes = "增值税发票识别")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "图片", value = "图片", dataType = "map", paramType = "query", required = true)
    })
    @PostMapping(value = "/transferInvoiceToText")
    public ResponseData<InvoiceVO> transferInvoiceToText(@RequestParam("file") MultipartFile file) throws IOException, ParseException {
        InvoiceVO invoiceVO = textService.transferInvoiceToText(file);
        if(!Constant.SUCCESS.equals(invoiceVO.getCode())){
            return ResponseData.error(invoiceVO.getDesc());
        }
        return ResponseData.success(invoiceVO);
    }

    @ApiOperation(value = "拍照速算识别", notes = "拍照速算识别")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "图片", value = "图片", dataType = "map", paramType = "query", required = true)
    })
    @PostMapping(value = "/takePictureToCalculate")
    public ResponseData<TakePictureToCalculateVO> takePictureToCalculate(@RequestParam("file") MultipartFile file) throws Exception {
        TakePictureToCalculateVO takePictureToCalculateVO = textService.takePictureToCalculate(file);
        if(!Constant.SUCCESS.equals(takePictureToCalculateVO.getCode())){
            return ResponseData.error(takePictureToCalculateVO.getDesc());
        }
        return ResponseData.success(takePictureToCalculateVO);
    }

    @ApiOperation(value = "公式识别", notes = "公式识别")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "图片", value = "图片", dataType = "map", paramType = "query", required = true)
    })
    @PostMapping(value = "/formulaToCalculate")
    public ResponseData<FormulaToCalculateVO> formulaToCalculate(@RequestParam("file") MultipartFile file) throws Exception {
        FormulaToCalculateVO  formulaToCalculateVO = textService.formulaToCalculate(file);
        if(!Constant.SUCCESS.equals(formulaToCalculateVO.getCode())){
            return ResponseData.error(formulaToCalculateVO.getDesc());
        }
        return ResponseData.success(formulaToCalculateVO);
    }

    @ApiOperation(value = "指尖文字识别", notes = "指尖文字识别")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "图片", value = "图片", dataType = "map", paramType = "query", required = true)
    })
    @PostMapping(value = "/fingertipOfImageToText")
    public ResponseData<FingerToTextVO> fingertipOfImageToText(FingerDTO fingerDTO,@RequestParam("file") MultipartFile file) throws Exception {
        FingerToTextVO fingerToText = textService.fingertipOfImageToText(fingerDTO,file);
        if(!Constant.SUCCESS.equals(fingerToText.getCode())){
            return ResponseData.error(fingerToText.getDesc());
        }
        return ResponseData.success(fingerToText);
    }

}
