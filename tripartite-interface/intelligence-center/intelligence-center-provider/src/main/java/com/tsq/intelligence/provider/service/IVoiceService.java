package com.tsq.intelligence.provider.service;

import com.tsq.intelligence.api.dto.VoiceDTO;
import com.tsq.intelligence.api.vo.VoiceVO;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface IVoiceService {

   VoiceVO transferVoiceToTextDelay(VoiceDTO voiceDTO, MultipartFile audio) throws IOException;
}
