package com.tsq.intelligence.provider.service;

import com.tsq.intelligence.api.vo.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface ICheckService {
    /**
     * 物体识别
     * @param  file
     * @throws IOException
     */
    CurrencyCheckVO currencyCheck(MultipartFile file) throws IOException;

    /**
     * 场景识别
     * @param  file
     * @throws IOException
     */
    SceneCheckVO imageCheck(MultipartFile file) throws IOException;

    /**
     * 色情内容过滤
     * @param  file
     * @throws IOException
     */
    PornCheckVO pornCheck(MultipartFile file) throws IOException;

    /**
     * 政治人物检查
     * @param  file
     * @throws IOException
     */
    PoliticianCheckVO politicianCheck(MultipartFile file) throws IOException;

    /**
     * 暴恐信息过滤
     * @param  file
     * @throws IOException
     */
    SensitiveCheckVO sensitiveCheck(MultipartFile file) throws IOException;

    /**
     * 广告过滤
     * @param  file
     * @throws IOException
     */
    AdvertisingCheckVO advertisingCheck(MultipartFile file) throws IOException;
}
