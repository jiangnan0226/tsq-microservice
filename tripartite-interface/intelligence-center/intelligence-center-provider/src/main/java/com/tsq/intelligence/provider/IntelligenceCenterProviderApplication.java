package com.tsq.intelligence.provider;

import org.minbox.framework.api.boot.autoconfigure.swagger.annotation.EnableApiBootSwagger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author lvyang
 * -javaagent:E:\Installation\skywalking\apache-skywalking-apm-6.6.0\agent\skywalking-agent.jar
 * -Dskywalking.agent.service_name=admin-center
 */
@SpringBootApplication(scanBasePackages = "com.tsq")
@EnableDiscoveryClient
@EnableApiBootSwagger
@EnableCaching
public class IntelligenceCenterProviderApplication {
    public static void main(String[] args) {
        SpringApplication.run(IntelligenceCenterProviderApplication.class, args);
    }
}
