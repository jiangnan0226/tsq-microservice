package com.tsq.intelligence.provider.service;

import com.tsq.common.model.ResponseData;
import com.tsq.intelligence.api.dto.*;
import com.tsq.intelligence.api.vo.*;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.ParseException;
import java.util.Map;

public interface ITextService {

    /**
     * 手写文字识别
     */
    HandWritingVO transferImageToText(HandWritingDTO handWritingDTO, MultipartFile file) throws IOException, ParseException;

    /**
     * 印刷体文字识别
     *
     * @param file
     * @throws IOException
     */
    PrintWordVO transferPrintWordToText(HandWritingDTO handWritingDTO ,MultipartFile file) throws IOException;

    /**
     * 多语种印刷体文字识别
     *
     * @param handWritingDTO  file
     * @throws IOException
     */
    MorePrintWordVO transferMorePrintWordToText(HandWritingDTO handWritingDTO ,MultipartFile file) throws IOException;

    /**
     * 名片识别
     *
     * @param handWritingDTO  file
     * @throws IOException
     */
    BusinessCardVO transferCardToText(HandWritingDTO handWritingDTO ,MultipartFile file) throws IOException;

    /**
     * 身份证识别
     *
     * @param idCardDTO  file
     * @throws IOException
     */
    IdCardVO transferIdCardToText(IdCardDTO idCardDTO ,MultipartFile file) throws IOException;

    /**
     * 银行卡识别
     *
     * @param bankCardDTO  file
     * @throws IOException
     */
    BankCardVO transferBankCardToText(BankCardDTO bankCardDTO,MultipartFile file) throws IOException;

    /**
     * 营业执照识别
     *
     * @param commonDTO  file
     * @throws IOException
     */
    BusinessLicenseVO transferLicenseToText(CommonDTO commonDTO,MultipartFile file) throws IOException;

    /**
     * 增值税发票识别
     *
     * @param file
     * @throws IOException
     */
    InvoiceVO transferInvoiceToText(MultipartFile file) throws IOException;

    /**
     * 拍照速算识别
     *
     * @param file
     * @throws IOException
     */
    TakePictureToCalculateVO takePictureToCalculate(MultipartFile file) throws Exception;

    /**
     * 公式识别
     *
     * @param file
     * @throws IOException
     */
    FormulaToCalculateVO formulaToCalculate(MultipartFile file) throws Exception;

    /**
     * 指尖文字识别
     *
     * @param fingerDTO  file
     * @throws IOException
     */
    FingerToTextVO fingertipOfImageToText(FingerDTO fingerDTO,MultipartFile file) throws Exception;
}
