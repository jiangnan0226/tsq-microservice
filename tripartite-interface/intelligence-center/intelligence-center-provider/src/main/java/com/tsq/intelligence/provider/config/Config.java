package com.tsq.intelligence.provider.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
public class Config {

    @Value("${inte-key.appid}")
    private String appId;

    @Value("${inte-key.text-api-key}")
    private String textApiKey;

    @Value("${inte-key.image-api-key}")
    private String imageApiKey;

    @Value("${inte-key.content-api-key}")
    private String contentApiKey;

    @Value("${inte-key.calculate-api-key}")
    private String calculateApiKey;

    @Value("${inte-key.calculate-api-secret}")
    private String calculateApiSecret;

    @Value("${inte-key.voice-api-secret}")
    private String voiceApiSecret;

}
