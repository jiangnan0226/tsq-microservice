package com.tsq.intelligence.provider.service.impl;

import com.tsq.intelligence.api.vo.*;
import com.tsq.intelligence.provider.utils.Constant;
import com.tsq.intelligence.provider.config.Config;
import com.tsq.intelligence.provider.service.ICheckService;
import com.tsq.intelligence.provider.utils.HttpUtil;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;


@Service
@Transactional(rollbackFor = Exception.class)
public class CheckServiceImpl implements ICheckService {

    @Autowired
    private Config config;

    /**
     * 组装场景识别  http请求头
     */
    private Map<String, String> buildHttpHeader(String imageName) throws UnsupportedEncodingException {
        String curTime = System.currentTimeMillis() / 1000L + "";
        String param = "{\"image_name\":\"" + imageName + "\"}";
        String paramBase64 = new String(Base64.encodeBase64(param.getBytes("UTF-8")));
        String checkSum = DigestUtils.md5Hex(config.getImageApiKey() + curTime + paramBase64);
        Map<String, String> header = new HashMap<String, String>();
        header.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
        header.put("X-Param", paramBase64);
        header.put("X-CurTime", curTime);
        header.put("X-CheckSum", checkSum);
        header.put("X-Appid", config.getAppId());
        return header;
    }

    /**
     * 物体识别
     * @param  file
     * @throws IOException
     */
    @Override
    public CurrencyCheckVO currencyCheck(MultipartFile file) throws IOException {
        String result = getSceneResult(file,Constant.CURRENCY_URL);
        CurrencyCheckVO currency = CurrencyCheckVO.retData(result);
        return currency;
    }

    /**
     *场景识别
     *
     * @param file
     * @throws IOException
     */
    @Override
    public SceneCheckVO imageCheck(MultipartFile file) throws IOException {
        String result = getSceneResult(file,Constant.SCENE_URL);
        SceneCheckVO sceneCheckVO = SceneCheckVO.retData(result);
        return sceneCheckVO;
    }

    /**
     * 获取请求结果  场景公共
     * @param file
     * @param httpURL
     * @return
     * @throws IOException
     */
    private String getSceneResult(MultipartFile file,String httpURL) throws IOException{
        String result = "";
        Map<String, String> header = buildHttpHeader(file.getOriginalFilename());
        byte[] imageByteArray = file.getBytes();
        result = HttpUtil.doPost(Constant.SCENE_URL, header, imageByteArray);
        return result;
    }

    /**
     * 组装内容审核 http请求头
     */
    private Map<String, String> buildContentHeader() throws UnsupportedEncodingException {
        String curTime = System.currentTimeMillis() / 1000L + "";
        String checkSum = DigestUtils.md5Hex(config.getContentApiKey() + curTime);
        Map<String, String> header = new HashMap<String, String>();
        header.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
        header.put("X-CurTime", curTime);
        header.put("X-CheckSum", checkSum);
        header.put("X-Appid", config.getAppId());
        return header;
    }

    /**
     * 色情内容过滤  WebAPI 调用示例程序
     *
     * @param file
     * @throws IOException
     */
    @Override
    public PornCheckVO pornCheck(MultipartFile file) throws IOException {
        String result = getResult(file,Constant.SEXY_URL);
        PornCheckVO pornCheckVO = PornCheckVO.retData(result);
        return pornCheckVO;
    }

    /**
     * 政治人物检查 WebAPI 调用示例程序
     *
     * @param file
     * @throws IOException
     */
    @Override
    public PoliticianCheckVO politicianCheck(MultipartFile file) throws IOException {
        String result = getResult(file,Constant.CELEBRITY_URL);
        PoliticianCheckVO politicianCheckVO = PoliticianCheckVO.retData(result);
        return politicianCheckVO;
    }

    /**
     * 暴恐信息过滤 WebAPI 调用示例程序
     *
     * @param file
     * @throws IOException
     */
    @Override
    public SensitiveCheckVO sensitiveCheck(MultipartFile file) throws IOException {
        String result = getResult(file,Constant.TERROT_URL);
        SensitiveCheckVO sensitiveCheckVO = SensitiveCheckVO.retData(result);
        return sensitiveCheckVO;
    }

    /**
     * 广告过滤 WebAPI 调用示例程序
     *
     * @param file
     * @throws IOException
     */
    @Override
    public AdvertisingCheckVO advertisingCheck(MultipartFile file) throws IOException {
        String result = getResult(file,Constant.ADVERTISE_URL);
        AdvertisingCheckVO advertisingCheckVO = AdvertisingCheckVO.retData(result);
        return advertisingCheckVO;
    }

    /**
     * 获取请求结果  公共
     * @param file
     * @param httpURL
     * @return
     * @throws IOException
     */
    private String getResult(MultipartFile file,String httpURL) throws IOException {
        String result = "";
        Map<String, String> header = buildContentHeader();
        byte[] imageByteArray = file.getBytes();
        String imageBase64 = new String(Base64.encodeBase64(imageByteArray), "UTF-8");
        result = HttpUtil.doPost(httpURL, header, "file=" + URLEncoder.encode(imageBase64, "UTF-8"));
        return result;
    }

}
