package com.tsq.intelligence.provider.utils;

/**
 * @Author：ly
 * @Description:
 * @Date: 2020/7/2 0002 上午 10:26
 */
public class Constant {

    // OCR手写文字识别webapi接口地址
    public static final String HAND_WRITING_URL = "https://webapi.xfyun.cn/v1/service/v1/ocr/handwriting";

    // OCR印刷文字识别webapi接口地址
    public static final String GENERAL_URL = "http://webapi.xfyun.cn/v1/service/v1/ocr/general";

    // OCR印刷文字识别webapi接口地址
    public static final String MORE_GENERAL_URL = "http://webapi.xfyun.cn/v1/service/v1/ocr/recognize_document";

    // OCR名片识别 webapi 接口地址
    public static final String BUSINESS_CARD_URL = "http://webapi.xfyun.cn/v1/service/v1/ocr/business_card";

    // OCR身份证识别 webapi 接口地址
    public static final String ID_CARD_URL = "http://webapi.xfyun.cn/v1/service/v1/ocr/idcard";

    // OCR银行卡识别 webapi 接口地址
    public static final String BANK_CARD_URL = "http://webapi.xfyun.cn/v1/service/v1/ocr/bankcard";

    // OCR 营业执照识别 webapi 接口地址
    public static final String LICENSE_URL = "http://webapi.xfyun.cn/v1/service/v1/ocr/business_license";

    // OCR 增值税发票识别 webapi 接口地址
    public static final String INVOICE_URL = "http://webapi.xfyun.cn/v1/service/v1/ocr/invoice";

    // OCR 拍照速算识别/公式识别 webapi 接口地址
    public static final String WEBITR_URL = "https://rest-api.xfyun.cn/v2/itr";

    // 指尖文字识别 webapi 接口地址
    public static final String FINGERTIP_URL = "https://tyocr.xfyun.cn/v2/ocr";

    // 名片识别  引擎类型
    public static final String BUSINESS_CARD = "business_card";

    // 多语种印刷  引擎类型
    public static final String RECOGNIZE_DOCUNMENT = "recognize_document";

    // 身份证识别  引擎类型
    public static final String ID_CARD = "idcard";

    // 银行卡识别  引擎类型
    public static final String BANK_CARD = "bankcard";

    // 营业执照识别 引擎类型
    public static final String BUSINESS_LICENSE = "business_license";

    // 增值税识别 引擎类型
    public static final String INVOICE = "invoice";

    // 物体识别 webapi 接口地址
    public static final String CURRENCY_URL = "http://tupapi.xfyun.cn/v1/currency";

    // 场景识别 webapi 接口地址
    public static final String SCENE_URL = "http://tupapi.xfyun.cn/v1/scene";

    //色情内容过滤 webapi 接口地址
    public static final String SEXY_URL = "http://api.xfyun.cn/v1/service/v1/image_identify/sexy_filter";

    //政治任务检查 webapi 接口地址
    public static final String CELEBRITY_URL = "http://api.xfyun.cn/v1/service/v1/image_identify/celebrity_filter";

    //暴恐敏感信息 webapi 接口地址
    public static final String TERROT_URL = "http://api.xfyun.cn/v1/service/v1/image_identify/terror_filter";

    //广告过滤 webapi 接口地址
    public static final String ADVERTISE_URL = "http://api.xfyun.cn/v1/service/v1/image_identify/ad_filter";

    //语音转写 url
    public static final String LFASR_URL = "http://raasr.xfyun.cn/api";

    public static final String PREPARE = "/prepare";

    public static final String UPLOAD = "/upload";

    public static final String MERGE = "/merge";

    public static final String GET_RESULT = "/getResult";

    public static final String GET_PROGRESS = "/getProgress";

    //文件分片大小,可根据实际情况调整 10M
    public static final int SLICE_SIZE = 10485760;

    //成功code
    public static final String SUCCESS = "0";
}
