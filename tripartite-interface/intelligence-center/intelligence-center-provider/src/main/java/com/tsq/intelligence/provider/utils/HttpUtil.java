package com.tsq.intelligence.provider.utils;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpRequest;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Http请求工具类  by lvyang
 */
@Slf4j
public class HttpUtil {
    private HttpUtil() {

    }

    /**
     * 发送post请求(byte[])
     * @param url
     * @param header
     * @param body
     * @return
     */
    public static String doPost(String url, Map<String, String> header, byte[] body){
        String result = "";
        try {
            result= HttpRequest.post(url).headerMap(header,true).body(body).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());
        }
        return result;
    }

    /**
     * 发送post请求(字符串)
     * @param url
     * @param header
     * @param body
     * @return
     */
    public static String doPost(String url, Map<String, String> header, String body){
        String result = "";
        try {
            result= HttpRequest.post(url).headerMap(header,true).body(body).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());
        }
        return result;
    }

    /**
     * 发送post请求(map)
     * @param url
     * @param prepareParam
     * @return
     */
    public static String doPost(String url, Map<String, String> header, Map<String, Object> prepareParam) {
        String result = "";
        try {
            result = HttpRequest.post(url).headerMap(header,true).form(prepareParam).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());
        }
        return result;
    }

    /**
     * 发送post请求(map,bodyByte,fileName)
     * @param url
     * @param body
     * @return
     */
    public static String doPost(String url, Map<String, String> header,Map<String, Object> body, byte[] bodyByte,String fileName) {
        String result = "";
        try {
            result = HttpRequest.post(url).headerMap(header,true).form(body).form("content",bodyByte,fileName).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());
        }
        return result;
    }
}
