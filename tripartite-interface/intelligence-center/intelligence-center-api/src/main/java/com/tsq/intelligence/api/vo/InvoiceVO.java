package com.tsq.intelligence.api.vo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@Data
public class InvoiceVO implements Serializable{

    private static final long serialVersionUID = 8136920766492805485L;

    @ApiModelProperty(value = "结果码")
    private String code;

    @ApiModelProperty(value = "描述")
    private String desc;

    @ApiModelProperty(value = "会话ID")
    private String sid;

    @ApiModelProperty(value = "返回数据")
    private BusinessLicenseVO.DataBean data;

    @NoArgsConstructor
    @Data
    public static class DataBean{

        @ApiModelProperty(value = "错误码")
        private Integer errorCode;

        @ApiModelProperty(value = "错误提示")
        private String errorMsg;

        @ApiModelProperty(value = "识别耗时")
        private TimeCostBean timeCost;

        @ApiModelProperty(value = "类型")
        private String type;

        @ApiModelProperty(value = "校验码")
        private String vatInvoiceCorrectCode;

        @ApiModelProperty(value = "校验码位置")
        private VatInvoiceCorrectCodePosBean vatInvoiceCorrectCodePos;

        @ApiModelProperty(value = "发票代码")
        private String vatInvoiceDaima;

        @ApiModelProperty(value = "发票代码位置")
        private VatInvoiceDaimaPosBean vatInvoiceDaimaPos;

        @ApiModelProperty(value = "货物或服务名称")
        private String vatInvoiceGoodsList;

        @ApiModelProperty(value = "货物或服务名称位置")
        private VatInvoiceGoodsListPosBean vatInvoiceGoodsListPos;

        @ApiModelProperty(value = "发票号码")
        private String vatInvoiceHaoma;

        @ApiModelProperty(value = "发票号码位置")
        private VatInvoiceHaomaPosBean vatInvoiceHaomaPos;

        @ApiModelProperty(value = "开票日期")
        private String vatInvoiceIssueDate;

        @ApiModelProperty(value = "开票日期位置")
        private VatInvoiceIssueDatePosBean vatInvoiceIssueDatePos;

        @ApiModelProperty(value = "机打号码")
        private String vatInvoiceJidaHaoma;

        @ApiModelProperty(value = "购买方名称")
        private String vatInvoicePayerName;

        @ApiModelProperty(value = "购买方名称位置")
        private VatInvoicePayerNamePosBean vatInvoicePayerNamePos;

        @ApiModelProperty(value = "金额明细")
        private String vatInvoicePriceList;

        @ApiModelProperty(value = "金额明细位置")
        private VatInvoicePriceListPosBean vatInvoicePriceListPos;

        @ApiModelProperty(value = "纳税人识别号")
        private String vatInvoiceRatePayerId;

        @ApiModelProperty(value = "会话ID")
        private VatInvoiceRatePayerIdPosBean vatInvoiceRatePayerIdPos;

        @ApiModelProperty(value = "销售方地址电话")
        private String vatInvoiceSellerAddrTell;

        @ApiModelProperty(value = "销售方地址电话位置")
        private VatInvoiceSellerAddrTellPosBean vatInvoiceSellerAddrTellPos;

        @ApiModelProperty(value = "销售方开户行及账号")
        private String vatInvoiceSellerBankAccount;

        @ApiModelProperty(value = "销售方开户行及账号位置")
        private VatInvoiceSellerBankAccountPosBean vatInvoiceSellerBankAccountPos;

        @ApiModelProperty(value = "销售方纳税人识别号")
        private String vatInvoiceSellerId;

        @ApiModelProperty(value = "销售方纳税人识别号位置")
        private VatInvoiceSellerIdPosBean vatInvoiceSellerIdPos;

        @ApiModelProperty(value = "销售方名称")
        private String vatInvoiceSellerName;

        @ApiModelProperty(value = "销售方名称位置")
        private VatInvoiceSellerNamePosBean vatInvoiceSellerNamePos;

        @ApiModelProperty(value = "税额明细")
        private String vatInvoiceTaxList;

        @ApiModelProperty(value = "税额明细位置")
        private VatInvoiceTaxListPosBean vatInvoiceTaxListPos;

        @ApiModelProperty(value = "税率")
        private String vatInvoiceTaxRate;

        @ApiModelProperty(value = "税率明细")
        private String vatInvoiceTaxRateList;

        @ApiModelProperty(value = "税率明细位置")
        private VatInvoiceTaxRateListPosBean vatInvoiceTaxRateListPos;

        @ApiModelProperty(value = "税率位置")
        private VatInvoiceTaxRatePosBean vatInvoiceTaxRatePos;

        @ApiModelProperty(value = "税额合计")
        private String vatInvoiceTaxTotal;

        @ApiModelProperty(value = "税额合计位置")
        private VatInvoiceTaxTotalPosBean vatInvoiceTaxTotalPos;

        @ApiModelProperty(value = "合计")
        private String vatInvoiceTotal;

        @ApiModelProperty(value = "价税合计大写")
        private String vatInvoiceTotalCoverTax;

        @ApiModelProperty(value = "价税合计小写")
        private String vatInvoiceTotalCoverTaxDigits;

        @ApiModelProperty(value = "价税合计小写位置")
        private VatInvoiceTotalCoverTaxDigitsPosBean vatInvoiceTotalCoverTaxDigitsPos;

        @ApiModelProperty(value = "价税合计大写位置")
        private VatInvoiceTotalCoverTaxPosBean vatInvoiceTotalCoverTaxPos;

        @ApiModelProperty(value = "合计位置")
        private VatInvoiceTotalPosBean vatInvoiceTotalPos;

        @ApiModelProperty(value = "发票类型")
        private String vatInvoiceType;

        @ApiModelProperty(value = "专票/普票")
        private String vatInvoiceZhuanYongFlag;

        @NoArgsConstructor
        @Data
        public static class TimeCostBean{

            @ApiModelProperty(value = "")
            private Integer preprocess;

            @ApiModelProperty(value = "")
            private Integer recognize;
        }

        @NoArgsConstructor
        @Data
        public static class VatInvoiceCorrectCodePosBean  {

            @ApiModelProperty(value = "高")
            private Integer height;

            @ApiModelProperty(value = "左")
            private Integer left;

            @ApiModelProperty(value = "上")
            private Integer top;

            @ApiModelProperty(value = "宽")
            private Integer width;
        }

        @NoArgsConstructor
        @Data
        public static class VatInvoiceDaimaPosBean {

            @ApiModelProperty(value = "高")
            private Integer height;

            @ApiModelProperty(value = "左")
            private Integer left;

            @ApiModelProperty(value = "上")
            private Integer top;

            @ApiModelProperty(value = "宽")
            private Integer width;
        }

        @NoArgsConstructor
        @Data
        public static class VatInvoiceGoodsListPosBean  {

            @ApiModelProperty(value = "高")
            private Integer height;

            @ApiModelProperty(value = "左")
            private Integer left;

            @ApiModelProperty(value = "上")
            private Integer top;

            @ApiModelProperty(value = "宽")
            private Integer width;
        }

        @NoArgsConstructor
        @Data
        public static class VatInvoiceHaomaPosBean {

            @ApiModelProperty(value = "高")
            private Integer height;

            @ApiModelProperty(value = "左")
            private Integer left;

            @ApiModelProperty(value = "上")
            private Integer top;

            @ApiModelProperty(value = "宽")
            private Integer width;
        }

        @NoArgsConstructor
        @Data
        public static class VatInvoiceIssueDatePosBean{

            @ApiModelProperty(value = "高")
            private Integer height;

            @ApiModelProperty(value = "左")
            private Integer left;

            @ApiModelProperty(value = "上")
            private Integer top;

            @ApiModelProperty(value = "宽")
            private Integer width;
        }

        @NoArgsConstructor
        @Data
        public static class VatInvoicePayerNamePosBean {

            @ApiModelProperty(value = "高")
            private Integer height;

            @ApiModelProperty(value = "左")
            private Integer left;

            @ApiModelProperty(value = "上")
            private Integer top;

            @ApiModelProperty(value = "宽")
            private Integer width;
        }

        @NoArgsConstructor
        @Data
        public static class VatInvoicePriceListPosBean{

            @ApiModelProperty(value = "高")
            private Integer height;

            @ApiModelProperty(value = "左")
            private Integer left;

            @ApiModelProperty(value = "上")
            private Integer top;

            @ApiModelProperty(value = "宽")
            private Integer width;
        }

        @NoArgsConstructor
        @Data
        public static class VatInvoiceRatePayerIdPosBean{

            @ApiModelProperty(value = "高")
            private Integer height;

            @ApiModelProperty(value = "左")
            private Integer left;

            @ApiModelProperty(value = "上")
            private Integer top;

            @ApiModelProperty(value = "宽")
            private Integer width;
        }

        @NoArgsConstructor
        @Data
        public static class VatInvoiceSellerAddrTellPosBean{

            @ApiModelProperty(value = "高")
            private Integer height;

            @ApiModelProperty(value = "左")
            private Integer left;

            @ApiModelProperty(value = "上")
            private Integer top;

            @ApiModelProperty(value = "宽")
            private Integer width;
        }

        @NoArgsConstructor
        @Data
        public static class VatInvoiceSellerBankAccountPosBean{

            @ApiModelProperty(value = "高")
            private Integer height;

            @ApiModelProperty(value = "左")
            private Integer left;

            @ApiModelProperty(value = "上")
            private Integer top;

            @ApiModelProperty(value = "宽")
            private Integer width;
        }

        @NoArgsConstructor
        @Data
        public static class VatInvoiceSellerIdPosBean{

            @ApiModelProperty(value = "高")
            private Integer height;

            @ApiModelProperty(value = "左")
            private Integer left;

            @ApiModelProperty(value = "上")
            private Integer top;

            @ApiModelProperty(value = "宽")
            private Integer width;
        }

        @NoArgsConstructor
        @Data
        public static class VatInvoiceSellerNamePosBean{

            @ApiModelProperty(value = "高")
            private Integer height;

            @ApiModelProperty(value = "左")
            private Integer left;

            @ApiModelProperty(value = "上")
            private Integer top;

            @ApiModelProperty(value = "宽")
            private Integer width;
        }

        @NoArgsConstructor
        @Data
        public static class VatInvoiceTaxListPosBean {

            @ApiModelProperty(value = "高")
            private Integer height;

            @ApiModelProperty(value = "左")
            private Integer left;

            @ApiModelProperty(value = "上")
            private Integer top;

            @ApiModelProperty(value = "宽")
            private Integer width;
        }

        @NoArgsConstructor
        @Data
        public static class VatInvoiceTaxRateListPosBean{

            @ApiModelProperty(value = "高")
            private Integer height;

            @ApiModelProperty(value = "左")
            private Integer left;

            @ApiModelProperty(value = "上")
            private Integer top;

            @ApiModelProperty(value = "宽")
            private Integer width;
        }

        @NoArgsConstructor
        @Data
        public static class VatInvoiceTaxRatePosBean{

            @ApiModelProperty(value = "高")
            private Integer height;

            @ApiModelProperty(value = "左")
            private Integer left;

            @ApiModelProperty(value = "上")
            private Integer top;

            @ApiModelProperty(value = "宽")
            private Integer width;
        }

        @NoArgsConstructor
        @Data
        public static class VatInvoiceTaxTotalPosBean {

            @ApiModelProperty(value = "高")
            private Integer height;

            @ApiModelProperty(value = "左")
            private Integer left;

            @ApiModelProperty(value = "上")
            private Integer top;

            @ApiModelProperty(value = "宽")
            private Integer width;
        }

        @NoArgsConstructor
        @Data
        public static class VatInvoiceTotalCoverTaxDigitsPosBean  {

            @ApiModelProperty(value = "高")
            private Integer height;

            @ApiModelProperty(value = "左")
            private Integer left;

            @ApiModelProperty(value = "上")
            private Integer top;

            @ApiModelProperty(value = "宽")
            private Integer width;
        }

        @NoArgsConstructor
        @Data
        public static class VatInvoiceTotalCoverTaxPosBean{
            @ApiModelProperty(value = "高")
            private Integer height;

            @ApiModelProperty(value = "左")
            private Integer left;

            @ApiModelProperty(value = "上")
            private Integer top;

            @ApiModelProperty(value = "宽")
            private Integer width;
        }

        @NoArgsConstructor
        @Data
        public static class VatInvoiceTotalPosBean{

            @ApiModelProperty(value = "高")
            private Integer height;

            @ApiModelProperty(value = "左")
            private Integer left;

            @ApiModelProperty(value = "上")
            private Integer top;

            @ApiModelProperty(value = "宽")
            private Integer width;
        }
    }

    /**
     * 构造返回数据
     * @param result
     * @return
     */
    public static InvoiceVO retData(String result){
        InvoiceVO invoiceVO = JSON.parseObject(result,new TypeReference<InvoiceVO>() {});
        return invoiceVO;
    }
}
