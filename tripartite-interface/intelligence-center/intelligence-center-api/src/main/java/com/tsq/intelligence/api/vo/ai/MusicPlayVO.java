package com.tsq.intelligence.api.vo.ai;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@Data
public class MusicPlayVO implements Serializable {//3


    @ApiModelProperty(value = "会话ID")
    private String sid;

    @ApiModelProperty(value = "接口类型")
    private String type;

    @ApiModelProperty(value = "音频链接")
    private String url;

    @ApiModelProperty(value = "播报内容")
    private String text;


    /**
     * 构造返回数据
     * @param result
     * @return
     */
    public static MusicPlayVO retData(String result){
        MusicPlayVO musicPlayVO = JSON.parseObject(result,new TypeReference<MusicPlayVO>() {});
        return musicPlayVO;
    }
}
