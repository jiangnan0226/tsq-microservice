package com.tsq.intelligence.api.vo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@Data
public class PornCheckVO implements Serializable {

    private static final long serialVersionUID = 7394374368450346218L;

    @ApiModelProperty(value = "结果码")
    private String code;

    @ApiModelProperty(value = "描述")
    private String desc;

    @ApiModelProperty(value = "会话ID")
    private String sid;

    @ApiModelProperty(value = "返回数据")
    private DataBean data;

    @NoArgsConstructor
    @Data
    public static class DataBean{

        @ApiModelProperty(value = "四种类型的置信度")
        private ScoreBean score;

        @ApiModelProperty(value = "过滤采用的场景参数")
        private String scenario;

        @ApiModelProperty(value = "true表示图片确定违规（此时is_review一定为false），false表示图片可能违规（需is_review为true）或者正常（需is_review为false），当上传多张图片时，为其中最严重的图片的结果")
        private Boolean isPorn;

        @ApiModelProperty(value = "true表示图片可能违规（此时is_porn一定为false），false表示图片一定违规（需is_porn为true）或者正常（需is_porn为false），当上传多张图片时，为其中最严重的图片的结果")
        private Boolean isReview;

        @ApiModelProperty(value = "图片违规的置信度")
        private Double illegalConfidence;

        @ApiModelProperty(value = "图片唯一标识")
        private String imageId;

        @NoArgsConstructor
        @Data
        public static class ScoreBean{

            @ApiModelProperty(value = "正常的置信度")
            private Double normal;

            @ApiModelProperty(value = "性感的置信度")
            private Double sexy;

            @ApiModelProperty(value = "高度性感的置信度")
            private Double porn;

            @ApiModelProperty(value = "色情的置信度")
            private Double verySexy;
        }
    }

    /**
     * 构造返回数据
     * @param result
     * @return
     */
    public static PornCheckVO retData(String result){
        PornCheckVO pornCheckVO = JSON.parseObject(result,new TypeReference<PornCheckVO>() {});
        return pornCheckVO;
    }
}
