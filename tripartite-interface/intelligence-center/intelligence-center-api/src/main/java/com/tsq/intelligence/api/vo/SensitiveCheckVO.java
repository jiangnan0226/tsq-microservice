package com.tsq.intelligence.api.vo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@NoArgsConstructor
@Data
public class SensitiveCheckVO implements Serializable {

    private static final long serialVersionUID = -5090434351209139984L;

    @ApiModelProperty(value = "结果码")
    private String code;

    @ApiModelProperty(value = "描述")
    private String desc;

    @ApiModelProperty(value = "会话ID")
    private String sid;

    @ApiModelProperty(value = "返回数据")
    private DataBean data;

    @NoArgsConstructor
    @Data
    public static class DataBean{

        @ApiModelProperty(value = "图片唯一标识")
        private String imageId;

        @ApiModelProperty(value = "图片类别")
        private Integer type;

        @ApiModelProperty(value = "置信度")
        private Double confidence;

        @ApiModelProperty(value = "图片过滤后详细类别信息")
        private List<CategoriesBean> categories;

        @NoArgsConstructor
        @Data
        public static class CategoriesBean{

            @ApiModelProperty(value = "图片类别，0：正常图片，1：包含武器类信息图片，2：包含军警服饰类信息图片，3：包含宗教服饰类信息图片，4：包含国家标志类信息图片，5：包含台标类信息图片，6：包含旗帜类信息图片，7：包含恐怖血腥类信息图片（暂不支持），当同时上传多张图片时，type为所有结果中最严重的类型")
            private Integer type;

            @ApiModelProperty(value = "置信度")
            private Double confidence;
        }
    }

    /**
     * 构造返回数据
     * @param result
     * @return
     */
    public static SensitiveCheckVO retData(String result){
        SensitiveCheckVO sensitiveCheckVO = JSON.parseObject(result,new TypeReference<SensitiveCheckVO>() {});
        return sensitiveCheckVO;
    }
}
