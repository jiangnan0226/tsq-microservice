package com.tsq.intelligence.api.vo.ai;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@Data
public class WeatherVO implements Serializable {//5

    @ApiModelProperty(value = "会话ID")
    private String sid;

    @ApiModelProperty(value = "接口类型")
    private String type;

    @ApiModelProperty(value = "播报内容")
    private String text;

    @ApiModelProperty(value = "图片")
    private String img = "";

    @ApiModelProperty(value = "现在温度")
    private String temp;

    @ApiModelProperty(value = "天气情况")
    private String weather;

    @ApiModelProperty(value = "温度总概")
    private String tempRange;

    @ApiModelProperty(value = "湿度")
    private String humidity;

    @ApiModelProperty(value = "风向")
    private String wind;

    @ApiModelProperty(value = "紫外线")
    private String uv;

    @ApiModelProperty(value = "穿衣")
    private String ct;

    @ApiModelProperty(value = "感冒")
    private String gm;

    @ApiModelProperty(value = "洗车")
    private String xc;

    @ApiModelProperty(value = "运动")
    private String yd;

    /**
     * 构造返回数据
     * @param result
     * @return
     */
    public static WeatherVO retData(String result){
        WeatherVO hotelSearchVO = JSON.parseObject(result,new TypeReference<WeatherVO>() {});
        return hotelSearchVO;
    }
}
