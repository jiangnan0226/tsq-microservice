package com.tsq.intelligence.api.vo.ai;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@NoArgsConstructor
@Data
public class MapUVO implements Serializable {//1

    @ApiModelProperty(value = "会话ID")
    private String sid;

    @ApiModelProperty(value = "接口类型")
    private String type;

    @ApiModelProperty(value = "返回类型")
    private String reType;

    @ApiModelProperty(value = "关键字")
    private String value;

    @ApiModelProperty(value = "播报内容")
    private String text;

    @ApiModelProperty(value = "返回数据")
    private List<DataBean> item;

    @NoArgsConstructor
    @Data
    public static class DataBean {
        @ApiModelProperty(value = "名称")
        private String name;

        @ApiModelProperty(value = "与中心点距离（输入GPS时提供）")
        private String distance;

        @ApiModelProperty(value = "图片")
        private String img = "";

        @ApiModelProperty(value = "纬度")
        private String latitude;

        @ApiModelProperty(value = "经度")
        private String longitude;

        @ApiModelProperty(value = "地点类型")
        private String category;
    }

    /**
     * 构造返回数据
     * @param result
     * @return
     */
    public static MapUVO retData(String result){
        MapUVO mapUVO = JSON.parseObject(result,new TypeReference<MapUVO>() {});
        return mapUVO;
    }

}
