package com.tsq.intelligence.api.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class HandWritingDTO extends CommonDTO implements Serializable{

    private static final long serialVersionUID = -8081524509625773758L;

    @ApiModelProperty(value = "语言，可选值：en（英文），cn|en（中文或中英混合）")
    private String language;

    @ApiModelProperty(value = "是否返回文本位置信息，可选值：false（否），true（是），默认为false")
    private String location;

}
