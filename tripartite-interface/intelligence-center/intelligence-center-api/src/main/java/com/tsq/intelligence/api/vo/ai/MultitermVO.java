package com.tsq.intelligence.api.vo.ai;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@Data
public class MultitermVO implements Serializable {//8
    @ApiModelProperty(value = "会话ID")
    private String sid;

    @ApiModelProperty(value = "接口类型")
    private String type;

    @ApiModelProperty(value = "播报内容")
    private String text;

    @ApiModelProperty(value = "链接")
    private String url = "";

    @ApiModelProperty(value = "返回类型")
    private String reType = "";



    /**
     * 构造返回数据
     * @param result
     * @return
     */
    public static MultitermVO retData(String result){
        MultitermVO multitermVO = JSON.parseObject(result,new TypeReference<MultitermVO>() {});
        return multitermVO;
    }
}
