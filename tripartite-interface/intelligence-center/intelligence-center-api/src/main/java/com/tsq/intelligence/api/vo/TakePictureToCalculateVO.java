package com.tsq.intelligence.api.vo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@NoArgsConstructor
@Data
public class TakePictureToCalculateVO implements Serializable{

    private static final long serialVersionUID = 8892606380498368086L;

    @ApiModelProperty(value = "结果码")
    private String code;

    @ApiModelProperty(value = "描述")
    private String desc;

    @ApiModelProperty(value = "会话ID")
    private String sid;

    @ApiModelProperty(value = "返回数据")
    private DataBean data;

    @NoArgsConstructor
    @Data
    public static class DataBean{

        @ApiModelProperty(value = "返回信息")
        private ITRResultBean ITRResult;

        @NoArgsConstructor
        @Data
        public static class ITRResultBean{

            @ApiModelProperty(value = "异常信息属性")
            private Integer attrException;

            @ApiModelProperty(value = "题型")
            private String category;

            @ApiModelProperty(value = "当前题型所有小题的位置等信息")
            private MultiLineInfoBean multiLineInfo;

            @ApiModelProperty(value = "接口版本号")
            private String version;

            @ApiModelProperty(value = "当前题型所有小题的识别结果")
            private List<RecogResultBean> recogResult;

            @NoArgsConstructor
            @Data
            public static class MultiLineInfoBean{

                @ApiModelProperty(value = "每个小题的位置等信息")
                private List<ImpLineInfoBean> impLineInfo;

                @NoArgsConstructor
                @Data
                public static class ImpLineInfoBean{

                    @ApiModelProperty(value = "该行公式所在的区域位置")
                    private ImpLineRectBean impLineRect;

                    @ApiModelProperty(value = "拒识标志")
                    private Integer recRejection;

                    @ApiModelProperty(value = "预留字段，无需关注")
                    private Integer strictScore;

                    @ApiModelProperty(value = "该速算题判决正误信息")
                    private Integer totalScore;

                    @NoArgsConstructor
                    @Data
                    public static class ImpLineRectBean{

                        @ApiModelProperty(value = "该行公式所在矩形区域左上角顶点的像素横坐标")
                        private Integer leftUpPointX;

                        @ApiModelProperty(value = "该行公式所在矩形区域左上角顶点的像素纵坐标")
                        private Integer leftUpPointY;

                        @ApiModelProperty(value = "该行公式所在矩形区域右下角顶点的像素横坐标")
                        private Integer rightDownPointX;

                        @ApiModelProperty(value = "该行公式所在矩形区域右下角顶点的像素纵坐标")
                        private Integer rightDownPointY;
                    }
                }
            }

            @NoArgsConstructor
            @Data
            public static class RecogResultBean{

                @ApiModelProperty(value = "预留字段，无需关注")
                private Object lineCharResult;

                @ApiModelProperty(value = "每个小题的识别结果")
                private List<LineWordResultBean> lineWordResult;

                @NoArgsConstructor
                @Data
                public static class LineWordResultBean{

                    @ApiModelProperty(value = "该行公式起始位置")
                    private List<Integer> begPos;

                    @ApiModelProperty(value = "该行公式起始位置横坐标")
                    private List<Integer> begPosX;

                    @ApiModelProperty(value = "该行公式起始位置纵坐标")
                    private List<Integer> begPosY;

                    @ApiModelProperty(value = "该行公式结束位置")
                    private List<Integer> endPos;

                    @ApiModelProperty(value = "该行公式结束位置横坐标")
                    private List<Integer> endPosX;

                    @ApiModelProperty(value = "该行公式结束位置纵坐标")
                    private List<Integer> endPosY;

                    @ApiModelProperty(value = "该行公式识别结果")
                    private List<String> wordContent;

                    @ApiModelProperty(value = "该行公式的后验概率")
                    private List<Double> wordGwpp;
                }
            }
        }
    }

    /**
     * 构造返回数据
     * @param result
     * @return
     */
    public static TakePictureToCalculateVO retData(String result){
        TakePictureToCalculateVO takePictureToCalculateVO = JSON.parseObject(result,new TypeReference<TakePictureToCalculateVO>() {});
        return takePictureToCalculateVO;
    }
}
