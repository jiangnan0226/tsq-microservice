package com.tsq.intelligence.api.vo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@NoArgsConstructor
@Data
public class SceneCheckVO implements Serializable {

    private static final long serialVersionUID = -949272983005320275L;

    @ApiModelProperty(value = "结果码")
    private String code;

    @ApiModelProperty(value = "描述")
    private String desc;

    @ApiModelProperty(value = "会话ID")
    private String sid;

    @ApiModelProperty(value = "返回数据")
    private DataBean data;

    @NoArgsConstructor
    @Data
    public static class DataBean{

        @ApiModelProperty(value = "文件列表")
        private List<FileListBean> fileList;

        @NoArgsConstructor
        @Data
        public static class FileListBean{

            @ApiModelProperty(value = "大于等于0时，表明图片属于哪个分类或结果；等于-1时，代表该图片文件有错误，或者格式不支持（gif图不支持）")
            private Integer label;

            @ApiModelProperty(value = "图片的url地址或名称")
            private String name;

            @ApiModelProperty(value = "介于0-1间的浮点数，表示该图像被识别为某个分类的概率值，概率越高、机器越肯定")
            private Double rate;

            @ApiModelProperty(value = "本次识别结果是否存在偏差，返回true时存在偏差，可信度较低，返回false时可信度较高，具体可参考rate参数值")
            private Boolean review;

            @ApiModelProperty(value = "图片标签，值为Local Image或Using Buffer(无实际意义)")
            private String tag;

            @ApiModelProperty(value = "表示前5个最可能场景的label")
            private List<Integer> labels;

            @ApiModelProperty(value = "和labels对应，前5个最可能场景对应得分")
            private List<Double> rates;
        }
    }

    /**
     * 构造返回数据
     * @param result
     * @return
     */
    public static SceneCheckVO retData(String result){
        SceneCheckVO sceneCheckVO = JSON.parseObject(result,new TypeReference<SceneCheckVO>() {});
        return sceneCheckVO;
    }
}
