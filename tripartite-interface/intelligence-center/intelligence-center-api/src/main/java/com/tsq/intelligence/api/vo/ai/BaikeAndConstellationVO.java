package com.tsq.intelligence.api.vo.ai;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@Data
public class BaikeAndConstellationVO implements Serializable {//6、7


    @ApiModelProperty(value = "会话ID")
    private String sid;

    @ApiModelProperty(value = "接口类型")
    private String type;

    @ApiModelProperty(value = "图片")
    private String img = "";

    @ApiModelProperty(value = "网址")
    private String url;

    @ApiModelProperty(value = "网址跳转")
    private String urlGoto;

    @ApiModelProperty(value = "播报内容")
    private String text;

    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "内容")
    private String summary;


    /**
     * 构造返回数据
     * @param result
     * @return
     */
    public static BaikeAndConstellationVO retData(String result){
        BaikeAndConstellationVO baikeAndConstellationVO = JSON.parseObject(result,new TypeReference<BaikeAndConstellationVO>() {});
        return baikeAndConstellationVO;
    }
}
