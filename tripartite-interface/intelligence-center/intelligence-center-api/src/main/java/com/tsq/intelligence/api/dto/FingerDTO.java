package com.tsq.intelligence.api.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class FingerDTO implements Serializable {

    private static final long serialVersionUID = 7246961809253008878L;

    @ApiModelProperty(value = "模式，选择范围：finger,finger+ocr(默认值)finger模式：只进行手指检测，返回手指位置、方向、宽度等信息,finger+ocr模式：进行手指检测以及OCR识别，返回手指指向的字、词、句信息")
    private String mode;

    @ApiModelProperty(value ="根据指尖位置选取ROI（感兴趣区域）的宽度倍数，即设置ROI的宽度是手指宽度的几倍（宽度= cut_w_scale * 手指宽度），默认3.0，取值范围：[0,65536]")
    private Float cutWScale;

    @ApiModelProperty(value ="根据指尖位置选取ROI（感兴趣区域）的高度倍数，即设置ROI的高度是手指宽度的几倍（高度= cut_h_scale * 手指宽度），默认1.0，取值范围：[0,65536]")
    private Float cutHScale;

    @ApiModelProperty(value ="根据指尖位置选取ROI（感兴趣区域）的往下平移的倍数，即设置ROI往下平移的距离是手指宽度的几倍（平移量= cut_shift * 手指宽度），默认0，取值范围：[0,1]")
    private Float cutShift;

    @ApiModelProperty(value ="引擎内部处理模块输入图像宽度，取值范围：[1,65536],若应用端上传图像宽为input_w，scale为缩放系数，则resize_w=input_w*scale,若不缩放直接按原图处理，引擎耗时会变长，建议根据实际情况测试以寻求最佳值")
    private Integer resizeW;

    @ApiModelProperty(value ="引擎内部处理模块输入图像高度，取值范围：[1,65536], 若应用端上传图像高为input_h，scale为缩放系数，则resize_h=input_h*scale, 若不缩放直接按原图处理，引擎耗时会变长，建议根据实际情况测试以寻求最佳值")
    private Integer resizeH;

}
