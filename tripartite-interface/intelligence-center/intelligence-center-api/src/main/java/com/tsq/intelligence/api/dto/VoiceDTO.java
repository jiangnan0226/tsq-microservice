package com.tsq.intelligence.api.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class VoiceDTO implements Serializable {

    private static final long serialVersionUID = -8567769842999740838L;

    @ApiModelProperty(value = "转写类型，默认 0,0: (标准版，格式: wav,flac,opus,mp3,m4a),2: (电话版，已取消)")
    private String lfasrType;

    @ApiModelProperty(value = "转写结果是否包含分词信息")
    private String hasParticiple;

    @ApiModelProperty(value = "转写结果中最大的候选词个数")
    private String maxAlternatives;

    @ApiModelProperty(value = "发音人个数，可选值：0-10，0表示盲分,注：发音人分离目前还是测试效果达不到商用标准，如测试无法满足您的需求，请慎用该功能。")
    private String speakerNumber;

    @ApiModelProperty(value = "转写结果中是否包含发音人分离信息")
    private String hasSeperate;

    @ApiModelProperty(value = "支持两种参数,1: 通用角色分离 2: 电话信道角色分离（适用于speaker_number为2的说话场景）")
    private String roleType;

    @ApiModelProperty(value = "是否需要对转写结果进行敏感词检测")
    private String hasSensitive;

    @ApiModelProperty(value = "敏感词检测类型")
    private String sensitiveType;

    @ApiModelProperty(value = "自定义的敏感词")
    private String keywords;

    @ApiModelProperty(value = "语种 cn:中英文&中文（默认） en:英文（英文不支持热词）")
    private String language;

    @ApiModelProperty(value = "垂直领域个性化参数: 法院: court 教育: edu 金融: finance 医疗: medical 科技: tech")
    private String pd;

}
