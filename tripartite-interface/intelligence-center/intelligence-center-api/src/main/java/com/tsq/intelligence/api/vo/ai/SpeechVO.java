package com.tsq.intelligence.api.vo.ai;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@NoArgsConstructor
@Data
public class SpeechVO implements Serializable {//9

    @ApiModelProperty(value = "会话ID")
    private String sid;

    @ApiModelProperty(value = "接口类型")
    private String type;

    @ApiModelProperty(value = "返回数据")
    private List<DataBean> item;

    @ApiModelProperty(value = "播报内容")
    private String text;

    @NoArgsConstructor
    @Data
    public static class DataBean {

        @ApiModelProperty(value = "视频链接")
        private String video;

        @ApiModelProperty(value = "图片")
        private String img = "";

        @ApiModelProperty(value = "标题")
        private String name;

        @ApiModelProperty(value = "内容描述")
        private String description;

        @ApiModelProperty(value = "音频链接")
        private String url;

        @ApiModelProperty(value = "种类")
        private String category;


    }


    /**
     * 构造返回数据
     * @param result
     * @return
     */
    public static SpeechVO retData(String result){
        SpeechVO speechVO = JSON.parseObject(result,new TypeReference<SpeechVO>() {});
        return speechVO;
    }
}
