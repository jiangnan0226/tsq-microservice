package com.tsq.intelligence.api.vo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@Data
public class IdCardVO implements Serializable {

    private static final long serialVersionUID = -3560381289517288947L;

    @ApiModelProperty(value = "结果码")
    private String code;

    @ApiModelProperty(value = "描述")
    private String desc;

    @ApiModelProperty(value = "会话ID")
    private String sid;

    @ApiModelProperty(value = "返回数据")
    private DataBean data;

    @NoArgsConstructor
    @Data
    public static class DataBean{

        @ApiModelProperty(value = "住址")
        private String address;

        @ApiModelProperty(value = "出生日期")
        private String birthday;

        @ApiModelProperty(value = "边缘遮挡")
        private Boolean borderCovered;

        @ApiModelProperty(value = "证件完整")
        private Boolean complete;

        @ApiModelProperty(value = "错误码")
        private Integer errorCode;

        @ApiModelProperty(value = "错误信息")
        private String errorMsg;

        @ApiModelProperty(value = "黑白图像")
        private Boolean grayImage;

        @ApiModelProperty(value = "头像模糊")
        private Boolean headBlurred;

        @ApiModelProperty(value = "边缘遮挡")
        private Boolean headCovered;

        @ApiModelProperty(value = "身份证号")
        private String idNumber;

        @ApiModelProperty(value = "姓名")
        private String name;

        @ApiModelProperty(value = "民族")
        private String people;

        @ApiModelProperty(value = "性别")
        private String sex;

        @ApiModelProperty(value = "类型")
        private String type;
    }

    /**
     * 构造返回数据
     * @param result
     * @return
     */
    public static IdCardVO retData(String result){
        IdCardVO idCard = JSON.parseObject(result,new TypeReference<IdCardVO>() {});
        return idCard;
    }
}
