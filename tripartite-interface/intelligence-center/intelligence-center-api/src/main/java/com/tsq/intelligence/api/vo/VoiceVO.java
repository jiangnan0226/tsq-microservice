package com.tsq.intelligence.api.vo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Data
public class VoiceVO implements Serializable {

    private static final long serialVersionUID = 1308751568893411676L;

    @ApiModelProperty(value = "成功码")
    private Integer ok;

    @ApiModelProperty(value = "错误码")
    private Integer errNo;

    @ApiModelProperty(value = "错误提示")
    private String failed;

    @ApiModelProperty(value = "转写结果")
    private String data;

    @ApiModelProperty(value = "句子最终内容")
    private String content;


    /**
     * 构造返回数据
     * @param result
     * @return
     */
    public static VoiceVO retData(String result){
        VoiceVO voiceVO = JSON.parseObject(result,new TypeReference<VoiceVO>() {});
        JSONArray jSONArray = JSON.parseArray(voiceVO.getData());
        StringBuilder retStr = new StringBuilder();
        jSONArray.stream().forEach(
                jsonobejct->retContent((JSONObject) jsonobejct,retStr)
        );
        voiceVO.setContent(retStr.toString());
        return voiceVO;
    }

    /**
     * 拼接返回内容
     * @param
     * @return
     */
    private static StringBuilder retContent(JSONObject jsonobejct, StringBuilder retStr) {
        return retStr.append(jsonobejct.getString("onebest"));
    }
}
