package com.tsq.intelligence.api.vo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@Data
public class FingerToTextVO implements Serializable {

    private static final long serialVersionUID = -7399798984846617998L;

    @ApiModelProperty(value = "结果码")
    private String code;

    @ApiModelProperty(value = "描述")
    private String desc;

    @ApiModelProperty(value = "会话ID")
    private String sid;

    @ApiModelProperty(value = "返回数据")
    private DataBean data;

    @NoArgsConstructor
    @Data
    public static class DataBean{

        @ApiModelProperty(value = "手指信息")
        private FingerPosBean fingerPos;

        @ApiModelProperty(value = "高")
        private Integer height;

        @ApiModelProperty(value = "识别模式：finger,finger+ocr")
        private String mode;

        @ApiModelProperty(value = "引擎版本")
        private String versionEngine;

        @ApiModelProperty(value = "json版本\t")
        private String versionJson;

        @ApiModelProperty(value = "宽")
        private Integer width;

        @NoArgsConstructor
        @Data
        public static class FingerPosBean{

            @ApiModelProperty(value = "手指方向，范围[-180,180]")
            private Double angle;

            @ApiModelProperty(value = "手指方向向量坐标（x）")
            private Integer dirX;

            @ApiModelProperty(value = "手指方向向量坐标（y）")
            private Integer dirY;

            @ApiModelProperty(value = "手指位置坐标（x）")
            private Integer posX;

            @ApiModelProperty(value = "手指位置坐标（y）")
            private Integer posY;

            @ApiModelProperty(value = "")
            private Double thickness;
        }
    }

    /**
     * 构造返回数据
     * @param result
     * @return
     */
    public static FingerToTextVO retData(String result){
        FingerToTextVO fingerToText = JSON.parseObject(result,new TypeReference<FingerToTextVO>() {});
        return fingerToText;
    }
}
