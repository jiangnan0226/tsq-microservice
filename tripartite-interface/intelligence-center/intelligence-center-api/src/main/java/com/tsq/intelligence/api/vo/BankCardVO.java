package com.tsq.intelligence.api.vo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@Data
public class BankCardVO implements Serializable {

    private static final long serialVersionUID = -3620248599262281526L;

    @ApiModelProperty(value = "结果码")
    private String code;

    @ApiModelProperty(value = "描述")
    private String desc;

    @ApiModelProperty(value = "会话ID")
    private String sid;

    @ApiModelProperty(value = "返回数据")
    private DataBean data;

    @NoArgsConstructor
    @Data
    public static class DataBean{

        @ApiModelProperty(value = "银行卡号")
        private String cardNumber;

        @ApiModelProperty(value = "错误码")
        private Integer errorCode;

        @ApiModelProperty(value = "错误信息")
        private String errorMsg;

        @ApiModelProperty(value = "持卡人")
        private String holderName;

        @ApiModelProperty(value = "发卡机构")
        private String issuer;

        @ApiModelProperty(value = "")
        private String issuerId;

        @ApiModelProperty(value = "银行卡类型")
        private String type;

        @ApiModelProperty(value = "有效期")
        private String validate;
    }

    /**
     * 构造返回数据
     * @param result
     * @return
     */
    public static BankCardVO retData(String result){
        BankCardVO bankCardVO = JSON.parseObject(result,new TypeReference<BankCardVO>() {});
        return bankCardVO;
    }
}
