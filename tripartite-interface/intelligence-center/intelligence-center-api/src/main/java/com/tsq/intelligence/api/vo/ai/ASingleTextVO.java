package com.tsq.intelligence.api.vo.ai;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@Data
public class ASingleTextVO implements Serializable {//2


    @ApiModelProperty(value = "会话ID")
    private String sid;

    @ApiModelProperty(value = "接口类型")
    private String type;

    @ApiModelProperty(value = "播报内容")
    private String text = "";

    @ApiModelProperty(value = "操作类型")
    private String intent = "";

    @ApiModelProperty(value = "关键字")
    private String value = "";


    /**
     * 构造返回数据
     * @param result
     * @return
     */
    public static ASingleTextVO retData(String result){
        ASingleTextVO guessNumberVO = JSON.parseObject(result,new TypeReference<ASingleTextVO>() {});
        return guessNumberVO;
    }
}
