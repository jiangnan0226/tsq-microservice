package com.tsq.intelligence.api.vo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@NoArgsConstructor
@Data
public class BusinessCardVO implements Serializable {

    private static final long serialVersionUID = 8698619062733122048L;

    @ApiModelProperty(value ="名片切边增强图像，jpg/jpeg格式，二进制数据Base64编码（使用前注意解码）")
    private String bizCardPic;

    @ApiModelProperty(value = "结果码")
    private String code;

    @ApiModelProperty(value = "返回数据")
    private DataBean data;

    @ApiModelProperty(value = "描述")
    private String desc;

    @ApiModelProperty(value = "会话ID")
    private String sid;

    @NoArgsConstructor
    @Data
    public static class DataBean{

        @ApiModelProperty(value = "")
        private String rotationAngle;

        @ApiModelProperty(value = "一个结构化的标示形式，表示物理地址")
        private List<AddressBean> address;

        @ApiModelProperty(value = "一个结构化的标示形式，表示物理地址")
        private List<EmailBean> email;

        @ApiModelProperty(value = "显示完整姓名")
        private List<FormattedNameBean> formattedName;

        @ApiModelProperty(value = "个人或对象的物理邮件投递或交付地址")
        private List<LabelBean> label;

        @ApiModelProperty(value = "一个结构化的表示形式，表示人的姓氏，名字或其他信息")
        private List<NameBean> name;

        @ApiModelProperty(value = "单位或组织的可选名称")
        private List<OrganizationBean> organization;

        @ApiModelProperty(value = "电话号码，电话通信的规范数字字符串")
        private List<TelephoneBean> telephone;

        @ApiModelProperty(value = "代表个人在公司或组织内的职位，职能或其他相关属性")
        private List<TitleBean> title;

        @ApiModelProperty(value = "包含一个URL的值")
        private List<UrlBean> url;

        @NoArgsConstructor
        @Data
        public static class AddressBean{

            @ApiModelProperty(value = "具体内容")
            private ItemBean item;

            @ApiModelProperty(value = "位置")
            private String position;

            @NoArgsConstructor
            @Data
            public static class ItemBean{

                @ApiModelProperty(value = "国家")
                private String country;

                @ApiModelProperty(value = "城市")
                private String locality;

                @ApiModelProperty(value = "街道")
                private String street;

                @ApiModelProperty(value = "地址类型")
                private List<String> type;
            }
        }

        @NoArgsConstructor
        @Data
        public static class EmailBean{

            @ApiModelProperty(value = "邮箱")
            private String item;

            @ApiModelProperty(value = "位置")
            private String position;
        }

        @NoArgsConstructor
        @Data
        public static class FormattedNameBean{

            @ApiModelProperty(value = "名字")
            private String item;

            @ApiModelProperty(value = "位置")
            private String position;

        }

        @NoArgsConstructor
        @Data
        public static class LabelBean{

            @ApiModelProperty(value = "个人或对象的物理邮件投递或交付地址")
            private ItemBeanX item;

            @ApiModelProperty(value = "位置")
            private String position;

            @NoArgsConstructor
            @Data
            public static class ItemBeanX{

                @ApiModelProperty(value = "个人或对象的物理邮件投递或交付地址")
                private String address;

                @ApiModelProperty(value = "类型")
                private List<String> type;

            }
        }

        @NoArgsConstructor
        @Data
        public static class NameBean{

            @ApiModelProperty(value = "个人或对象的物理邮件投递或交付地址")
            private ItemBeanXX item;

            @ApiModelProperty(value = "位置")
            private String position;

            @NoArgsConstructor
            @Data
            public static class ItemBeanXX{

                @ApiModelProperty(value = "姓")
                private String familyName;

                @ApiModelProperty(value = "名")
                private String givenName;
            }
        }

        @NoArgsConstructor
        @Data
        public static class OrganizationBean{

            @ApiModelProperty(value = "单位或组织的可选名称")
            private ItemBeanXXX item;

            @ApiModelProperty(value = "位置")
            private String position;

            @NoArgsConstructor
            @Data
            public static class ItemBeanXXX {

                @ApiModelProperty(value = "单位或组织的可选名称")
                private String name;

            }
        }

        @NoArgsConstructor
        @Data
        public static class TelephoneBean{

            @ApiModelProperty(value = "电话号码，电话通信的规范数字字符串")
            private ItemBeanXXXX item;

            @ApiModelProperty(value = "位置")
            private String position;

            @NoArgsConstructor
            @Data
            public static class ItemBeanXXXX{

                @ApiModelProperty(value = "电话号码，电话通信的规范数字字符串")
                private String number;

                @ApiModelProperty(value = "类型")
                private List<String> type;

            }
        }

        @NoArgsConstructor
        @Data
        public static class TitleBean{

            @ApiModelProperty(value = "代表个人在公司或组织内的职位，职能或其他相关属性")
            private String item;

            @ApiModelProperty(value = "位置")
            private String position;

        }

        @NoArgsConstructor
        @Data
        public static class UrlBean{

            @ApiModelProperty(value = "包含一个URL的值")
            private String item;

            @ApiModelProperty(value = "位置")
            private String position;

        }
    }


    /**
     * 构造返回数据
     * @param result
     * @return
     */
    public static BusinessCardVO retData(String result){
        BusinessCardVO businessCardVO = JSON.parseObject(result,new TypeReference<BusinessCardVO>() {});
        return businessCardVO;
    }



}
