package com.tsq.intelligence.api.vo.ai;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@NoArgsConstructor
@Data
public class NewsVO implements Serializable {//4

    @ApiModelProperty(value = "会话ID")
    private String sid;

    @ApiModelProperty(value = "接口类型")
    private String type;

    @ApiModelProperty(value = "播报内容")
    private String text = "";

    @ApiModelProperty(value = "返回数据")
    private List<DataBean> item;

    @NoArgsConstructor
    @Data
    public static class DataBean {
        @ApiModelProperty(value = "图片链接")
        private String img = "";

        @ApiModelProperty(value = "标题")
        private String text;

        @ApiModelProperty(value = "新闻链接（根据新闻媒体类型返回。0：页面地址；1：音频链接；2：视频链接。）")
        private String url;

        @ApiModelProperty(value = "新闻媒体类型（0：文本新闻；1：音频新闻；2：视频新闻）")
        private String type;

        @ApiModelProperty(value = "新闻介绍")
        private String description = "";

        @ApiModelProperty(value = "新闻来源")
        private String source;

        @ApiModelProperty(value = "新闻名称")
        private String title;


    }

    /**
     * 构造返回数据
     * @param result
     * @return
     */
    public static NewsVO retData(String result){
        NewsVO hotelSearchVO = JSON.parseObject(result,new TypeReference<NewsVO>() {});
        return hotelSearchVO;
    }
}
