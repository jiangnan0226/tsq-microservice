package com.tsq.intelligence.api.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class CommonDTO implements Serializable {

    private static final long serialVersionUID = -7606681767961603854L;

    @ApiModelProperty(value = "手机序列号")
    private String imei;

    @ApiModelProperty(value = "操作系统版本")
    private String osid;

    @ApiModelProperty(value = "厂商|全称|机型信息|操作系统版本|分辨率")
    private String ua;
}
