package com.tsq.intelligence.api.vo;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class HandWritingVO implements Serializable {

    private static final long serialVersionUID = -28077984874828172L;

    @ApiModelProperty(value = "结果码")
    private String code;

    @ApiModelProperty(value = "描述")
    private String desc;

    @ApiModelProperty(value = "会话ID")
    private String sid;

    @ApiModelProperty(value = "识别内容")
    private List<String> wordList;

    @ApiModelProperty(value = "返回数据")
    private data data;

    @Data
    public static class data {

        @ApiModelProperty(value = "区域块信息")
        private List<Block> block;

    }

    @Data
    public static class Block{

        @ApiModelProperty(value = "行信息")
        private List<Line> line;

        @ApiModelProperty(value = "区域块类型（text-文本，image-图片）")
        private String type;

    }

    @Data
    public static class Line{

        @ApiModelProperty(value = "后验概率")
        private String confidence;

        @ApiModelProperty(value = "行信息")
        private List<Word> word;

        @ApiModelProperty(value = "位置信息")
        private Location location;
    }

    @Data
    public static class Word{

        @ApiModelProperty(value = "内容")
        private String content;

    }

    @Data
    public static class Location{

        @ApiModelProperty(value = "右下角位置信息")
        private RightBottom rightBottom;

        @ApiModelProperty(value = "右下角位置信息")
        private TopLeft topLeft;

    }

    @Data
    public static class RightBottom{

        @ApiModelProperty(value = "右下角位置信息x 像素")
        private Integer x;

        @ApiModelProperty(value = "右下角位置信息y 像素")
        private Integer y;

    }

    @Data
    public static class TopLeft{

        @ApiModelProperty(value = "左下角位置信息x 像素")
        private Integer x;

        @ApiModelProperty(value = "左下角位置信息y 像素")
        private Integer y;

    }

    /**
     * 构造返回数据
     * @param result
     * @return
     */
    public static HandWritingVO retData(String result){
        HandWritingVO handWriting = JSON.parseObject(result,new TypeReference<HandWritingVO>() {});
        List<String> retList = new ArrayList<String>();
        List<Block> blockList = handWriting.getData().getBlock();
        if(null!=blockList && blockList.size()>0){
            blockList.stream().forEach(
                    block -> {
                        StringBuilder retStr = new StringBuilder();
                        List<Line> lineList = block.getLine();
                        if(null!=lineList && lineList.size()>0) {
                            lineList.stream().forEach(
                                    line -> {
                                        List<Word> wordList = line.getWord();
                                        wordList.stream().forEach(
                                                word ->{
                                                    retStr.append(word.getContent() + " ");
                                                }
                                        );
                                    }
                            );
                        }
                        retList.add(retStr.toString());
                        retStr.delete(0,retStr.length());
                    }
            );
        }
        handWriting.setWordList(retList);
        return handWriting;
    }

}
