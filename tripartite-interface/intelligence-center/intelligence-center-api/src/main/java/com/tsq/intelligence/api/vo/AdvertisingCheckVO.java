package com.tsq.intelligence.api.vo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@NoArgsConstructor
@Data
public class AdvertisingCheckVO implements Serializable {

    private static final long serialVersionUID = 4136152558555123988L;

    @ApiModelProperty(value = "结果码")
    private String code;

    @ApiModelProperty(value = "描述")
    private String desc;

    @ApiModelProperty(value = "会话ID")
    private String sid;

    @ApiModelProperty(value = "返回数据")
    private DataBean data;

    @NoArgsConstructor
    @Data
    public static class DataBean {

        @ApiModelProperty(value = "是否包含广告文本，如明确售卖信息、联系方式、微信手机qq号等,多张图片时，为最严重的结果")
        private Boolean hasAdtext;

        @ApiModelProperty(value = "图片中出现色情广告文本")
        private Boolean hasPtext;

        @ApiModelProperty(value = "是否包含二维码，多张图片时，为最严重的结果")
        private Boolean hasQr;

        @ApiModelProperty(value = "详情列表")
        private List<DetailsBean> details;

        @NoArgsConstructor
        @Data
        public static class DetailsBean{

            @ApiModelProperty(value = "是否包含广告文本，如明确售卖信息、联系方式、微信手机qq号等,多张图片时，为最严重的结果")
            private Boolean hasAdtext;

            @ApiModelProperty(value = "图片中出现色情广告文本")
            private Boolean hasPtext;

            @ApiModelProperty(value = "是否包含二维码，多张图片时，为最严重的结果")
            private Boolean hasQr;

            @ApiModelProperty(value = "图片唯一标识")
            private String imageId;

            @ApiModelProperty(value = "图片路径")
            private String imageName;

            @ApiModelProperty(value = "图片中所有识别到的文本内容，每一个block是一段文本")
            private List<TextsBean> texts;

            @NoArgsConstructor
            @Data
            public static class TextsBean {

                @ApiModelProperty(value = "文本内容")
                private String content;

                @ApiModelProperty(value = "该文本是否为广告")
                private Boolean isAdtext;

                @ApiModelProperty(value = "该文本是否出现色情广告文本")
                private Boolean isPtext;

                @ApiModelProperty(value = "是否包含二维码，多张图片时，为最严重的结果")
                private Boolean isQr;

                @ApiModelProperty(value = "置信度，介于0-1，越接近1越确认")
                private Integer score;

                @ApiModelProperty(value = "文本的坐标，从左上、右上、右下到左下，顺时针方向展示")
                private List<VertexBean> vertex;

                @NoArgsConstructor
                @Data
                public static class VertexBean{

                    @ApiModelProperty(value = "坐标x")
                    private Integer x;

                    @ApiModelProperty(value = "坐标y")
                    private Integer y;
                }
            }
        }
    }

    /**
     * 构造返回数据
     * @param result
     * @return
     */
    public static AdvertisingCheckVO retData(String result){
        AdvertisingCheckVO advertisingCheckVO = JSON.parseObject(result,new TypeReference<AdvertisingCheckVO>() {});
        return advertisingCheckVO;
    }
}
