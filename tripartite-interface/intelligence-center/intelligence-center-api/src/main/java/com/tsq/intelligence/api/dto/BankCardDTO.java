package com.tsq.intelligence.api.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class BankCardDTO extends CommonDTO implements Serializable {

    private static final long serialVersionUID = -655957921710488422L;

    @ApiModelProperty(value = "是否返回卡号区域截图，默认为0，如果设为 1，则返回base64编码的卡号区域截图")
    private String cardNumberImage;

}
