package com.tsq.intelligence.api.vo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@Data
public class BusinessLicenseVO implements Serializable {

    private static final long serialVersionUID = 2382741366533178642L;

    @ApiModelProperty(value = "结果码")
    private String code;

    @ApiModelProperty(value = "描述")
    private String desc;

    @ApiModelProperty(value = "会话ID")
    private String sid;

    @ApiModelProperty(value = "返回数据")
    private DataBean data;

    @NoArgsConstructor
    @Data
    public static class DataBean{

        @ApiModelProperty(value = "住所/经营场所/主要经营场所/营业场所")
        private String bizLicenseAddress;

        @ApiModelProperty(value = "名称")
        private String bizLicenseCompanyName;

        @ApiModelProperty(value = "类型/公司类型/主体类型")
        private String bizLicenseCompanyType;

        @ApiModelProperty(value = "注册号")
        private String bizLicenseCreditCode;

        @ApiModelProperty(value = "营业期限")
        private String bizLicenseOperatingPeriod;

        @ApiModelProperty(value = "法定代表人/负责人/经营者/经营者姓名")
        private String bizLicenseOwnerName;

        @ApiModelProperty(value = "注册资本")
        private String bizLicenseRegCapital;

        @ApiModelProperty(value = "经营范围")
        private String bizLicenseScope;

        @ApiModelProperty(value = "成立日期/注册日期")
        private String bizLicenseStartTime;

        @ApiModelProperty(value = "识别错误码")
        private Integer errorCode;

        @ApiModelProperty(value = "错误原因描述")
        private String errorMsg;

        @ApiModelProperty(value = "营业执照")
        private String type;
    }
    /**
     * 构造返回数据
     * @param result
     * @return
     */
    public static BusinessLicenseVO retData(String result){
        BusinessLicenseVO businessLicenseVO = JSON.parseObject(result,new TypeReference<BusinessLicenseVO>() {});
        return businessLicenseVO;
    }
}
