package com.tsq.intelligence.api.vo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@Data
public class PoliticianCheckVO implements Serializable {

    private static final long serialVersionUID = 2834155389222867319L;

    @ApiModelProperty(value = "结果码")
    private String code;

    @ApiModelProperty(value = "描述")
    private String desc;

    @ApiModelProperty(value = "会话ID")
    private String sid;

    @ApiModelProperty(value = "返回数据")
    private DataBean data;

    @NoArgsConstructor
    @Data
    public static class DataBean{

        @ApiModelProperty(value = "图片唯一标识")
        private String imageId;

        @ApiModelProperty(value = "")
        private ScoreBean score;

        @ApiModelProperty(value = "")
        private String scene;

        @ApiModelProperty(value = "是否为政治人物")
        private Boolean isPorn;

        @ApiModelProperty(value = "")
        private Boolean isReview;

        @ApiModelProperty(value = "")
        private Double illegalConfidence;

        @NoArgsConstructor
        @Data
        public static class ScoreBean{

            @ApiModelProperty(value = "")
            private Double normal;

            @ApiModelProperty(value = "")
            private Double sexy;

            @ApiModelProperty(value = "")
            private Double porn;

            @ApiModelProperty(value = "")
            private Double verySexy;
        }
    }

    /**
     * 构造返回数据
     * @param result
     * @return
     */
    public static PoliticianCheckVO retData(String result){
        PoliticianCheckVO politicianCheckVO = JSON.parseObject(result,new TypeReference<PoliticianCheckVO>() {});
        return politicianCheckVO;
    }
}
