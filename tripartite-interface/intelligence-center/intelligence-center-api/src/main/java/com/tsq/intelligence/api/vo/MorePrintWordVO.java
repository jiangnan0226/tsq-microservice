package com.tsq.intelligence.api.vo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Data
public class MorePrintWordVO implements Serializable {

    private static final long serialVersionUID = 4000790708358037330L;

    @ApiModelProperty(value = "结果码")
    private String code;

    @ApiModelProperty(value = "描述")
    private String desc;

    @ApiModelProperty(value = "会话ID")
    private String sid;

    @ApiModelProperty(value = "识别内容")
    private List<String> wordsList;

    @ApiModelProperty(value = "返回数据")
    private data data;

    @Data
    public static class data {

        @ApiModelProperty(value = "文本信息")
        private Document document;

        @ApiModelProperty(value = "引擎识别耗时，以毫秒为单位")
        private Integer engine_cost;

        @ApiModelProperty(value = "引擎错误码")
        private Integer error_code;

        @ApiModelProperty(value = "引擎错误描述")
        private String error_msg;

    }

    @Data
    public static class Document {

        @ApiModelProperty(value = "文字块")
        private List<Blocks> blocks;

        @ApiModelProperty(value = "倾斜的文本引擎会转正后识别，该字段表示文本块的旋转角度")
        private Integer  rotate_angle;

    }

    @Data
    public static class Blocks {

        @ApiModelProperty(value = "文字行")
        private List<Lines> lines;

        @ApiModelProperty(value = "位置")
        private Position position;

    }

    @Data
    public static class Lines {

        @ApiModelProperty(value ="文本行下的char_centers字段表示文本行中每个字符的中心点位置")
        private List<CharCenters> charCenters;

        @ApiModelProperty(value ="文本行下的characters字段表示文本行中每个字符的坐标、置信度、内容")
        private List<Characters> characters;

        @ApiModelProperty(value = "位置")
        private Position position;

        @ApiModelProperty(value ="文本内容")
        private String text;

        @ApiModelProperty(value ="置信度")
        private Integer score;

    }

    @Data
    public static class Position {
        @ApiModelProperty(value ="外接矩形的位置和大小")
        private BoundingBox boundingBox;

        @ApiModelProperty(value ="y坐标")
        private List<Vertices> vertices;
    }

    @Data
    public static class BoundingBox {

        @ApiModelProperty(value ="高")
        private Integer height;

        @ApiModelProperty(value ="左")
        private Integer left;

        @ApiModelProperty(value ="上")
        private Integer top;

        @ApiModelProperty(value ="宽")
        private Integer width;
    }

    @Data
    public static class Vertices {

        @ApiModelProperty(value ="高")
        private Integer x;

        @ApiModelProperty(value ="左")
        private Integer y;

    }

    @Data
    public static class CharCenters {
        @ApiModelProperty(value ="x坐标")
        private Integer x;

        @ApiModelProperty(value ="y坐标")
        private Integer y;
    }

    @Data
    public static class Characters {
        @ApiModelProperty(value = "位置")
        private Position position;

        @ApiModelProperty(value ="文本内容")
        private String text;

        @ApiModelProperty(value ="置信度")
        private Integer score;
    }

    /**
     * 构造返回数据
     * @param result
     * @return
     */
    public static MorePrintWordVO retData(String result){
        MorePrintWordVO printWord = JSON.parseObject(result,new TypeReference<MorePrintWordVO>() {});
        List<String> retList = new ArrayList<String>();
        List<Blocks> blocksList = printWord.getData().getDocument().getBlocks();
        if(null!=blocksList && blocksList.size()>0){
            blocksList.stream().forEach(
                    block->{
                        StringBuilder retStr = new StringBuilder();
                        List<Lines> lineList = block.getLines();
                        if(null!=lineList && lineList.size()>0) {
                            lineList.stream().forEach(
                                    line->{
                                        retStr.append(line.getText());
                                    }
                            );
                        }
                        retList.add(retStr.toString());
                        retStr.delete(0,retStr.length());
                    }
            );
        }
        printWord.setWordsList(retList);
        return printWord;
    }

}
