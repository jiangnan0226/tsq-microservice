package com.tsq.intelligence.api.vo.ai;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class MusicVO {

    @ApiModelProperty(value = "会话ID")
    private String sid;

    @ApiModelProperty(value = "接口类型")
    private String type;

    @ApiModelProperty(value = "播报内容")
    private String text = "";

    @ApiModelProperty(value = "歌曲链接")
    private String url = "";

    @ApiModelProperty(value = "歌名")
    private String value;

    /**
     * 构造返回数据
     * @param result
     * @return
     */
    public static MusicVO retData(String result){
        MusicVO musicVO = JSON.parseObject(result,new TypeReference<MusicVO>() {});
        return musicVO;
    }
}
