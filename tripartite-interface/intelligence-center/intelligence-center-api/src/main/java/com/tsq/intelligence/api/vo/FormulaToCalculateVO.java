package com.tsq.intelligence.api.vo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@NoArgsConstructor
@Data
public class FormulaToCalculateVO implements Serializable {

    private static final long serialVersionUID = 7917906413211682022L;

    @ApiModelProperty(value = "结果码")
    private String code;

    @ApiModelProperty(value = "描述")
    private String desc;

    @ApiModelProperty(value = "会话ID")
    private String sid;

    @ApiModelProperty(value = "返回数据")
    private DataBean data;

    @NoArgsConstructor
    @Data
    public static class DataBean{

        @ApiModelProperty(value = "识别引擎相关信息，无需关注")
        private EngineInfoBean engineInfo;

        @ApiModelProperty(value = "协议版本号")
        private String protocol;

        @ApiModelProperty(value = "版面分析结果数组，数组中的每个元素为一个识别区域（文本行、图像），元素排序顺序为从上到下，从左至右")
        private List<RegionBean> region;

        @NoArgsConstructor
        @Data
        public static class EngineInfoBean{

            @ApiModelProperty(value = "识别引擎参数")
            private String category;

            @ApiModelProperty(value = "识别引擎名称")
            private String name;

            @ApiModelProperty(value = "识别引擎版本号")
            private String version;
        }

        @NoArgsConstructor
        @Data
        public static class RegionBean{

            @ApiModelProperty(value = "识别区域形状和位置信息")
            private CoordBean coord;

            @ApiModelProperty(value = "识别区域识别信息")
            private RecogBean recog;

            @ApiModelProperty(value = "识别区域类型，取值为text（文本行）/graph（图像）")
            private String type;

            @NoArgsConstructor
            @Data
            public static class CoordBean{

                @ApiModelProperty(value = "识别区域像素点x坐标列表，其中最小值为识别区域矩形左上角点x坐标，最大值与最小值的差值为矩形宽度。以上传图片的左上角为原点，横向为x轴（向右为正），纵向为y轴（向下为正）")
                private List<String> x;

                @ApiModelProperty(value = "识别区域像素点y坐标列表，其中最小值为识别区域矩形左上角点y坐标，最大值与最小值的差值为矩形高度。以上传图片的左上角为原点，横向为x轴（向右为正），纵向为y轴（向下为正）")
                private List<String> y;
            }

            @NoArgsConstructor
            @Data
            public static class RecogBean{

                @ApiModelProperty(value = "文本行识别结果，若文本行内部含有图像，以graph:id标记，其中id为graph数组中的序列号")
                private String content;

                @ApiModelProperty(value = "异常信息，0代表正常")
                private int exception;

                @ApiModelProperty(value = "文本行识别结果单字数组（包含文本行中的图像）")
                private List<ElementBean> element;

                @NoArgsConstructor
                @Data
                public static class ElementBean{

                    @ApiModelProperty(value = "识别置信度")
                    private int conf;

                    @ApiModelProperty(value = "文本行识别结果，若文本行内部含有图像，以graph:id标记，其中id为graph数组中的序列号")
                    private String content;
                }
            }
        }
    }

    /**
     * 构造返回数据
     * @param result
     * @return
     */
    public static FormulaToCalculateVO retData(String result){
        FormulaToCalculateVO formulaToCalculateVO = JSON.parseObject(result,new TypeReference<FormulaToCalculateVO>() {});
        return formulaToCalculateVO;
    }
}
