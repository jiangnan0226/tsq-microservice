package com.tsq.intelligence.api.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class IdCardDTO implements Serializable {

    private static final long serialVersionUID = 6472603162039806573L;

    @ApiModelProperty(value = "是否返回头像图片：默认head_portrait=0，即不返回头像图片，head_portrait=1，则返回身份证头像照片（Base64编码）")
    private String headPortrait;

    @ApiModelProperty(value = "是否返回切片图，默认crop_image=0，1表示返回身份证切片照片（Base64编码）")
    private String cropImage;

    @ApiModelProperty(value = "是否返回身份证号码区域截图，默认id_number_image=0，即不返回身份号码区域截图，1表示返回证件号区域截图（Base64编码）")
    private String idNumberImage;

    @ApiModelProperty(value = "是否先对图片进行切片后再识别，默认recognize_mode=0，即直接对图片进行识别，1表示采用先切片后识别的模式")
    private String recognizeMode;


}
