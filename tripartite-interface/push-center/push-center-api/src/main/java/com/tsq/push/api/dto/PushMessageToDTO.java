package com.tsq.push.api.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 推送消息/通知
 */
@Data
public class PushMessageToDTO{
    /**
     * 系统规定参数。
     * 推消息给IOS设备：PushMessageToiOS
     * 推消息给Android设备：PushMessageToAndroid
     * 推通知给Android设备：PushNoticeToAndroid
     * 推通知给IOS设备：PushNoticeToiOS
     */
    @ApiModelProperty(value = "系统规定参数。取值：PushMessageToiOS；PushMessageToAndroid；PushNoticeToiOS；PushNoticeToAndroid")
    private String action;

    /**
     *AppKey信息
     */
    @ApiModelProperty(value = "AppKey信息")
    private Long appKey;

    /**
     *发送的消息内容
     */
    @ApiModelProperty(value = "发送的消息内容")
    private String body;

    /**
     推送目标: DEVICE:按设备推送 ALIAS : 按别名推送 ACCOUNT:按帐号推送  TAG:按标签推送; ALL: 广播推送
     */
    @ApiModelProperty(value = "推送目标。DEVICE:按设备推送;ALIAS : 按别名推送;ACCOUNT:按帐号推送;TAG:按标签推送;ALL: 广播推送")
    private String target;

    /**
     *根据Target来设定，如Target=DEVICE, 则对应的值为 设备id1,设备id2. 多个值使用逗号分隔.(帐号与设备有一次最多100个的限制)
     */
    @ApiModelProperty(value = "Target具体值。根据Target来设定")
    private String targetValue;

    /**
     *发送的消息的标题
     */
    @ApiModelProperty(value = "发送的消息的标题")
    private String title;

    /**
     * 设备类型 ANDROID iOS ALL.
     */
    @ApiModelProperty(value = "设备类型 ANDROID;iOS;ALL")
    private String deviceType;

    /**
     * 延后推送。可选，如果不设置表示立即推送
     */
    @ApiModelProperty(value = "延后推送。可选，如果不设置表示立即推送")
    private String pushTime;

    /**
     * 消息类型 MESSAGE NOTICE
     */
    @ApiModelProperty(value = "消息类型 MESSAGE NOTICE")
    private String pushType;

    /**
     * 定时推送时间
     */
    @ApiModelProperty(value = "定时推送时间。格式yyyy-MM-dd HH:mm:ss")
    private String pushDate;
}
