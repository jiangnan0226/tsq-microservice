package com.tsq.push.api.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 批量推送接口
 */
@Data
public class MassPushDTO{
    /**
     * 系统规定参数。取值：MassPush。
     */
    @ApiModelProperty(value = "系统规定参数,无需赋值")
    private String action;

    /**
     * AppKey信息
     */
    @ApiModelProperty(value = "AppKey信息")
    private Long appKey;

    /**
     * Android推送时通知的内容/消息的内容；iOS消息/通知内容，推送的内容大小是有限制的，参见产品限制。
     */
    @ApiModelProperty(value = "消息/通知内容")
    private String pushTaskNBody;

    /**
     * 设备类型，取值范围为：
     * iOS：iOS设备
     * ANDROID：Android设备
     * ALL：全部类型设备
     */
    @ApiModelProperty(value = "设备类型。iOS：iOS设备；ANDROID：Android设备；ALL：全部类型设备")
    private String pushTaskNDeviceType;

    /**
     * 推送类型。取值：
     * MESSAGE：表示消息。
     * NOTICE：表示通知。
     */
    @ApiModelProperty(value = "推送类型。MESSAGE：表示消息；NOTICE：表示通知")
    private String pushTaskNPushType;

    /**
     * 推送目标。可取值：
     *
     * DEVICE：根据设备推送。
     * ACCOUNT：根据账号推送。
     * ALIAS：根据别名推送。
     */
    @ApiModelProperty(value = "推送目标。DEVICE：根据设备推送；ACCOUNT：根据账号推送；ALIAS：根据别名推送")
    private String pushTaskNTarget;

    /**
     * 根据Target来设定，多个值使用逗号分隔，超过限制需要分多次推送。
     *
     * Target=DEVICE，值如deviceid1,deviceid2（最多支持1000个）。
     * Target=ACCOUNT，值如account1,account2（最多支持1000个）。
     * Target=ALIAS，值如alias1,alias2（最多支持1000个）。
     */
    @ApiModelProperty(value = "Target具体值。根据Target来设定")
    private String pushTaskNTargetValue;

    /**
     * Android推送时通知/消息的标题以及iOS消息的标题（必填）。iOS 10+通知显示标题，（iOS 8.2 <= iOS系统 < iOS 10）替换通知应用名称（选填）。
     */
    @ApiModelProperty(value = "推送时通知/消息的标题")
    private String pushTaskNTitle;

    /**
     * 批量发送数量，最大值100
     */
    @ApiModelProperty(value = "批量发送数量。最大值100")
    private int amount;


}
