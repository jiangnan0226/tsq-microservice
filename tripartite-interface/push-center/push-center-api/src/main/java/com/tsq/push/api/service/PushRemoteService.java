package com.tsq.push.api.service;

import com.tsq.common.model.ResponseData;
import com.tsq.push.api.dto.*;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

public interface PushRemoteService {
    /**
     * 推送消息/通知
     * @param pushMessageToDTO
     * @return
     */
    @PostMapping("/push/pushMessageTo")
    ResponseData<PushMessageToDTO> pushMessageTo(@RequestBody PushMessageToDTO pushMessageToDTO);

    /**
     * 推送高级接口
     * @param pushAdvancedDTO
     * @return
     */
    @PostMapping("/push/pushAdvanced")
    ResponseData<PushAdvancedDTO> pushAdvanced(@RequestBody PushAdvancedDTO pushAdvancedDTO);

    /**
     * 持续推送接口
     * @param continuouslyPushDTO
     * @return
     */
    @PostMapping("/push/continuouslyPush")
    ResponseData<ContinuouslyPushDTO> continuouslyPush(@RequestBody ContinuouslyPushDTO continuouslyPushDTO);

    /**
     * 批量推送接口
     * @param massPushDTO
     * @return
     */
    @PostMapping("/push/massPush")
    ResponseData<MassPushDTO> massPush(@RequestBody MassPushDTO massPushDTO);

    /**
     * 取消定时推送任务
     * @param cancelPushDTO
     * @return
     */
    @PostMapping("/push/cancelPush")
    ResponseData<CancelPushDTO> cancelPush(@RequestBody CancelPushDTO cancelPushDTO);
}
