package com.tsq.push.api.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 取消定时推送任务
 */
@Data
public class CancelPushDTO{
    /**
     * 系统规定参数。取值：CancelPush
     */
    @ApiModelProperty(value = "系统规定参数,无需赋值")
    private String action;

    /**
     * AppKey信息
     */
    @ApiModelProperty(value = "AppKey信息")
    private Long appKey;

    /**
     *某次推送任务的消息ID。
     */
    @ApiModelProperty(value = "某次推送任务的消息ID")
    private Long messageId;
}
