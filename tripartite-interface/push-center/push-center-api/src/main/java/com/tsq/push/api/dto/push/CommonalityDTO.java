package com.tsq.push.api.dto.push;

import lombok.Data;

/**
 * 公共请求参数
 */
@Data
public class CommonalityDTO {
    /**
     *返回值的类型，支持JSON与XML，默认为XML
     */
    private String format = "JSON";
    /**
     *当前请设置为cn-hangzhou
     */
    private String regionId = "cn-hangzhou";
    /**
     *API版本号，为日期形式YYYY-MM-DD
     */
    private String version = "2016-08-01";
    /**
     *阿里云颁发给用户的访问服务所用的密钥ID
     */
    private String accessKeyId = "LTAI4GAhSQJhai2TGRGjxQAS";
    /**
     *签名结果串
     */
    private String signature;
    /**
     *签名方式，目前支持HMAC-SHA1
     */
    private String signatureMethod = "HMAC-SHA1";
    /**
     *请求的时间戳
     */
    private String timestamp;
    /**
     *签名算法版本，目前版本是1.0
     */
    private String signatureVersion = "1.0";
    /**
     *唯一随机数
     */
    private String signatureNonce;


}
