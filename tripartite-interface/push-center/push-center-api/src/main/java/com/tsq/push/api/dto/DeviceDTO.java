package com.tsq.push.api.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * APP维度推送统计
 */
@Data
public class DeviceDTO {
//    /**
//     * 系统规定参数。取值：
//     * APP维度推送统计：QueryAliases
//     * 绑定别名：QueryAliases
//     * 查询别名：QueryAliases
//     */
//    @ApiModelProperty(value = "系统规定参数,无需写入请求")
//    private String action;

    /**
     * AppKey信息
     */
    @ApiModelProperty(value = "AppKey信息")
    private Long appKey;

    /**
     * 查询的结束时间，ISO-8601格式，格式为YYYY-MM-DDThh:mm:ssZ。
     */
    @ApiModelProperty(value = "查询的结束时间。格式为YYYY-MM-DDThh:mm:ssZ")
    private String endTime;

    /**
     * 返回的数据粒度，HOUR：是小时粒度，DAY：是天粒度。
     * 小时粒度允许查24小时内数据，天粒度允许查31内天数据，目前只支持天粒度查询。
     */
    @ApiModelProperty(value = "返回的数据粒度。DAY：天粒度（31内天数据）")
    private String granularity;

    /**
     *  查询的起始时间，ISO-8601格式，格式为YYYY-MM-DDThh:mm:ssZ。
     */
    @ApiModelProperty(value = "查询的起始时间。格式为YYYY-MM-DDThh:mm:ssZ")
    private String startTime;

    /**
     * 需要绑定的别名。
     * 一次最多只能绑定10个，多个Alias用逗号分隔，Alias最长128个字节（中文算三个字符），一个设备最多绑定128个别名，一个别名最多允许绑定128个设备。
     */
    @ApiModelProperty(value = "需要绑定的别名")
    private String aliasName;

    /**
     *设备在推送的唯一标识，32位，数字和小写字母组合。
     */
    @ApiModelProperty(value = "设备在推送的唯一标识")
    private String deviceId;

    /**
     * 推送的消息ID，推送之后会返回该ID。
     */
    @ApiModelProperty(value = "推送的消息ID。推送之后会返回该ID")
    private Long messageId;

    /**
     * 设备类型，取值范围为：
     * iOS：iOS设备
     * ANDROID：Andriod设备
     * ALL：全部类型设备
     */
    @ApiModelProperty(value = "设备类型。iOS：iOS设备；ANDROID：Andriod设备；ALL：全部类型设备")
    private String deviceType;

    /**
     * 查询的是新增设备数还是历史累计设备数。可取值：
     * NEW：新增设备
     * TOTAL：累计设备数
     */
    @ApiModelProperty(value = "查询的是新增设备数还是历史累计设备数。NEW：新增设备；TOTAL：累计设备数")
    private String queryType;

    /**
     * 设备在推送的唯一标识，32位，数字和小写字母组合，多个设备查询用“,”分割，一次最多查100个。
     */
    @ApiModelProperty(value = "设备在推送的唯一标识")
    private String deviceIds;

    /**
     * 设备或account或alias， 每次只能查询1个clientKey。
     */
    @ApiModelProperty(value = "设备或account或alias.每次只能查询1个clientKey")
    private String clientKey;

    /**
     * ClientKey的类型。可取值：
     * DEVICE：是设备
     * ACCOUNT：是账号
     * ALIAS：是别名
     */
    @ApiModelProperty(value = "ClientKey的类型。DEVICE：是设备；ACCOUNT：是账号；ALIAS：是别名")
    private String keyType;

    /**
     * 绑定的Tag，多个Tag用逗号分隔，系统总共支持1万个Tag，一次最多能绑定10个Tag。
     */
    @ApiModelProperty(value = "绑定的Tag。多个Tag用逗号分隔，系统总共支持1万个Tag，一次最多能绑定10个Tag")
    private String tagName;

    /**
     * 账户，一次仅支持查询一个。
     */
    @ApiModelProperty(value = "账户。一次仅支持查询一个")
    private String account;

    /**
     * 别名，一次仅支持查询一个。
     */
    @ApiModelProperty(value = "别名。一次仅支持查询一个")
    private String alias;

    /**
     * 是否全部解绑，默认为”false”。
     * 如果值为”true”，则解绑一个设备当前绑定的所有别名；如果值为”false”，则解绑”AliasName”指定的别名。
     */
    @ApiModelProperty(value = "是否全部解绑。默认为”false”")
    private Boolean unbindAll;
}
