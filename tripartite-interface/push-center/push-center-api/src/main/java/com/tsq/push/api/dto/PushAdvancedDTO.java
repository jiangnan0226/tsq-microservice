package com.tsq.push.api.dto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 推送高级接口
 */
@Data
public class PushAdvancedDTO{
    /**
     * 系统规定参数。
     * push
     */
    @ApiModelProperty(value = "系统规定参数,无需赋值")
    private String action;

    /**
     * AppKey信息
     */
    @ApiModelProperty(value = "AppKey信息")
    private Long appKey;

    /**
     *发送的消息内容
     */
    @ApiModelProperty(value = "发送的消息内容")
    private String body;

    /**
     * 设备类型，取值范围为：
     * iOS：iOS设备
     * ANDROID：Android设备
     * ALL：全部类型设备
     */
    @ApiModelProperty(value = "设备类型。iOS：iOS设备；ANDROID：Android设备；ALL：全部类型设备")
    private String deviceType;

    /**
     * 推送类型。取值：
     *
     * MESSAGE：表示消息。
     * NOTICE：表示通知。
     */
    @ApiModelProperty(value = "推送类型。MESSAGE：表示消息；NOTICE：表示通知")
    private String pushType;

    /**
     *推送目标。可取值：
     * DEVICE：根据设备推送。
     * ACCOUNT：根据账号推送。
     * ALIAS：根据别名推送。
     * TAG：根据标签推送。
     * ALL：推送给全部设备（同一种DeviceType的两次全推的间隔至少为1秒）。
     * TBD：初始化持续推送，推送目标由后续的ContinuouslyPush接口指定。
     */
    @ApiModelProperty(value = "推送目标。DEVICE：根据设备推送；ACCOUNT：根据账号推送；ALIAS：根据别名推送；TAG：根据标签推送；ALL：推送给全部设备；TBD：初始化持续推送")
    private String target;

    /**
     *根据Target来设定，多个值使用逗号分隔，最多支持100个
     */
    @ApiModelProperty(value = "Target具体值。根据Target来设定")
    private String targetValue;

    /**
     *发送的消息的标题
     */
    @ApiModelProperty(value = "发送的消息的标题")
    private String title;

    /**
     * 定时推送时间
     */
    @ApiModelProperty(value = "定时推送时间。格式yyyy-MM-dd HH:mm:ss")
    private String pushDate;
}
