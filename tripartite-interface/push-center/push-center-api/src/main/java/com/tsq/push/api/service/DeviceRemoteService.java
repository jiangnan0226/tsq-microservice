package com.tsq.push.api.service;

import com.tsq.common.model.ResponseData;
import com.tsq.push.api.dto.DeviceDTO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

public interface DeviceRemoteService {
    /**
     * APP维度推送统计
     */
    @GetMapping("/device/queryPushStatByApp")
    ResponseData<DeviceDTO> queryPushStatByApp(@RequestParam(name = "deviceDTO") DeviceDTO deviceDTO);

    /**
     * 任务维度推送统计
     */
    @GetMapping("/device/queryPushStatByMsg")
    ResponseData<DeviceDTO> queryPushStatByMsg(@RequestParam(name = "deviceDTO") DeviceDTO deviceDTO);

    /**
     * 设备新增与留存/查询App维度的设备统计
     */
    @GetMapping("/device/queryDeviceStat")
    ResponseData<DeviceDTO> queryDeviceStat(@RequestParam(name = "deviceDTO") DeviceDTO deviceDTO);

    /**
     * 去重设备统计
     */
    @GetMapping("/device/queryUniqueDeviceStat")
    ResponseData<DeviceDTO> queryUniqueDeviceStat(@RequestParam(name = "deviceDTO") DeviceDTO deviceDTO);

    /**
     * 查询设备详情
     */
    @GetMapping("/device/queryDeviceInfo")
    ResponseData<DeviceDTO> queryDeviceInfo(@RequestParam(name = "deviceDTO") DeviceDTO deviceDTO);

    /**
     * 查询TAG
     */
    @GetMapping("/device/queryTags")
    ResponseData<DeviceDTO> queryTags(@RequestParam(name = "deviceDTO") DeviceDTO deviceDTO);

    /**
     * 绑定TAG
     */
    @PostMapping("/device/bindTag")
    ResponseData<DeviceDTO> bindTag(@RequestBody DeviceDTO deviceDTO);

    /**
     * 解绑TAG
     */
    @PostMapping("/device/unbindTag")
    ResponseData<DeviceDTO> unbindTag(@RequestBody DeviceDTO deviceDTO);

    /**
     * TAG列表
     */
    @GetMapping("/device/listTags")
    ResponseData<DeviceDTO> listTags(@RequestParam(name = "deviceDTO") DeviceDTO deviceDTO);

    /**
     * 删除TAG
     */
    @PostMapping("/device/removeTag")
    ResponseData<DeviceDTO> removeTag(@RequestBody DeviceDTO deviceDTO);

    /**
     * 查询别名
     * @param deviceDTO
     * @return
     */
    @GetMapping("/device/queryAliases")
    ResponseData<DeviceDTO> queryAliases(@RequestParam(name = "deviceDTO") DeviceDTO deviceDTO);

    /**
     * 绑定别名
     * @param deviceDTO
     * @return
     */
    @PostMapping("/device/bindAliases")
    ResponseData<DeviceDTO> bindAliases(@RequestBody DeviceDTO deviceDTO);

    /**
     * 通过别名查询设备列表
     * @param deviceDTO
     * @return
     */
    @GetMapping("/device/queryDevicesByAlias")
    ResponseData<DeviceDTO> queryDevicesByAlias(@RequestParam(name = "deviceDTO") DeviceDTO deviceDTO);

    /**
     * 解绑别名
     * @param deviceDTO
     * @return
     */
    @PostMapping("/device/unbindAlias")
    ResponseData<DeviceDTO> unbindAlias(@RequestBody DeviceDTO deviceDTO);

    /**
     * 通过账户查询设备列表
     * @param deviceDTO
     * @return
     */
    @GetMapping("/device/queryDevicesByAccount")
    ResponseData<DeviceDTO> queryDevicesByAccount(@RequestParam(name = "deviceDTO") DeviceDTO deviceDTO);

}
