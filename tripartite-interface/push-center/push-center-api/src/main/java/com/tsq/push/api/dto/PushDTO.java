package com.tsq.push.api.dto;


import com.aliyuncs.push.model.v20160801.MassPushRequest;
import com.tsq.push.api.dto.base.PushDevice;
import com.tsq.push.api.dto.base.PushText;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Tango
 * @since 2020/8/11
 */
@Getter
@Setter
public class PushDTO extends MassPushRequest.PushTask {

    public PushDTO(){
        pushDeviceList = new ArrayList<>();
        pushTaskList = new ArrayList<>();
    }

    private String appKey;
    private String userId;

    //消息一致
    private List<PushDevice> pushDeviceList;
    private PushText pushText;

    //消息不一致
    private List<MassPushRequest.PushTask> pushTaskList;


}
