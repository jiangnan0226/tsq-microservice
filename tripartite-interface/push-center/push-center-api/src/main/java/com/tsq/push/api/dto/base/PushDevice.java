package com.tsq.push.api.dto.base;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Tango
 * @since 2020/8/11
 */
@Getter
@Setter
public class PushDevice {

    public PushDevice(){}

    public PushDevice(String target, String targetValue) {
        this.deviceType = "ALL";
        this.target = target;
        this.targetValue = targetValue;
    }

    private String deviceType;  //IOS ANDROID ALL
    private String target;
    private String targetValue;
}

