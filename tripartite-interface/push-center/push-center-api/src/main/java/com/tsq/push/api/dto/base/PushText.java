package com.tsq.push.api.dto.base;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Tango
 * @since 2020/8/11
 */
@Getter
@Setter
public class PushText {
    public PushText() {
    }

    public PushText(String pushType, String title, String body) {
        this.pushType = pushType;
        this.title = title;
        this.body = body;
    }

    private String pushType;
    private String title;
    private String body;
}
