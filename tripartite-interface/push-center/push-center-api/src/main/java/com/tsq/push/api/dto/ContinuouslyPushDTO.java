package com.tsq.push.api.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 持续推送接口
 */
@Data
public class ContinuouslyPushDTO{
    /**
     * 系统规定参数。
     * ContinuouslyPush
     */
    @ApiModelProperty(value = "系统规定参数,无需赋值")
    private String action;

    /**
     * AppKey信息
     */
    @ApiModelProperty(value = "AppKey信息")
    private Long appKey;

    /**
     * 消息ID。
     * 调用Push接口，设置Target为TBD，调用后获得的MessageId。它代表了一个已经保存到推送系统的消息。
     */
    @ApiModelProperty(value = "某次推送任务的消息ID")
    private String messageId;

    /**
     * 推送目标：
     * DEVICE：根据设备推送
     * ACCOUNT：根据账号推送
     * ALIAS：根据别名推送
     * 持续推送只支持这3种目标类型。
     */
    @ApiModelProperty(value = "推送目标。DEVICE：根据设备推送；ACCOUNT：根据账号推送；ALIAS：根据别名推送")
    private String target;

    /**
     *根据Target来设定，多个值使用逗号分隔，超过限制需要分多次推送。
     * Target=DEVICE，值如deviceid1,deviceid2（最多支持1000个）。
     * Target=ACCOUNT，值如account1,account2（最多支持100个）。
     * Target=ALIAS，值如alias1,alias2（最多支持1000个）。
     */
    @ApiModelProperty(value = "Target具体值。根据Target来设定")
    private String targetValue;
}