package com.tsq.push.client;

import com.tsq.common.model.ResponseData;
import com.tsq.push.api.InterfaceService;
import com.tsq.push.api.dto.DeviceDTO;
import com.tsq.push.api.service.DeviceRemoteService;
import feign.hystrix.FallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;

@FeignClient(name = InterfaceService.SERVICE_NAME, fallbackFactory = DeviceClientService.DeviceServiceFallbackFactory.class)
public interface DeviceClientService extends DeviceRemoteService {
    @Component
    class DeviceServiceFallbackFactory implements FallbackFactory<DeviceClientService> {
        @Override
        public DeviceClientService create(Throwable throwable) {
            return new DeviceClientService() {

                @Override
                public ResponseData<DeviceDTO> queryPushStatByApp(DeviceDTO deviceDTO) {
                    return ResponseData.error();
                }

                @Override
                public ResponseData<DeviceDTO> queryPushStatByMsg(DeviceDTO deviceDTO) {
                    return ResponseData.error();
                }

                @Override
                public ResponseData<DeviceDTO> queryDeviceStat(DeviceDTO deviceDTO) {
                    return ResponseData.error();
                }

                @Override
                public ResponseData<DeviceDTO> queryUniqueDeviceStat(DeviceDTO deviceDTO) {
                    return ResponseData.error();
                }

                @Override
                public ResponseData<DeviceDTO> queryDeviceInfo(DeviceDTO deviceDTO) {
                    return ResponseData.error();
                }

                @Override
                public ResponseData<DeviceDTO> queryTags(DeviceDTO deviceDTO) {
                    return ResponseData.error();
                }

                @Override
                public ResponseData<DeviceDTO> bindTag(DeviceDTO deviceDTO) {
                    return ResponseData.error();
                }

                @Override
                public ResponseData<DeviceDTO> unbindTag(DeviceDTO deviceDTO) {
                    return ResponseData.error();
                }

                @Override
                public ResponseData<DeviceDTO> listTags(DeviceDTO deviceDTO) {
                    return ResponseData.error();
                }

                @Override
                public ResponseData<DeviceDTO> removeTag(DeviceDTO deviceDTO) {
                    return ResponseData.error();
                }

                @Override
                public ResponseData<DeviceDTO> queryAliases(DeviceDTO deviceDTO) {
                    return ResponseData.error();
                }

                @Override
                public ResponseData<DeviceDTO> bindAliases(DeviceDTO deviceDTO) {
                    return ResponseData.error();
                }

                @Override
                public ResponseData<DeviceDTO> queryDevicesByAlias(DeviceDTO deviceDTO) {
                    return ResponseData.error();
                }

                @Override
                public ResponseData<DeviceDTO> unbindAlias(DeviceDTO deviceDTO) {
                    return ResponseData.error();
                }

                @Override
                public ResponseData<DeviceDTO> queryDevicesByAccount(DeviceDTO deviceDTO) {
                    return ResponseData.error();
                }
            };
        }
    }
}
