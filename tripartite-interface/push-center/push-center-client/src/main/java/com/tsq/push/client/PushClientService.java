package com.tsq.push.client;

import com.tsq.common.model.ResponseData;
import com.tsq.push.api.InterfaceService;
import com.tsq.push.api.dto.*;
import com.tsq.push.api.service.PushRemoteService;
import feign.hystrix.FallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;

@FeignClient(name = InterfaceService.SERVICE_NAME, fallbackFactory = PushClientService.PushServiceFallbackFactory.class)
public interface PushClientService extends PushRemoteService {
    @Component
    class PushServiceFallbackFactory implements FallbackFactory<PushClientService> {
        @Override
        public PushClientService create(Throwable throwable) {
            return new PushClientService() {
                @Override
                public ResponseData<PushMessageToDTO> pushMessageTo(PushMessageToDTO pushMessageToDTO) {
                    return ResponseData.error();
                }

                @Override
                public ResponseData<PushAdvancedDTO> pushAdvanced(PushAdvancedDTO pushAdvancedDTO) {
                    return ResponseData.error();
                }

                @Override
                public ResponseData<ContinuouslyPushDTO> continuouslyPush(ContinuouslyPushDTO continuouslyPushDTO) {
                    return ResponseData.error();
                }

                @Override
                public ResponseData<MassPushDTO> massPush(MassPushDTO massPushDTO) {
                    return ResponseData.error();
                }

                @Override
                public ResponseData<CancelPushDTO> cancelPush(CancelPushDTO cancelPushDTO) {
                    return ResponseData.error();
                }
            };
        }
    }
}
