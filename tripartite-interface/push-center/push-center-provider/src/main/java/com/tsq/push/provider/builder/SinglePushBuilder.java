package com.tsq.push.provider.builder;

import com.aliyuncs.push.model.v20160801.PushRequest;
import com.tsq.push.api.dto.PushDTO;

/**
 * 创建单人推送
 *
 * @author Tango
 * @since 2020/8/11
 */
public class SinglePushBuilder implements PushBuilder {

    private PushRequest request;

    SinglePushBuilder() {
        request = new PushRequest();
        request.setDeviceType("ALL");
        request.setIOSApnsEnv("DEV");
        request.setAndroidNotifyType("BOTH");
        request.setAndroidNotificationChannel("1");
        request.setAndroidNotificationBarType(1);
        request.setStoreOffline(true);
        request.setAndroidRemind(true);
    }

    //应用key
    public SinglePushBuilder key(String appKey) {
        request.setAppKey(Long.valueOf(appKey));
        return this;
    }

    //目标
    public SinglePushBuilder device(String target, String targetValue) {
        request.setTarget(target);
        request.setTargetValue(targetValue);
        return this;
    }

    //推送内容
    public SinglePushBuilder info(String pushType, String title, String body) {
        request.setPushType(pushType);
        request.setTitle(title);
        request.setBody(body);
        request.setAndroidPopupTitle(title);
        request.setAndroidPopupBody(body);
        return this;
    }

    //页面跳转等参数
    public SinglePushBuilder action(String openType, String activity, String extParameters) {
        request.setAndroidOpenType(openType);
        request.setAndroidActivity(activity);
        request.setAndroidPopupActivity(activity);
        request.setAndroidExtParameters(extParameters);
        return this;
    }


    /**
     * 从DTO创建
     * @param pushDTO
     * @return PushRequest
     */
    public PushRequest buildWithDTO(PushDTO pushDTO) {
        return key(pushDTO.getAppKey())
                .device(pushDTO.getTarget(), pushDTO.getTargetValue())
                .action(pushDTO.getAndroidOpenType(), pushDTO.getAndroidActivity(), pushDTO.getAndroidExtParameters())
                .info(pushDTO.getPushType(), pushDTO.getTitle(), pushDTO.getBody())
                .build();
    }


    public PushRequest build() {
        return request;
    }


}
