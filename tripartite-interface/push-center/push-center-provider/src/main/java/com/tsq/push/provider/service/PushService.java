package com.tsq.push.provider.service;

import com.aliyuncs.push.model.v20160801.*;
import com.tsq.common.model.ResponseData;
import com.tsq.push.api.dto.*;

public interface PushService {
    /**
     * 推送
     * @param pushDTO
     * @return
     */
    ResponseData<PushResponse> pushTo(PushDTO pushDTO);



    /**
     * 推送
     * @param pushRequest
     * @return
     */
    ResponseData<PushResponse> pushTo(PushRequest pushRequest);

    /**
     * 批量推送
     * @param massPushRequest
     * @return
     */
    ResponseData<MassPushResponse> pushMass(MassPushRequest massPushRequest);


    /**
     * 推送消息/通知
     * @param pushMessageToDTO
     * @return
     */
    ResponseData<PushResponse> pushMessageTo(PushMessageToDTO pushMessageToDTO);
    /**
     * 推送高级接口
     * @param pushAdvancedDTO
     * @return
     */
    ResponseData<PushResponse> pushAdvanced(PushAdvancedDTO pushAdvancedDTO);
    /**
     * 持续推送接口
     * @param continuouslyPushDTO
     * @return
     */
    ResponseData<ContinuouslyPushResponse> continuouslyPush(ContinuouslyPushDTO continuouslyPushDTO);
    /**
     * 批量推送接口
     * @param massPushDTO
     * @return
     */
    ResponseData<MassPushResponse> massPush(MassPushDTO massPushDTO);
    /**
     * 取消定时推送任务
     *  @param cancelPushDTO
     *  @return
     */
    ResponseData<CancelPushResponse> cancelPush(CancelPushDTO cancelPushDTO);

}
