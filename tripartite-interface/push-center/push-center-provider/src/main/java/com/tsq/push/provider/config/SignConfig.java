package com.tsq.push.provider.config;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SignConfig {

    @Value("${push.region-id}")
    private String regionId;

    @Value("${push.access-key-id}")
    private String accessKeyId;

    @Value("${push.secret}")
    private String secret;

    @Bean("sign")
    public DefaultAcsClient sign() {
        IClientProfile profile = DefaultProfile.getProfile(regionId, accessKeyId, secret);
        DefaultAcsClient client = new DefaultAcsClient(profile);
//        pushRequest.setStoreOffline(true); // 离线消息是否保存,若保存, 在推送时候，用户即使不在线，下一次上线则会收到
        return client;
    }

}
