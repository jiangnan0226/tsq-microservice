package com.tsq.push.provider.controller;

import com.aliyuncs.push.model.v20160801.*;
import com.tsq.common.model.ResponseData;
import com.tsq.push.api.dto.DeviceDTO;
import com.tsq.push.provider.service.DeviceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/device")
@Api(value = "DeviceController", tags = {"TAG、Alias、Account、查询相关接口"})
public class DeviceController {

    @Autowired
    private DeviceService deviceService;

    /**
     * APP维度推送统计
     */
    @ApiOperation(value = "APP维度推送统计", notes = "APP维度推送统计")
    @GetMapping(value = "/queryPushStatByApp")
    public ResponseData<QueryPushStatByAppResponse> queryPushStatByApp(DeviceDTO deviceDTO){
        return deviceService.queryPushStatByApp(deviceDTO);
    }

    /**
     * 任务维度推送统计
     */
    @ApiOperation(value = "任务维度推送统计", notes = "任务维度推送统计")
    @GetMapping(value = "/queryPushStatByMsg")
    public ResponseData<QueryPushStatByMsgResponse> queryPushStatByMsg(DeviceDTO deviceDTO){
        return deviceService.queryPushStatByMsg(deviceDTO);
    }

    /**
     * 设备新增与留存/查询App维度的设备统计
     */
    @ApiOperation(value = "设备新增与留存", notes = "设备新增与留存")
    @GetMapping(value = "/queryDeviceStat")
    public ResponseData<QueryDeviceStatResponse> queryDeviceStat(DeviceDTO deviceDTO){
        return deviceService.queryDeviceStat(deviceDTO);
    }

    /**
     * 去重设备统计
     */
    @ApiOperation(value = "去重设备统计", notes = "去重设备统计")
    @GetMapping(value = "/queryUniqueDeviceStat")
    public ResponseData<QueryUniqueDeviceStatResponse> queryUniqueDeviceStat(DeviceDTO deviceDTO){
        return deviceService.queryUniqueDeviceStat(deviceDTO);
    }

    /**
     * 查询设备详情
     */
    @ApiOperation(value = "查询设备详情", notes = "查询设备详情")
    @GetMapping(value = "/queryDeviceInfo")
    public ResponseData<QueryDeviceInfoResponse> queryDeviceInfo(DeviceDTO deviceDTO){
        return deviceService.queryDeviceInfo(deviceDTO);
    }

    /**
     * 批量检查设备有效性
     */
    @ApiOperation(value = "批量检查设备有效性", notes = "批量检查设备有效性")
    @GetMapping(value = "/checkDevices")
    public ResponseData<CheckDevicesResponse> checkDevices(DeviceDTO deviceDTO){
        return deviceService.checkDevices(deviceDTO);
    }

    /**
     * 查询TAG
     */
    @ApiOperation(value = "查询TAG", notes = "查询TAG")
    @GetMapping(value = "/queryTags")
    public ResponseData<QueryTagsResponse> queryTags(DeviceDTO deviceDTO){
        return deviceService.queryTags(deviceDTO);
    }
    /**
     * 绑定TAG
     */
    @ApiOperation(value = "绑定TAG", notes = "绑定TAG")
    @PostMapping(value = "/bindTag")
    public ResponseData<BindTagResponse> bindTag(@RequestBody DeviceDTO deviceDTO){
        return deviceService.bindTag(deviceDTO);
    }

    /**
     * 解绑TAG
     */
    @ApiOperation(value = "绑定TAG", notes = "绑定TAG")
    @PostMapping(value = "/unbindTag")
    public ResponseData<UnbindTagResponse> unbindTag(@RequestBody DeviceDTO deviceDTO){
        return deviceService.unbindTag(deviceDTO);
    }

    /**
     * TAG列表
     */
    @ApiOperation(value = "TAG列表", notes = "TAG列表")
    @GetMapping(value = "/listTags")
    public ResponseData<ListTagsResponse> listTags(DeviceDTO deviceDTO){
        return deviceService.listTags(deviceDTO);
    }

    /**
     * 删除TAG
     */
    @ApiOperation(value = "删除TAG", notes = "删除TAG")
    @PostMapping(value = "/removeTag")
    public ResponseData<RemoveTagResponse> removeTag(@RequestBody DeviceDTO deviceDTO){
        return deviceService.removeTag(deviceDTO);
    }

    /**
     * 查询别名
     */
    @ApiOperation(value = "查询别名", notes = "查询别名测试")
    @GetMapping(value = "/queryAliases")
    public ResponseData<QueryAliasesResponse> queryAliases(DeviceDTO deviceDTO) {
        return deviceService.queryAliases(deviceDTO);
    }

    /**
     * 绑定别名
     */
    @ApiOperation(value = "绑定别名", notes = "绑定别名")
    @PostMapping(value = "/bindAliases")
    public ResponseData<BindAliasResponse> bindAliases(@RequestBody DeviceDTO deviceDTO){
        return deviceService.bindAliases(deviceDTO);
    }
    /**
     * 通过别名查询设备列表
     * @param deviceDTO
     * @return
     */
    @ApiOperation(value = "通过别名查询设备列表", notes = "通过别名查询设备列表")
    @GetMapping(value = "/queryDevicesByAlias")
    public ResponseData<QueryDevicesByAliasResponse> queryDevicesByAlias(DeviceDTO deviceDTO){
        return deviceService.queryDevicesByAlias(deviceDTO);
    }

    /**
     * 解绑别名
     * @param deviceDTO
     * @return
     */
    @ApiOperation(value = "解绑别名", notes = "解绑别名")
    @PostMapping(value = "/unbindAlias")
    public ResponseData<UnbindAliasResponse> unbindAlias(@RequestBody DeviceDTO deviceDTO){
        return deviceService.unbindAlias(deviceDTO);
    }

    /**
     * 通过账户查询设备列表
     * @param deviceDTO
     * @return
     */
    @ApiOperation(value = "通过账户查询设备列表", notes = "通过账户查询设备列表")
    @GetMapping(value = "/queryDevicesByAccount")
    public ResponseData<QueryDevicesByAccountResponse> queryDevicesByAccount(DeviceDTO deviceDTO){
        return deviceService.queryDevicesByAccount(deviceDTO);
    }

}
