package com.tsq.push.provider.controller;

import com.aliyuncs.push.model.v20160801.*;
import com.tsq.common.model.ResponseData;
import com.tsq.push.api.dto.*;
import com.tsq.push.provider.service.PushService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/push")
@Api(value = "PushController", tags = {"推送接口"})
public class PushController{

    @Autowired
    private PushService pushService;


    @ApiOperation(value = "单人推送", notes = "单人推送")
    @PostMapping("/to")
    public ResponseData<PushResponse> pushTo(@RequestBody PushDTO pushDTO){

        return pushService.pushTo(pushDTO);
    }


    @ApiOperation(value = "批量推送接口", notes = "批量推送接口")
    @PostMapping(value = "/mass")
    public ResponseData<MassPushResponse> pushMass(@RequestBody MassPushRequest massPushRequest) {
        return pushService.pushMass(massPushRequest);
    }










    /**
     * 持续推送接口
     * @param continuouslyPushDTO
     * @return
     */
    @ApiOperation(value = "持续推送接口", notes = "持续推送接口")
    @PostMapping(value = "/continuouslyPush")
    public ResponseData<ContinuouslyPushResponse> continuouslyPush(@RequestBody ContinuouslyPushDTO continuouslyPushDTO) {
        return pushService.continuouslyPush(continuouslyPushDTO);
    }


    /**
     * 批量推送接口
     * @param massPushDTO
     * @return
     */
    @ApiOperation(value = "批量推送接口", notes = "批量推送接口")
    @PostMapping(value = "/massPush")
    public ResponseData<MassPushResponse> massPush(@RequestBody MassPushDTO massPushDTO){
        return pushService.massPush(massPushDTO);
    }


    /**
     * 取消定时推送任务
     *  @param cancelPushDTO
     *  @return
     */
    @ApiOperation(value = "取消定时推送任务", notes = "取消定时推送任务")
    @PostMapping(value = "cancelPush")
    public ResponseData<CancelPushResponse> cancelPush(@RequestBody CancelPushDTO cancelPushDTO){
        return pushService.cancelPush(cancelPushDTO);
    }
}
