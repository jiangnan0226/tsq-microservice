package com.tsq.push.provider.builder;

import com.aliyuncs.RpcAcsRequest;
import com.tsq.push.api.dto.PushDTO;

/**
 * 推送建造
 *
 * @author Tango
 * @since 2020/8/11
 */
public interface PushBuilder {

    static SinglePushBuilder one() {
        return new SinglePushBuilder();
    }

    static MassPushBuilder many() {
        return new MassPushBuilder();
    }

    PushBuilder key(String appKey);

    PushBuilder device(String target, String targetValue);

    PushBuilder info(String pushType, String title, String body);

    RpcAcsRequest<?> buildWithDTO(PushDTO pushDto);

    RpcAcsRequest<?> build();
}
