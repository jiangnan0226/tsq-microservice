package com.tsq.push.provider.service.impl;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.push.model.v20160801.*;
import com.tsq.common.model.ResponseData;
import com.tsq.push.api.dto.DeviceDTO;
import com.tsq.push.provider.service.DeviceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
@Slf4j
public class DeviceServiceImpl implements DeviceService {

    @Autowired
    private DefaultAcsClient sign;

    @Override
    public ResponseData<QueryPushStatByAppResponse> queryPushStatByApp(DeviceDTO deviceDTO) {
        QueryPushStatByAppRequest queryPushStatByAppRequest=new QueryPushStatByAppRequest();
        queryPushStatByAppRequest.setActionName("QueryPushStatByApp");
        queryPushStatByAppRequest.setAppKey(deviceDTO.getAppKey());
        queryPushStatByAppRequest.setEndTime(deviceDTO.getEndTime());
        queryPushStatByAppRequest.setGranularity(deviceDTO.getGranularity());
        queryPushStatByAppRequest.setStartTime(deviceDTO.getStartTime());
//        DefaultAcsClient client =sign();
        QueryPushStatByAppResponse queryPushStatByAppResponse = null;
        try {
            queryPushStatByAppResponse = sign.getAcsResponse(queryPushStatByAppRequest);
        } catch (ClientException e) {
            log.info("context", e);
            return ResponseData.error("请求失败！Code："+e.getErrCode()+",Msg："+e.getErrMsg());
        }
        return ResponseData.success(queryPushStatByAppResponse);
    }

    @Override
    public ResponseData<QueryPushStatByMsgResponse> queryPushStatByMsg(DeviceDTO deviceDTO) {
        QueryPushStatByMsgRequest queryPushStatByMsgRequest=new QueryPushStatByMsgRequest();
        queryPushStatByMsgRequest.setActionName("QueryPushStatByMsg");
        queryPushStatByMsgRequest.setAppKey(deviceDTO.getAppKey());
        queryPushStatByMsgRequest.setMessageId(deviceDTO.getMessageId());
//        DefaultAcsClient client =sign();
        QueryPushStatByMsgResponse queryPushStatByMsgResponse = null;
        try {
            queryPushStatByMsgResponse = sign.getAcsResponse(queryPushStatByMsgRequest);
        } catch (ClientException e) {
            log.info("context", e);
            return ResponseData.error("请求失败！Code："+e.getErrCode()+",Msg："+e.getErrMsg());
        }
        return ResponseData.success(queryPushStatByMsgResponse);
    }

    @Override
    public ResponseData<QueryDeviceStatResponse> queryDeviceStat(DeviceDTO deviceDTO) {
        QueryDeviceStatRequest queryDeviceStatRequest=new QueryDeviceStatRequest();
        queryDeviceStatRequest.setActionName("QueryDeviceStat");
        queryDeviceStatRequest.setAppKey(deviceDTO.getAppKey());
        queryDeviceStatRequest.setDeviceType(deviceDTO.getDeviceType());
        queryDeviceStatRequest.setEndTime(deviceDTO.getEndTime());
        queryDeviceStatRequest.setQueryType(deviceDTO.getQueryType());
        queryDeviceStatRequest.setStartTime(deviceDTO.getStartTime());
//        DefaultAcsClient client =sign();
        QueryDeviceStatResponse queryDeviceStatResponse = null;
        try {
            queryDeviceStatResponse = sign.getAcsResponse(queryDeviceStatRequest);
        } catch (ClientException e) {
            log.info("context", e);
            return ResponseData.error("请求失败！Code："+e.getErrCode()+",Msg："+e.getErrMsg());
        }
        return ResponseData.success(queryDeviceStatResponse);
    }

    @Override
    public ResponseData<QueryUniqueDeviceStatResponse> queryUniqueDeviceStat(DeviceDTO deviceDTO) {
        QueryUniqueDeviceStatRequest queryUniqueDeviceStatRequest=new QueryUniqueDeviceStatRequest();
        queryUniqueDeviceStatRequest.setActionName("QueryUniqueDeviceStat");
        queryUniqueDeviceStatRequest.setAppKey(deviceDTO.getAppKey());
        queryUniqueDeviceStatRequest.setEndTime(deviceDTO.getEndTime());
        queryUniqueDeviceStatRequest.setGranularity(deviceDTO.getGranularity());
        queryUniqueDeviceStatRequest.setStartTime(deviceDTO.getStartTime());
//        DefaultAcsClient client =sign();
        QueryUniqueDeviceStatResponse queryUniqueDeviceStatResponse = null;
        try {
            queryUniqueDeviceStatResponse = sign.getAcsResponse(queryUniqueDeviceStatRequest);
        } catch (ClientException e) {
            log.info("context", e);
            return ResponseData.error("请求失败！Code："+e.getErrCode()+",Msg："+e.getErrMsg());
        }
        return ResponseData.success(queryUniqueDeviceStatResponse);
    }

    @Override
    public ResponseData<QueryDeviceInfoResponse> queryDeviceInfo(DeviceDTO deviceDTO) {
        QueryDeviceInfoRequest queryDeviceInfoRequest=new QueryDeviceInfoRequest();
        queryDeviceInfoRequest.setActionName("QueryDeviceInfo");
        queryDeviceInfoRequest.setAppKey(deviceDTO.getAppKey());
        queryDeviceInfoRequest.setDeviceId(deviceDTO.getDeviceId());
//        DefaultAcsClient client =sign();
        QueryDeviceInfoResponse queryDeviceInfoResponse = null;
            try {
            queryDeviceInfoResponse = sign.getAcsResponse(queryDeviceInfoRequest);
        } catch (ClientException e) {
            log.info("context", e);
            return ResponseData.error("请求失败！Code："+e.getErrCode()+",Msg："+e.getErrMsg());
        }
        return ResponseData.success(queryDeviceInfoResponse);
    }

    @Override
    public ResponseData<CheckDevicesResponse> checkDevices(DeviceDTO deviceDTO) {
        CheckDevicesRequest checkDevicesRequest=new CheckDevicesRequest();
        checkDevicesRequest.setActionName("CheckDevices");
        checkDevicesRequest.setAppKey(deviceDTO.getAppKey());
        checkDevicesRequest.setDeviceIds(deviceDTO.getDeviceIds());
//        DefaultAcsClient client =sign();
        CheckDevicesResponse checkDevicesResponse = null;
        try {
            checkDevicesResponse = sign.getAcsResponse(checkDevicesRequest);
        } catch (ClientException e) {
            log.info("context", e);
            return ResponseData.error("请求失败！Code："+e.getErrCode()+",Msg："+e.getErrMsg());
        }
        return ResponseData.success(checkDevicesResponse);
    }

    @Override
    public ResponseData<QueryTagsResponse> queryTags(DeviceDTO deviceDTO) {
        QueryTagsRequest queryTagsRequest=new QueryTagsRequest();
        queryTagsRequest.setActionName("QueryTags");
        queryTagsRequest.setAppKey(deviceDTO.getAppKey());
        queryTagsRequest.setClientKey(deviceDTO.getClientKey());
        queryTagsRequest.setKeyType(deviceDTO.getKeyType());
//        DefaultAcsClient client =sign();
        QueryTagsResponse queryTagsResponse = null;
        try {
            queryTagsResponse = sign.getAcsResponse(queryTagsRequest);
        } catch (ClientException e) {
            log.info("context", e);
            return ResponseData.error("请求失败！Code："+e.getErrCode()+",Msg："+e.getErrMsg());
        }
        return ResponseData.success(queryTagsResponse);
    }

    @Override
    public ResponseData<BindTagResponse> bindTag(DeviceDTO deviceDTO) {
        BindTagRequest bindTagRequest=new BindTagRequest();
        bindTagRequest.setActionName("BindTag");
        bindTagRequest.setAppKey(deviceDTO.getAppKey());
        bindTagRequest.setClientKey(deviceDTO.getClientKey());
        bindTagRequest.setKeyType(deviceDTO.getKeyType());
        bindTagRequest.setTagName(deviceDTO.getTagName());
//        DefaultAcsClient client =sign();
        BindTagResponse bindTagResponse = null;
        try {
            bindTagResponse = sign.getAcsResponse(bindTagRequest);
        } catch (ClientException e) {
            log.info("context", e);
            return ResponseData.error("请求失败！Code："+e.getErrCode()+",Msg："+e.getErrMsg());
        }
        return ResponseData.success(bindTagResponse);
    }

    @Override
    public ResponseData<UnbindTagResponse> unbindTag(DeviceDTO deviceDTO) {
        UnbindTagRequest unbindTagRequest=new UnbindTagRequest();
        unbindTagRequest.setActionName("UnbindTag");
        unbindTagRequest.setAppKey(deviceDTO.getAppKey());
        unbindTagRequest.setClientKey(deviceDTO.getClientKey());
        unbindTagRequest.setKeyType(deviceDTO.getKeyType());
        unbindTagRequest.setTagName(deviceDTO.getTagName());
//        DefaultAcsClient client =sign();
        UnbindTagResponse unbindTagResponse = null;
        try {
            unbindTagResponse = sign.getAcsResponse(unbindTagRequest);
        } catch (ClientException e) {
            log.info("context", e);
            return ResponseData.error("请求失败！Code："+e.getErrCode()+",Msg："+e.getErrMsg());
        }
        return ResponseData.success(unbindTagResponse);
    }

    @Override
    public ResponseData<ListTagsResponse> listTags(DeviceDTO deviceDTO) {
        ListTagsRequest listTagsRequest=new ListTagsRequest();
        listTagsRequest.setActionName("ListTags");
        listTagsRequest.setAppKey(deviceDTO.getAppKey());
//        DefaultAcsClient client =sign();
        ListTagsResponse listTagsResponse = null;
        try {
            listTagsResponse = sign.getAcsResponse(listTagsRequest);
        } catch (ClientException e) {
            log.info("context", e);
            return ResponseData.error("请求失败！Code："+e.getErrCode()+",Msg："+e.getErrMsg());
        }
        return ResponseData.success(listTagsResponse);
    }

    @Override
    public ResponseData<RemoveTagResponse> removeTag(DeviceDTO deviceDTO) {
        RemoveTagRequest removeTagRequest=new RemoveTagRequest();
        removeTagRequest.setActionName("RemoveTag");
        removeTagRequest.setAppKey(deviceDTO.getAppKey());
        removeTagRequest.setTagName(deviceDTO.getTagName());
//        DefaultAcsClient client =sign();
        RemoveTagResponse removeTagResponse = null;
        try {
            removeTagResponse = sign.getAcsResponse(removeTagRequest);
        } catch (ClientException e) {
            log.info("context", e);
            return ResponseData.error("请求失败！Code："+e.getErrCode()+",Msg："+e.getErrMsg());
        }
        return ResponseData.success(removeTagResponse);
    }

    public ResponseData<QueryAliasesResponse> queryAliases(DeviceDTO deviceDTO){
        QueryAliasesRequest queryAliasesRequest=new QueryAliasesRequest();
        queryAliasesRequest.setActionName("QueryAliases");
        queryAliasesRequest.setAppKey(deviceDTO.getAppKey());
        queryAliasesRequest.setDeviceId(deviceDTO.getDeviceId());
//        DefaultAcsClient client =sign();
        QueryAliasesResponse queryAliasesResponse = null;
        try {
            queryAliasesResponse = sign.getAcsResponse(queryAliasesRequest);
        } catch (ClientException e) {
            log.info("context", e);
            return ResponseData.error("请求失败！Code："+e.getErrCode()+",Msg："+e.getErrMsg());
        }
        return ResponseData.success(queryAliasesResponse);
    }

    @Override
    public ResponseData<BindAliasResponse> bindAliases(DeviceDTO deviceDTO) {
        BindAliasRequest bindAliasRequest=new BindAliasRequest();
        bindAliasRequest.setActionName("BindAlias");
        bindAliasRequest.setAppKey(deviceDTO.getAppKey());
        bindAliasRequest.setDeviceId(deviceDTO.getDeviceId());
        bindAliasRequest.setAliasName(deviceDTO.getAliasName());
//        DefaultAcsClient client =sign();
        BindAliasResponse bindAliasResponse = null;
        try {
            bindAliasResponse = sign.getAcsResponse(bindAliasRequest);
        } catch (ClientException e) {
            log.info("context", e);
            return ResponseData.error("请求失败！Code："+e.getErrCode()+",Msg："+e.getErrMsg());
        }
        return ResponseData.success(bindAliasResponse);
    }

    @Override
    public ResponseData<QueryDevicesByAliasResponse> queryDevicesByAlias(DeviceDTO deviceDTO) {
        QueryDevicesByAliasRequest queryDevicesByAliasRequest=new QueryDevicesByAliasRequest();
        queryDevicesByAliasRequest.setActionName("QueryDevicesByAlias");
        queryDevicesByAliasRequest.setAppKey(deviceDTO.getAppKey());
        queryDevicesByAliasRequest.setAlias(deviceDTO.getAlias());
//        DefaultAcsClient client =sign();
        QueryDevicesByAliasResponse queryDevicesByAliasResponse = null;
        try {
            queryDevicesByAliasResponse = sign.getAcsResponse(queryDevicesByAliasRequest);
        } catch (ClientException e) {
            log.info("context", e);
            return ResponseData.error("请求失败！Code："+e.getErrCode()+",Msg："+e.getErrMsg());
        }
        return ResponseData.success(queryDevicesByAliasResponse);
    }

    @Override
    public ResponseData<UnbindAliasResponse> unbindAlias(DeviceDTO deviceDTO) {
        UnbindAliasRequest unbindAliasRequest=new UnbindAliasRequest();
        unbindAliasRequest.setActionName("UnbindAlias");
        unbindAliasRequest.setAppKey(deviceDTO.getAppKey());
        unbindAliasRequest.setDeviceId(deviceDTO.getDeviceId());
        unbindAliasRequest.setAliasName(deviceDTO.getAliasName());
        unbindAliasRequest.setUnbindAll(deviceDTO.getUnbindAll());
//        DefaultAcsClient client =sign();
        UnbindAliasResponse unbindAliasResponse = null;
        try {
            unbindAliasResponse = sign.getAcsResponse(unbindAliasRequest);
        } catch (ClientException e) {
            log.info("context", e);
            return ResponseData.error("请求失败！Code："+e.getErrCode()+",Msg："+e.getErrMsg());
        }
        return ResponseData.success(unbindAliasResponse);
    }

    @Override
    public ResponseData<QueryDevicesByAccountResponse> queryDevicesByAccount(DeviceDTO deviceDTO) {
        QueryDevicesByAccountRequest queryDevicesByAccountRequest=new QueryDevicesByAccountRequest();
        queryDevicesByAccountRequest.setActionName("QueryDevicesByAccount");
        queryDevicesByAccountRequest.setAppKey(deviceDTO.getAppKey());
        queryDevicesByAccountRequest.setAccount(deviceDTO.getAccount());
//        DefaultAcsClient client =sign();
        QueryDevicesByAccountResponse queryDevicesByAccountResponse = null;
        try {
            queryDevicesByAccountResponse = sign.getAcsResponse(queryDevicesByAccountRequest);
        } catch (ClientException e) {
            log.info("context", e);
            return ResponseData.error("请求失败！Code："+e.getErrCode()+",Msg："+e.getErrMsg());
        }
        return ResponseData.success(queryDevicesByAccountResponse);
    }

}
