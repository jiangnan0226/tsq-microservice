package com.tsq.push.provider;

import org.minbox.framework.api.boot.autoconfigure.swagger.annotation.EnableApiBootSwagger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
//import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication(scanBasePackages = "com.tsq")
//@EnableDiscoveryClient
@EnableApiBootSwagger
@EnableCaching
public class PushCenterProviderApplication {
    public static void main(String[] args) {
        SpringApplication.run(PushCenterProviderApplication.class, args);
    }
}
