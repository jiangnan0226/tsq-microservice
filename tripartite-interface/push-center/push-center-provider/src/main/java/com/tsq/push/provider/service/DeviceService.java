package com.tsq.push.provider.service;

import com.aliyuncs.push.model.v20160801.*;
import com.tsq.common.model.ResponseData;
import com.tsq.push.api.dto.DeviceDTO;

public interface DeviceService {
    /**
     * APP维度推送统计
     * @param deviceDTO
     * @return
     */
    ResponseData<QueryPushStatByAppResponse> queryPushStatByApp(DeviceDTO deviceDTO);
    /**
     * 任务维度推送统计
     * @param deviceDTO
     * @return
     */
    ResponseData<QueryPushStatByMsgResponse> queryPushStatByMsg(DeviceDTO deviceDTO);
    /**
     * 设备新增与留存/查询App维度的设备统计
     * @param deviceDTO
     * @return
     */
    ResponseData<QueryDeviceStatResponse> queryDeviceStat(DeviceDTO deviceDTO);

    /**
     * 去重设备统计
     * @param deviceDTO
     * @return
     */
    ResponseData<QueryUniqueDeviceStatResponse> queryUniqueDeviceStat(DeviceDTO deviceDTO);

    /**
     * 查询设备详情
     * @param deviceDTO
     * @return
     */
    ResponseData<QueryDeviceInfoResponse> queryDeviceInfo(DeviceDTO deviceDTO);

    /**
     * 批量检查设备有效性
     * @param deviceDTO
     * @return
     */
    ResponseData<CheckDevicesResponse> checkDevices(DeviceDTO deviceDTO);

    /**
     * 查询TAG
     * @param deviceDTO
     * @return
     */
    ResponseData<QueryTagsResponse> queryTags(DeviceDTO deviceDTO);

    /**
     * 绑定TAG
     * @param deviceDTO
     * @return
     */
    ResponseData<BindTagResponse> bindTag(DeviceDTO deviceDTO);

    /**
     * 解绑TAG
     * @param deviceDTO
     * @return
     */
    ResponseData<UnbindTagResponse> unbindTag(DeviceDTO deviceDTO);

    /**
     * TAG列表
     * @param deviceDTO
     * @return
     */
    ResponseData<ListTagsResponse> listTags(DeviceDTO deviceDTO);

    /**
     * 删除TAG
     * @param deviceDTO
     * @return
     */
    ResponseData<RemoveTagResponse> removeTag(DeviceDTO deviceDTO);

    /**
     * 查询别名
     * @param deviceDTO
     * @return
     */
    ResponseData<QueryAliasesResponse> queryAliases(DeviceDTO deviceDTO);
    /**
     * 绑定别名
     * @param deviceDTO
     * @return
     */
    ResponseData<BindAliasResponse> bindAliases(DeviceDTO deviceDTO);

    /**
     * 通过别名查询设备列表
     * @param deviceDTO
     * @return
     */
    ResponseData<QueryDevicesByAliasResponse> queryDevicesByAlias(DeviceDTO deviceDTO);

    /**
     * 解绑别名
     * @param deviceDTO
     * @return
     */
    ResponseData<UnbindAliasResponse> unbindAlias(DeviceDTO deviceDTO);
    /**
     * 通过账户查询设备列表
     * @param deviceDTO
     * @return
     */
    ResponseData<QueryDevicesByAccountResponse> queryDevicesByAccount(DeviceDTO deviceDTO);
}
