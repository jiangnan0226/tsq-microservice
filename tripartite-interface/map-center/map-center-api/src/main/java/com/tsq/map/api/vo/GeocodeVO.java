package com.tsq.map.api.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * <p>
 * 地理编码返回参数
 * </p>
 *
 * @author xhy
 * @since 2020-06-22
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "GeocodeVO对象", description = "地理编码返回参数")
public class GeocodeVO {

    @ApiModelProperty("返回状态")
    private Integer status;

    @ApiModelProperty("返回的状态信息")
    private String info;

    @ApiModelProperty(value = "返回参数数组")
    private List<Geocodes> geocodes;

    /**
     * <p>地理编码返回结果集</p>
     * */
    @Data
    public class Geocodes{

        @ApiModelProperty(value = "结构化地址信息")
        private String formattedAddress;

        @ApiModelProperty(value = "国家")
        private String country;

        @ApiModelProperty(value = "地址所在的省份名")
        private String province;

        @ApiModelProperty(value = "地址所在的城市名")
        private String city;

        @ApiModelProperty(value = "城市编码")
        private String citycode;

        @ApiModelProperty(value = "地址所在的区")
        private String district;

        @ApiModelProperty(value = "街道")
        private String street;

        @ApiModelProperty(value = "门牌")
        private String number;

        @ApiModelProperty(value = "区域编码")
        private String adcode;

        @ApiModelProperty(value = "坐标点")
        private String location;

        @ApiModelProperty(value = "匹配级别")
        private String level;
    }

}
