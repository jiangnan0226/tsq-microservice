package com.tsq.map.api.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * <p>
 * 输出提示返回参数
 * </p>
 * @author xhy
 * @since 2020-06-22
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "AssistantVO对象", description = "输入提示返回参数")
public class AssistantVO {

    @ApiModelProperty("返回状态")
    private Integer status;

    @ApiModelProperty("返回的状态信息")
    private String info;

    @ApiModelProperty("建议提示列表")
    private List<TipsBean> tips;

    @Data
    public static class TipsBean{

        @ApiModelProperty("返回数据ID")
        private String id;

        @ApiModelProperty("tip名称")
        private String name;

        @ApiModelProperty("所属区域")
        private String district;

        @ApiModelProperty("区域编码")
        private String adcode;

        @ApiModelProperty("tip中心点坐标")
        private String location;

        @ApiModelProperty("详细地址")
        private String address;
    }
}
