package com.tsq.map.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ApiModel(value = "DistrictDTO", description = "行政区域查询请求参数")
public class DistrictDTO {

    @ApiModelProperty("查询关键字")
    private String keywords;

    @ApiModelProperty("子级行政区")
    private String subdistrict;

    @ApiModelProperty("需要第几页数据")
    private String page;

    @ApiModelProperty("最外层返回数据个数")
    private String offset;

    @ApiModelProperty("返回结果控制")
    private String extensions;

    @ApiModelProperty("根据区划过滤")
    private String filter;

    @ApiModelProperty("回调函数")
    private String callback;

    @ApiModelProperty("返回数据格式类型")
    private String output;
}
