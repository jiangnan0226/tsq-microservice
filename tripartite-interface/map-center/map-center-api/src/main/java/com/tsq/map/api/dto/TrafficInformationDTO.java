package com.tsq.map.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ApiModel(value = "TrafficInformationDTO对象", description = "交通态势请求参数")
public class TrafficInformationDTO {

    @ApiModelProperty("道路等级")
    private String level;

    @ApiModelProperty("返回结果控制")
    private String extensions;

    @ApiModelProperty("回调函数")
    private String callback;

    @ApiModelProperty("返回数据格式类型")
    private String output;

    @ApiModelProperty("代表此为矩形区域查询")
    private String rectangle;

    @ApiModelProperty("中心点坐标")
    private String location;

    @ApiModelProperty("半径")
    private String radius;

    @ApiModelProperty("道路名")
    private String name;

    @ApiModelProperty("城市名")
    private String city;

    @ApiModelProperty("城市编码")
    private String adcode;
}
