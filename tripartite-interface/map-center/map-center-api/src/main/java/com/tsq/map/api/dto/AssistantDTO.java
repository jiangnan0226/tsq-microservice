package com.tsq.map.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ApiModel(value = "AssistantDTO对象", description = "输出提示请求参数")
public class AssistantDTO{

    @ApiModelProperty("查询关键词")
    private String keywords;

    @ApiModelProperty("POI分类")
    private String type;

    @ApiModelProperty("坐标")
    private String location;

    @ApiModelProperty("搜索城市")
    private String city;

    @ApiModelProperty("仅返回指定城市数据")
    private String citylimit;

    @ApiModelProperty("返回的数据类型")
    private String datatype;

    @ApiModelProperty("返回数据格式类型")
    private String output;

    @ApiModelProperty("回调函数")
    private String callback;
}
