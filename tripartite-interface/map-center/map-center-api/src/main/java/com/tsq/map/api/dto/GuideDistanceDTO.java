package com.tsq.map.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ApiModel(value = "GuideDistanceDTO对象", description = "路径规划（距离测量）请求参数")
public class GuideDistanceDTO {

    @ApiModelProperty("出发点")
    private String origins;

    @ApiModelProperty("目的地")
    private String destination;

    @ApiModelProperty("路径计算的方式和方法")
    private String type;

    @ApiModelProperty("返回数据格式类型")
    private String output;

    @ApiModelProperty("回调函数")
    private String callback;
}
