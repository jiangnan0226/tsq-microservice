package com.tsq.map.api.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
@ApiModel(value = "GuideDistanceVO对象", description = "路径规划（距离测量）返回参数")
public class GuideDistanceVO {

    @ApiModelProperty("返回状态")
    private Integer status;

    @ApiModelProperty("返回的状态信息")
    private String info;

    @ApiModelProperty("距离信息列表")
    private List<ResultsBean> results;

    @Data
    public static class ResultsBean {
        @ApiModelProperty("起点坐标，起点坐标序列号（从１开始）")
        private String originId;

        @ApiModelProperty("终点坐标，终点坐标序列号（从１开始） ")
        private String destId;

        @ApiModelProperty("路径距离，单位：米 ")
        private String distance;

        @ApiModelProperty("预计行驶时间，单位：秒 ")
        private String duration;
    }
}
