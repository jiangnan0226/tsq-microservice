package com.tsq.map.api.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ApiModel(value = "StatusGeofenceVO对象", description = "更新,激活，删除围栏返回参数")
public class StatusGeofenceVO {

    @ApiModelProperty("返回状态")
    private Integer errcode;

    @ApiModelProperty("返回的状态信息")
    private String errmsg;

    @ApiModelProperty("返回数据内容消息体")
    private DataBean data;

    @Data
    public static class DataBean{

        @ApiModelProperty("状态说明信息")
        private String message;

        @ApiModelProperty("状态值")
        private String status;
    }
}
