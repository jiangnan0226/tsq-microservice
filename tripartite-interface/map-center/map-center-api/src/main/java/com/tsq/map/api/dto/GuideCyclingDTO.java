package com.tsq.map.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ApiModel(value = "GuideCyclingDTO对象", description = "路径规划（骑行方案）请求参数")
public class GuideCyclingDTO {

    @ApiModelProperty("出发点经纬度")
    private String origin;

    @ApiModelProperty("目的地经纬度")
    private String destination;

}
