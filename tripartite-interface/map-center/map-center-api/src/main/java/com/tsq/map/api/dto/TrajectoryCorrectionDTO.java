package com.tsq.map.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@ApiModel(value = "TrajectoryCorrectionDTO对象", description = "轨迹纠偏请求参数")
public class TrajectoryCorrectionDTO {

        @ApiModelProperty("经度")
        private BigDecimal x;

        @ApiModelProperty("纬度")
        private BigDecimal y;

        @ApiModelProperty("角度")
        private Integer ag;

        @ApiModelProperty("时间")
        private Integer tm;

        @ApiModelProperty("速度")
        private Integer sp;

}
