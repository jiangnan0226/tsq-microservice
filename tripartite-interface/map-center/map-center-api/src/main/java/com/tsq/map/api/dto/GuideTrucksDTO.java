package com.tsq.map.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ApiModel(value = "GuideTrucksDTO对象", description = "路径规划（货车方案）请求参数")
public class GuideTrucksDTO {

    @ApiModelProperty("出发点经纬度")
    private String origin;

    @ApiModelProperty("出发POI的唯一编号 ")
    private String originid;

    @ApiModelProperty("出发POI的类型")
    private String originidtype;

    @ApiModelProperty("目的地经纬度")
    private String destination;

    @ApiModelProperty("终点POI的唯一编号 ")
    private String destinationid;

    @ApiModelProperty("终点POI的类型")
    private String destinationtype;

    @ApiModelProperty("设备唯一编号")
    private String diu;

    @ApiModelProperty("驾车选择策略")
    private String strategy;

    @ApiModelProperty("途经点 ")
    private String waypoints;

    @ApiModelProperty("车辆大小")
    private String size;

    @ApiModelProperty("车辆高度")
    private String height;

    @ApiModelProperty("车辆宽度")
    private String width;

    @ApiModelProperty("车辆总重")
    private String load;

    @ApiModelProperty("货车核定载重 ")
    private String weight;

    @ApiModelProperty("车辆轴数")
    private String axis;

    @ApiModelProperty("车牌省份")
    private String province;

    @ApiModelProperty("车牌详情")
    private String number;

    @ApiModelProperty("车辆类型 ")
    private String cartype;

    @ApiModelProperty("避让区域 ")
    private String avoidpolygons;

    @ApiModelProperty("是否返回路线数据")
    private String showpolyline;

    @ApiModelProperty("是否返回steps字段内容")
    private String nosteps;
}
