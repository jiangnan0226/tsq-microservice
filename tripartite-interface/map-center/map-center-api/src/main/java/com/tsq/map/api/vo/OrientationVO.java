package com.tsq.map.api.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * IP定位
 * </p>
 *
 * @author xhy
 * @since 2020-06-23
 */
@Data

@Accessors(chain = true)
@ApiModel(value = "OrientationVO对象", description = "ip定位返回信息")
public class OrientationVO{

    @ApiModelProperty("返回状态")
    private Integer status;

    @ApiModelProperty("返回的状态信息")
    private String info;

    @ApiModelProperty("省份名称")
    private String province;

    @ApiModelProperty("城市名称")
    private String city;

    @ApiModelProperty("城市的adcode编码")
    private String adcode;

    @ApiModelProperty("所在城市矩形区域范围")
    private String rectangle;
}
