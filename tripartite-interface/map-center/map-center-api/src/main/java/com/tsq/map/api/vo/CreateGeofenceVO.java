package com.tsq.map.api.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ApiModel(value = "CreateGeofenceVO对象", description = "创建围栏返回参数")
public class CreateGeofenceVO {

    @ApiModelProperty("返回状态")
    private Integer errcode;

    @ApiModelProperty("返回的状态信息")
    private String errmsg;

    @ApiModelProperty("返回数据内容消息体")
    private DataBean data;

    @Data
    public static class DataBean{

        @ApiModelProperty("围栏全局id是一个地理围栏实体的全局id（gid），是围栏的唯一标识，可以用来查找该围栏实体本身的详细信息")
        private String gid;

        @ApiModelProperty("状态说明信息")
        private String message;

        @ApiModelProperty("围栏id为预留字段，暂未使用，固定返回0。")
        private Integer id;

        @ApiModelProperty("状态值")
        private String status;

    }
}
