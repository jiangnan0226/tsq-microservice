package com.tsq.map.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ApiModel(value = "GuideRidingDTO对象", description = "路径规划（乘车方案）请求参数")
public class GuideRidingDTO {

    @ApiModelProperty("出发点")
    private String origin;

    @ApiModelProperty("目的地")
    private String destination;

    @ApiModelProperty("城市/跨城规划时的起点城市")
    private String city;

    @ApiModelProperty("跨城公交规划时的终点城市")
    private String cityd;

    @ApiModelProperty("返回结果详略")
    private String extensions;

    @ApiModelProperty("公交换乘策略")
    private String strategy;

    @ApiModelProperty("是否计算夜班车")
    private String nightflag;

    @ApiModelProperty("出发日期")
    private String date;

    @ApiModelProperty("出发时间")
    private String time;

    @ApiModelProperty("返回数据格式类型")
    private String output;

    @ApiModelProperty("回调函数")
    private String callback;
}
