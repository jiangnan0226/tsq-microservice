package com.tsq.map.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ApiModel(value = "WalkingRoutesDTO对象", description = "路径规划(步行方案)请求参数")
public class WalkingRoutesDTO {

    @ApiModelProperty("出发点")
    private String origin;

    @ApiModelProperty("目的地")
    private String destination;

    @ApiModelProperty("返回数据格式类型")
    private String output;

    @ApiModelProperty("回调函数")
    private String callback;
}
