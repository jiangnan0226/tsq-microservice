package com.tsq.map.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ApiModel(value = "MonitoringGeofenceDTO对象", description = "围栏设备监控请求参数")
public class MonitoringGeofenceDTO {

    @ApiModelProperty("用户设备唯一标识符")
    private String diu;

    @ApiModelProperty("设备在开发者自有系统中的id")
    private String uid;

    @ApiModelProperty("设备位置坐标组")
    private String locations;

}
