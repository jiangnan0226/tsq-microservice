package com.tsq.map.api.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
@ApiModel(value = "GuideDrivingVO对象", description = "路径规划（驾车方案）返回参数")
public class GuideDrivingVO {

    @ApiModelProperty("返回状态")
    private Integer status;

    @ApiModelProperty("返回的状态信息")
    private String info;

    @ApiModelProperty("驾车路径规划信息列表")
    private RouteBean route;


    @Data
    public static class RouteBean {
        @ApiModelProperty("起点坐标")
        private String origin;

        @ApiModelProperty("终点坐标")
        private String destination;

        @ApiModelProperty("打车费用")
        private String taxiCost;

        @ApiModelProperty("驾车换乘方案")
        private List<RouteBean.PathsBean> paths;


        @Data
        public static class PathsBean {
            @ApiModelProperty("行驶距离")
            private String distance;

            @ApiModelProperty("预计行驶时间")
            private String duration;

            @ApiModelProperty("导航策略")
            private String strategy;

            @ApiModelProperty("此导航方案道路收费")
            private String tolls;

            @ApiModelProperty("收费路段距离")
            private String tollDistance;

            @ApiModelProperty("限行结果")
            private String restriction;

            @ApiModelProperty("红绿灯个数")
            private String trafficLights;

            @ApiModelProperty("导航路段")
            private List<RouteBean.PathsBean.StepsBean> steps;


            @Data
            public static class StepsBean {
                @ApiModelProperty("行驶指示")
                private String instruction;

                @ApiModelProperty("方向")
                private String orientation;

                @ApiModelProperty("此路段距离")
                private String distance;

                @ApiModelProperty("此段收费")
                private String tolls;

                @ApiModelProperty("收费路段距离")
                private String tollDistance;

                @ApiModelProperty("此路段坐标点串")
                private String polyline;

                @ApiModelProperty("导航主要动作")
                private String action;

                @ApiModelProperty("道路名称")
                private String road;

                @ApiModelProperty("主要收费道路")
                private String tollRoad;

                @ApiModelProperty("导航辅助动作")
                private String assistantAction;

                @ApiModelProperty("驾车导航详细信息")
                private List<RouteBean.PathsBean.StepsBean.TmcsBean> tmcs;

                @ApiModelProperty("城市信息")
                private List<RouteBean.PathsBean.StepsBean.CitiesBean> cities;


                @Data
                public static class TmcsBean {
                    @ApiModelProperty("此段路的长度")
                    private String distance;

                    @ApiModelProperty("此段路的交通情况")
                    private String status;

                    @ApiModelProperty("此段路的轨迹")
                    private String polyline;

                }


                @Data
                public static class CitiesBean {
                    @ApiModelProperty("城市名称")
                    private String name;

                    @ApiModelProperty("城市编号")
                    private String citycode;

                    @ApiModelProperty("城市邮政编码")
                    private String adcode;

                    @ApiModelProperty("区信息")
                    private List<RouteBean.PathsBean.StepsBean.CitiesBean.DistrictsBean> districts;


                    @Data
                    public static class DistrictsBean {
                        @ApiModelProperty("区名称")
                        private String name;

                        @ApiModelProperty("区邮政编号")
                        private String adcode;
                    }
                }
            }
        }
    }
}
