package com.tsq.map.api.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
@ApiModel(value = "SelectGeofenceVO对象", description = "查询围栏返回参数")
public class SelectGeofenceVO {

    @ApiModelProperty("返回状态")
    private Integer errcode;

    @ApiModelProperty("返回的状态信息")
    private String errmsg;

    @ApiModelProperty("返回内容消息体")
    private DataBean data;


    @Data
    public static class DataBean {

        @ApiModelProperty("当前页码")
        private int pageNo;

        @ApiModelProperty("每页记录数")
        private int pageSize;

        @ApiModelProperty("总记录数")
        private int totalRecord;

        @ApiModelProperty("围栏列表")
        private List<RsListBean> rsList;

        @Data
        public static class RsListBean {

            @ApiModelProperty("围栏所在地区adcode")
            private String adcode;

            @ApiModelProperty("围栏监控触发条件")
            private String alertCondition;

            @ApiModelProperty("圆形围栏中心点")
            private String center;

            @ApiModelProperty("围栏创建时间")
            private String createTime;

            @ApiModelProperty("围栏激活状态")
            private String enable;

            @ApiModelProperty("指定日期")
            private String fixedDate;

            @ApiModelProperty("围栏全局id")
            private String gid;

            @ApiModelProperty("围栏id")
            private String id;

            @ApiModelProperty("开发者key")
            private String key;

            @ApiModelProperty("围栏名称")
            private String name;

            @ApiModelProperty("多边形围栏点")
            private String points;

            @ApiModelProperty("圆形围栏半径")
            private int radius;

            @ApiModelProperty("一周内围栏监控的重复星期")
            private String repeat;

            @ApiModelProperty("一天内监控的具体时段")
            private String time;

            @ApiModelProperty("过期日期")
            private String validTime;
        }
    }
}
