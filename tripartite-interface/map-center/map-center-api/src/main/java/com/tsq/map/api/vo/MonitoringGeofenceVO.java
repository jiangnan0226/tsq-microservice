package com.tsq.map.api.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
@ApiModel(value = "MonitoringGeofenceDTO对象", description = "围栏设备监控返回参数")
public class MonitoringGeofenceVO {

    @ApiModelProperty("返回状态")
    private Integer errcode;

    @ApiModelProperty("返回的状态信息")
    private String errmsg;

    @ApiModelProperty("返回数据内容消息体")
    private DataBean data;

    @Data
    public static class DataBean {

        @ApiModelProperty("返回状态码 ")
        private Integer status;

        @ApiModelProperty("围栏事件列表   ")
        private List<FencingEventListBean> fencingEventList;

        @Data
        public static class FencingEventListBean {

            @ApiModelProperty("设备行为。对应3种与围栏的动态交互关系，进/出/停留： enter|leave|stay")
            private String clientAction;

            @ApiModelProperty("设备状态。当前与围栏的静态关系状态。是否在围栏里： in|out ")
            private String clientStatus;

            @ApiModelProperty("用户进入围栏时间。格式： yyyy-MM-dd HH:mm:ss ")
            private String enterTime;

            @ApiModelProperty("围栏信息")
            private FenceInfoBean fenceInfo;

            @ApiModelProperty("围栏距离设备的距离，没有命中围栏时返回。单位：米" +
                    "当设备不在围栏中时，会告知最近的围栏以及用户与这个围栏的距离，距离没有限制。")
            private FenceInfoBean nearestFenceDistance;

            @ApiModelProperty("最近的围栏id")
            private FenceInfoBean nearestDenceGid;

            @Data
            public static class FenceInfoBean {

                @ApiModelProperty("返回数据内容消息体")
                private String fenceCenter;

                @ApiModelProperty("全局围栏id")
                private String fenceGid;

                @ApiModelProperty("围栏名称")
                private String fenceName;
            }
        }
    }
}
