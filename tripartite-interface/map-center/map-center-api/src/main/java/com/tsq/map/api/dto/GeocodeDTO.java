package com.tsq.map.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ApiModel(value = "GeocodeDTO对象", description = "地理编码请求参数")
public class GeocodeDTO {

    @ApiModelProperty(value = "结构化地址信息")
    private String address;

    @ApiModelProperty(value = "指定查询的城市")
    private String city;

    @ApiModelProperty(value = "批量查询控制")
    private String batch;

    @ApiModelProperty(value = "返回数据格式类型")
    private String output;

    @ApiModelProperty(value = "回调函数")
    private String callback;
}
