package com.tsq.map.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ApiModel(value = "OrientationDTO对象", description = "ip定位请求参数")
public class OrientationDTO {

    @ApiModelProperty("ip地址")
    private String ip;

    @ApiModelProperty("返回格式")
    private String output;
}
