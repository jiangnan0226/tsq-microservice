package com.tsq.map.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ApiModel(value = "CoordinateDTO", description = "坐标转换请求参数")
public class CoordinateDTO {

    @ApiModelProperty("坐标")
    private String locations;

    @ApiModelProperty("原坐标系")
    private String coordsys;

    @ApiModelProperty("返回数据格式类型")
    private String output;


}
