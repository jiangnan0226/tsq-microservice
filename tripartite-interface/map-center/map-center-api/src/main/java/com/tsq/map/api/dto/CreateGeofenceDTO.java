package com.tsq.map.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ApiModel(value = "CreateGeofenceDTO对象", description = "创建围栏请求参数")
public class CreateGeofenceDTO {

    @ApiModelProperty("围栏名称")
    private String name;

    @ApiModelProperty("圆形围栏中心点")
    private String center;

    @ApiModelProperty("圆形围栏半径")
    private String radius;

    @ApiModelProperty("多边形围栏坐标点")
    private String points;

    @ApiModelProperty("围栏监控状态")
    private String enable;

    @ApiModelProperty("过期日期")
    private String validTime;

    @ApiModelProperty("一周内围栏监控日期的重复模式")
    private String repeat;

    @ApiModelProperty("指定日期列表")
    private String fixedDate;

    @ApiModelProperty("一天内围栏监控时段")
    private String time;

    @ApiModelProperty("围栏描述信息")
    private String desc;

    @ApiModelProperty("配置触发围栏所需动作")
    private String alertCondition;
}
