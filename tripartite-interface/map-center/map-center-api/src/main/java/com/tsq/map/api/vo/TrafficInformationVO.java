package com.tsq.map.api.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
@ApiModel(value = "TrafficInformationVO对象", description = "交通态势返回参数")
public class TrafficInformationVO {

    @ApiModelProperty("返回状态")
    private Integer status;

    @ApiModelProperty("返回的状态信息")
    private String info;

    @ApiModelProperty("交通态势信息")
    private TrafficinfoBean trafficinfo;

    @Data
    public static class TrafficinfoBean {

        @ApiModelProperty("路况综述")
        private String description;

        @ApiModelProperty("路况评价")
        private TrafficinfoBean.EvaluationBean evaluation;

        @ApiModelProperty("当extensions=all时返回,此为road列表其中包含道路信息")
        private List<TrafficinfoBean.RoadsBean> roads;

        @Data
        public static class EvaluationBean {

            @ApiModelProperty("畅通所占百分比")
            private String expedite;

            @ApiModelProperty("缓行所占百分比")
            private String congested;

            @ApiModelProperty("拥堵所占百分比")
            private String blocked;

            @ApiModelProperty("未知路段所占百分比")
            private String unknown;

            @ApiModelProperty("路况")
            private String status;

            @ApiModelProperty("道路描述")
            private String description;
        }

        @Data
        public static class RoadsBean {

            @ApiModelProperty("道路名称")
            private String name;

            @ApiModelProperty("路况")
            private String status;

            @ApiModelProperty("方向描述")
            private String direction;

            @ApiModelProperty("车行角度，判断道路正反向使用")
            private String angle;

            @ApiModelProperty("速度")
            private String speed;

            @ApiModelProperty("即locationcode的集合，是道路中某一段的id，一条路包括多个locationcode。")
            private String lcodes;

            @ApiModelProperty("道路坐标集，坐标集合")
            private String polyline;
        }
    }
}
