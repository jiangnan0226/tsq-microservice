package com.tsq.map.api.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 坐标转换返回参数
 * </p>
 * @author xhy
 * @since 2020-06-22
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "CoordinateVO对象", description = "坐标转换返回参数")
public class CoordinateVO {

    @ApiModelProperty("返回状态")
    private Integer status;

    @ApiModelProperty("返回的状态信息")
    private String info;

    @ApiModelProperty("转换之后的坐标。若有多个坐标，则用 “;”进行区分和间隔")
    private String locations;
}
