package com.tsq.map.api.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * <p>
 * POI搜索返回参数
 * </p>
 * @author xhy
 * @since 2020-06-23
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "SearchPoiVO对象", description = "搜索POI返回参数")
public class SearchPoiVO {

    @ApiModelProperty("返回状态")
    private Integer status;

    @ApiModelProperty("返回的状态信息")
    private String info;

    @ApiModelProperty("城市建议列表")
    private SuggestionBean suggestion;

    @ApiModelProperty("搜索POI信息列表")
    private List<PoisBean> pois;

    @ApiModelProperty("建议地址结果")
    private SugAddressBean sugAddressBean;

    @Data
    public static class SugAddressBean{
        @ApiModelProperty("搜索文本内容")
        private String name;

        @ApiModelProperty("地址")
        private String address;

        @ApiModelProperty("经纬度")
        private String location;

        @ApiModelProperty("国家")
        private String country;

        @ApiModelProperty("省市")
        private String pname;

        @ApiModelProperty("城市")
        private String cityname;

        @ApiModelProperty("adcode")
        private String adcode;

        @ApiModelProperty("区域名称")
        private String adname;

        @ApiModelProperty("地级市或县级市")
        private String district;

        @ApiModelProperty("所属aoi")
        private String aoi;

        @ApiModelProperty("街道名称")
        private String street;
    }
    @Data
    public static class SuggestionBean {
        @ApiModelProperty("关键字")
        private String keywords;

        @ApiModelProperty("城市列表")
        private List<CitiesBean> cities;
    }

    @Data
    public static class CitiesBean{
        @ApiModelProperty("名称")
        private String name;

        @ApiModelProperty("该城市包含此关键字的个数")
        private String num;

        @ApiModelProperty("该城市的citycode")
        private String citycode;

        @ApiModelProperty("该城市的adcode")
        private String adcode;
    }

    @Data
    public static class PoisBean {
        @ApiModelProperty("唯一ID")
        private String id;

        @ApiModelProperty("名称")
        private String name;

        @ApiModelProperty("兴趣点类型")
        private String type;

        @ApiModelProperty("兴趣点类型编码")
        private String typecode;

        @ApiModelProperty("地址")
        private String address;

        @ApiModelProperty("经纬度")
        private String location;

        @ApiModelProperty("POI的电话")
        private String tel;

        @ApiModelProperty("POI的网址")
        private String website;

        @ApiModelProperty("POI所在省份编码")
        private String pcode;

        @ApiModelProperty("POI所在省份名称")
        private String pname;

        @ApiModelProperty("城市编码")
        private String citycode;

        @ApiModelProperty("城市名")
        private String cityname;

        @ApiModelProperty("区域编码")
        private String adcode;

        @ApiModelProperty("区域名称")
        private String adname;

        @ApiModelProperty("地理格ID")
        private String gridcode;

        @ApiModelProperty("POI导航id")
        private String naviPoiid;

        @ApiModelProperty("POI的入口经纬度")
        private String entrLocation;

        @ApiModelProperty("别名")
        private String alias;

        @ApiModelProperty("是否有室内地图标志")
        private String indoorMap;

        @ApiModelProperty("室内地图相关数据")
        private PoisBean.IndoorDataBean indoorData;

        @ApiModelProperty("团购数据")
        private String groupbuyNum;

        @ApiModelProperty("优惠信息数目")
        private String discountNum;

        @ApiModelProperty("深度信息")
        private PoisBean.BizExtBean bizExt;

        @ApiModelProperty("父POI的ID")
        private String parent;

        @ApiModelProperty("该POI的特色内容")
        private String tag;

        @ApiModelProperty("行业类型")
        private String bizType;

        @ApiModelProperty("邮编")
        private String postcode;

        @ApiModelProperty("POI的电子邮箱")
        private String email;

        @ApiModelProperty("离中心点距离")
        private String distance;

        @ApiModelProperty("所属商圈")
        private String businessArea;

        @ApiModelProperty("POI的出口经纬度")
        private String exitLocation;


        @ApiModelProperty("照片相关信息")
        private List<PoisBean.PhotosBean> photos;

        @Data
        public static class IndoorDataBean {
            @ApiModelProperty("当前POI的父级POI")
            private String cpid;

            @ApiModelProperty("楼层索引")
            private String floor;

            @ApiModelProperty("所在楼层")
            private String truefloor;

            @ApiModelProperty("")
            private String cmsid;
        }

        @Data
        public static class BizExtBean {
            @ApiModelProperty("评分")
            private String rating;

            @ApiModelProperty("人均消费")
            private String cost;
        }

        @Data
        public static class PhotosBean {
            @ApiModelProperty("具体链接")
            private String url;

            @ApiModelProperty("图片介绍")
            private String title;
        }
    }
}
