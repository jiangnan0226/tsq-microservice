package com.tsq.map.api.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * <p>
 * 逆地理编码
 * </p>
 * @author xhy
 * @since 2020-06-22
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "ReverseGeocodeVO对象", description = "逆地理编码返回参数")
public class ReverseGeocodeVO{

    @ApiModelProperty("返回状态")
    private Integer status;

    @ApiModelProperty("返回的状态信息")
    private String info;

    @ApiModelProperty(value = "返回参数集")
    private Regeocode regeocode;



    /**
     * <p>返回参数结果集</p>
     * */
    @Data
    public static class Regeocode{

        @ApiModelProperty("结构化地址信息")
        private String formattedAddress;

        @ApiModelProperty(value = "地址元素列表")
        private AddressComponent addressComponent;

        @ApiModelProperty("道路信息列表")
        private List<Roads> roads;

        @ApiModelProperty("poi信息列表")
        private List<Pois> pois;

        @ApiModelProperty("道路交叉口列表")
        private List<Roadinters> roadinters;

        @ApiModelProperty("aoi信息列表")
        private List<Aois> aois;
    }
    @Data
    public static class AddressComponent{

        @ApiModelProperty("坐标点所在省名称")
        private String province;

        @ApiModelProperty("国家")
        private String country;

        @ApiModelProperty("坐标点所在城市名称")
        private String city;

        @ApiModelProperty("城市编码")
        private String citycode;

        @ApiModelProperty("坐标点所在区")
        private String district;

        @ApiModelProperty("行政区编码")
        private String adcode;

        @ApiModelProperty("坐标点所在乡镇/街道（此街道为社区街道，不是道路信息）")
        private String township;

        @ApiModelProperty("乡镇街道编码")
        private String towncode;

        @ApiModelProperty("所属海域信息")
        private String seaArea;

        @ApiModelProperty("门牌信息列表")
        private Neighborhood neighborhood;

        @ApiModelProperty("楼信息列表")
        private Building building;

        @ApiModelProperty("门牌信息列表")
        private StreetNumber streetNumber;

        @ApiModelProperty("经纬度所属商圈列表")
        private List<BusinessAreas> businessAreas;
    }

    @Data
    public static class Neighborhood{

        @ApiModelProperty("社区名称")
        private String name;

        @ApiModelProperty("POI类型")
        private String type;
    }

    @Data
    public static class Building{

        @ApiModelProperty("建筑名称")
        private String name;

        @ApiModelProperty("类型")
        private String type;
    }

    @Data
    public static class StreetNumber{

        @ApiModelProperty("街道名称")
        private String street;

        @ApiModelProperty("门牌号")
        private String number;

        @ApiModelProperty("坐标点")
        private String location;

        @ApiModelProperty("方向")
        private String direction;

        @ApiModelProperty("门牌地址到请求坐标的距离")
        private String distance;
    }

    @Data
    public static class BusinessAreas{

        @ApiModelProperty("商圈中心点经纬度")
        private String location;

        @ApiModelProperty("商圈名称")
        private String name;

        @ApiModelProperty("商圈所在区域的adcode ")
        private String id;
    }

    @Data
    public static class Roads{

        @ApiModelProperty("道路id")
        private String id;

        @ApiModelProperty("道路名称")
        private String name;

        @ApiModelProperty("道路到请求坐标的距离")
        private String distance;

        @ApiModelProperty("方位 ")
        private String direction;

        @ApiModelProperty("坐标点 ")
        private String location;
    }

    @Data
    public static class Pois{

        @ApiModelProperty("poi的id")
        private String id;

        @ApiModelProperty("poi点名称")
        private String name;

        @ApiModelProperty("poi类型")
        private String type;

        @ApiModelProperty("电话 ")
        private String tel;

        @ApiModelProperty("该POI的中心点到请求坐标的距离 ")
        private String distance;

        @ApiModelProperty("方向")
        private String direction;

        @ApiModelProperty("poi地址信息")
        private String address;

        @ApiModelProperty("坐标点")
        private String location;

        @ApiModelProperty("poi所在商圈名称 ")
        private String businessarea;
    }

    @Data
    public static class Roadinters{

        @ApiModelProperty("交叉路口到请求坐标的距离")
        private String distance;

        @ApiModelProperty("方位 ")
        private String direction;

        @ApiModelProperty("路口经纬度 ")
        private String location;

        @ApiModelProperty("第一条道路id")
        private String firstId;

        @ApiModelProperty("第一条道路名称")
        private String firstName;

        @ApiModelProperty("第二条道路id")
        private String secondId;

        @ApiModelProperty("第二条道路名称 ")
        private String secondName;
    }

    @Data
    public static class Aois{

        @ApiModelProperty("所属 aoi的id")
        private String id;

        @ApiModelProperty("所属 aoi 名称 ")
        private String name;

        @ApiModelProperty("所属 aoi 所在区域编码")
        private String adcode;

        @ApiModelProperty("所属 aoi 中心点坐标")
        private String location;

        @ApiModelProperty("所属aoi点面积")
        private String area;

        @ApiModelProperty("输入经纬度是否在aoi面之中")
        private String distance;
    }
}
