package com.tsq.map.api.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
@ApiModel(value = "GuideTrucksVO对象", description = "路径规划（货车方案）返回参数")
public class GuideTrucksVO {

    @ApiModelProperty("返回状态")
    private Integer status;

    @ApiModelProperty("返回的状态信息")
    private String info;

    @ApiModelProperty("返回参数对象")
    private DataBean data;

    @Data
    public static class DataBean {
        @ApiModelProperty("包含距离路线信息")
        private DataBean.RouteBean route;

        @ApiModelProperty("总共返回路线数")
        private Integer count;

        @Data
        public static class RouteBean {
            @ApiModelProperty("终点坐标")
            private String destination;

            @ApiModelProperty("起点坐标")
            private String origin;

            @ApiModelProperty("驾车的具体方案")
            private List<DataBean.RouteBean.PathsBean> paths;

            @Data
            public static class PathsBean {
                @ApiModelProperty("此方案的行驶距离 ")
                private Integer distance;

                @ApiModelProperty("此方案的耗时")
                private Integer duration;

                @ApiModelProperty("限行结果")
                private Integer restriction;

                @ApiModelProperty("导航策略")
                private String strategy;

                @ApiModelProperty("此导航方案道路收费距离长度 ")
                private Integer tollDistance;

                @ApiModelProperty("此导航方案道路收费金额 ")
                private Integer tolls;

                @ApiModelProperty("此方案红绿灯总数 ")
                private Integer trafficLights;

                @ApiModelProperty("具体方案")
                private List<DataBean.RouteBean.PathsBean.StepsBean> steps;

                @Data
                public static class StepsBean {
                    @ApiModelProperty("导航主要动作")
                    private String action;

                    @ApiModelProperty("导航辅助动作")
                    private String assistantAction;

                    @ApiModelProperty("此路段距离")
                    private Integer distance;

                    @ApiModelProperty("此路段预计时间")
                    private Integer duration;

                    @ApiModelProperty("行驶指示")
                    private String instruction;

                    @ApiModelProperty("方向")
                    private String orientation;

                    @ApiModelProperty("此路段的坐标点")
                    private String polyline;

                    @ApiModelProperty("道路名")
                    private Object road;

                    @ApiModelProperty("收费路段距离")
                    private Integer tollDistance;

                    @ApiModelProperty("主要收费道路")
                    private String tollRoad;

                    @ApiModelProperty("此段收费金额")
                    private Integer tolls;

                    @ApiModelProperty("途径城市列表（此节点及子节点目前还在开发，会在日后实现）")
                    private List<DataBean.RouteBean.PathsBean.StepsBean.CitiesBean> cities;

                    @ApiModelProperty("驾车导航详细信息")
                    private List<DataBean.RouteBean.PathsBean.StepsBean.TmcsBean> tmcs;

                    @Data
                    public static class CitiesBean {
                        @ApiModelProperty("途径城市adcode")
                        private String adcode;

                        @ApiModelProperty("途径城市citycode")
                        private Object citycode;

                        @ApiModelProperty("途径城市名字")
                        private Object name;

                        @ApiModelProperty("途径地")
                        private List<DataBean.RouteBean.PathsBean.StepsBean.DistrictsBean> districts;
                    }

                    @Data
                    public static class TmcsBean {
                        @ApiModelProperty("此段路长度")
                        private Integer distance;

                        @ApiModelProperty("此分段的路线")
                        private String polyline;

                        @ApiModelProperty("路况")
                        private String status;
                    }
                    @Data
                    public static class DistrictsBean{
                        @ApiModelProperty("途径地名字")
                        private String name ;

                        @ApiModelProperty("途径地adcode")
                        private String adcode ;
                    }
                }
            }
        }
    }
}
