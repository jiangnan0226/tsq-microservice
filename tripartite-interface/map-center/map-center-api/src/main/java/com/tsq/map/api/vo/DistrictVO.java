package com.tsq.map.api.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * <p>
 * 行政区域返回参数
 * </p>
 * @author xhy
 * @since 2020-06-22
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "District对象", description = "行政区域查询请求参数")
public class DistrictVO {

    @ApiModelProperty("返回状态")
    private Integer status;

    @ApiModelProperty("返回的状态信息")
    private String info;

    @ApiModelProperty("建议结果列表")
    private SuggestionBean suggestion;

    @ApiModelProperty("行政区列表")
    private List<DistrictsBeanXX> districts;

    @Data
    public static class SuggestionBean {
        @ApiModelProperty("建议关键字列表")
        private String keywords;

        @ApiModelProperty("建议城市列表")
        private String cities;
    }

    @Data
    public static class DistrictsBeanXX {
        @ApiModelProperty("城市编码")
        private String citycode;

        @ApiModelProperty("区域编码")
        private String adcode;

        @ApiModelProperty("行政区名称")
        private String name;

        @ApiModelProperty("区域中心点")
        private String center;

        @ApiModelProperty("行政区划级别")
        private String level;

        @ApiModelProperty("下级行政区列表，包含district元素")
        private List<DistrictsBeanXX.DistrictsBeanX> districts;

        @Data
        public static class DistrictsBeanX {
            @ApiModelProperty("城市编码")
            private String citycode;

            @ApiModelProperty("区域编码")
            private String adcode;

            @ApiModelProperty("行政区名称")
            private String name;

            @ApiModelProperty("区域中心点")
            private String center;

            @ApiModelProperty("行政区划级别")
            private String level;

            @ApiModelProperty("下级行政区列表，包含district元素")
            private List<DistrictsBeanXX.DistrictsBeanX.DistrictsBean> districts;

            @Data
            public static class DistrictsBean {
                @ApiModelProperty("城市编码")
                private String citycode;

                @ApiModelProperty("区域编码")
                private String adcode;

                @ApiModelProperty("行政区名称")
                private String name;

                @ApiModelProperty("区域中心点")
                private String center;

                @ApiModelProperty("行政区划级别")
                private String level;

                @ApiModelProperty("下级行政区列表，包含districtXXX元素")
                private List<DistrictsBeanXX.DistrictsBeanX.DistrictsBeanXXX> districts;
            }
            @Data
            public static class DistrictsBeanXXX {
                @ApiModelProperty("城市编码")
                private String citycode;

                @ApiModelProperty("区域编码")
                private String adcode;

                @ApiModelProperty("行政区名称")
                private String name;

                @ApiModelProperty("区域中心点")
                private String center;

                @ApiModelProperty("行政区划级别")
                private String level;

                @ApiModelProperty("下级行政区列表，包含district元素")
                private List<?> districts;
            }
        }
    }
}
