package com.tsq.map.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ApiModel(value = "GuideDrivingDTO对象", description = "路径规划（驾车方案）请求参数")
public class GuideDrivingDTO {

    @ApiModelProperty("出发点")
    private String origin;

    @ApiModelProperty("目的地")
    private String destination;

    @ApiModelProperty("出发点poiid")
    private String originid;

    @ApiModelProperty("目的地poiid")
    private String destinationid;

    @ApiModelProperty("起点的poi类别")
    private String origintype;

    @ApiModelProperty("终点的poi类别")
    private String destinationtype;

    @ApiModelProperty("驾车选择策略")
    private String strategy;

    @ApiModelProperty("途经点")
    private String waypoints;

    @ApiModelProperty("避让区域")
    private String avoidpolygons;

    @ApiModelProperty("避让道路名")
    private String avoidroad;

    @ApiModelProperty("用汉字填入车牌省份缩写，用于判断是否限行")
    private String province;

    @ApiModelProperty("填入除省份及标点之外，车牌的字母和数字（需大写）。用于判断限行相关。")
    private String number;

    @ApiModelProperty("车辆类型 0：普通汽车(默认值) 1：纯电动车 2：插电混动车")
    private String cartype;

    @ApiModelProperty("在路径规划中，是否使用轮渡 0:使用渡轮(默认) 1:不使用渡轮 ")
    private String ferry ;

    @ApiModelProperty("是否返回路径聚合信息 false:不返回路径聚合信息 true:返回路径聚合信息，在steps上层增加roads做聚合")
    private String roadaggregation;

    @ApiModelProperty("是否返回steps字段内容 当取值为0时，steps字段内容正常返回 当取值为1时，steps字段内容为空；")
    private String nosteps;

    @ApiModelProperty("返回数据格式类型")
    private String output;

    @ApiModelProperty("回调函数")
    private String callback ;

    @ApiModelProperty("返回结果控制")
    private String extensions;
}
