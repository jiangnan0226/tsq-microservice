package com.tsq.map.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ApiModel(value = "WeatherDTO对象", description = "天气查询请求参数")
public class WeatherDTO {

    @ApiModelProperty("城市编码")
    private String city;

    @ApiModelProperty("气象类型")
    private String extensions;

    @ApiModelProperty("返回格式")
    private String output;
}
