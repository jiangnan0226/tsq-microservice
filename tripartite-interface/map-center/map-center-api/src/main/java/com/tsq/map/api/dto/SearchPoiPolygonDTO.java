package com.tsq.map.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ApiModel(value = "SearchPoiPolygonDTO对象", description = "poi多边形搜索请求参数")
public class SearchPoiPolygonDTO {

    @ApiModelProperty("经纬度坐标对")
    private String polygon;

    @ApiModelProperty("查询关键字")
    private String keywords;

    @ApiModelProperty("查询POI类型")
    private String types;

    @ApiModelProperty("每页记录数据")
    private String offset;

    @ApiModelProperty("当前页数")
    private String page;

    @ApiModelProperty("返回结果控制")
    private String extensions;

    @ApiModelProperty("返回数据格式类型")
    private String output;

    @ApiModelProperty("回调函数")
    private String callback;
}
