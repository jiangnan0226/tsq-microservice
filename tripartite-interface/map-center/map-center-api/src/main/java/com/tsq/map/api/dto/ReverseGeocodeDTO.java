package com.tsq.map.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ApiModel(value = "ReverseGeocodeDTO对象", description = "逆地理编码请求参数")
public class ReverseGeocodeDTO {

    @ApiModelProperty("经纬度坐标")
    private String location;

    @ApiModelProperty("返回附近POI类型")
    private String poitype;

    @ApiModelProperty("搜索半径")
    private String radius;

    @ApiModelProperty("返回结果控制")
    private String extensions;

    @ApiModelProperty("批量查询控制")
    private String batch;

    @ApiModelProperty("道路等级")
    private String roadlevel;

    @ApiModelProperty("返回数据格式类型")
    private String output;

    @ApiModelProperty("回调函数")
    private String callback;

    @ApiModelProperty("是否优化POI返回顺序")
    private String homeorcorp;
}
