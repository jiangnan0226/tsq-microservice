package com.tsq.map.api.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
@ApiModel(value = "TrajectoryCorrectionDTO对象", description = "轨迹纠偏返回参数")
public class TrajectoryCorrectionVO {

    @ApiModelProperty("返回状态")
    private Integer errcode;

    @ApiModelProperty("返回的状态信息")
    private String errmsg;

    @ApiModelProperty("数据体")
    private DataBean data;

    @Data
    public static class DataBean{

        @ApiModelProperty("总距离")
        private String distance;

        @ApiModelProperty("返回坐标合集")
        private List<PointsBean> points;

        @Data
        public static class PointsBean{

            @ApiModelProperty("经度")
            private String x;

            @ApiModelProperty("维度")
            private String y;
        }
    }
}
