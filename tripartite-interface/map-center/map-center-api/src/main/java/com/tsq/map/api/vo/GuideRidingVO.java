package com.tsq.map.api.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * <p>
 * 路径规划（乘车方案）
 * </p>
 *
 * @author xhy
 * @since 2020-06-22
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "GuideRidingVO对象", description = "路径规划（乘车方案）返回参数")
public class GuideRidingVO {

    @ApiModelProperty("返回状态")
    private Integer status;

    @ApiModelProperty("返回的状态信息")
    private String info;

    @ApiModelProperty(value = "公交换乘信息列表")
    private Route route;

    @Data
    public static class Route{
        @ApiModelProperty(value = "起点坐标")
        private String origin;

        @ApiModelProperty(value = "终点坐标")
        private String destination;

        @ApiModelProperty(value = "起点和终点的步行距离")
        private String distance;

        @ApiModelProperty(value = "出租车费用")
        private String taxiCost;

        @ApiModelProperty("公交换乘方案列表")
        private List<Transits> transits;
    }
    @Data
    public static class Transits{
        @ApiModelProperty(value = "此换乘方案价格")
        private String cost;

        @ApiModelProperty(value = "此换乘方案预期时间")
        private String duration;

        @ApiModelProperty(value = "是否是夜班车")
        private String nightflag;

        @ApiModelProperty(value = "此方案总步行距离")
        private String walkingDistance;

        @ApiModelProperty(value = "换乘路段列表")
        private List<Segments> segments;
    }
    @Data
    public static class Segments{
        @ApiModelProperty(value = "此路段步行导航信息")
        private Walking walking;

        @ApiModelProperty(value = "此路段公交导航信息")
        private Bus bus;

        @ApiModelProperty(value = "地铁入口")
        private Entrance entrance;

        @ApiModelProperty(value = "地铁出口")
        private Exit exit;

        @ApiModelProperty(value = "乘坐火车的信息")
        private Railway railway;
    }
    @Data
    public static class Walking{
        @ApiModelProperty(value = "起点坐标")
        private String origin;

        @ApiModelProperty(value = "终点坐标")
        private String destination;

        @ApiModelProperty(value = "每段线路步行距离")
        private String distance;

        @ApiModelProperty(value = "步行预计时间")
        private String duration;

        @ApiModelProperty("步行路段列表")
        private List<Steps> steps;
    }
    @Data
    public static class Steps{
        @ApiModelProperty(value = "此段路的行走介绍")
        private String instruction;

        @ApiModelProperty(value = "路的名字")
        private String road;

        @ApiModelProperty(value = "此段路的距离")
        private String distance ;

        @ApiModelProperty(value = "此段路预计消耗时间")
        private String duration;

        @ApiModelProperty(value = "此段路的坐标")
        private String polyline;

        @ApiModelProperty(value = "步行主要动作")
        private String action;

        @ApiModelProperty(value = "步行辅助动作")
        private String assistantAction;
    }
    @Data
    public static class Bus{
        @ApiModelProperty(value = "步行路段列表")
        private List<Buslines> buslines ;
    }
    @Data
    public static class Buslines{
        @ApiModelProperty("此段起乘站信息")
        private DepartureStop departureStop;

        @ApiModelProperty("此段下车站")
        private ArrivalStop arrivalStop;

        @ApiModelProperty("此段途经公交站点列表")
        private List<ViaStops> viaStops;

        @ApiModelProperty("公交路线名称")
        private String name;

        @ApiModelProperty("公交路线id")
        private String id;

        @ApiModelProperty("公交类型")
        private String type ;

        @ApiModelProperty("公交行驶距离")
        private String distance ;

        @ApiModelProperty("公交预计行驶时间")
        private String duration ;

        @ApiModelProperty("此路段坐标集")
        private String polyline ;

        @ApiModelProperty("首班车时间")
        private String startTime  ;

        @ApiModelProperty("末班车时间")
        private String endTime  ;

        @ApiModelProperty("此段途经公交站数")
        private String viaNum  ;
    }
    @Data
    public static class DepartureStop{
        @ApiModelProperty(value = "站点名字")
        private String name ;

        @ApiModelProperty(value = "站点id")
        private String id ;

        @ApiModelProperty(value = "站点经纬度")
        private String location ;
    }
    @Data
    public static class ArrivalStop{
        @ApiModelProperty(value = "站点名字")
        private String name ;

        @ApiModelProperty(value = "站点id")
        private String id ;

        @ApiModelProperty(value = "站点经纬度")
        private String location ;
    }
    @Data
    public static class ViaStops{
        @ApiModelProperty(value = "途径公交站点信息")
        private String name ;

        @ApiModelProperty(value = "公交站点编号")
        private String id ;

        @ApiModelProperty(value = "公交站点经纬度")
        private String location ;
    }
    @Data
    public static class Entrance{
        @ApiModelProperty(value = "入口名称")
        private String name ;

        @ApiModelProperty(value = "入口经纬度")
        private String location ;
    }
    @Data
    public static class Exit{
        @ApiModelProperty(value = "出口名称")
        private String name ;

        @ApiModelProperty(value = "出口经纬度")
        private String location ;
    }
    @Data
    public static class Railway{
        @ApiModelProperty(value = "线路id编号")
        private String id ;

        @ApiModelProperty(value = "该线路车段耗时")
        private String time ;

        @ApiModelProperty(value = "线路名称")
        private String name ;

        @ApiModelProperty(value = "线路车次号")
        private String trip ;

        @ApiModelProperty(value = "该item换乘段的行车总距离")
        private String distance ;

        @ApiModelProperty(value = "线路车次类型")
        private String type ;

        @ApiModelProperty(value = "火车始发站信息")
        private TrainDepartureStop departureStop ;

        @ApiModelProperty(value = "火车到站信息")
        private TrainArrivalStop arrivalStop ;

        @ApiModelProperty(value = "途径站点信息")
        private List<ViaStop> viaStop ;

        @ApiModelProperty(value = "聚合的备选方案")
        private List<Alters> alters ;

        @ApiModelProperty(value = "仓位及价格信息")
        private List<Spaces> spaces ;
    }
    @Data
    public static class TrainDepartureStop{
        @ApiModelProperty("上车站点ID")
        private String id;

        @ApiModelProperty("上车站点名称")
        private String name;

        @ApiModelProperty("上车站点经纬度")
        private String location;

        @ApiModelProperty("上车站点所在城市的adcode")
        private String adcode;

        @ApiModelProperty("上车点发车时间")
        private String time;

        @ApiModelProperty("是否始发站，1表示为始发站，0表示非始发站")
        private String start;
    }

    @Data
    public static class TrainArrivalStop{
        @ApiModelProperty("下车站点ID")
        private String id;

        @ApiModelProperty("下车站点名称")
        private String name;

        @ApiModelProperty("下车站点经纬度")
        private String location;

        @ApiModelProperty("下车站点所在城市的adcode")
        private String adcode;

        @ApiModelProperty("到站时间，如大于24:00，则表示跨天")
        private String time;

        @ApiModelProperty("是否为终点站，1表示为终点站，0表示非终点站")
        private String end;
    }
    @Data
    public static class ViaStop{
        @ApiModelProperty("途径站点的名称")
        private String name;

        @ApiModelProperty("途径站点的ID")
        private String id;

        @ApiModelProperty("途径站点的坐标点")
        private String location;

        @ApiModelProperty("途径站点的进站时间，如大于24:00,则表示跨天")
        private String time;

        @ApiModelProperty("途径站点的停靠时间，单位：分钟")
        private String wait;
    }
    @Data
    public static class Alters{
        @ApiModelProperty("备选方案ID")
        private String id;

        @ApiModelProperty("备选线路名称")
        private String name;
    }
    @Data
    public static class Spaces{
        @ApiModelProperty("仓位编码")
        private String code;

        @ApiModelProperty("仓位费用")
        private String cost;
    }
}
