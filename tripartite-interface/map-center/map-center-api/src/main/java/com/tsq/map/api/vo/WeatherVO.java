package com.tsq.map.api.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
@ApiModel(value = "WeatherVO对象", description = "天气查询返回参数")
public class WeatherVO {

    @ApiModelProperty("返回状态")
    private Integer status;

    @ApiModelProperty("返回的状态信息")
    private String info;

    @ApiModelProperty("预报天气信息数据")
    private List<ForecastsBean> forecasts;

    @ApiModelProperty("实况天气数据信息")
    private List<LivesBean> lives;

    @Data
    public static class LivesBean{

        @ApiModelProperty("省份名")
        private String province;

        @ApiModelProperty("城市名")
        private String city;

        @ApiModelProperty("区域编码")
        private String adcode;

        @ApiModelProperty("天气现象（汉字描述）")
        private String weather;

        @ApiModelProperty("实时气温，单位：摄氏度")
        private String temperature;

        @ApiModelProperty("风向描述")
        private String winddirection;

        @ApiModelProperty("风力级别，单位：级")
        private String windpower;

        @ApiModelProperty("空气湿度")
        private String humidity;

        @ApiModelProperty("数据发布的时间")
        private String reporttime;

    }
    @Data
    public static class ForecastsBean {
        @ApiModelProperty("城市名称")
        private String city;

        @ApiModelProperty("城市编码")
        private String adcode;

        @ApiModelProperty("省份名称")
        private String province;

        @ApiModelProperty("预报发布时间")
        private String reporttime;

        @ApiModelProperty("预报数据list结构，元素cast,按顺序为当天、第二天、第三天的预报数据")
        private List<ForecastsBean.CastsBean> casts;

        @Data
        public static class CastsBean {
            @ApiModelProperty("日期")
            private String date;

            @ApiModelProperty("星期几")
            private String week;

            @ApiModelProperty("白天天气现象")
            private String dayweather;

            @ApiModelProperty("晚上天气现象")
            private String nightweather;

            @ApiModelProperty("白天温度")
            private String daytemp;

            @ApiModelProperty("晚上温度")
            private String nighttemp;

            @ApiModelProperty("白天风向")
            private String daywind;

            @ApiModelProperty("晚上风向")
            private String nightwind;

            @ApiModelProperty("白天风力")
            private String daypower;

            @ApiModelProperty("晚上风力")
            private String nightpower;
        }
    }
}
