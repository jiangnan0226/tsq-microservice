package com.tsq.map.api.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * <p>
 * 路径规划（骑行方案）返回参数
 * </p>
 * @author xhy
 * @since 2020-06-22
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "GuideCyclingVO对象", description = "路径规划（骑行方案）返回参数")
public class GuideCyclingVO {

    @ApiModelProperty("返回状态")
    private Integer status;

    @ApiModelProperty("返回的状态信息")
    private String info;

    @ApiModelProperty("返回参数对象")
    private DataBean data;

    @Data
    public static class DataBean {
        @ApiModelProperty("终点坐标")
        private String destination;

        @ApiModelProperty("起点坐标")
        private String origin;

        @ApiModelProperty("骑行方案列表信息")
        private List<DataBean.PathsBean> paths;


        @Data
        public static class PathsBean {
            @ApiModelProperty("起终点的骑行距离")
            private Integer distance;

            @ApiModelProperty("起终点的骑行时间")
            private Integer duration;

            @ApiModelProperty("具体骑行结果")
            private List<DataBean.PathsBean.StepsBean> steps;


            @Data
            public static class StepsBean {
                @ApiModelProperty("此段路骑行主要动作")
                private String action;

                @ApiModelProperty("此段路骑行辅助动作")
                private String assistantAction;

                @ApiModelProperty("此段路骑行距离")
                private Integer distance;

                @ApiModelProperty("此段路骑行耗时")
                private Integer duration;

                @ApiModelProperty("路段骑行指示")
                private String instruction;

                @ApiModelProperty("此段路骑行方向")
                private String orientation;

                @ApiModelProperty("此段路骑行的坐标点")
                private String polyline;

                @ApiModelProperty("此段路道路名称")
                private String road;

            }
        }
    }
}
