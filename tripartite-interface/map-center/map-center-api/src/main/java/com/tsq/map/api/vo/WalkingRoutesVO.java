package com.tsq.map.api.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * <p>
 * 路径规划（步行方案）
 * </p>
 *
 * @author xhy
 * @since 2020-06-22
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "WalkingRoutesVO对象", description = "路径规划(步行方案)返回参数")
public class WalkingRoutesVO {

    @ApiModelProperty("返回状态")
    private Integer status;

    @ApiModelProperty("返回的状态信息")
    private String info;

    @ApiModelProperty("路线信息列表")
    private Route route;

    @Data
    public static class Route{

        @ApiModelProperty("起点坐标")
        private String origin;

        @ApiModelProperty("终点坐标")
        private String destination;

        @ApiModelProperty("步行方案")
        private List<Paths> paths;

    }
    @Data
    public static class Paths{
        @ApiModelProperty("起点和终点的步行距离")
        private String distance;

        @ApiModelProperty("步行时间预计")
        private String duration;

        @ApiModelProperty("返回步行结果列表")
        private List<Steps> steps;
    }
    @Data
    public static class Steps{
        @ApiModelProperty("路段步行指示")
        private String instruction;

        @ApiModelProperty("道路名称")
        private String road;

        @ApiModelProperty("此路段距离")
        private String distance;

        @ApiModelProperty("方向")
        private String orientation;

        @ApiModelProperty("此路段预计步行时间")
        private String duration;

        @ApiModelProperty("此路段坐标点")
        private String polyline;

        @ApiModelProperty("步行主要动作")
        private String action;

        @ApiModelProperty("步行辅助动作")
        private String assistantAction;

        @ApiModelProperty("这段路是否存在特殊的方式")
        private String walkType;
    }
}
