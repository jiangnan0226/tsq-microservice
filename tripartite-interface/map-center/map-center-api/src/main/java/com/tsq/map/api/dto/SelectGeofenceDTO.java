package com.tsq.map.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ApiModel(value = "SelectGeofenceDTO对象", description = "查询围栏请求参数")
public class SelectGeofenceDTO {

    @ApiModelProperty("围栏id")
    private Integer id;

    @ApiModelProperty("围栏全局id")
    private String gid;

    @ApiModelProperty("围栏名称")
    private String name;

    @ApiModelProperty("当前页码")
    private Integer pageNo;

    @ApiModelProperty("每页数量")
    private Integer pageSize;

    @ApiModelProperty("围栏激活状态")
    private Boolean enable;

    @ApiModelProperty("按创建时间查询的起始时间")
    private String startTime;

    @ApiModelProperty("按创建时间查询的结束时间 ")
    private String end_time;
}
