package com.tsq.map.provider.service.Impl;

import com.alibaba.fastjson.JSON;
import com.tsq.map.api.dto.*;
import com.tsq.map.api.vo.*;
import com.tsq.map.provider.service.RoutePlanningService;
import com.tsq.map.provider.util.Constant;
import com.tsq.map.provider.util.TsqHttpUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class RoutePlanningServiceImpl implements RoutePlanningService {

    @Autowired
    private TsqHttpUtil tsqHttpUtil;

    /**
     * <p>路径规划（步行线路）</p>
     * @param walkingRoutesDTO 请求参数
     * */
    @Override
    public WalkingRoutesVO selectWalking(WalkingRoutesDTO walkingRoutesDTO) {
        String url = Constant.ROUTE_PLANNING_WALKING;
        String param = (String) tsqHttpUtil.httpGetRest(url,walkingRoutesDTO);
        return JSON.parseObject(param, WalkingRoutesVO.class);
    }

    /**
     * <p>路径规划（乘车线路）</p>
     * @param guideRidingDTO 请求参数
     * */
    @Override
    public GuideRidingVO selectRiding(GuideRidingDTO guideRidingDTO) {
        String url = Constant.ROUTE_PLANNING_RIDING;
        String param = (String) tsqHttpUtil.httpGetRest(url,guideRidingDTO);
        return JSON.parseObject(param, GuideRidingVO.class);
    }

    /**
     * <p>路径规划（驾车线路）</p>
     * @param guideDrivingDTO 请求参数
     * */
    @Override
    public GuideDrivingVO selectDriving(GuideDrivingDTO guideDrivingDTO) {
        String url = Constant.ROUTE_PLANNING_DRIVE;
        String param = (String) tsqHttpUtil.httpGetRest(url,guideDrivingDTO);
        return JSON.parseObject(param, GuideDrivingVO.class);
    }

    /**
     * <p>路径规划（骑行线路）</p>
     * @param origin 出发点经纬度, destination 目的地经纬度
     * */
    @Override
    public GuideCyclingVO selectCycling(String origin,String destination){
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("origin",origin);
            map.put("destination",destination);
            String url = Constant.ROUTE_PLANNING_CYCLING;
            String param = (String) tsqHttpUtil.httpGetRest(url,map);
            return JSON.parseObject(param, GuideCyclingVO.class);
        }catch (Exception e){
            return null;
        }
    }

    /**
     * <p>路径规划（货车路径规划）</p>
     * @param guideTrucksDTO 请求参数
     * */
    @Override
    public GuideTrucksVO selectTrucks(GuideTrucksDTO guideTrucksDTO) {
        String url = Constant.ROUTE_PLANNING_TRUCKS;
        String param = (String) tsqHttpUtil.httpGetRest(url,guideTrucksDTO);
        return JSON.parseObject(param, GuideTrucksVO.class);
    }

    /**
     * <p>路径规划（距离测量）</p>
     * @param guideDistanceDTO 请求参数
     * */
    @Override
    public GuideDistanceVO selectDistance(GuideDistanceDTO guideDistanceDTO) {
        String url = Constant.ROUTE_PLANNING_DISTANCE;
        String param = (String) tsqHttpUtil.httpGetRest(url,guideDistanceDTO);
        return JSON.parseObject(param, GuideDistanceVO.class);
    }
}
