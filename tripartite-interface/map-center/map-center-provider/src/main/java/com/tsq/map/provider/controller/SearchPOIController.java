package com.tsq.map.provider.controller;

import com.tsq.common.model.ResponseData;
import com.tsq.map.api.dto.SearchPoiAroundDTO;
import com.tsq.map.api.dto.SearchPoiDetailDTO;
import com.tsq.map.api.dto.SearchPoiKeywordDTO;
import com.tsq.map.api.dto.SearchPoiPolygonDTO;
import com.tsq.map.api.vo.SearchPoiVO;
import com.tsq.map.provider.service.SearchPOIService;
import com.tsq.map.provider.util.Constant;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/place")
public class SearchPOIController {

    @Autowired
    private SearchPOIService searchPOIService;

    @ApiOperation(value = "poi关键字搜索信息", notes = "poi关键字搜索信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "SearchPoiKeywordDTO", value = "poi关键字搜索请求参数", dataType = "", paramType = "query", required = true)
    })
    @GetMapping("keyWord")
    public ResponseData<SearchPoiVO> searchKeyword(SearchPoiKeywordDTO searchPoiKeywordDTO){
        SearchPoiVO searchPoiVO = searchPOIService.searchKeyword(searchPoiKeywordDTO);
        if(searchPoiVO.getStatus().equals(Constant.SUCCESS_STATUS)){
            return ResponseData.success(searchPoiVO);
        }else {
            return ResponseData.error(searchPoiVO.getInfo());
        }
    }

    @ApiOperation(value = "poi周边搜索信息", notes = "poi周边搜索信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "SearchPoiAroundDTO", value = "poi周边搜索请求参数", dataType = "", paramType = "query", required = true)
    })
    @GetMapping("around")
    public ResponseData<SearchPoiVO> searchAround(SearchPoiAroundDTO searchPoiAroundDTO){
        SearchPoiVO searchPoiVO = searchPOIService.searchAround(searchPoiAroundDTO);
        if(searchPoiVO.getStatus().equals(Constant.SUCCESS_STATUS)){
            return ResponseData.success(searchPoiVO);
        }else {
            return ResponseData.error(searchPoiVO.getInfo());
        }
    }

    @ApiOperation(value = "poi多边形搜索信息", notes = "poi多边形搜索信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "SearchPoiPolygonDTO", value = "poi多边形搜索请求参数", dataType = "", paramType = "query", required = true)
    })
    @GetMapping("polygon")
    public ResponseData<SearchPoiVO> searchPolygon(SearchPoiPolygonDTO searchPoiPolygonDTO){
        SearchPoiVO searchPoiVO = searchPOIService.searchPolygon(searchPoiPolygonDTO);
        if(searchPoiVO.getStatus().equals(Constant.SUCCESS_STATUS)){
            return ResponseData.success(searchPoiVO);
        }else {
            return ResponseData.error(searchPoiVO.getInfo());
        }
    }

    @ApiOperation(value = "poiId查询信息", notes = "poiId查询信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "SearchPoiDetailDTO", value = "poiId查询请求参数", dataType = "", paramType = "query", required = true)
    })
    @GetMapping("detail")
    public ResponseData<SearchPoiVO> searchDetail(SearchPoiDetailDTO searchPoiDetailDTO){
        SearchPoiVO searchPoiVO = searchPOIService.searchDetail(searchPoiDetailDTO);
        if(Constant.SUCCESS_STATUS.equals(searchPoiVO.getStatus())){
            return ResponseData.success(searchPoiVO);
        }else {
            return ResponseData.error(searchPoiVO.getInfo());
        }
    }
}
