package com.tsq.map.provider.service;

import com.tsq.map.api.dto.AssistantDTO;
import com.tsq.map.api.vo.AssistantVO;

public interface AssistantService {
    /**
     * <p>输入提示</p>
     * @param assistantDTO
     * */
    AssistantVO selectAssistant(AssistantDTO assistantDTO);
}
