package com.tsq.map.provider.service;

import com.tsq.map.api.dto.*;
import com.tsq.map.api.vo.*;

public interface RoutePlanningService {
    /**
     * <p>路径规划（步行线路）</p>
     * @param walkingRoutesDTO 请求参数
     * */
    WalkingRoutesVO selectWalking(WalkingRoutesDTO walkingRoutesDTO);

    /**
     * <p>路径规划（乘车路径规划）</p>
     * @param guideRidingDTO 请求参数
     * */
    GuideRidingVO selectRiding(GuideRidingDTO guideRidingDTO);

    /**
     * <p>路径规划（驾车路径规划）</p>
     * @param guideDrivingDTO 请求参数
     * */
    GuideDrivingVO selectDriving(GuideDrivingDTO guideDrivingDTO);

    /**
     * <p>路径规划（骑行路径规划）</p>
     * @param origin 出发点经纬度, destination 目的地经纬度
     * */
    GuideCyclingVO selectCycling(String origin,String destination);

    /**
     * <p>路径规划（货车路径规划）</p>
     * @param guideTrucksDTO 请求参数
     * */
    GuideTrucksVO selectTrucks(GuideTrucksDTO guideTrucksDTO);

    /**
     * <p>路径规划（距离测量）</p>
     * @param guideDistanceDTO 请求参数
     * */
    GuideDistanceVO selectDistance(GuideDistanceDTO guideDistanceDTO);
}
