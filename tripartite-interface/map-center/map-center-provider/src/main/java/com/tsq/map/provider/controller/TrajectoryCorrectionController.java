package com.tsq.map.provider.controller;

import com.tsq.common.model.ResponseData;
import com.tsq.map.api.dto.TrajectoryCorrectionDTO;
import com.tsq.map.api.vo.TrajectoryCorrectionVO;
import com.tsq.map.provider.service.TrajectoryCorrectionService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@RequestMapping("/track")
public class TrajectoryCorrectionController {

    @Autowired
    private TrajectoryCorrectionService trajectoryCorrectionService;
    @Autowired
    private RestTemplate restTemplate;

    @ApiOperation(value = "轨迹纠偏信息", notes = "轨迹纠偏信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "TrajectoryCorrectionDTO", value = "轨迹纠偏请求参数", dataType = "", paramType = "query", required = true)
    })
    @PostMapping("trajectoryCorrection")
    public ResponseData<ResponseEntity<TrajectoryCorrectionVO>> selectTrajectoryCorrectionVO(@RequestBody List<TrajectoryCorrectionDTO> list) {
//        ResponseEntity<TrajectoryCorrectionVO> trajectoryCorrectionVO = trajectoryCorrectionService.selectTrajectoryCorrectionVO(list);
        ResponseEntity<TrajectoryCorrectionVO> trajectoryCorrectionVO = trajectoryCorrectionService.selectTrajectoryCorrectionVO(list);
        if("OK".equals(trajectoryCorrectionVO.getBody().getErrmsg())){
            return ResponseData.success(trajectoryCorrectionVO);
        }else {
            return ResponseData.error(trajectoryCorrectionVO.getBody().getErrmsg());
        }
    }
}
