package com.tsq.map.provider.controller;

import com.tsq.common.model.ResponseData;
import com.tsq.map.api.dto.CreateGeofenceDTO;
import com.tsq.map.api.dto.MonitoringGeofenceDTO;
import com.tsq.map.api.dto.SelectGeofenceDTO;
import com.tsq.map.api.dto.UpdateGeofenceDTO;
import com.tsq.map.api.vo.CreateGeofenceVO;
import com.tsq.map.api.vo.MonitoringGeofenceVO;
import com.tsq.map.api.vo.SelectGeofenceVO;
import com.tsq.map.api.vo.StatusGeofenceVO;
import com.tsq.map.provider.service.GeofenceService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/geofence")
public class GeofenceController {

    @Autowired
    private GeofenceService geofenceService;

    @ApiOperation(value = "地理围栏（创建围栏）信息", notes = "地理围栏（创建围栏）信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "TrajectoryCorrectionDTO", value = "地理围栏（创建围栏）请求参数", dataType = "", paramType = "query", required = true)
    })
    @PostMapping("createGeofence")
    public ResponseData<CreateGeofenceVO> createGeofenceVO(@RequestBody CreateGeofenceDTO createGeofenceDTO){
        CreateGeofenceVO createGeofenceVO = geofenceService.createGeofence(createGeofenceDTO);
        if(createGeofenceVO.getErrmsg().equalsIgnoreCase("OK")){
            return ResponseData.success(createGeofenceVO);
        }else {
            return ResponseData.error(createGeofenceVO.getErrmsg());
        }
    }

    @ApiOperation(value = "地理围栏（查询围栏）信息", notes = "地理围栏（查询围栏）信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "SelectGeofenceDTO", value = "地理围栏（创建围栏）请求参数", dataType = "", paramType = "query", required = true)
    })
    @GetMapping("selectGeofence")
    public ResponseData<SelectGeofenceVO> selectGeofenceVO(SelectGeofenceDTO selectGeofenceDTO){
        SelectGeofenceVO selectGeofenceVO = geofenceService.selectGeofence(selectGeofenceDTO);
        if(selectGeofenceVO.getErrmsg().equalsIgnoreCase("OK")){
            return ResponseData.success(selectGeofenceVO);
        }else {
            return ResponseData.error(selectGeofenceVO.getErrmsg());
        }
    }

    @ApiOperation(value = "地理围栏（更新围栏）信息", notes = "地理围栏（更新围栏）信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "UpdateGeofenceDTO", value = "地理围栏（更新围栏）请求参数", dataType = "", paramType = "query", required = true)
    })
    @PostMapping("updateGeofence")
    public ResponseData<StatusGeofenceVO> updateGeofenceVO(@RequestBody UpdateGeofenceDTO updateGeofenceDTO, @Param(value = "gid")String gid){
        StatusGeofenceVO statusGeofenceVO = geofenceService.updateGeofence(updateGeofenceDTO,gid);
        if(statusGeofenceVO.getErrmsg().equalsIgnoreCase("OK")){
            return ResponseData.success(statusGeofenceVO);
        }else {
            return ResponseData.error(statusGeofenceVO.getErrmsg());
        }
    }

    @ApiOperation(value = "地理围栏（启动/停止）信息", notes = "地理围栏（启动/停止）信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "enable", value = "围栏状态", dataType = "", paramType = "query", required = true),
            @ApiImplicitParam(name = "gid",value = "围栏全局ID",paramType = "query" ,required = true)
    })
    @PostMapping("statusGeofance")
    public ResponseData<StatusGeofenceVO> statusGeofenceVO(@RequestBody String enable,String gid){
        StatusGeofenceVO statusGeofenceVO = geofenceService.statusGeofence(enable,gid);
        if(statusGeofenceVO.getErrmsg().equalsIgnoreCase("OK")){
            return ResponseData.success(statusGeofenceVO);
        }else {
            return ResponseData.error(statusGeofenceVO.getErrmsg());
        }
    }

    @ApiOperation(value = "删除围栏信息", notes = "删除围栏信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "gid", value = "围栏删除全局ID", dataType = "", paramType = "delete", required = true)
    })
    @DeleteMapping("deleteGeofence")
    public ResponseData<StatusGeofenceVO> deleteGenfenceVO(@RequestParam String gid){
        StatusGeofenceVO statusGeofenceVO = geofenceService.deleteGeofence(gid);
        if(statusGeofenceVO.getErrmsg().equalsIgnoreCase("OK")){
            return ResponseData.success(statusGeofenceVO);
        }else {
            return ResponseData.error(statusGeofenceVO.getErrmsg());
        }
    }

    @ApiOperation(value = "围栏监控信息", notes = "围栏监控信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "UpdateGeofenceDTO", value = "围栏监控请求参数", dataType = "", paramType = "query", required = true)
    })
    @GetMapping("monitoringGeofence")
    public ResponseData<MonitoringGeofenceVO> monitoringGeofenceVO(MonitoringGeofenceDTO monitoringGeofenceDTO){
        MonitoringGeofenceVO monitoringGeofenceVO = geofenceService.selectMonitoringGeofenceVO(monitoringGeofenceDTO);
        if("OK".equalsIgnoreCase(monitoringGeofenceVO.getErrmsg())){
            return ResponseData.success(monitoringGeofenceVO);
        }else {
            return ResponseData.error(monitoringGeofenceVO.getErrmsg());
        }
    }
}
