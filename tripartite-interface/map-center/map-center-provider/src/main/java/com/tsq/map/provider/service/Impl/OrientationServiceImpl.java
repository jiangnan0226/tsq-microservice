package com.tsq.map.provider.service.Impl;

import com.alibaba.fastjson.JSON;
import com.tsq.map.api.vo.OrientationVO;
import com.tsq.map.provider.service.OrientationService;
import com.tsq.map.provider.util.Constant;
import com.tsq.map.provider.util.TsqHttpUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class OrientationServiceImpl implements OrientationService {

    @Autowired
    private TsqHttpUtil tsqHttpUtil;

    /**
     * <p>IP定位</p>
     * @param ip
     * */
    @Override
    public OrientationVO selectOrientation(String ip) throws Exception {
        Map<String, Object> map = new HashMap<>();
        map.put("ip",ip);
        String url = Constant.IP_LOCATION ;
        String param = (String) tsqHttpUtil.httpGetRest(url,map);
        return JSON.parseObject(param, OrientationVO.class);
    }

}
