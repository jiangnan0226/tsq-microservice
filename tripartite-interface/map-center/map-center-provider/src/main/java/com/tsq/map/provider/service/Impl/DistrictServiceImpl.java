package com.tsq.map.provider.service.Impl;

import com.alibaba.fastjson.JSON;
import com.tsq.map.api.dto.DistrictDTO;
import com.tsq.map.api.vo.DistrictVO;
import com.tsq.map.provider.service.DistrictService;
import com.tsq.map.provider.util.Constant;
import com.tsq.map.provider.util.TsqHttpUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DistrictServiceImpl implements DistrictService {

    @Autowired
    private TsqHttpUtil tsqHttpUtil;

    /**
     * <p>行政区域查询</p>
     * @param districtDTO
     * */
    @Override
    public DistrictVO selectDistrict(DistrictDTO districtDTO) {
        String url = Constant.REGIONAL_QUERY;
        String param = (String) tsqHttpUtil.httpGetRest(url,districtDTO);
        return JSON.parseObject(param, DistrictVO.class);
    }

}
