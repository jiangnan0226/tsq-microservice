package com.tsq.map.provider.service;

import com.tsq.map.api.dto.CreateGeofenceDTO;
import com.tsq.map.api.dto.MonitoringGeofenceDTO;
import com.tsq.map.api.dto.SelectGeofenceDTO;
import com.tsq.map.api.dto.UpdateGeofenceDTO;
import com.tsq.map.api.vo.CreateGeofenceVO;
import com.tsq.map.api.vo.MonitoringGeofenceVO;
import com.tsq.map.api.vo.SelectGeofenceVO;
import com.tsq.map.api.vo.StatusGeofenceVO;

public interface GeofenceService {

    /**
     * <p>创建地理围栏</p>
     * @param createGeofenceDTO
     * */
    CreateGeofenceVO createGeofence(CreateGeofenceDTO createGeofenceDTO);

    /**
     * <p>查询地理围栏</p>
     * @param selectGeofenceDTO
     * */
    SelectGeofenceVO selectGeofence(SelectGeofenceDTO selectGeofenceDTO);

    /**
     * <p>更新地理围栏</p>
     * @param updateGeofenceDTO
     * */
    StatusGeofenceVO updateGeofence(UpdateGeofenceDTO updateGeofenceDTO,String gid);

    /**
     * <p>围栏启动/停止</p>
     * @param gid 围栏全局ID，status 围栏状态
     * */
    StatusGeofenceVO statusGeofence(String gid,String status);

    /**
     * <p>删除围栏</p>
     * @param gid 围栏全局ID
     * */
    StatusGeofenceVO deleteGeofence(String gid);

    /**
     * <p>地理围栏（围栏设备监控）</p>
     * @param monitoringGeofenceDTO
     * */
    MonitoringGeofenceVO selectMonitoringGeofenceVO(MonitoringGeofenceDTO monitoringGeofenceDTO);
}
