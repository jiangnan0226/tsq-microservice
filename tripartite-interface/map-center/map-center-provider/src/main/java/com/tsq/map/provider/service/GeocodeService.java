package com.tsq.map.provider.service;

import com.tsq.map.api.dto.GeocodeDTO;
import com.tsq.map.api.dto.ReverseGeocodeDTO;
import com.tsq.map.api.vo.GeocodeVO;
import com.tsq.map.api.vo.ReverseGeocodeVO;

public interface GeocodeService {

    /**
     * 调用高德第三方查询地理编码
     * @param geocodeDTO 查询参数
     * @return
     */
    GeocodeVO selectGeocoding(GeocodeDTO geocodeDTO);

    /**
     * 调用高德第三方查询逆地理编码
     * @param reverseGeocodeDTO 查询参数
     * @return
     */
    ReverseGeocodeVO selectRegeocoding(ReverseGeocodeDTO reverseGeocodeDTO);
}
