package com.tsq.map.provider.service;

import com.tsq.map.api.dto.WeatherDTO;
import com.tsq.map.api.vo.WeatherVO;

public interface WeatherService {

    /**
     * <p>天气查询</p>
     * @param weatherDTO
     * */
    WeatherVO selectWeather(WeatherDTO weatherDTO);
}
