package com.tsq.map.provider.service.Impl;

import com.alibaba.fastjson.JSON;
import com.tsq.map.api.dto.GeocodeDTO;
import com.tsq.map.api.dto.ReverseGeocodeDTO;
import com.tsq.map.api.vo.GeocodeVO;
import com.tsq.map.api.vo.ReverseGeocodeVO;
import com.tsq.map.provider.service.GeocodeService;
import com.tsq.map.provider.util.Constant;
import com.tsq.map.provider.util.TsqHttpUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GeocodeServiceImpl implements GeocodeService {

    @Autowired
    private TsqHttpUtil tsqHttpUtil;

    /**
     * 调用高德第三方查询地理编码
     * @param geocodeDTO 查询参数
     * @return
     */
    @Override
    public GeocodeVO selectGeocoding(GeocodeDTO geocodeDTO) {
        String url = Constant.GEOCODING;
        String param = (String) tsqHttpUtil.httpGetRest(url,geocodeDTO);
        return JSON.parseObject(param, GeocodeVO.class);
    }

    /**
     * 调用高德第三方查询逆地理编码
     * @param reverseGeocodeDTO 查询参数
     * @return
     */
    @Override
    public ReverseGeocodeVO selectRegeocoding(ReverseGeocodeDTO reverseGeocodeDTO) {
        String url = Constant.INVERSE_GEOCDING;
        String param = (String) tsqHttpUtil.httpGetRest(url,reverseGeocodeDTO);
        return JSON.parseObject(param, ReverseGeocodeVO.class);
    }

}


