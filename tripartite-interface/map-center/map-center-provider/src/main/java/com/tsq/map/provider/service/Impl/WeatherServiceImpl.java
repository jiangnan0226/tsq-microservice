package com.tsq.map.provider.service.Impl;

import com.alibaba.fastjson.JSON;
import com.tsq.map.api.dto.WeatherDTO;
import com.tsq.map.api.vo.WeatherVO;
import com.tsq.map.provider.service.WeatherService;
import com.tsq.map.provider.util.Constant;
import com.tsq.map.provider.util.TsqHttpUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WeatherServiceImpl implements WeatherService {

    @Autowired
    private TsqHttpUtil tsqHttpUtil;

    /**
     * <p>天气查询</p>
     * @param weatherDTO
     * */
    @Override
    public WeatherVO selectWeather(WeatherDTO weatherDTO) {
        String url = Constant.THE_WEATHER_QUERY;
        String param = (String) tsqHttpUtil.httpGetRest(url,weatherDTO);
        return JSON.parseObject(param, WeatherVO.class);
    }

}
