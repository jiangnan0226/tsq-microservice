package com.tsq.map.provider.controller;

import com.tsq.common.model.ResponseData;
import com.tsq.map.api.dto.AssistantDTO;
import com.tsq.map.api.vo.AssistantVO;
import com.tsq.map.provider.service.AssistantService;
import com.tsq.map.provider.util.Constant;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/assistant")
public class AssistantController {

    @Autowired
    private AssistantService assistantService;

    @ApiOperation(value = "输入提示集合信息", notes = "输入提示集合信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "assistantDTO", value = "输入提示信息", dataType = "", paramType = "query", required = true)
    })
    @GetMapping("inputTips")
    public ResponseData<AssistantVO> selectAssistant(AssistantDTO assistantDTO){
        AssistantVO assistantVO = assistantService.selectAssistant(assistantDTO);
        if(Constant.SUCCESS_STATUS.equals(assistantVO.getStatus())){
            return ResponseData.success(assistantVO);
        }else {
            return ResponseData.error(assistantVO.getInfo());
        }
    }
}
