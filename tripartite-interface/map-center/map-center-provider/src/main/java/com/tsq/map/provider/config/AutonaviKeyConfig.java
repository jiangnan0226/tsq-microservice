package com.tsq.map.provider.config;

import com.tsq.map.provider.util.TsqHttpUtil;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author：xhy
 * @Description:
 * @Date: 2020/6/29 13:50
 */

@Data
@Configuration
public class AutonaviKeyConfig {

    @Value("${my-key.key}")
    private String key;


    @Bean
    public TsqHttpUtil tsqHttpUtil(){
        return new TsqHttpUtil(key);
    }
}
