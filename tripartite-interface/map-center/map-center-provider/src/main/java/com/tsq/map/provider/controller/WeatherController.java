package com.tsq.map.provider.controller;

import com.tsq.common.model.ResponseData;
import com.tsq.map.api.dto.WeatherDTO;
import com.tsq.map.api.vo.WeatherVO;
import com.tsq.map.provider.service.WeatherService;
import com.tsq.map.provider.util.Constant;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/weather")
public class WeatherController {

    @Autowired
    private WeatherService weatherService;

    @ApiOperation(value = "天气查询信息", notes = "天气查询信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "TrafficInformationDTO", value = "天气查询请求参数", dataType = "", paramType = "query", required = true)
    })
    @GetMapping("weatherInfo")
    public ResponseData<WeatherVO> selectWeather(WeatherDTO weatherDTO){
        WeatherVO weatherVO = weatherService.selectWeather(weatherDTO);
        if(Constant.SUCCESS_STATUS.equals(weatherVO.getStatus())){
            return ResponseData.success(weatherVO);
        }else {
            return ResponseData.error(weatherVO.getInfo());
        }
    }
}
