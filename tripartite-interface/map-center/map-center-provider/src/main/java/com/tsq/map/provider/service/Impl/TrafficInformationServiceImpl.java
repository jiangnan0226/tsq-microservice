package com.tsq.map.provider.service.Impl;

import com.alibaba.fastjson.JSON;
import com.tsq.map.api.dto.TrafficInformationDTO;
import com.tsq.map.api.vo.TrafficInformationVO;
import com.tsq.map.provider.service.TrafficInformationService;
import com.tsq.map.provider.util.Constant;
import com.tsq.map.provider.util.TsqHttpUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TrafficInformationServiceImpl implements TrafficInformationService {

    @Autowired
    private TsqHttpUtil tsqHttpUtil;

    /**
     * <p>查询交通态势</p>
     * @param trafficInformationDTO
     * */
    @Override
    public TrafficInformationVO selectTrafficInformation(TrafficInformationDTO trafficInformationDTO) {
        String url = null;
        if(null != trafficInformationDTO.getRectangle()){
            //矩形区域交通态势查询请求地址
            url = Constant.TRAFFIC_RECTANGEL;
        }else if (null != trafficInformationDTO.getLocation()){
            //圆形区域交通态势查询请求地址
            url = Constant.TRAFFIC_ROUNDNESS;
        }else if (null != trafficInformationDTO.getName()){
            //指定线路交通态势查询请求地址
            url = Constant.TRAFFIC_CIRCUIT;
        }else {
            System.out.println("参数错误");
        }
        String param = (String) tsqHttpUtil.httpGetRest(url,trafficInformationDTO);
        return JSON.parseObject(param, TrafficInformationVO.class);
    }

}
