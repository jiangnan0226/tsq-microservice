package com.tsq.map.provider.controller;

import com.tsq.common.model.ResponseData;
import com.tsq.map.api.dto.*;
import com.tsq.map.api.vo.*;
import com.tsq.map.provider.service.RoutePlanningService;
import com.tsq.map.provider.util.Constant;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("guide")
public class RoutePlanningController {

    @Autowired
    private RoutePlanningService routePlanningService;

    @ApiOperation(value = "路径规划(步行方案)信息", notes = "路径规划(步行方案)信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "WalkingRoutesDTO", value = "路径规划(步行方案)请求参数", dataType = "", paramType = "query", required = true)
    })
    @GetMapping("walking")
    public ResponseData<WalkingRoutesVO> selectWalkingRoutes(WalkingRoutesDTO walkingRoutesDTO){
        WalkingRoutesVO walkingRoutesVO = routePlanningService.selectWalking(walkingRoutesDTO);
        if(walkingRoutesVO.getStatus().equals(Constant.SUCCESS_STATUS)){
            return ResponseData.success(walkingRoutesVO);
        }else {
            return ResponseData.error(walkingRoutesVO.getInfo());
        }
    }

    @ApiOperation(value = "路径规划（乘车方案）信息", notes = "路径规划（乘车方案）信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "GuideRidingDTO", value = "路径规划（乘车方案）请求参数", dataType = "", paramType = "query", required = true)
    })
    @GetMapping("riding")
    public  ResponseData<GuideRidingVO> selectGuideRiding(GuideRidingDTO guideRidingDTO){
        GuideRidingVO guideRidingVO = routePlanningService.selectRiding(guideRidingDTO);
        if(guideRidingVO.getStatus().equals(Constant.SUCCESS_STATUS)){
            return ResponseData.success(guideRidingVO);
        }else {
            return ResponseData.error(guideRidingVO.getInfo());
        }
    }

    @ApiOperation(value = "路径规划（驾车方案）信息", notes = "路径规划（驾车方案）信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "GuideDrivingDTO", value = "路径规划（驾车方案）请求参数", dataType = "", paramType = "query", required = true)
    })
    @GetMapping("driving")
    public ResponseData<GuideDrivingVO> selectDriving(GuideDrivingDTO guideDrivingDTO){
        GuideDrivingVO guideDrivingVO = routePlanningService.selectDriving(guideDrivingDTO);
        if(guideDrivingVO.getStatus().equals(Constant.SUCCESS_STATUS)){
            return ResponseData.success(guideDrivingVO);
        }else {
            return ResponseData.error(guideDrivingVO.getInfo());
        }
    }

    @ApiOperation(value = "路径规划（骑行方案）信息", notes = "路径规划（骑行方案）信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "GuideCyclingDTO", value = "路径规划（骑行方案）请求参数", dataType = "", paramType = "query", required = true)
    })
    @GetMapping("cycling")
    public ResponseData<GuideCyclingVO> selectCycling(String origin,String destination){
        GuideCyclingVO guideCyclingVO = routePlanningService.selectCycling(origin,destination);
        if(guideCyclingVO.getStatus().equals(Constant.SUCCESS_STATUS)){
            return ResponseData.success(guideCyclingVO);
        }else {
            return ResponseData.error(guideCyclingVO.getInfo());
        }
    }

    @ApiOperation(value = "路径规划（货车方案）信息", notes = "路径规划（货车方案）信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "GuideTrucksDTO", value = "路径规划（货车方案）请求参数", dataType = "", paramType = "query", required = true)
    })
    @GetMapping("trucks")
    public ResponseData<GuideTrucksVO> selectTrucks(GuideTrucksDTO guideTrucksDTO){
        GuideTrucksVO guideTrucksVO = routePlanningService.selectTrucks(guideTrucksDTO);
        if(guideTrucksVO.getStatus().equals(Constant.SUCCESS_STATUS)){
            return ResponseData.success(guideTrucksVO);
        }else {
            return ResponseData.error(guideTrucksVO.getInfo());
        }
    }

    @ApiOperation(value = "路径规划（距离测量）信息", notes = "路径规划（距离测量）信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "GuideDistanceDTO", value = "路径规划（距离测量）请求参数", dataType = "", paramType = "query", required = true)
    })
    @GetMapping("distance")
    public ResponseData<GuideDistanceVO> selectDistance(GuideDistanceDTO guideDistanceDTO){
        GuideDistanceVO guideDistanceVO = routePlanningService.selectDistance(guideDistanceDTO);
        if(Constant.SUCCESS_STATUS.equals(guideDistanceVO.getStatus())){
            return ResponseData.success(guideDistanceVO);
        }else {
            return ResponseData.error(guideDistanceVO.getInfo());
        }
    }
}
