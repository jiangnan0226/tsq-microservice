package com.tsq.map.provider.service.Impl;

import com.alibaba.fastjson.JSON;
import com.tsq.map.api.dto.CoordinateDTO;
import com.tsq.map.api.vo.CoordinateVO;
import com.tsq.map.provider.service.CoordinateService;
import com.tsq.map.provider.util.Constant;
import com.tsq.map.provider.util.TsqHttpUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CoordinateServiceImpl implements CoordinateService {

    @Autowired
    private TsqHttpUtil tsqHttpUtil;

    /**
     * <p>坐标转换</p>
     * @param coordinateDTO
     * */
    @Override
    public CoordinateVO convert(CoordinateDTO coordinateDTO) {
        String url = Constant.COORDINATE_TRANSFORMATION;
        String param = (String) tsqHttpUtil.httpGetRest(url ,coordinateDTO);
        return JSON.parseObject(param, CoordinateVO.class);
    }

}
