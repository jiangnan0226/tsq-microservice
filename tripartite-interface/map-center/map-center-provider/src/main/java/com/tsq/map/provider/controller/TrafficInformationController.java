package com.tsq.map.provider.controller;

import com.tsq.common.model.ResponseData;
import com.tsq.map.api.dto.TrafficInformationDTO;
import com.tsq.map.api.vo.TrafficInformationVO;
import com.tsq.map.provider.service.TrafficInformationService;
import com.tsq.map.provider.util.Constant;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("traffic")
public class TrafficInformationController {

    @Autowired
    private TrafficInformationService trafficInformationService;

    @ApiOperation(value = "交通态势信息", notes = "交通态势信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "TrafficInformationDTO", value = "交通态势请求参数", dataType = "", paramType = "query", required = true)
    })
    @GetMapping("status")
    public ResponseData<TrafficInformationVO> selectTrafficInformation(TrafficInformationDTO trafficInformationDTO){
        TrafficInformationVO trafficInformationVO = trafficInformationService.selectTrafficInformation(trafficInformationDTO);
        if(Constant.SUCCESS_STATUS.equals(trafficInformationVO.getStatus())){
            return ResponseData.success(trafficInformationVO);
        }else {
            return ResponseData.error(trafficInformationVO.getInfo());
        }
    }
}
