package com.tsq.map.provider.controller;

import com.tsq.common.model.ResponseData;
import com.tsq.map.api.dto.DistrictDTO;
import com.tsq.map.api.vo.DistrictVO;
import com.tsq.map.provider.service.DistrictService;
import com.tsq.map.provider.util.Constant;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/district")
public class DistrictController {

    @Autowired
    private DistrictService districtService;

    @ApiOperation(value = "行政区域查询集合参数", notes = "行政区域查询集合参数")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "DistrictDTO", value = "行政区域查询请求参数", dataType = "", paramType = "query", required = true)
    })
    @GetMapping("select")
    public ResponseData<DistrictVO> selectDistrict(DistrictDTO districtDTO){
        DistrictVO districtVO = districtService.selectDistrict(districtDTO);
        if(Constant.SUCCESS_STATUS.equals(districtVO.getStatus())){
            return ResponseData.success(districtVO);
        }else {
            return ResponseData.error(districtVO.getInfo());
        }
    }
}
