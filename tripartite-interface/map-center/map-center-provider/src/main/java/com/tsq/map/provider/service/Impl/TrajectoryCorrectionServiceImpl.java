package com.tsq.map.provider.service.Impl;

import com.alibaba.fastjson.JSON;
import com.tsq.map.api.dto.TrajectoryCorrectionDTO;
import com.tsq.map.api.vo.TrajectoryCorrectionVO;
import com.tsq.map.provider.service.TrajectoryCorrectionService;
import com.tsq.map.provider.util.Constant;
import com.tsq.map.provider.util.TsqHttpUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class TrajectoryCorrectionServiceImpl implements TrajectoryCorrectionService {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private TsqHttpUtil tsqHttpUtil;

    @Override
    public ResponseEntity<TrajectoryCorrectionVO> selectTrajectoryCorrectionVO(List<TrajectoryCorrectionDTO> list) {
        String url = Constant.TRAJECTORY_CORRECTION + "?key=" + tsqHttpUtil.getKey();
        String jsonData = JSON.toJSONString(list);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(jsonData, headers);
        return restTemplate.postForEntity(url, request, TrajectoryCorrectionVO.class);
    }

}
