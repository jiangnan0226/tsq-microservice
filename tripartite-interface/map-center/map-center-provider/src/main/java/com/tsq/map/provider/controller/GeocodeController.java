package com.tsq.map.provider.controller;

import com.tsq.common.model.ResponseData;
import com.tsq.map.api.dto.GeocodeDTO;
import com.tsq.map.api.dto.ReverseGeocodeDTO;
import com.tsq.map.api.vo.GeocodeVO;
import com.tsq.map.api.vo.ReverseGeocodeVO;
import com.tsq.map.provider.service.GeocodeService;
import com.tsq.map.provider.util.Constant;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/geocode")
public class GeocodeController {

    @Autowired
    private GeocodeService geocodeService;

    @ApiOperation(value = "地理编码集合信息", notes = "地理编码集合信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "DistrictDTO", value = "地理编码请求参数", dataType = "", paramType = "query", required = true)
    })
    @GetMapping("geocoding")
    public ResponseData<GeocodeVO> selectGeocoding(GeocodeDTO geocodeDTO){
        GeocodeVO geocodeVO = geocodeService.selectGeocoding(geocodeDTO);
        if(geocodeVO.getStatus().equals(Constant.SUCCESS_STATUS)){
            return ResponseData.success(geocodeVO);
        }else {
            return ResponseData.error(geocodeVO.getInfo());
        }
    }

    @ApiOperation(value = "逆地理编码集合信息", notes = "逆地理编码集合信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "DistrictDTO", value = "逆地理编码请求参数", dataType = "", paramType = "query", required = true)
    })
    @GetMapping("reGeocoding")
    public ResponseData<ReverseGeocodeVO> selectReverseGeocode(ReverseGeocodeDTO reverseGeocodeDTO){
        ReverseGeocodeVO reverseGeocodeVO = geocodeService.selectRegeocoding(reverseGeocodeDTO);
        if(Constant.SUCCESS_STATUS.equals(reverseGeocodeVO.getStatus())){
            return ResponseData.success(reverseGeocodeVO);
        }else {
            return ResponseData.error(reverseGeocodeVO.getInfo());
        }
    }
}
