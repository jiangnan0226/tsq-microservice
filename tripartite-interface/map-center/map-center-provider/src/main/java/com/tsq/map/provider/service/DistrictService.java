package com.tsq.map.provider.service;

import com.tsq.map.api.dto.DistrictDTO;
import com.tsq.map.api.vo.DistrictVO;

public interface DistrictService {

    /**
     * <p>行政区域查询</p>
     * @param districtDTO
     * */
    DistrictVO selectDistrict(DistrictDTO districtDTO);
}
