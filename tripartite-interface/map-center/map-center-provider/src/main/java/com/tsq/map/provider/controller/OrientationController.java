package com.tsq.map.provider.controller;

import com.tsq.common.model.ResponseData;
import com.tsq.map.api.vo.OrientationVO;
import com.tsq.map.provider.service.GeocodeService;
import com.tsq.map.provider.service.OrientationService;
import com.tsq.map.provider.util.Constant;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/orientation")
public class OrientationController {

    @Autowired
    private OrientationService orientationService;

    @Autowired
    private GeocodeService geocodeService;

    @ApiOperation(value = "ip定位集合信息", notes = "ip定位集合信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "OrientationDTO", value = "Ip定位请求参数", dataType = "", paramType = "query", required = true)
    })
    @GetMapping("ip")
    public ResponseData<OrientationVO> selectOrientation(String ip) throws Exception{
        OrientationVO orientationVO = orientationService.selectOrientation(ip);
        if(Constant.SUCCESS_STATUS.equals(orientationVO.getStatus())){
            return ResponseData.success(orientationVO);
        }else {
            return ResponseData.error(orientationVO.getInfo());
        }
    }
}
