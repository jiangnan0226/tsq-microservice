package com.tsq.map.provider.service;

import com.tsq.map.api.dto.TrajectoryCorrectionDTO;
import com.tsq.map.api.vo.TrajectoryCorrectionVO;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface TrajectoryCorrectionService {

    /**
     * <p>轨迹纠偏返回信息</p>
     *
     * @return*/
    ResponseEntity<TrajectoryCorrectionVO> selectTrajectoryCorrectionVO(List<TrajectoryCorrectionDTO> list);
}
