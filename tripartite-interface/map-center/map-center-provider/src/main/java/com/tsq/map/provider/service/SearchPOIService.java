package com.tsq.map.provider.service;

import com.tsq.map.api.dto.SearchPoiAroundDTO;
import com.tsq.map.api.dto.SearchPoiDetailDTO;
import com.tsq.map.api.dto.SearchPoiKeywordDTO;
import com.tsq.map.api.dto.SearchPoiPolygonDTO;
import com.tsq.map.api.vo.SearchPoiVO;

public interface SearchPOIService {
    /**
     * <p>关键字搜索</p>
     * @param searchPoiKeywordDTO
     * */
    SearchPoiVO searchKeyword(SearchPoiKeywordDTO searchPoiKeywordDTO);

    /**
     * <p>周边搜索</p>
     * @param searchPoiAroundDTO
     * */
    SearchPoiVO searchAround(SearchPoiAroundDTO searchPoiAroundDTO);

    /**
     * <p>多边形搜索</p>
     * @param searchPoiPolygonDTO
     * */
    SearchPoiVO searchPolygon(SearchPoiPolygonDTO searchPoiPolygonDTO);

    /**
     * <p>ID查询</p>
     * @param searchPoiDetailDTO
     * */
    SearchPoiVO searchDetail(SearchPoiDetailDTO searchPoiDetailDTO);

}
