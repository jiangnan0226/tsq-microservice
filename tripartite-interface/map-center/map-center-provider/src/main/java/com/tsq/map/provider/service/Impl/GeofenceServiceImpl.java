package com.tsq.map.provider.service.Impl;

import cn.hutool.http.HttpRequest;
import com.alibaba.fastjson.JSON;
import com.tsq.map.api.dto.CreateGeofenceDTO;
import com.tsq.map.api.dto.MonitoringGeofenceDTO;
import com.tsq.map.api.dto.SelectGeofenceDTO;
import com.tsq.map.api.dto.UpdateGeofenceDTO;
import com.tsq.map.api.vo.CreateGeofenceVO;
import com.tsq.map.api.vo.MonitoringGeofenceVO;
import com.tsq.map.api.vo.SelectGeofenceVO;
import com.tsq.map.api.vo.StatusGeofenceVO;
import com.tsq.map.provider.service.GeofenceService;
import com.tsq.map.provider.util.Constant;
import com.tsq.map.provider.util.TsqHttpUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GeofenceServiceImpl implements GeofenceService {

    @Autowired
    private TsqHttpUtil tsqHttpUtil;

    /**
     * <p>创建地理围栏</p>
     * @param createGeofenceDTO
     * */
    @Override
    public CreateGeofenceVO createGeofence(CreateGeofenceDTO createGeofenceDTO) {
        String url = Constant.GEO_FENCES;
        String param = (String) tsqHttpUtil.httpPostRest(url,createGeofenceDTO);
        return JSON.parseObject(param,CreateGeofenceVO.class);
    }

    /**
     * <p>查询地理围栏</p>
     * @param selectGeofenceDTO
     * */
    @Override
    public SelectGeofenceVO selectGeofence(SelectGeofenceDTO selectGeofenceDTO) {
        String url = Constant.GEO_FENCES;
        String param = (String) tsqHttpUtil.httpGetRest(url,selectGeofenceDTO);
        return JSON.parseObject(param,SelectGeofenceVO.class);
    }

    /**
     * <p>更细地理围栏</p>
     * @param updateGeofenceDTO
     * */
    @Override
    public StatusGeofenceVO updateGeofence(UpdateGeofenceDTO updateGeofenceDTO,String gid) {
        String url = Constant.GEO_FENCES+"?key="+ tsqHttpUtil.getKey() +"&gid="+gid;
        String param = HttpRequest.patch(url).body(JSON.toJSONString(updateGeofenceDTO)).execute().body();
        return JSON.parseObject(param,StatusGeofenceVO.class);
    }

    /**
     * <p>围栏启动/停止</p>
     * @param gid 围栏全局ID，status 围栏状态
     * */
    @Override
    public StatusGeofenceVO statusGeofence(String gid, String status) {
        String url = Constant.GEO_FENCES+"?key="+ tsqHttpUtil.getKey() +"&gid="+gid;
        String param = HttpRequest.patch(url).body(status).execute().body();
        return JSON.parseObject(param,StatusGeofenceVO.class);
    }

    /**
     * <p>删除围栏</p>
     * @param gid 围栏全局ID，status 围栏状态
     * */
    @Override
    public StatusGeofenceVO deleteGeofence(String gid) {
        String url = Constant.GEO_FENCES+"?key="+ tsqHttpUtil.getKey() +"&gid="+gid;
        String param = HttpRequest.delete(url).execute().body();
        return JSON.parseObject(param,StatusGeofenceVO.class);
    }

    /**
     * <p>围栏监控</p>
     * @param monitoringGeofenceDTO
     * */
    @Override
    public MonitoringGeofenceVO selectMonitoringGeofenceVO(MonitoringGeofenceDTO monitoringGeofenceDTO) {
        String url = Constant.MONITORING;
        String param = (String) tsqHttpUtil.httpGetRest(url,monitoringGeofenceDTO);
        return JSON.parseObject(param,MonitoringGeofenceVO.class);
    }

}
