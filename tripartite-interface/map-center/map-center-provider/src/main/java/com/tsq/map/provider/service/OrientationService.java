package com.tsq.map.provider.service;

import com.tsq.map.api.vo.OrientationVO;

public interface OrientationService {

    /**
     * <p>ID查询</p>
     * @param ip
     * */
    OrientationVO selectOrientation(String ip) throws Exception;
}
