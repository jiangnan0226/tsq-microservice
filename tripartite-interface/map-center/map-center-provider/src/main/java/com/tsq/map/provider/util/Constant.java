package com.tsq.map.provider.util;

public class Constant {

    //成功状态码
    public static final Integer SUCCESS_STATUS = 1;

    //失败状态码
    public static final Integer ERROR_STATUS = 1;

    //输入提示请求地址
    public static final String INPUT_PROMPT = "https://restapi.amap.com/v3/assistant/inputtips";

    //坐标转换请求地址
    public static final String COORDINATE_TRANSFORMATION = "https://restapi.amap.com/v3/assistant/coordinate/convert";

    //行政区域查询请求地址
    public static final String REGIONAL_QUERY = "https://restapi.amap.com/v3/config/district";

    //地理编码请求地址
    public static final String GEOCODING = "https://restapi.amap.com/v3/geocode/geo";

    //逆地理编码请求地址
    public static final String INVERSE_GEOCDING = "https://restapi.amap.com/v3/geocode/regeo";

    //ip定位请求地址
    public static final String IP_LOCATION = "https://restapi.amap.com/v3/ip";

    //路径规划（步行方案）请求地址
    public static final String ROUTE_PLANNING_WALKING = "https://restapi.amap.com/v3/direction/walking";

    //路径规划（乘车方案）请求地址
    public static final String ROUTE_PLANNING_RIDING = "https://restapi.amap.com/v3/direction/transit/integrated";

    //路径规划（驾车方案）请求地址
    public static final String ROUTE_PLANNING_DRIVE = "https://restapi.amap.com/v3/direction/driving";

    //路径规划（骑行方案）请求地址
    public static final String ROUTE_PLANNING_CYCLING = "https://restapi.amap.com/v4/direction/bicycling";

    //路径规划（货车方案）请求地址
    public static final String ROUTE_PLANNING_TRUCKS = "https://restapi.amap.com/v4/direction/truck";

    //路径规划（距离测量）请求地址
    public static final String ROUTE_PLANNING_DISTANCE = "https://restapi.amap.com/v3/distance";

    //关键字搜索请求地址
    public static final String KEYWORD_SEARCH = "https://restapi.amap.com/v3/place/text";

    //周边搜索请求地址
    public static final String AROUND_SEARCH = "https://restapi.amap.com/v3/place/around";

    //多边形搜索请求地址
    public static final String POLYGON_SEARCH = "https://restapi.amap.com/v3/place/polygon";

    //ID查询请求地址
    public static final String ID_SEARCH = "https://restapi.amap.com/v3/place/detail";

    //交通态势查询（矩形区域查询）请求地址
    public static final  String TRAFFIC_RECTANGEL = "https://restapi.amap.com/v3/traffic/status/rectangle";

    //交通态势查询（圆形区域查询）请求地址
    public static final  String TRAFFIC_ROUNDNESS = "https://restapi.amap.com/v3/traffic/status/circle";

    //交通态势查询（指定线路查询）请求地址
    public static final  String TRAFFIC_CIRCUIT = "https://restapi.amap.com/v3/traffic/status/road";

    //天气查询请求地址
    public static final String THE_WEATHER_QUERY = "https://restapi.amap.com/v3/weather/weatherInfo";

    //轨迹纠偏请求地址
    public static final String TRAJECTORY_CORRECTION = "https://restapi.amap.com/v4/grasproad/driving";

    //地理围栏
    public static final String GEO_FENCES = "https://restapi.amap.com/v4/geofence/meta";

    //地理围栏（设备监控）
    public static final String MONITORING = "https://restapi.amap.com/v4/geofence/status";

}
