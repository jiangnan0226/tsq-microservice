package com.tsq.map.provider.service;

import com.tsq.map.api.dto.CoordinateDTO;
import com.tsq.map.api.vo.CoordinateVO;

public interface CoordinateService {

    /**
     * <p>坐标转换</p>
     * @param coordinateDTO
     * */
    CoordinateVO convert(CoordinateDTO coordinateDTO);
}
