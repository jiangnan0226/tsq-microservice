package com.tsq.map.provider.service.Impl;

import com.alibaba.fastjson.JSON;
import com.tsq.map.api.dto.SearchPoiAroundDTO;
import com.tsq.map.api.dto.SearchPoiDetailDTO;
import com.tsq.map.api.dto.SearchPoiKeywordDTO;
import com.tsq.map.api.dto.SearchPoiPolygonDTO;
import com.tsq.map.api.vo.SearchPoiVO;
import com.tsq.map.provider.service.SearchPOIService;
import com.tsq.map.provider.util.Constant;
import com.tsq.map.provider.util.TsqHttpUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SearchPOIServiceImpl implements SearchPOIService {

    @Autowired
    private TsqHttpUtil tsqHttpUtil;

    /**
     * <p>关键字搜索</p>
     * @param searchPoiKeywordDTO
     * */
    @Override
    public SearchPoiVO searchKeyword(SearchPoiKeywordDTO searchPoiKeywordDTO) {
        String url = Constant.KEYWORD_SEARCH;//请求接口地址
        String param = (String) tsqHttpUtil.httpGetRest(url,searchPoiKeywordDTO);
        return JSON.parseObject(param, SearchPoiVO.class);
    }

    /**
     * <p>周边搜索</p>
     * @param searchPoiAroundDTO
     * */
    @Override
    public SearchPoiVO searchAround(SearchPoiAroundDTO searchPoiAroundDTO) {
        String url = Constant.AROUND_SEARCH;//请求接口地址
        String param = (String) tsqHttpUtil.httpGetRest(url,searchPoiAroundDTO);
        return JSON.parseObject(param, SearchPoiVO.class);
    }

    /**
     * <p>多边形搜索</p>
     * @param searchPoiPolygonDTO
     * */
    @Override
    public SearchPoiVO searchPolygon(SearchPoiPolygonDTO searchPoiPolygonDTO) {
        String url = Constant.POLYGON_SEARCH;//请求接口地址
        String param = (String) tsqHttpUtil.httpGetRest(url,searchPoiPolygonDTO);
        return JSON.parseObject(param, SearchPoiVO.class);
    }

    /**
     * <p>ID查询</p>
     * @param searchPoiDetailDTO
     * */
    @Override
    public SearchPoiVO searchDetail(SearchPoiDetailDTO searchPoiDetailDTO) {
        String url = Constant.ID_SEARCH;//请求接口地址
        String param = (String) tsqHttpUtil.httpGetRest(url,searchPoiDetailDTO);
        return JSON.parseObject(param, SearchPoiVO.class);
    }

}
