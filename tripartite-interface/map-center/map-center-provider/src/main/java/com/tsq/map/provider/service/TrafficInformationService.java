package com.tsq.map.provider.service;

import com.tsq.map.api.dto.TrafficInformationDTO;
import com.tsq.map.api.vo.TrafficInformationVO;

public interface TrafficInformationService {

    /**
     * <p>查询交通态势</p>
     * @param trafficInformationDTO 请求参数
     * */
    TrafficInformationVO selectTrafficInformation(TrafficInformationDTO trafficInformationDTO);

}
