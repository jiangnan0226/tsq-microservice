package com.tsq.map.provider.service.Impl;

import com.alibaba.fastjson.JSON;
import com.tsq.map.api.dto.AssistantDTO;
import com.tsq.map.api.vo.AssistantVO;
import com.tsq.map.provider.service.AssistantService;
import com.tsq.map.provider.util.Constant;
import com.tsq.map.provider.util.TsqHttpUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AssistantServiceImpl implements AssistantService {

    @Autowired
    private TsqHttpUtil tsqHttpUtil;
    /**
     * <p>输入提示</p>
     * @param assistantDTO
     * */
    @Override
    public AssistantVO selectAssistant(AssistantDTO assistantDTO) {
        String url = Constant.INPUT_PROMPT;//请求接口地址
        String param = (String) tsqHttpUtil.httpGetRest(url ,assistantDTO);
        return JSON.parseObject(param,AssistantVO.class);
    }
}
