package com.tsq.map.provider.controller;

import com.tsq.common.model.ResponseData;
import com.tsq.map.api.dto.CoordinateDTO;
import com.tsq.map.api.vo.CoordinateVO;
import com.tsq.map.provider.service.CoordinateService;
import com.tsq.map.provider.util.Constant;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("coordinate")
public class CoordinateController {

    @Autowired
    private CoordinateService coordinateService;

    @ApiOperation(value = "坐标转换集合参数", notes = "坐标转换集合参数")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "CoordinateDTO", value = "坐标转换请求参数", dataType = "", paramType = "query", required = true)
    })
    @GetMapping("convert")
    public ResponseData<CoordinateVO> convert(CoordinateDTO coordinateDTO){
        CoordinateVO coordinateVO = coordinateService.convert(coordinateDTO);
        if(Constant.SUCCESS_STATUS.equals(coordinateVO.getStatus())){
            return ResponseData.success(coordinateVO);
        }else {
            return ResponseData.error(coordinateVO.getInfo());
        }
    }
}
