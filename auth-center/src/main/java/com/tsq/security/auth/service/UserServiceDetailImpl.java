package com.tsq.security.auth.service;

import com.tsq.admin.api.model.SysUserInfo;
import com.tsq.admin.client.SysModuleResourcesClientService;
import com.tsq.admin.client.SysUserInfoClientService;
import com.tsq.common.constant.CommonConstant;
import com.tsq.common.model.ResponseData;
import com.tsq.security.auth.model.AuthUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author dcy
 * @Date: 2019/3/19 16:31
 * @Description:
 */
@Service
public class UserServiceDetailImpl implements UserDetailsService {

    @Autowired
    private SysUserInfoClientService sysUserInfoClientService;
    @Autowired
    private SysModuleResourcesClientService sysModuleResourcesClientService;
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // Basic ZGN5X2FkbWluX2NsaWVudDoxMjM0NTY=
        //查询用户信息
        ResponseData<SysUserInfo> userInfoByUsername = sysUserInfoClientService.getUserInfoByUsername(username);
        if (Boolean.TRUE.equals(userInfoByUsername.getSuccess())) {
            SysUserInfo sysUserInfo = userInfoByUsername.getData();
            // 查询权限
            ResponseData<Set<String>> allPermissionByUserId = sysUserInfoClientService.getAllPermissionByUserId(sysUserInfo.getUserId());
            if (Boolean.TRUE.equals(allPermissionByUserId.getSuccess())) {
                ResponseData<List<Map<String, Object>>> moduleResourcesListData = sysModuleResourcesClientService.getModuleByUserId(sysUserInfo.getUserId());
                if (Boolean.TRUE.equals(moduleResourcesListData.getSuccess())) {
                    // 存到redis中
                    redisTemplate.opsForValue().set(CommonConstant.REDIS_USER_MODULE_LIST_KEY + sysUserInfo.getUserId(), moduleResourcesListData.getData());
                }
                AuthUser authUser = new AuthUser();
                authUser.setSysUserInfo(sysUserInfo);
                sysUserInfo.setAllPermissionSet(allPermissionByUserId.getData());
                return authUser;
            }
            return null;
        }
        return null;
    }

    //granter用
    public RedisTemplate<String, Object> getRedisTemplate(){
        return redisTemplate;
    }

    //granter用 登录时注册
    public boolean save(SysUserInfo sysUserInfo){
        ResponseData<Boolean> result = sysUserInfoClientService.save(sysUserInfo);
        return Boolean.TRUE.equals(result.getSuccess());
    }
}
