package com.tsq.admin.provider.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tsq.admin.api.model.SysRole;
import com.tsq.admin.api.model.SysUserInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author dcy
 * @since 2019-09-06
 */
public interface SysUserInfoMapper extends BaseMapper<SysUserInfo> {

    /**
     * 根据用户id 查询已授权角色列表
     *
     * @param userId
     * @return
     */
    List<SysRole> getAuthRoleListByUserId(@Param("userId") String userId);

    /**
     * 根据用户id 查询已授权角色id
     * @param userId
     * @return
     */
    Set<String> getAuthRoleIdsListByUserId(@Param("userId") String userId);

    /**
     * 根据用户id 查询已授权角色唯一标识（roleKey）
     *
     * @param userId
     * @return
     */
    Set<String> getAuthRoleSetByUserId(@Param("userId") String userId);


    /**
     * 根据权限id 查询已授权菜单唯一标识（menu_code）
     *
     * @param menuPowerIds
     * @return
     */
    Set<String> getPermissionListByMenuPowerIds(@Param("menuPowerIds") Set<String> menuPowerIds);


    /**
     * 根据权限id 查询已授权的模块唯一标识（module_code）
     *
     * @param modulePowerIds
     * @return
     */
    Set<String> getModuleListByModulePowerIds(@Param("modulePowerIds") Set<String> modulePowerIds);
}
