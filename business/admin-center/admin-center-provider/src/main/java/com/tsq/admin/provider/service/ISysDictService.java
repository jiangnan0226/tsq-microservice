package com.tsq.admin.provider.service;

import com.tsq.admin.api.model.SysDict;
import com.tsq.web.base.service.BaseService;

import java.util.List;

/**
 * <p>
 * 字典类型表 服务类
 * </p>
 *
 * @author dcy
 * @since 2019-09-06
 */
public interface ISysDictService extends BaseService<SysDict> {


    /**
     * 根据分组类型查询字典项tree
     *
     * @param groupType
     * @return
     */
    List<SysDict> getDictTreeListByGroupType(String groupType);

    /**
     * 获取tree-table 数据
     *
     * @return
     */
    List<SysDict> getDictTreeTableList();

    /**
     * 根据类型查询字典项
     * @param type
     * @return
     */
    List<SysDict> getDictListByType(String type);
}
