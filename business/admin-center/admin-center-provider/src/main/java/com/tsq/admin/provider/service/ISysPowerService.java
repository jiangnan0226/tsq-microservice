package com.tsq.admin.provider.service;

import com.tsq.admin.api.dto.SysPowerMenuDTO;
import com.tsq.admin.api.dto.SysPowerModuleDTO;
import com.tsq.admin.api.model.SysPower;
import com.tsq.web.base.service.BaseService;

import java.util.Set;

/**
 * <p>
 * 权限表 服务类
 * </p>
 *
 * @author dcy
 * @since 2019-09-06
 */
public interface ISysPowerService extends BaseService<SysPower> {

    /**
     * 保存授权模块
     *
     * @param sysPowerModuleDTO
     * @return
     */
    Boolean saveAuthModule(SysPowerModuleDTO sysPowerModuleDTO);

    /**
     * 保存授权菜单
     *
     * @param sysPowerMenuDTO
     * @return
     */
    Boolean saveAuthMenu(SysPowerMenuDTO sysPowerMenuDTO);


    /**
     * 根据角色id查询菜单类型的权限id
     * @param roleIds
     * @return
     */
    Set<String> getMenuPowerIdByRoleIds(Set<String> roleIds);

    /**
     * 根据角色id查询模块类型的权限id
     * @param roleIds
     * @return
     */
    Set<String> getModulePowerIdByRoleIds(Set<String> roleIds);
}
