package com.tsq.admin.provider.service;

import com.tsq.admin.api.dto.SysUserInfoRoleDTO;
import com.tsq.admin.api.model.SysRole;
import com.tsq.admin.api.model.SysUserInfo;
import com.tsq.web.base.service.BaseService;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author dcy
 * @since 2019-09-06
 */
public interface ISysUserInfoService extends BaseService<SysUserInfo> {

    /**
     * 根据用户id 查询已授权角色列表
     *
     * @param userId
     * @return
     */
    List<SysRole> getAuthRoleListByUserId(String userId);

    /**
     * 根据用户id 查询已授权的角色id
     *
     * @param userId
     * @return
     */
    Set<String> getAuthRoleIdsListByUserId(String userId);


    /**
     * 保存授权角色
     *
     * @param sysUserInfoRoleDTO
     * @return
     */
    boolean saveAuthRole(SysUserInfoRoleDTO sysUserInfoRoleDTO);


    /**
     * 根据用户id 查询已授权角色列表
     *
     * @param userId
     * @return
     */
    Set<String> getAuthRoleSetByUserId(String userId);


    /**
     * 根据权限id 查询已授权菜单列表
     *
     * @param menuPowerIds
     * @return
     */
    Set<String> getPermissionListByMenuPowerIds(Set<String> menuPowerIds);


    /**
     * 根据权限id 查询已授权的模块权限
     *
     * @param modulePowerIds
     * @return
     */
    Set<String> getModuleListByModulePowerIds(Set<String> modulePowerIds);
}
