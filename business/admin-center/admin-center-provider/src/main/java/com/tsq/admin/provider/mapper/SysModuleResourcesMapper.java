package com.tsq.admin.provider.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tsq.admin.api.model.SysModuleResources;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author dcy
 * @since 2019-09-16
 */
public interface SysModuleResourcesMapper extends BaseMapper<SysModuleResources> {

    /**
     * 根据用户id 查询对应资源
     * @param roleIds
     * @return
     */
    List<Map<String,Object>> getModuleByRoleIds(@Param("roleIds") Set<String> roleIds);
}
