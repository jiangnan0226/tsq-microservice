package com.tsq.admin.provider.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.tsq.admin.api.model.SysDict;
import com.tsq.admin.provider.mapper.SysDictMapper;
import com.tsq.admin.provider.service.ISysDictService;
import com.tsq.common.constant.CommonConstant;
import com.tsq.web.base.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * <p>
 * 字典类型表 服务实现类
 * </p>
 *
 * @author dcy
 * @since 2019-09-06
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysDictServiceImpl extends BaseServiceImpl<SysDictMapper, SysDict> implements ISysDictService {

    @Autowired
    private SysDictMapper sysDictMapper;

    @Cacheable(value = "dict", key = "'group-'+#groupType", unless = "#result eq null")
    @Override
    public List<SysDict> getDictTreeListByGroupType(String groupType) {
        // 获取所有字典项
        List<SysDict> dictList = sysDictMapper.selectList(new LambdaQueryWrapper<SysDict>().eq(SysDict::getDictType, groupType).orderByAsc(SysDict::getLocation));
        List<SysDict> treeDataList = new ArrayList<>();
        dictList.stream().forEach(sysDict -> {
            if (CommonConstant.DEFAULT_PARENT_VAL.equalsIgnoreCase(sysDict.getParentId())) {
                treeDataList.add(sysDict);
            }
        });
        recursionTreeTableChildren(treeDataList, dictList);
        return treeDataList;
    }


    @Override
    public List<SysDict> getDictTreeTableList() {
        // 获取所有字典项
        List<SysDict> dictList = sysDictMapper.selectList(new LambdaQueryWrapper<SysDict>().orderByAsc(SysDict::getLocation));
        List<SysDict> treeDataList = new ArrayList<>();
        dictList.stream().forEach(sysDict -> {
            if (CommonConstant.DEFAULT_PARENT_VAL.equalsIgnoreCase(sysDict.getParentId())) {
                treeDataList.add(sysDict);
            }
        });
        recursionTreeTableChildren(treeDataList, dictList);
        return treeDataList;
    }

    @Cacheable(value = "dict", key = "#type", unless = "#result eq null")
    @Override
    public List<SysDict> getDictListByType(String type) {
        return super.list(new LambdaQueryWrapper<SysDict>().eq(SysDict::getType, type).orderByAsc(SysDict::getLocation));
    }

    private void recursionTreeTableChildren(List<SysDict> treeDataList, List<SysDict> dictList) {
        for (SysDict treeData : treeDataList) {
            List<SysDict> childrenList = new ArrayList<>();
            for (SysDict sysDict : dictList) {
                if (sysDict.getParentId().equals(treeData.getDictId())) {
                    childrenList.add(sysDict);
                }
            }
            if (!CollUtil.isEmpty(childrenList)) {
                treeData.setChildren(childrenList);
                recursionTreeTableChildren(childrenList, dictList);
            }
        }
    }

    @CacheEvict(value = "dict", allEntries = true)
    @Override
    public boolean save(SysDict entity) {
        return super.save(entity);
    }

    @CacheEvict(value = "dict", allEntries = true)
    @Override
    public boolean removeById(Serializable id) {
        return super.removeById(id);
    }

    @CacheEvict(value = "dict", allEntries = true)
    @Override
    public boolean updateById(SysDict entity) {
        return super.updateById(entity);
    }

    @CacheEvict(value = "dict", allEntries = true)
    @Override
    public boolean saveOrUpdate(SysDict entity) {
        return super.saveOrUpdate(entity);
    }

    @CacheEvict(value = "dict", allEntries = true)
    @Override
    public boolean removeByIds(Collection<? extends Serializable> idList) {
        return super.removeByIds(idList);
    }
}
