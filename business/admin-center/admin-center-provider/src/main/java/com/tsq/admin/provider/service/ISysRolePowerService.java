package com.tsq.admin.provider.service;

import com.tsq.admin.api.model.SysRolePower;
import com.tsq.web.base.service.BaseService;

/**
 * <p>
 * 角色与权限关联表 服务类
 * </p>
 *
 * @author dcy
 * @since 2019-09-06
 */
public interface ISysRolePowerService extends BaseService<SysRolePower> {

}
