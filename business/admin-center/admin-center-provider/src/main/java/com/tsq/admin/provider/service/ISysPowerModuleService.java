package com.tsq.admin.provider.service;

import com.tsq.admin.api.model.SysPowerModule;
import com.tsq.web.base.service.BaseService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author dcy
 * @since 2019-09-16
 */
public interface ISysPowerModuleService extends BaseService<SysPowerModule> {

}
