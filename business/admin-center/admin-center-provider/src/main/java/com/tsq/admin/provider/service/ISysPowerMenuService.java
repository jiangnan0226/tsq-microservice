package com.tsq.admin.provider.service;

import com.tsq.admin.api.model.SysPowerMenu;
import com.tsq.web.base.service.BaseService;

/**
 * <p>
 * 权限菜单关联表 服务类
 * </p>
 *
 * @author dcy
 * @since 2019-09-06
 */
public interface ISysPowerMenuService extends BaseService<SysPowerMenu> {

}
