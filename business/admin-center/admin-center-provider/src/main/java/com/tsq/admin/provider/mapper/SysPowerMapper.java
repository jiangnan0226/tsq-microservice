package com.tsq.admin.provider.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tsq.admin.api.model.SysPower;
import org.apache.ibatis.annotations.Param;

import java.util.Set;

/**
 * <p>
 * 权限表 Mapper 接口
 * </p>
 *
 * @author dcy
 * @since 2019-09-06
 */
public interface SysPowerMapper extends BaseMapper<SysPower> {

    /**
     * 根据角色ids 查询菜单类型的权限id
     * @param roleIds
     * @return
     */
    Set<String> getMenuPowerIdByRoleIds(@Param("roleIds") Set<String> roleIds);

    /**
     * 根据角色ids 查询模块类型的权限id
     * @param roleIds
     * @return
     */
    Set<String> getModulePowerIdByRoleIds(@Param("roleIds") Set<String> roleIds);
}
