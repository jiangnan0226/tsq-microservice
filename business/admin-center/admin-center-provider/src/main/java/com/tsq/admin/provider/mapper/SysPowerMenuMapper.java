package com.tsq.admin.provider.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tsq.admin.api.model.SysPowerMenu;

/**
 * <p>
 * 权限菜单关联表 Mapper 接口
 * </p>
 *
 * @author dcy
 * @since 2019-09-06
 */
public interface SysPowerMenuMapper extends BaseMapper<SysPowerMenu> {

}
