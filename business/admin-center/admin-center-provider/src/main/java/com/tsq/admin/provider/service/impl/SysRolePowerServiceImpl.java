package com.tsq.admin.provider.service.impl;

import com.tsq.admin.api.model.SysRolePower;
import com.tsq.admin.provider.mapper.SysRolePowerMapper;
import com.tsq.admin.provider.service.ISysRolePowerService;
import com.tsq.web.base.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 角色与权限关联表 服务实现类
 * </p>
 *
 * @author dcy
 * @since 2019-09-06
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysRolePowerServiceImpl extends BaseServiceImpl<SysRolePowerMapper, SysRolePower> implements ISysRolePowerService {

    @Autowired
    private SysRolePowerMapper sysRolePowerMapper;

}
