package com.tsq.admin.provider.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.tsq.admin.api.dto.SysPowerMenuDTO;
import com.tsq.admin.api.dto.SysPowerModuleDTO;
import com.tsq.admin.api.model.SysPower;
import com.tsq.admin.api.model.SysPowerMenu;
import com.tsq.admin.api.model.SysPowerModule;
import com.tsq.admin.provider.mapper.*;
import com.tsq.admin.provider.service.ISysPowerService;
import com.tsq.common.constant.CommonConstant;
import com.tsq.common.context.BaseContextHandler;
import com.tsq.web.base.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * <p>
 * 权限表 服务实现类
 * </p>
 *
 * @author dcy
 * @since 2019-09-06
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysPowerServiceImpl extends BaseServiceImpl<SysPowerMapper, SysPower> implements ISysPowerService {

    @Autowired
    private SysPowerMapper sysPowerMapper;
    @Autowired
    private SysPowerModuleMapper sysPowerModuleMapper;
    @Autowired
    private SysPowerMenuMapper sysPowerMenuMapper;
    @Autowired
    private SysModuleResourcesMapper sysModuleResourcesMapper;
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;
    @Autowired
    private SysUserInfoMapper sysUserInfoMapper;

    /**
     * 清空数据 方法调用后清空所有缓存
     *
     * @param sysPowerModuleDTO
     * @return
     */
    @Override
    public Boolean saveAuthModule(SysPowerModuleDTO sysPowerModuleDTO) {
        boolean success = false;
        if (StrUtil.isNotBlank(sysPowerModuleDTO.getPowerId()) && sysPowerModuleDTO.getModuleIds() != null) {
            // 删除关联表
            sysPowerModuleMapper.delete(new LambdaQueryWrapper<SysPowerModule>().eq(SysPowerModule::getPowId, sysPowerModuleDTO.getPowerId()));
            // 添加关联表
            sysPowerModuleDTO.getModuleIds().forEach(moduleId -> sysPowerModuleMapper.insert(new SysPowerModule().setPowId(sysPowerModuleDTO.getPowerId()).setModuleId(moduleId)));
            success = true;
        }
        if (success) {
            String userId = BaseContextHandler.getUserID();
            // 删除缓存
            redisTemplate.delete(CommonConstant.REDIS_USER_MODULE_LIST_KEY + userId);
            // 在查询权限
            Set<String> authRoleIds = sysUserInfoMapper.getAuthRoleIdsListByUserId(userId);
            List<Map<String, Object>> moduleResourcesList = sysModuleResourcesMapper.getModuleByRoleIds(authRoleIds);
            redisTemplate.opsForValue().set(CommonConstant.REDIS_USER_MODULE_LIST_KEY + userId, moduleResourcesList);
        }
        return success;
    }

    @Override
    public Boolean saveAuthMenu(SysPowerMenuDTO sysPowerMenuDTO) {
        boolean success = false;
        if (StrUtil.isNotBlank(sysPowerMenuDTO.getPowerId()) && sysPowerMenuDTO.getMenuIds() != null) {
            // 删除关联表
            sysPowerMenuMapper.delete(new LambdaQueryWrapper<SysPowerMenu>().eq(SysPowerMenu::getPowId, sysPowerMenuDTO.getPowerId()));
            // 添加关联表
            sysPowerMenuDTO.getMenuIds().forEach(menuId -> sysPowerMenuMapper.insert(new SysPowerMenu().setPowId(sysPowerMenuDTO.getPowerId()).setMenuId(menuId)));
            success = true;
        }
        return success;
    }

    @Override
    public Set<String> getMenuPowerIdByRoleIds(Set<String> roleIds) {
        return sysPowerMapper.getMenuPowerIdByRoleIds(roleIds);
    }

    @Override
    public Set<String> getModulePowerIdByRoleIds(Set<String> roleIds) {
        return sysPowerMapper.getModulePowerIdByRoleIds(roleIds);
    }
}
