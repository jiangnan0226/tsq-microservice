package com.tsq.admin.provider.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tsq.admin.api.model.SysPowerModule;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author dcy
 * @since 2019-09-16
 */
public interface SysPowerModuleMapper extends BaseMapper<SysPowerModule> {

}
