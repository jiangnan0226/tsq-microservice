package com.tsq.admin.provider.service;

import com.tsq.admin.api.model.SysUserRole;
import com.tsq.web.base.service.BaseService;

/**
 * <p>
 * 用户角色关联表 服务类
 * </p>
 *
 * @author dcy
 * @since 2019-09-06
 */
public interface ISysUserRoleService extends BaseService<SysUserRole> {

}
