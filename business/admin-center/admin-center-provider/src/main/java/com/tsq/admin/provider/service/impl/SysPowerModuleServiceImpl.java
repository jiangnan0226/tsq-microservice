package com.tsq.admin.provider.service.impl;

import com.tsq.admin.api.model.SysPowerModule;
import com.tsq.admin.provider.mapper.SysPowerModuleMapper;
import com.tsq.admin.provider.service.ISysPowerModuleService;
import com.tsq.web.base.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author dcy
 * @since 2019-09-16
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysPowerModuleServiceImpl extends BaseServiceImpl<SysPowerModuleMapper, SysPowerModule> implements ISysPowerModuleService {

    @Autowired
    private SysPowerModuleMapper sysPowerModuleMapper;

}
