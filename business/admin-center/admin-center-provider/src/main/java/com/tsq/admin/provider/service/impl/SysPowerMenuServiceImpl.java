package com.tsq.admin.provider.service.impl;

import com.tsq.admin.api.model.SysPowerMenu;
import com.tsq.admin.provider.mapper.SysPowerMenuMapper;
import com.tsq.admin.provider.service.ISysPowerMenuService;
import com.tsq.web.base.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 权限菜单关联表 服务实现类
 * </p>
 *
 * @author dcy
 * @since 2019-09-06
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysPowerMenuServiceImpl extends BaseServiceImpl<SysPowerMenuMapper, SysPowerMenu> implements ISysPowerMenuService {

    @Autowired
    private SysPowerMenuMapper sysPowerMenuMapper;

}
