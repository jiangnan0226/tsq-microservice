package com.tsq.admin.provider.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.tsq.admin.api.dto.SysUserInfoRoleDTO;
import com.tsq.admin.api.model.SysRole;
import com.tsq.admin.api.model.SysUserInfo;
import com.tsq.admin.api.model.SysUserRole;
import com.tsq.admin.provider.mapper.SysModuleResourcesMapper;
import com.tsq.admin.provider.mapper.SysUserInfoMapper;
import com.tsq.admin.provider.mapper.SysUserRoleMapper;
import com.tsq.admin.provider.service.ISysUserInfoService;
import com.tsq.common.constant.CommonConstant;
import com.tsq.web.base.service.impl.BaseServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author dcy
 * @since 2019-09-06
 */
@Service
@Transactional(rollbackFor = Exception.class)
@Slf4j
public class SysUserInfoServiceImpl extends BaseServiceImpl<SysUserInfoMapper, SysUserInfo> implements ISysUserInfoService {

    @Autowired
    private SysUserInfoMapper sysUserInfoMapper;
    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;
    @Autowired
    private SysModuleResourcesMapper sysModuleResourcesMapper;
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;


    @Override
    public List<SysRole> getAuthRoleListByUserId(String userId) {
        return sysUserInfoMapper.getAuthRoleListByUserId(userId);
    }

    @Override
    public Set<String> getAuthRoleIdsListByUserId(String userId) {
        return sysUserInfoMapper.getAuthRoleIdsListByUserId(userId);
    }

    /**
     * 删除缓存
     *
     * @param sysUserInfoRoleDTO
     * @return
     */
    @Override
    public boolean saveAuthRole(SysUserInfoRoleDTO sysUserInfoRoleDTO) {
        boolean success = false;
        if (StrUtil.isNotBlank(sysUserInfoRoleDTO.getUserId()) && sysUserInfoRoleDTO.getRoleIds() != null) {
            // 删除关联表
            sysUserRoleMapper.delete(new LambdaQueryWrapper<SysUserRole>().eq(SysUserRole::getUserId, sysUserInfoRoleDTO.getUserId()));
            // 添加关联表
            sysUserInfoRoleDTO.getRoleIds().forEach(roleId -> sysUserRoleMapper.insert(new SysUserRole().setUserId(sysUserInfoRoleDTO.getUserId()).setRoleId(roleId)));
            success = true;
        }
        if (success) {
            // 删除缓存
            redisTemplate.delete(CommonConstant.REDIS_USER_MODULE_LIST_KEY + sysUserInfoRoleDTO.getUserId());
            // 在查询权限
            Set<String> authRoleIds = sysUserInfoMapper.getAuthRoleIdsListByUserId(sysUserInfoRoleDTO.getUserId());
            List<Map<String, Object>> moduleResourcesList = sysModuleResourcesMapper.getModuleByRoleIds(authRoleIds);
            redisTemplate.opsForValue().set(CommonConstant.REDIS_USER_MODULE_LIST_KEY + sysUserInfoRoleDTO.getUserId(), moduleResourcesList);
        }
        return success;
    }

    @Override
    public Set<String> getAuthRoleSetByUserId(String userId) {
        return sysUserInfoMapper.getAuthRoleSetByUserId(userId);
    }

    @Override
    public Set<String> getPermissionListByMenuPowerIds(Set<String> menuPowerIds) {
        return sysUserInfoMapper.getPermissionListByMenuPowerIds(menuPowerIds);
    }

    @Override
    public Set<String> getModuleListByModulePowerIds(Set<String> modulePowerIds) {
        return sysUserInfoMapper.getModuleListByModulePowerIds(modulePowerIds);
    }
}
