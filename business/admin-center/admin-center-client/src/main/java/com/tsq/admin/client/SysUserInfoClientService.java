package com.tsq.admin.client;

import com.tsq.admin.api.InterfaceService;
import com.tsq.admin.api.model.SysUserInfo;
import com.tsq.admin.api.service.SysUserInfoRemoteService;
import com.tsq.common.model.ResponseData;
import feign.hystrix.FallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * @Author：dcy
 * @Description: 用户表暴露远程接口+熔断
 * @Date: 2020/3/2 13:26
 */
@FeignClient(name = InterfaceService.SERVICE_NAME, fallbackFactory = SysUserInfoClientService.SysUserInfoServiceFallbackFactory.class)
public interface SysUserInfoClientService extends SysUserInfoRemoteService {


    @Component
    class SysUserInfoServiceFallbackFactory implements FallbackFactory<SysUserInfoClientService> {

        @Override
        public SysUserInfoClientService create(Throwable throwable) {
            return new SysUserInfoClientService() {
                @Override
                public ResponseData<SysUserInfo> getUserInfoByUsername(String username) {
                    return ResponseData.error();
                }

                @Override
                public ResponseData<Set<String>> getAllPermissionByUserId(String userId) {
                    return ResponseData.error();
                }

                @Override
                public ResponseData<Boolean> save(SysUserInfo sysUserInfo) {
                    return ResponseData.error();
                }
            };
        }
    }
}
