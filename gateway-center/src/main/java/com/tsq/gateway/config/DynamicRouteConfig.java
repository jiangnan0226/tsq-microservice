package com.tsq.gateway.config;

import com.alibaba.cloud.nacos.NacosConfigProperties;
import com.tsq.gateway.route.NacosRouteDefinitionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author：dcy
 * @Description: 动态路由配置
 * @Date: 2020-02-22 15:04
 */
@Configuration
@ConditionalOnProperty(prefix = "tsq.gateway.dynamic-route", name = "enabled", havingValue = "true")
public class DynamicRouteConfig {
    @Autowired
    private ApplicationEventPublisher publisher;

    /**
     * Nacos实现方式
     */
    @Configuration
    @ConditionalOnProperty(prefix = "tsq.gateway.dynamic-route", name = "data-type", havingValue = "nacos", matchIfMissing = true)
    public class NacosDynRoute {

        @Bean
        public NacosRouteDefinitionRepository nacosRouteDefinitionRepository(NacosConfigProperties nacosConfigProperties) {
            return new NacosRouteDefinitionRepository(publisher, nacosConfigProperties);
        }
    }
}
